<%@page import="hoainiem.objects.*,hoainiem.gui.user.*, hoainiem.library.*" %>
<%@page import="hoainiem.*" %>

<%
String referer = request.getHeader("Referer");

String username = Utilities.getValue(request.getParameter("txtUserName"));
String userpass = Utilities.getValue(request.getParameter("txtUserPass"));
userpass = Utilities.md5(userpass);

if(username != null && userpass != null){
  if(!username.equalsIgnoreCase("") && !userpass.equalsIgnoreCase("")){
    ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
    UserControl uc = new UserControl(cp);
    if(cp==null){
      getServletContext().setAttribute("CPool",uc.getCP());
    }
    UserObject user = (UserObject) uc.getUserObject(username , userpass);

    if(user!=null){
      request.getSession(true).setAttribute("userSigned", user );
      response.sendRedirect("../../community/");
    }else{

      response.sendRedirect(referer);
    }
  }else{
    response.sendRedirect(referer);
  }
}else{
  response.sendRedirect(referer);
}
%>
