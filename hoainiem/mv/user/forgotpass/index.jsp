<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE HTML>
<html>
  <jsp:include flush="true" page="../../master/head.jsp"></jsp:include>
  <body>
    <jsp:include flush="true" page="../../master/header.jsp"></jsp:include>
    <div class="container main-max row">
        <jsp:include flush="true" page="forgotpass.jsp"></jsp:include>
    </div>
    <jsp:include flush="true" page="../../master/footer.jsp"></jsp:include>
    <jsp:include flush="true" page="../../master/messagebox.jsp"></jsp:include>

    <jsp:include flush="true" page="../../master/scripts.jsp"></jsp:include>
    <script src="../../js/userforgotpass.js" language="javascript"></script>
  </body>
</html>

