<%@page import="hoainiem.*, hoainiem.objects.*, hoainiem.library.*"%>
<%@page import="hoainiem.gui.user.*, hoainiem.mail.*, hoainiem.mail.*"%>

<%@page import="hoainiem.values.gui.*"%>
<%
  String webModule = AllGUI.WebModule;
  session.removeAttribute("sendCode");
  byte lang_value = Utilities.getLangValue(session);

  UserObject userSignup = (UserObject) session.getAttribute("userSignup");
  if(userSignup!=null){
    String to = userSignup.getUser_mail();
    if(to!=null && !to.equalsIgnoreCase("")){
      //L\u1EA5y th\uFFFDng tin \u0111\u1ED1i t\u01B0\u1EE3ng ch\u1EE9c n\u0103ng g\u1EEDi tin m\u1EB7c \u0111\u1ECBnh l\uFFFD index 0
      ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
      UserControl uc = new UserControl(cp);

      String sendCode  = EmailSender.randomCode((byte)4);

      String subject = EmailStruct.create_Mail_Confirm_Code_Title(lang_value);
      String message = EmailStruct.create_Mail_Confirm_Code_Content(sendCode, lang_value);
      boolean result = EmailSender.sendMail(to, subject, message);
      if(result) {
        sendCode = Utilities.md5(sendCode);
        request.getSession().setAttribute("sendCode",sendCode);
        response.sendRedirect("/"+webModule+"/user/confirm/");
      }else{
        response.sendRedirect("/"+webModule+"/user/signup/?err=notok");
      }
    }else{
      response.sendRedirect("/"+webModule+"/user/signup/?err=value");
    }
  }else{
      response.sendRedirect("/"+webModule+"/user/signup/?err=param");
  }
%>
