<%@page import="hoainiem.*, hoainiem.objects.*, hoainiem.library.*"%>
<%@page import="hoainiem.gui.user.*"%>
<%@page import="hoainiem.values.gui.*"%>
<%
  String webModule = AllGUI.WebModule;

  String confirmCode = request.getParameter("txtConfirmCode");
  String sendCode = (String) session.getAttribute("sendCode");
  UserObject userSignup = (UserObject) session.getAttribute("userSignup");

  if(userSignup!=null && sendCode!=null  && confirmCode!=null){
    if(!confirmCode.equalsIgnoreCase("") && !sendCode.equalsIgnoreCase("")){
      confirmCode = Utilities.md5(confirmCode);
      boolean result = confirmCode.equalsIgnoreCase(sendCode);
      if(result){
        session.removeAttribute("userSignup");
        request.getSession().setAttribute("userConfirmed",userSignup);
        response.sendRedirect("/"+webModule+"/user/createpass/");
      }else{
        response.sendRedirect("/"+webModule+"/user/confirm/?err=notok");
      }
    }else{
      response.sendRedirect("/"+webModule+"/user/confirm/?err=value");
    }
  }else{
      response.sendRedirect("/"+webModule+"/user/confirm/?err=param");
  }
%>
