<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
  <jsp:include flush="true" page="../../master/head.jsp"></jsp:include>
  <body>
    <jsp:include flush="true" page="../../master/scripts.jsp"></jsp:include>
    <script src="../../js/userverify.js" language="javascript"></script>

    <jsp:include flush="true" page="../../master/header.jsp"></jsp:include>
    <div class="container main-max row">
        <jsp:include flush="true" page="verify.jsp"></jsp:include>
    </div>
    <jsp:include flush="true" page="../../master/footer.jsp"></jsp:include>
    <jsp:include flush="true" page="../../master/messagebox.jsp"></jsp:include>
  </body>
</html>
