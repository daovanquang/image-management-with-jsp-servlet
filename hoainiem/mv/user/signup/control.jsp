<%@page import="hoainiem.*, hoainiem.objects.*, hoainiem.library.*"%>
<%@page import="hoainiem.gui.user.*"%>
<%@page import="hoainiem.values.gui.*"%>
<%
String webModule = AllGUI.WebModule;

//check user signined will return
UserObject user = (UserObject) session.getAttribute("userSigned");
if(user==null){
  String username = request.getParameter("txtUserName");
  String usermail = request.getParameter("txtUserMail");

  if (username != null && usermail != null) {

    username = username.trim();
    usermail = usermail.trim();
    if (!username.equalsIgnoreCase("") && !usermail.equalsIgnoreCase("")) {
      //Tham chieu ngu canh ung dung
      ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
      //Tao doi tuong thuc thi chuc nang
      UserControl uc = new UserControl(cp);

      //set ConnectionPool if it is null
      if (cp == null) {
        getServletContext().setAttribute("CPool", uc.getCP());
      }

      //kiem tra da ton tai user muon them da ton tai chua
      UserObject validUser = uc.getUserObject(username);

      if(validUser==null){
        UserObject nUser = new UserObject();
        nUser.setUser_name(username);
        nUser.setUser_mail(usermail);
        request.getSession(true).setAttribute("userSignup", nUser);
        response.sendRedirect("/"+webModule+"/user/confirm/activecode.jsp");
      }else {
        response.sendRedirect("/"+webModule+"/user/signup/?err=notok");
      }
    }
    else {
      response.sendRedirect("/"+webModule+"/user/signup/?err=value");
    }

  }
  else {
    response.sendRedirect("/"+webModule+"/user/signup/?err=param");
  }
}else {
  response.sendRedirect("/"+webModule+"/user/signin/?err=haveanaccount");
}
%>
