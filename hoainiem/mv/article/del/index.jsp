<%@page import="hoainiem.gui.article.*, hoainiem.gui.image.*, hoainiem.gui.comment.*, hoainiem.gui.video.*"%>
<%@page import="hoainiem.*, hoainiem.objects.*, hoainiem.library.*" %>
<%
int aid = Utilities.getIntParam(request,"aid");
String date = UtilitiesCalendar.getCurrentTime("yyyy-MM-dd HH:mm");
if(aid>0){
  ArticleObject article = new ArticleObject();
  article.setArticle_deleted_date(date);
  article.setArticle_id(aid);
  ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
  ArticleControl ac = new ArticleControl(cp);
  ac.delArticle(article);

  ImageObject image = new ImageObject();
  image.setImage_deleted_date(date);
  image.setImage_article_id(aid);
  ImageControl ic = new ImageControl(cp);
  //del in dir

  //del in db
  ic.delImage(image);

  CommentObject comment = new CommentObject();
  comment.setComment_deleted_date(date);
  comment.setComment_article_id(aid);
  CommentControl cc = new CommentControl(cp);
  cc.delComment(comment);

  VideoObject video = new VideoObject();
  video.setVideo_article_id(aid);
  video.setVideo_deleted_date(date);
  VideoControl vc = new VideoControl(cp);
  vc.delVideo(video);

  vc.releaseConnection();

  String referer = request.getHeader("Referer");
  response.sendRedirect(referer);
}
%>
