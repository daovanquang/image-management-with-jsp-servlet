<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="hoainiem.values.gui.*, hoainiem.lang.gui.*, hoainiem.library.Utilities"%>
<%
String webModule = AllGUI.WebModule;
byte lang_value = Utilities.getLangValue(session);
%>
<html>
<head>
<title>404 Not Found</title>
<link href="/<%=webModule%>/imgs/favicon.png" rel="icon" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="/<%=webModule%>/css/error.css">
</head>
<body>
<div class="face">
  <div class="band">
    <div class="red"></div>
    <div class="white"></div>
    <div class="blue"></div>
</div>
<div class="eyes"></div>
<div class="dimples"></div>
<div class="mouth"></div>
</div>

<h1 class="text-center"><%=HomeLang.PAGE_NOT_FOUND[lang_value]%></h1>
<a href="/<%=webModule%>/" class="text-center">
  <div class="btn"><%=HomeLang.RETURN_HOME[lang_value]%></div>
</a>
</body>
</html>
