<%@page import="hoainiem.*, hoainiem.objects.*, hoainiem.library.*"%>
<%@page import="hoainiem.gui.keyword.*, hoainiem.sql.*"%>
<%@page import="hoainiem.machinelearning.objects.*, hoainiem.machinelearning.decisiontree.*, hoainiem.machinelearning.trainingdata.*"%>
<%@page import="hoainiem.lang.gui.*"%>
<%@page import="java.util.*"%>
<%
String keyword = request.getParameter("q");
session.setAttribute("keyword",keyword);

if(keyword!=null){
  keyword = Utilities.getValue(keyword).trim();

  byte lang_value = Utilities.getLangValue(session);

  /*====- Find the connection manager -===*/
  ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
  KeywordControl kc = new KeywordControl(cp);

  KeywordObject similar = new KeywordObject();
  similar.setKeyword_value(keyword);
  String result = kc.getKeywords(similar, 0, (byte)10, false, lang_value);

  boolean isExistsKey = Utilities.containsString(result, HomeLang.NOT_FOUND_RESULTS[lang_value]);
  if(!isExistsKey){
    /*====- update count -====*/
    kc.editKeyword(similar,KeywordSQL.COMMAND_UPDATE_SEARCH_COUNT);
  } else {
    /*====- Analysis of Data -====*/
    TrainingDataControl tdc = new TrainingDataControl(cp);
    ArrayList<TrainingDataObject> items = tdc.getTrainingDataObjects(null,0,(byte)10);

    if(items!=null){

      int rows = items.size();

      if(rows>0){
        /*declare input data*/
        ArrayList<String> dependent_titles = new ArrayList<String>();
        dependent_titles.add("avg_search_month");
        dependent_titles.add("word_num");
        dependent_titles.add("compete_level");

        /*3 is col number of data to training*/
        String [][]dependent_values = new  String[rows][3];

        String independent_title = "INSERT";
        ArrayList<Boolean>  independent_values = new ArrayList<Boolean>();
        int i = 0;
        for(TrainingDataObject item:items){
          dependent_values[i][0] = item.getTrainingdata_avg_search_month();
          dependent_values[i][1] = item.getTrainingdata_word_num();
          dependent_values[i][2] = item.getTrainingdata_compete_level();

          independent_values.add(item.isTrainingdata_insert());

          i++;
        }

        DecisionTreeObject nDecisionTreeObject = new DecisionTreeObject(dependent_titles, dependent_values, independent_title, independent_values);
        DecisionTreeControl dtc = new DecisionTreeControl();

        /*over bround*/
        Tree tree = null;
        if(nDecisionTreeObject!=null){
          tree = dtc.generateTree(nDecisionTreeObject);
        }

        if(tree!=null){
          /*decision insert key*/

          /*Test case 1*/
          ArrayList<String> names = new ArrayList<String>();
          names.add("avg");
          names.add("word_num");
          names.add("compete_level");

          ArrayList<String> values = new ArrayList<String>();
          values.add("big");
          values.add("extra-large");
          values.add("hight");

          String isInsert = tree.getPropertyByValues(names, values, null);
          /*End test case 1*/

          boolean valid = isInsert.equalsIgnoreCase("true") ? true : false;

          if(valid){
            KeywordObject nKeyword = new KeywordObject();
            nKeyword.setKeyword_title("");
            nKeyword.setKeyword_summary("");
            nKeyword.setKeyword_created_date(UtilitiesCalendar.getCurrentTime("yyyy/MM/dd HH:mm"));
            nKeyword.setKeyword_value(keyword);

            kc.addKeyword(nKeyword);
          }
        }
      }
    }
  }
  session.setAttribute("result", result);
  response.sendRedirect("../result/");
}
%>
