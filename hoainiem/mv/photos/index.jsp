<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
  <jsp:include flush="true" page="/master/head.jsp"></jsp:include>
  <jsp:include flush="true" page="/master/header.jsp"></jsp:include>
  <body>
    <script src="../js/photos.js" language="javascript"></script>
    <div class="container main-1200 row">
       <jsp:include flush="true" page="photos.jsp"></jsp:include>
    </div>

    <jsp:include flush="true" page="/master/footer.jsp"></jsp:include>
    <jsp:include flush="true" page="/master/scripts.jsp"></jsp:include>
</body>
</html>

