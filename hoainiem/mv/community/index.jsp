<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
  <jsp:include flush="true" page="/master/head.jsp"></jsp:include>
  <body>
      <jsp:include flush="true" page="/master/header.jsp"></jsp:include>
      <div class="body main-1200 mb-20 row">
        <!--section left-->
        <jsp:include flush="true" page="navleft.jsp"></jsp:include>
        <!--end section left-->
        <div class="section-center mt-10">
          <jsp:include flush="true" page="post.jsp"></jsp:include>
          <jsp:include flush="true" page="article.jsp"></jsp:include>
        </div>
        <!--section right-->
        <div class="section-right">
          <jsp:include flush="true" page="recentphotos.jsp"></jsp:include>
          <jsp:include flush="true" page="proposedgroup.jsp"></jsp:include>
        </div>
        <!--end section right-->
    </div>
    <jsp:include flush="true" page="/master/footer.jsp"></jsp:include>
    <jsp:include flush="true" page="/master/scripts.jsp"></jsp:include>
    <script src="../js/community.js" language="javascript"></script>
  </body>
</html>
