<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@page import="hoainiem.lang.gui.*, hoainiem.library.*" %>
<%@page import="hoainiem.*,hoainiem.objects.*" %>
<%@page import="hoainiem.gui.user.*" %>
<%
//get User Signed
UserObject user = (UserObject) session.getAttribute("userSigned");

byte lang_value = Utilities.getLangValue(session);
//Tim bo quan ly ket noi
ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
String title = HomeLang.LOGIN[lang_value];
String path = "../user/signin";
String icon = "black-lock";

if(user!=null){
   title = HomeLang.LOGOUT[lang_value];
   path = "../user/signout";
   icon = "logout";
}
%>

<section class="section-navleft">
<ul class="section-navleft__info list-unstyle">
  <li class="sub-account-icon">
  <a href="<%=path%>">
    <i class="ic ic-md ic_utl-md ic_<%=icon%>-md pull-left mr-5"></i>
    <span><%=title%></span>
  </a>
  </li>
  <li class="active">
    <a href="">
      <i class="ic ic-md ic_gui-md ic_news-md pull-left mr-5"></i>
      <span><%=HomeLang.NEWS[lang_value] %></span>
    </a>
  </li>
</ul>
<% if(user!=null){%>
<div  class="toggle btn btn-default btn-sm" onclick="swapHide('section-navleft__community','section-navleft__management--hide')">
  <span class="text-uppercase"><%=HomeLang.MANAGEMENT[lang_value]%></span>
  <i class="pull-right ic ic-sm ic_utl-sm ic_arwdown-sm"></i>
</div>
<ul id="section-navleft__community" class="section-navleft__management section-navleft__management--hide list-unstyle">
  <li  class="heading">
    <%=HomeLang.GROUP[lang_value]%>
  </li>

  <li class="">
    <a href="">
      <i class="ic ic-md ic_gui-md ic_members-md pull-left mr-5"></i>
      <span><%=HomeLang.MEMBERS[lang_value] %></span>
    </a>
  </li>
  <li class="">
    <a href="">
      <i class="ic ic-md ic_gui-md ic_photos-md pull-left mr-5"></i>
      <span><%=HomeLang.PHOTOS[lang_value] %></span>
    </a>
  </li>
  <li>
    <a href="">
      <i class="ic ic-md ic_gui-md ic_comments-md pull-left mr-5"></i>
      <span><%=HomeLang.COMMENTS[lang_value] %></span>
    </a>
  </li>
</ul>
<%}%>
<ul class="section-navleft__support">
  <li  class="heading">
  <span><%=HomeLang.HELP_CENTER[lang_value] %></span>
  </li>
  <li class="">
    <a href="../faqs/">
      <i class="ic ic-md ic_gui-md ic_faqs-md pull-left mr-5"></i>
      <span><%=HomeLang.FAQS[lang_value] %></span>
    </a>
  </li>
  <li class="">
    <a href="../contact-us/">
      <i class="ic ic-md ic_gui-md ic_contactus-md pull-left mr-5"></i>
      <span><%=HomeLang.CONTACT_US[lang_value] %></span>
    </a>
  </li>
</ul>
</section>
