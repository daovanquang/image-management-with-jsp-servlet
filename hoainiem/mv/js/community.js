/*Khai báo label error, list card , card, itemShow, icon close image cho UI*/
var labelErrorJSON ='{"Id":"post__label-error","hideClass":"hide", "content":""}',
labelErrorObj = JSON.parse(labelErrorJSON);

var listCardJSON ='{"Id":"listCard","hideClass":"hide"}',
listCardObj = JSON.parse(listCardJSON);

var listInputJSON ='{"Id":"listInput"}',
listInputObj = JSON.parse(listInputJSON);

var virtualCardJSON = '{"Id":"virtualCard", "fnClickInput":""}',
virtualCardObj = JSON.parse(virtualCardJSON);

var cardJSON = '{"elm":"div", "className":"image-card card__img-cover pull-left image-hover-blur relative"}',
cardObj = JSON.parse(cardJSON);

var itemShowJSON = '{"elm":"img","type":"","firstId":"item_","Id":"", "name":"", "className":"absolute img-fill", "src":"", "alt":""}',
itemShowObj = JSON.parse(itemShowJSON);

var iconCloseJSON = '{"elm":"i", "className":"ic ic_utl-sm ic-sm ic_close-sm image__icon-close absolute"}',
iconCloseObj = JSON.parse(iconCloseJSON);

//khai báo virtual input đầu vào mặc định duy nhất cho ảnh(lưu json)
var virtualCardJSON = '{"Id":"virtualCard"}';
virtualCardObj = JSON.parse(virtualCardJSON);

var imageInputJSON ='{"Id":"imagePost","accept":"", "multiple":true, "directory":false,"type":"img"}',
imageInputObj = JSON.parse(imageInputJSON);

var articleJSON ='{"author_fullname":"","author_image":"","author_groupid":0,"author_permission":0,"article_title":"","article_tags":"","dir_img":"dir_img_","imgs":[],"img_index_inc":0,"dir_file":"dir_file_","files":[],"file_index_inc":0,"maxSize":0, "usedMemory":0}',
articleObj = JSON.parse(articleJSON);

//khai báo input đầu vào lưu trữ các file khác(lưu bằng path)
var otherInputJSON ='{"Id":"","accept":"", "multiple":false, "directory":false, "type":"", "fnChange":""}',
otherInputObj = JSON.parse(otherInputJSON);

function activePostControl(numChoose){
	var tabs = document.frmPost.querySelectorAll(".tabs");
	activeTab(tabs,numChoose,"active");
}
function selectFilePost(numChoose){
	
	activePostControl(numChoose);
	//MegaByte convert
	articleObj.maxSize = 25*1024*1024;

	if(numChoose == 3){
		//chọn file khác
		otherInputObj.accept = "*";
	}
	else if(numChoose == 2){
		//chọn video
		otherInputObj.accept = "video/*,  video/x-m4v, video/webm, video/x-ms-wmv, video/x-msvideo, video/3gpp, video/flv, video/x-flv, video/mp4, video/quicktime, video/mpeg, video/ogv, .ts, .mkv";
		otherInputObj.multiple = false;
		otherInputObj.directory=false;

	}else if(numChoose == 1){
		//chọn thư mục ảnh3.1.1	JPEG/JFIF
		imageInputObj.accept = "image/jpg, image/jpeg, image/png, image/bmp, image/svg, image/gif, image/heic, image/heif";
		imageInputObj.multiple = false;
		imageInputObj.directory=true;

	}else {
		//chọn nhiều ảnh
		imageInputObj.accept = "image/jpg, image/jpeg, image/png, image/bmp, image/svg, image/gif, image/heic, image/heif";
		imageInputObj.multiple = true;
		imageInputObj.directory= false;
	}

	if(articleObj.usedMemory <= articleObj.maxSize){
		swapHideImagesPost();
	}else{
		swapHideImagesPost();
	}
	controlInputFile(numChoose);
}
function createInputVideoAndOtherFile(){
	//check valid create input for video or other file
	var existsInput = null;
	var valid = true;
	//create input for video or for other file
	if(articleObj.file_index_inc >= 0){
		var beforeChildId = articleObj.dir_file + articleObj.file_index_inc;
		var listInput = document.getElementById(listInputObj.Id);	
		existsInput = existsElmById(listInput,beforeChildId);
		if(existsInput != null && existsInput.files[0] == null){
			valid = false;
		}
	}

	if(valid){
		otherInputObj.Id = articleObj.dir_file + articleObj.file_index_inc; //tạo 1 input mới
		var otherInputFileNode = '<input type="file" name=\''+otherInputObj.Id+'\' id=\''+otherInputObj.Id+'\' hidden="true" accept=\''+otherInputObj.accept+'\'>'; 		
		//append node to list
		var listWrapper = document.getElementById(listInputObj.Id);
		listWrapper.innerHTML += otherInputFileNode;
		setAttInput(otherInputObj);

	}else{
		//reset input accept
		existsInput.setAttribute('accept',otherInputObj.accept);
	}
	//
	otherInputObj.fnChange = "setCardVideoOrOtherFilePost()";
	document.getElementById(otherInputObj.Id).setAttribute("onchange",otherInputObj.fnChange);
}
function controlInputFile(numChoose){
	//viết tường minh
	var virtualCard = document.getElementById(virtualCardObj.Id);
	otherInputObj.type = (numChoose==2)? "video" : (numChoose==3)? "other":"";
	itemShowObj.type = otherInputObj.type;

	if(numChoose == 2 || numChoose == 3){
		createInputVideoAndOtherFile();
		//tạo định danh
		virtualCardObj.fnClickInput = 'createInputVideoAndOtherFile();selectElmById(\''+otherInputObj.Id+'\');';
	}else{
		//viết tường minh
		imageInputObj.type = "img";
		itemShowObj.type = imageInputObj.type;
		//select input for image(this is imagePost)
		virtualCardObj.fnClickInput = 'selectElmById(\''+imageInputObj.Id+'\');';
		
		document.getElementById(imageInputObj.Id).setAttribute("onchange","setCardImgPost();");
		//
		setAttInput(imageInputObj);
	}
	virtualCard.setAttribute('onclick',virtualCardObj.fnClickInput);
}
//lối tắt
function selectVirtualCard(){
	selectElmById(virtualCardObj.Id);
}
function swapHideImagesPost(){
	var listWapper = document.getElementById("listOfPost");
	if(listWapper.classList.contains("hide")){
		listWapper.classList.remove("hide");
	}
}

function setAttInput(obj){
	var elm = document.getElementById(obj.Id);
	elm.setAttribute("accept",obj.accept);
	
	if(obj.multiple && !elm.multiple) elm.setAttribute("multiple","multiple");
	else if(!obj.multiple && elm.multiple) elm.removeAttribute("multiple");

	if(obj.directory && !elm.webkitdirectory) {
		elm.setAttribute("webkitdirectory","webkitdirectory");
		elm.setAttribute("mozdirectory","mozdirectory");
	}
	else if(!obj.directory && elm.webkitdirectory) {
		elm.removeAttribute("webkitdirectory","webkitdirectory");
		elm.removeAttribute("mozdirectory","mozdirectory");
	}
}
/*03-11-2018 3:08*/
function calcListSize(listObj, files){
	valid = true;
	for(let i=0; i<files.length; i++){
		listObj.usedMemory += files[i].size;
	}
}

/*đọc file từ input preview  on image*/
 function readURL(id, file) {
 	var itemShow = document.getElementById(id);
 	readAnImage(itemShow,file);
}

/*Thêm 1 image item vào item list*/
function addAnCardToList(listCardObj, itemCardObj, itemShowObj, iconObj, labelObj){

	var listCard=document.getElementById(listCardObj.Id);

	var cardItem = document.createElement(itemCardObj.elm);
	cardItem.setAttribute('class',itemCardObj.className); 
	//add elm
	listCard.appendChild(cardItem);
	
	var firstId = itemShowObj.firstId;
	fileId = (itemShowObj.type == "img") ? (articleObj.dir_img + articleObj.img_index_inc) : (articleObj.dir_file + articleObj.file_index_inc); //do khi thêm input articleObj.file_index_inc đã ++
	itemShowObj.Id = firstId + fileId;

	var itemShow = document.createElement(itemShowObj.elm);
	itemShow.setAttribute('id', itemShowObj.Id)
	itemShow.setAttribute('class', itemShowObj.className);
	itemShow.setAttribute('src', itemShowObj.src);
	itemShow.setAttribute('alt', itemShowObj.alt);
	cardItem.appendChild(itemShow);

	if(itemShowObj.type != "img"){
		var nameNode =  document.createElement("span");
		nameNode.setAttribute('class', 'absolute')
		nameNode.innerHTML = itemShowObj.name;
		cardItem.appendChild(nameNode);
	}
	

	//icon close card
	var iconClose = document.createElement(iconCloseObj.elm);
	iconClose.setAttribute('class', iconCloseObj.className);
	var fnRemove = 'removeElm(\''+listCardObj.Id+'\',\''+listCardObj.hideClass+'\',this.parentNode,\''+itemShowObj.type+'\',\''+fileId+'\',\''+fileId+'\',\''+labelErrorObj.Id+'\',\''+labelErrorObj.hideClass+'\');';
	iconClose.setAttribute('onclick',fnRemove);
	cardItem.appendChild(iconClose);	
}

//for image choose
function setCardImgPost(){

	var elm = document.getElementById("imagePost"),
	len = elm.files.length;
	var valid = true;

	if( len > 0){
		//Thêm item vào list
		for(let i=0; i < len; i++){
			let temp = articleObj.usedMemory + elm.files[i].size;
			//Thêm 1 item vào list
			if(temp <= articleObj.maxSize) {
				//declare
				var firstId = itemShowObj.firstId;
				var fileId = articleObj.dir_img + articleObj.img_index_inc;
				var file = elm.files[i];
				//get file name
				itemShowObj.alt = file.name;
				articleObj.usedMemory = temp;
				addAnCardToList(listCardObj, cardObj, itemShowObj, iconCloseObj, labelErrorObj);
				readURL(firstId + fileId, file);
				//add data object
				var imgs = articleObj.imgs;
				var dataObj = {
				    "Id" : fileId,
				    "type" : imageInputObj.type,
				    "name" : file.name,
				    "size" : file.size,
				    "value" : ""
				};
				//set value
				getBase64(dataObj, file);
				imgs.push(dataObj);
				articleObj.img_index_inc++;
			}else{
				valid = false;
			}
		}
	}
	showOverStack(valid);
}
function showOverStack(valid){
	//MegaByte
	var MAX = articleObj.maxSize / Math.pow(1024,2);

	if(!valid){
		labelErrorObj.content="Bạn chỉ được phép thêm tối đa "+MAX+" MB/lần";
		labelError = document.getElementById(labelErrorObj.Id);
		labelError.innerHTML = labelErrorObj.content;
		if(labelError.classList.contains(labelError.hideClass)) labelError.classList.remove(labelError.hideClass);
	}
}
//for video choose
function setCardVideoOrOtherFilePost(){

	//kiểm tra tồn tại file chưa
	var MAX = articleObj.maxSize / Math.pow(1024,2);
	var elm = document.getElementById(otherInputObj.Id);
	len = elm.files.length;
	var valid = true;

	if( len > 0){
		//Thêm item vào list
		let temp = articleObj.usedMemory + elm.files[0].size;
		
		//Thêm 1 item vào list
		if(temp <= articleObj.maxSize) {

			//get file name
			itemShowObj.alt = elm.files[0].name;
			itemShowObj.name = elm.files[0].name;
			itemShowObj.src = "";
			articleObj.usedMemory = temp;
			addAnCardToList(listCardObj, cardObj, itemShowObj, iconCloseObj, labelErrorObj);
			// dont readURL
			//add data object
			var files = articleObj.files;
			var id = articleObj.dir_file + articleObj.file_index_inc;

			var dataObj = {
			    "Id" : id,
			    "type" : itemShowObj.type,
			    "name" :  elm.files[0].name,
			    "size" : elm.files[0].size,
			    "value" : ""
			};
			
			files.push(dataObj);
			articleObj.file_index_inc ++;
		}else{
			valid = false;
		}
	}
	
	showOverStack(valid);
}

/*Xóa item khỏi list */
function removeElm(listCardId, listCard_hideClass, childNode, fileType ,fileId,inputFileId, labelId,label_hideClass){
	var listElm=document.getElementById(listCardId);
	var label = document.getElementById(labelId);
	var cardnum = (fileType=="img") ? articleObj.img_index_inc : articleObj.file_index_inc;
	if (cardnum>0) {
	    listElm.removeChild(childNode);
	    if(fileType == "img"){
	    	var imgs = articleObj.imgs;
	    	articleObj.usedMemory -= removeObjectById(imgs, fileId);
	   	}else{
	   		var files = articleObj.files;
	   		articleObj.usedMemory -= removeObjectById(files, fileId);
	   		var listInput = document.getElementById(listInputObj.Id);
	   		removeElmById(listInput,fileId);
	   	}
	}
	if(articleObj.usedMemory<=articleObj.maxSize){
		label.classList.add(label_hideClass);
	}
}
function checkValidArticleAE(fn){
	var title = fn.txtArticleTitle.value;
	//fileCount = articleObj.img_index_inc + articleObj.file_index_inc;
	var validTitle = true, validFileCount = true;
	message = "";

	if(title==""){
		validTitle = false;
	}
	
	/*
	if(fileCount<1){
		validFileCount = false;
	}
	if(!validFileCount){
		message = "Bạn phải thêm ít nhất 1 ảnh";	
	}
	*/

	if(!validTitle){
		message = "Bạn phải nhập tiêu đề";
	}
	

	if(message!="") alert(message);
	return validTitle && validFileCount;

}

function process(xmlHttp){
	if(XMLHttpRequest.readyState == 4 && XMLHttpRequest.status == 200){
		var fn = document.frmPost;
		cleanUp(fn);
		fn.txtArticleTitle.value = xmlHttp.responseText;
	}
}

function sendJsonArticle(fn, method, action){
	// construct an HTTP request
	var xhr = getXmlHttpObject();
	if (xhr == null) {
		alert("Trình duyệt của bạn không hỗ trợ HTTP Request");
		return;
	}

	var formData = new FormData();
	
	formData.append("txtArticleImageAttach",JSON.stringify(articleObj));

	var files = articleObj.files;
	for(var i=0;i<files.length;i++){
		var name = files[i].name;
		var id = files[i].Id;
		var input = document.getElementById(id);
		formData.append(name,input.files[0]);
	}

	xhr.open(method, action);
	// send the collected data as JSON
	XMLHttpRequest.onreadystatechange = process(xhr);
	xhr.send(formData);
	xhr.onloadend = function (){	
		alert("success");
		cleanUp(fn);
		location.reload();
	};
	xhr.onerror = function(){
		alert("Lỗi");
	};
}
function cleanUp(fn){
	if(fn!=null){
		//reset properties
		listCardObj.numCardPost=0;
		listCardObj.size = 0;
		var listCard = document.getElementById(listCardObj.Id);
		listCard.innerHTML = '';
		var listInput = document.getElementById(listInputObj.Id);
		listInput.innerHTML = '';
		fn.txtArticleTitle.value = '';
		fn.txtArticleTags.value = '';

		articleObj.imgs = [];
		articleObj.img_index_inc = 0;
		articleObj.files = [];
		articleObj.file_index_inc = 0;
	}
};
function submitArticleAE(fn){
	var action = "http://localhost:8080/mv/article/ae/",
	method = "POST";
	//submit
	if(checkValidArticleAE(fn)){
		//add infor
		articleObj.author_fullname = fn.txtArticleAuthorFullName.value;
		articleObj.author_image = fn.txtArticleAuthorImage.value;
		articleObj.author_groupid = parseInt(fn.txtArticleAuthorGroupId.value);
		articleObj.author_permission = parseInt(fn.txtArticleAuthorPermission.value);
		articleObj.article_title = fn.txtArticleTitle.value.toHtmlEntities();
		articleObj.article_tags = fn.txtArticleTags.value.toHtmlEntities();

		sendJsonArticle(fn,method,action);
	} 

}


var commentJSON = '{"article_id":0, "user_fullname":"", "user_image":"", "user_permission":0, "content":""}',
commentObj = JSON.parse(commentJSON);

function checkValidComment(fn){
	var content = fn.txtCommentContent.value;
	var validContent = true;
	var message = "";
	if(content==""){
		validContent = false;
		message = "Bạn phải nhập nội dung";
	}

	if(message!=""){
		alert(message);
	}

	return validContent;
}

function sendJsonComment(fn, method, action){
	// construct an HTTP request
	var xhr = getXmlHttpObject();
	if (xhr == null) {
		alert("Trình duyệt của bạn không hỗ trợ HTTP Request");
		return;
	}

	xhr.open(method, action);
	// send the collected data as JSON
	XMLHttpRequest.onreadystatechange = process(xhr);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(commentObj));
	xhr.onloadend = function (){	
		alert("success");
		cleanComment(fn);
		location.reload();
	};
	xhr.onerror = function(){
		alert("Lỗi");
	};
}
function cleanComment(fn){
	fn.txtCommentContent.value = "";
	commentObj.content = "";
}
function submitCommentAE(fn){
	var action = "http://localhost:8080/mv/comment/ae/",
	method = "POST";
	if(checkValidComment(fn)){
		//add infor
		commentObj.article_id = parseInt(fn.txtArticleId.value);
		commentObj.user_image = fn.txtCommentUserImage.value;
		commentObj.user_fullname = fn.txtCommentUserFullName.value;
		commentObj.user_permission = parseInt(fn.txtCommentUserPermission.value);
		commentObj.content = fn.txtCommentContent.value.toHtmlEntities();
		sendJsonComment(fn,method,action);
	} 
}

/*----------------------------------------end------------------------------------------*/

/*END FOR POST*/