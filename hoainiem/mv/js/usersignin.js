function isChecked(fn){
	if(fn.txtUserName.autocomplete =="off" && fn.txtUserPass.autocomplete == "off"){
		fn.txtUserName.autocomplete = "on";
		fn.txtUserPass.autocomplete = "new-password";
	}else{
		fn.txtUserName.autocomplete = "off";
		fn.txtUserPass.autocomplete = "off";
	}
}

/*Các kịch bản xử lý cho login.html*/
function checkVaLidLogin(fn, lang_value){
	//Lấy dữ liệu trên giao diện
	var username=fn.txtUserName.value;
	var userpass=fn.txtUserPass.value;
	
	//Các hiển thị xác nhận sự hợp lệ
	var validUserName=true;
	var validUserPass=true;
	
	//Hiển thị thông báo lỗi
	var message="";
	
	//kiểm tra mật khẩu	
	if(isSpace(userpass)){
		validUserPass=false;
		message = ERR.EMPTY_FIELD[lang_value];
	}else if(isLessLength(userpass,5) || !isLessLength(userpass,100)){
		validUserPass=false;
		message = ERR.LENGTH_FIELD[lang_value] + " " + 5 +"-"+100+" "+CHARACTERS[lang_value];				
	}
	
	//Kiểm tra tên đăng nhập
	username=username.trim();
	if(isSpace(username)){
		validUserName=false;
		message = ERR.EMPTY_FIELD[lang_value];
	}else{
		if(checkHaveSpace(username)){
			validUserName=false;
		    message = ERR.HAVE_SPACE_FIELD[lang_value];
		}else if(isLessLength(username,5) || !isLessLength(username,50)){
			validUserName=false;
		    message = ERR.LENGTH_FIELD[lang_value] + " " + 5 +"-"+50+" "+CHARACTERS[lang_value];
		}else if(username.indexOf("@")!=-1||username.indexOf(".")!=-1){
			//Cấu trúc hợp thư
			if(checkValidMail(username)) {
				 message = ERR.MAIL_STRUCT_FIELD[lang_value];
				}
            }	
        }	
	//Thông báo
	if(message!=""){
		if(!validUserName){
			fn.txtUserName.focus();
			fn.txtUserName.select();
			showLabel("label-username-error","hide", message);
		}
		else
		if(!validUserPass){
			fn.txtUserPass.focus();
			fn.txtUserPass.select();
			showLabel("label-userpass-error", "hide", message);
		}
	}
	/*The logical AND operator,return a if a is falsy, return b if a is truthy*/
	return validUserName && validUserPass;
}

function signin(fn,lang_value){
	if(checkVaLidLogin(fn, lang_value)){
		fn.method = "POST"; //goi den doPost()
		fn.action = "/"+webModule+"/user/signin/control.jsp";
		fn.submit();
	}
}