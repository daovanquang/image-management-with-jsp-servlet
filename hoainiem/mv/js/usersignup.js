function checkSignup(fn, lang_value){
	//Lấy dữ liệu trên giao diện
	var username=fn.txtUserName.value,
	mail = fn.txtUserMail.value;
	
	//Các hiển thị xác nhận sự hợp lệ
	var validUserName=true,
	validMail=true;
	
	//Hiển thị thông báo lỗi
	var message="";
	
	//Kiểm tra email
	if(!checkValidMail(mail)){
		validMail = false;
		message = ERR.MAIL_STRUCT_FIELD[lang_value];

	}

	//Kiểm tra tên đăng nhập
	username=username.trim();
	if(isSpace(username)){
		validUserName=false;
		message = ERR.EMPTY_FIELD[lang_value];
	}else{
		if(checkHaveSpace(username)){
			validUserName=false;
		    message = ERR.EMPTY_FIELD[lang_value];
		}else if(!isLessLength(username,50)){
			validUserName=false;
		    message = ERR.OVER_LENGTH_FIELD[lang_value];
		}else if(username.indexOf("@")!=-1||username.indexOf(".")!=-1){
			//Cấu trúc hợp thư
			if(checkValidMail(username)) {
			 	message = ERR.MAIL_STRUCT_FIELD[lang_value];
			}	
         }	
     }	



	//Thông báo
	if(message!=""){
		if(!validUserName){
			fn.txtUserName.focus();
			fn.txtUserName.select();
			showLabel("label-username-error","hide", message);
		}else if(!validMail){
			fn.txtUserMail.focus();
			fn.txtUserMail.select();
			showLabel("label-usermail-error", "hide", message);
		}
	}
	/*The logical AND operator,return a if a is falsy, return b if a is truthy*/
	return validUserName && validMail;
}

function signup(fn, lang_value){
	if(checkSignup(fn, lang_value)){
		submitForm(fn,"POST","/"+webModule+"/user/signup/control.jsp");
	}
}