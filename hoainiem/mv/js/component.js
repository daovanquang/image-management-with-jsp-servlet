function selectElmById(id){
	var elm = document.getElementById(id);
	if(elm != null) elm.click();
}

function swapHide(id, classHide){
	var elm = document.getElementById(id);
	if(elm.classList.contains(classHide)) elm.classList.remove(classHide);
	else elm.classList.add(classHide);
}
/*FOR TOGGLE*/
function swapMenu(id, classHide, elm){
	swapHide(id, classHide);
	if(elm.classList.contains("ic_menu-md")){
		elm.classList.remove("ic_menu-md");
		elm.classList.add("ic_close-md");
	}
	else{
		elm.classList.remove("ic_close-md");
		elm.classList.add("ic_menu-md");
		
	}
}
/*END FOR TOGGLE*/
function showOnlyTab(itemsClass, classHide, index){
	var items = document.querySelectorAll("."+itemsClass);
	var len = items.length; 
	for(let i=0;i<len;i++){
		if(!items[i].classList.contains(classHide)) items[i].classList.add(classHide);
	}
	items[index].classList.remove(classHide);
}
function showOnlyTabByObj(objTab){
	var tabContainer = document.getElementById(objTab.tabContainerId),
	items = tabContainer.querySelectorAll("."+objTab.tabItemClass),
	tabs = tabContainer.querySelectorAll("."+objTab.tabControlClass),
	len = items.length; 

	for(let i=0;i<len;i++){
		if(tabs[i].classList.contains(objTab.activeTabControlClass)) tabs[i].classList.remove(objTab.activeTabControlClass);
		if(!items[i].classList.contains(objTab.hideTabItemClass)) items[i].classList.add(objTab.hideTabItemClass);
	}

	tabs[objTab.index].classList.add(objTab.activeTabControlClass);
	items[objTab.index].classList.remove(objTab.hideTabItemClass);

}

function loadTab(objTab){
	var tabContainer = document.getElementById(objTab.tabContainerId),
	items = tabContainer.querySelectorAll("."+objTab.tabItemClass),
	tabs = tabContainer.querySelectorAll("."+objTab.tabControlClass),
	len= items.length;
	for(let i=0;i< len;i++){
		tabs[i].addEventListener("click",function(){
			objTab.index = i;
			showOnlyTabByObj(objTab);
			//reset if list hide then show
			response("extra-large","image-list--extra-large");
		},false);
		
	}
}
function getNum(value){
	return parseInt(value.replace(/[^\d.]/g, ''));
}
function isUndefinedNum(value){
	if(!value){
		value=0;
	}else{
		value = getNum(value);
	}
	return value;
}
function getOutOfElm(elm){
	var outOfElm=0;
	outOfElm += isUndefinedNum(elm.style.marginLeft);
	outOfElm += isUndefinedNum(elm.style.marginRight);
	outOfElm += isUndefinedNum(elm.style.paddingLeft);
	outOfElm += isUndefinedNum(elm.style.paddingRight);
	outOfElm += isUndefinedNum(elm.style.borderLeftWidth);
	outOfElm += isUndefinedNum(elm.style.borderRightWidth);
	return outOfElm;
}
function setChildsWidth(item, value){
	item.style.width = value - getOutOfElm(item) + 'px';
}
function setChildsHeight(item, value){
	item.style.height = value - getOutOfElm(item) + 'px';
}
function getOuterElmWidth(elm){
	return Math.ceil(elm.offsetWidth + getOutOfElm(elm));
}
function getInnerElmWidth(elm){
	return Math.floor(elm.offsetWidth + getOutOfElm(elm));
}
function getOuterParentWidth(items,indexStart, indexStop){
	var sum = 0;
	for(let i= indexStart;i<indexStop;i++){
		sum += getOuterElmWidth(items[i]);
	}
	return sum;
}
/*FOR SLIDE*/
function prevSlide(obj){
	slide= document.getElementById(obj.slideId),
	list= slide.querySelectorAll("."+obj.listClass)[0],
	btns = slide.querySelectorAll("."+obj.btnClass),
	items = slide.querySelectorAll("."+obj.itemClass),
	len = slide.querySelectorAll("."+obj.itemClass).length;
	if(obj.dotClass!=null) dots = slide.querySelectorAll("."+obj.dotClass);
	//change index to active and show new slide
	if(obj.index > 0) {
		if(!list.classList.contains('animated')) list.classList.add('animated');
		obj.index --;
	} else {
		obj.index = len - obj.numSlideShow;
		if(list.classList.contains('animated')) list.classList.remove('animated');
		var width=0;
		for(var i=0; i< obj.index; i++) width += items[i].offsetWidth;
		list.style.webkitTransform = 'translate3d('+(-width)+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+(-width)+ 'px, 0, 0)';
	}
	changeSlide(obj);
	//change dots
	if(obj.dotClass!=null) changeDot(obj);

}
function nextSlide(obj){
	var slide= document.getElementById(obj.slideId), 
	list= slide.querySelectorAll("."+obj.listClass)[0],
	btns = slide.querySelectorAll("."+obj.btnClass),
	items = slide.querySelectorAll("."+obj.itemClass),
	len = slide.querySelectorAll("."+obj.itemClass).length;
	if(obj.dotClass!=null) dots = slide.querySelectorAll("."+obj.dotClass);

	if(obj.index < len - obj.numSlideShow)  {
		if(!list.classList.contains('animated')) list.classList.add('animated');
		obj.index ++;
	} else {
		obj.index = 0;
		if(list.classList.contains('animated')) list.classList.remove('animated');
			
		list.style.webkitTransform = 'translate3d('+0+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+0+ 'px, 0, 0)';
	}
	changeSlide(obj);
	//change dots
	if(obj.dotClass!=null) changeDot(obj);
}
function activeTab(tabs,index,classActive){
	for(var i=0;i<tabs.length;i++) {
		if(tabs[i].classList.contains(classActive)){
			tabs[i].classList.remove(classActive);
		}
	}
	tabs[index].classList.add(classActive);
}
function changeDot(obj){
	var slide= document.getElementById(obj.slideId);
	dots = slide.querySelectorAll("."+obj.dotClass);
	activeTab(dots,obj.index,"active")
}
function changeSlide(obj){
	var slide= document.getElementById(obj.slideId), 
	list= slide.querySelectorAll("."+obj.listClass)[0],
	items = slide.querySelectorAll("."+obj.itemClass);
	
	var widthOfDiv = Math.floor(slide.offsetWidth/ parseInt(obj.numSlideShow)),
	sum = (obj.index)*widthOfDiv,
	maxTranslateX = (items.length - parseInt(obj.numSlideChange) + 1)*widthOfDiv;
	if(!list.classList.contains('animated')) list.classList.add('animated');
	
	if(sum >=0 && sum<maxTranslateX){
		sum=-sum;
		list.style.webkitTransform = 'translate3d('+sum+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+sum+ 'px, 0, 0)';
	}
}

function resizeSlide(obj){
	var slide= document.getElementById(obj.slideId),
	list=  slide.querySelectorAll("."+obj.listClass)[0],
	items = slide.querySelectorAll("."+obj.itemClass);

	if(!list.classList.contains('animated')) list.classList.add('animated');

	//reset width

	var widthOfDiv = Math.floor(slide.offsetWidth/ parseInt(obj.numSlideShow)),
	sum = (obj.index)*widthOfDiv,
	maxTranslateX = (items.length - parseInt(obj.numSlideShow) + 1)*widthOfDiv;

	//reset width
	setHeightWidth(obj);	

	if(sum >= 0 && sum<maxTranslateX){
		sum=-sum;
		list.style.webkitTransform = 'translate3d('+sum+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+sum+ 'px, 0, 0)';
	}	
}

function setHeightWidth(obj){
	var slide= document.getElementById(obj.slideId),
	list =  slide.querySelectorAll("."+obj.listClass)[0],
	items = slide.querySelectorAll("."+obj.itemClass),
	images = slide.querySelectorAll("."+obj.imageClass);
	widthOfDiv = Math.floor(slide.offsetWidth/parseInt(obj.numSlideShow));

	var height = 0;
	if(obj.numSlideShow==1){
		height = Math.floor(3*widthOfDiv/8);
	}else{
		height = Math.floor(2*widthOfDiv/3);
	}

	//reset width
	for(var i=0;i<items.length;i++){
		var item = items[i];
		var img = images[i];
		setChildsWidth(item,widthOfDiv);
		if(obj.isResponseHeight==true){
			if(img!=null) setChildsHeight(img,height);
			else setChildsHeight(item,height);
		}
	}
	list.style.width = items.length * widthOfDiv + 'px';
}
function loadSlide(obj){
	setHeightWidth(obj);
	if(obj.dotClass!=null) changeDot(obj);
}
/*END FOR SLIDE*/


/*Component form*/
function isSpace(text){
	if(text=="") return true;
	else return false;
}
function isLessLength(text,length){
	if(text.length<length) return true;
	else return false;
}

function checkValidMail(mail){
	var partern=/\w+@\w+[.]\w/;
	if(!mail.match(partern)) return false;
	else return true;
}
function checkHaveSpace(text){
	if(text.indexOf(" ")==-1) return false;
	else return true;
}

function submitForm(fn,method,action){
	fn.method = method; //goi den method
	fn.action = action; //Duong dan
	fn.submit();
}

//message log

function changeLabel(input,labelId){
	if(input.value!="") hideLabel(labelId,"hide");
}
function showLabel(labelId,classHide,text){
	var label = document.getElementById(labelId);
	label.innerHTML = text;
	if(label.classList.contains(classHide)){
		label.classList.remove(classHide);
	}
}
function hideLabel(labelId,classHide){
	var label = document.getElementById(labelId);
	if(!label.classList.contains(classHide)){
		label.classList.add(classHide);
	}
}

//FOR DEL 
function confirmDel(url){
	var message = "Xác nhận xóa";
	if(window.confirm(message)){
		window.location.href=url;
	}else{
		return false;
	}
}

/*Gererate SelectBox*/
function generateSelectBox(slcName,labels, values,defaultIndex,className,fnChange){
	var name = (slcName==null) ?  "" : " name=\'"+slcName+"\' " ;
	var cls = (className==null) ?  "" : " class=\'"+className+"\' " ;
	var onchange = (fnChange==null) ?  "" : " onchange=\'"+fnChange+"\' " ;
	var tmp="<select "+name+cls+onchange+" >";
	for(let i=0;i<labels.length;i++){
		let value = (values==null) ? i : values[i];
		if(i==defaultIndex) tmp += "<option value=\'"+value+"\' selected>"+labels[i]+"</option>"; 
		else  tmp += "<option value=\'"+value+"\'>"+labels[i]+"</option>"; 
	}
	tmp += "</select>";
	document.write(tmp);
}

function generateSelectBoxNation(name,strJSON,defaultIndex,className,fnChange){
	var objNation = JSON.parse(strJSON);
	var labels = objNation.values,
	values = objNation.codes;
	generateSelectBox(name, labels, values, defaultIndex, className, fnChange);

	
}

function generateSelectBoxProviceCity(name, strJSON, className){
	var objProviceCity = JSON.parse(strJSON); 
	generateEmptySelectBox(name, objProviceCity.title, className);

	var slcNation = document.getElementsByName('slcNation')[0];
	slcNation.addEventListener("change", function(){
		fnChangeProviceCity('slcNation', name, strJSON, 0);
	},false);
}
function fnChangeProviceCity(slcNationName , name, strJSON,defaultIndex){
	
	var slcNation = document.getElementsByName(slcNationName)[0];
	var slcProvinceCity = document.getElementsByName(name)[0];
	if(slcNation!=undefined){
		var objProviceCity = JSON.parse(strJSON);
		slcProvinceCity.innerHTML = "";
		var tmp = '';
		var valueNation = slcNation[slcNation.selectedIndex].value;
		var nations = objProviceCity.nations,
		nation_len = nations.length;
		for(i=0; i<nation_len; i++){
			if(nations[i].nation_code == valueNation){
				var ProviceCitys = nations[i].values;
				var pc_len = ProviceCitys.length;
				for(j=0; j<pc_len;j++){
					tmp += (j==defaultIndex) ? "<option value=\'"+j+"\' selected>"+ProviceCitys[j]+"</option>" : "<option value=\'"+i+"\'>"+ProviceCitys[j]+"</option>";
				}
				break;
			}
		}
		slcProvinceCity.innerHTML = tmp;
	}
}

/*Generate check box list from select box value*/
function generateCheckBoxs(name, labels){
	var tmp = "";
	for(let i=0;i<labels.length;i++){
		if(i%2==0) {
			tmp+="<div class=\"row\">";
		}
		
		tmp += "<div class=\"col-6\">";
		tmp += "<input type=\"checkbox\" name=\'"+name+"\' id=\"chk"+i+"\" disabled/><label class=\"form-label\" for=\"chk"+i+"\">";
		tmp += "Quản lý "+labels[i];
		tmp += "</div>";
		
		if(i%2==1 || i==labels.length-1){
			tmp+="</div>";
		}
	}
	document.write(tmp);
}

function setCheckbox(fn,checkboxsName,dis,chk){
	for(var i=0;i<fn.elements.length;i++){
		//định vị vào các checkbox
		if(fn.elements[i].type=="checkbox" && fn.elements[i].name==checkboxsName){
			fn.elements[i].disabled = dis;
			fn.elements[i].checked = chk;
		}
	}
}
function generateInput(name,className, placeholder){
	var tmp = "<input type=\'text\' name = \'"+name+"\' class=\'"+className+"\' placeholder=\'"+placeholder+"\' />";
	document.write(tmp);
}
function generateHiddenInput(name){
	var tmp = "<input type=\'hidden\' name = \'"+name+"\' />";
	document.write(tmp);
}

function generateEmptySelectBox(slcName,firstLabel,className){
	var name = (slcName==null) ?  "" : " name=\'"+slcName+"\' " ;
	var cls = (className==null) ?  "" : " class=\'"+className+"\' " ;
	var tmp="<select "+name+cls+" >";
	tmp += "<option value=\'"+0+"\' selected>"+firstLabel+"</option>"; 
	tmp += "</select>";
	document.write(tmp);
}

function generateSelectBoxDate(fn,inputSaveName,slcYearName,slcMonthName,slcDayName,fromYear,toYear,lang_value){

	var fromYear = parseInt(fromYear),
	toYear = parseInt(toYear);
	var labelYear,labelMonth,labelDay;
	
	generateHiddenInput(inputSaveName);
	
	//set Label
	labelYear = YEAR[lang_value];
	labelMonth = MONTH[lang_value];
	labelDay = DATE[lang_value];
	
	var labelsYear = setLabels(fromYear, toYear);
	generateEmptySelectBox(slcYearName,labelYear,'col-3  pull-left');
	var slcYear = document.getElementsByName(slcYearName)[0];
	setSelectBox(slcYear, labelsYear);
	
	var labelsMonth = setLabels(0, 12);
	document.write("<div class=\"col-6 text-center\"><div class=\"col-3\">&nbsp;</div>");
	generateEmptySelectBox(slcMonthName,labelMonth,'col-6');
	document.write("</div>");
	var slcMonth = document.getElementsByName(slcMonthName)[0];
	setSelectBox(slcMonth, labelsMonth);
	
	
	generateEmptySelectBox(slcDayName,labelDay,'col-3  pull-right');
	var slcDay = document.getElementsByName(slcDayName)[0];
	elmResult = document.getElementsByName(inputSaveName)[0];
	
	slcYear.addEventListener("change",function(){
		if(slcYear.selectedIndex != 0 && slcMonth.selectedIndex != 0){
			setSelectBoxDay(slcYear, slcMonth,slcDay, elmResult, lang_value);
		}
	},false);
	
	slcMonth.addEventListener("change",function(){
		if(slcYear.selectedIndex != 0 && slcMonth.selectedIndex != 0){
			setSelectBoxDay(slcYear, slcMonth,slcDay, elmResult, lang_value);
		}
	},false);
}
//chuyen doi ten nhãn dán khi nhỏ hơn 10 thành 0x 
function setLabels(from, to){
	var max = Math.abs(to - from);
	var labels = new Array(max);
	for(let i = 0; i< max; i++){
		if(from+i<9) temp = "0" + (i+1);
		else temp = from+i+1;
		labels[i] = temp;

	}
	
	return labels;
}

//Thiết lập lại thuộc tính cho selectBox
function setSelectBox(elm, optionsLabel){
	var len = elm.childElementCount-1;
	var newlen = optionsLabel.length;
	distLen = newlen - len;
	if(distLen>0){
		for (var i=len ; i< newlen; i++) {
			var option = document.createElement("option");
			option.text = optionsLabel[i];
			option.value = i+1;
			elm.appendChild(option);	
		}
	}else{
		for (var i=len ; i> newlen; i--) {
			elm.removeChild(elm.lastChild);	
		}
	}
}
//don dep


//Thiết lập lại thuộc tính cho selectBox day lấy giá trị từ selectbox year, month
function setSelectBoxDay(elmYear, elmMonth,elmDay, elmResult, lang_value){
	
	var year = parseInt(elmYear.value),
	month = parseInt(elmMonth.value);
	var date = [31,28,31,30,31,30,31,31,30,31,30,31]; 
	//kiem tra nam nhuan
	if(year%4==0 || (year%4==0 && year%100!=0)){
		date[1] = 29;
	}

	var len = date[month-1];

	var labelsDay = setLabels(0, len);

	setSelectBox(elmDay,labelsDay);
}

function readAnImage(itemShow, file) {
    if (file) {
    	itemShow.alt = file.name;
        var reader = new FileReader();
        reader.onload = function (e) {
            itemShow.src = e.target.result;
        };

        reader.readAsDataURL(file);
    }
}

function resetValue(elm){
	elm.value="";
}

function removeLastIndexChar(str,char){
	var at = str.length - 1;
	if(str!=null&&str!=""&&char!=null&&char!=""){
		while(str[at]!=char){
			at--;
		}
	}
	str = str.substring(0,at+1);
	return str;
}

function convertArrayToText(arr){
	var str = "";
    for(let i=0;i<arr.length;i++){
    	str += arr[i];
    }
    return str;
}

function getBase64(obj, file){
	var reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function() {
		obj.value = reader.result;
	};
}

function searchObjectById(dataObjs, id){
	var dataObj;
	for(let i=0; i<dataObjs.length; i++){
		if(dataObjs[i].Id == id){
			dataObj = dataObjs[i];
			break;
		}
	}
	return dataObj;
}

function removeObjectById(dataObjs, id){
	
	var size = 0;
	for(let i=dataObjs.length-1; i>=0 ; i--){
		if(dataObjs[i].Id == id){
			size = dataObjs[i].size;
			dataObjs.splice(i, 1);;
			break;
		}
	}
	return size;
}
function existsElmById(list, id){
	var childs = list.childNodes;
	var len = childs.length;
	var child = null;
	if(len > 0){
		for(let i = len - 1; i>=0 ; i--){
			if(childs[i].id == id){
				child = childs[i];
				break;
			}
		}
	}
	return child;
}
function removeElmById(list, id){
	list.removeChild(existsElmById(list, id));
}

function getXmlHttpObject() {
	var xmlHttp = null;
	try {
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		try {
			xmlHttp = new ActiveXObject('MSXML2.XMLHTTP');
		} catch (e) {
			xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
		}
	}
	return xmlHttp;
}

/*change row textarea*/
function addClass(elm, className){
	if(!elm.classList.contains(className)){
		elm.classList.add(className);
	}
}

function removeClass(elm, className){
	if(elm.classList.contains(className)){
		elm.classList.remove(className);
	}
}
function isEnter(event) {
	var evtobj = window.event? event : e;
    if (evtobj.keyCode == 13 || evtobj.which == 13) {
        return true;
    }
    return false;
}

function isShiftEnter(event) {
	var evtobj = window.event? event : e;
    if ((evtobj.keyCode == 13 || evtobj.which == 13) && evtobj.shiftKey) {
        return true;
    }
    return false;
}
function isBackspaceOrDel(event){
	var evtobj = window.event? event : e;
	var evtobj = window.event? event : e
    if (evtobj.keyCode == 8 || evtobj.which == 46) {
        return true;
    }
    return false;
}

function setStyle(elm, value){
	elm.setAttribute("style",value);
}

/*to HTML Entities*/
String.prototype.toHtmlEntities = function() {
    return this.replace(/./gm, function(s) {
        return "&#" + s.charCodeAt(0) + ";";
    });
};

String.fromHtmlEntities = function(string) {
    return (string+"").replace(/&#\d+;/gm,function(s) {
        return String.fromCharCode(s.match(/\d+/gm)[0]);
    })
};