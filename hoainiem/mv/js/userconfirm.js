function checkValidConfirmForm(fn, messageId, lang_value){
	var codeConfirm =  fn.txtConfirmCode.value;

	var validCode = true;
	var message="";
	if(codeConfirm==undefined){
		validCode = false;
		message = "Lỗi";
	}else{
		codeConfirm = codeConfirm.trim();
		if(codeConfirm==""){
			validCode = false;
			message = ERR.EMPTY_FIELD[lang_value];
		}else{
			if(!codeConfirm.match(/^\d{4}$/g)){
				validCode = false;
				message = ERR.CONFIRM_CODE_STRUCT_FIELD[lang_value];
			}
		}
	}
	
	if(message!=""){
		fn.txtConfirmCode.focus();
		fn.txtConfirmCode.select();
		resetValue(fn.txtConfirmCode);
		showLabel(messageId, "hide", message);
	} 
	return validCode;
}
function confirmForm(fn,messageId, lang_value){
	if(checkValidConfirmForm(fn, messageId, lang_value)){
		submitForm(fn,"POST","/"+webModule+"/user/confirm/control.jsp");
	}
}