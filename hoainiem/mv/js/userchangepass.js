
function checkVaLidChangePass(fn){
	//Lấy dữ liệu trên giao diện
	var oldpass=fn.txtUserOldPass.value,
	userpass=fn.txtUserPass.value,
	repeatpass  =fn.txtUserRepeatPass.value;
	
	//Các hiển thị xác nhận sự hợp lệ
	var validUserOldPass=true,
	validUserPass=true,
	validUserRepeatPass=true,
	noDuplicatePass = true;
	
	//Hiển thị thông báo lỗi
	var message="";

	//Kiểm tra 2 mk

	
	if(userpass!=repeatpass){
     	validUserRepeatPass = false;
     	message = "Mật khẩu nhập lại phải giống mật khẩu mới";
    }

    if(userpass==oldpass){
     	noDuplicatePass = false;
     	message = "Mật khẩu mới phải khác mật khẩu cũ";
    }

	//Kiểm tra mật khẩu
	if(isSpace(userpass)){
		validUserPass=false;
		message="Thiếu mật khẩu mới";
	}else if(isLessLength(userpass,5)){
		validUserPass=false;
		message="Mật khẩu phải lớn hơn 5 kí tự"				
	}

	if(isSpace(oldpass)){
		validUserOldPass=false;
		message="Thiếu mật khẩu cũ";
	}else if(isLessLength(oldpass,5)){
		validUserOldPass=false;
		message="Mật khẩu lớn hơn 5 kí tự"				
	}

	//Thông báo
	if(message!=""){
		if(!validUserOldPass){
			fn.txtUserPass.value="";
			fn.txtUserOldPass.select();
			fn.txtUserRepeatPass.value="";
			showLabel("label-useroldpass-error", "hide", message);
		}else if(!validUserPass){
			fn.txtUserPass.value = "";
			fn.txtUserPass.select();
			showLabel("label-userpass-error", "hide", message);

		}else if(!noDuplicatePass){
			fn.txtUserPass.select();
			showLabel("label-userpass-error", "hide", message);
			fn.txtUserRepeatPass.value="";

		}else if(!validUserRepeatPass){		
			fn.txtUserRepeatPass.value="";
			fn.txtUserPass.focus();
			fn.txtUserRepeatPass.select();
			
			showLabel("label-userpass-error", "hide", message);
			showLabel("label-userrepeatpass-error", "hide", message);
		}
	}

	/*The logical AND operator,return a if a is falsy, return b if a is truthy*/
	return validUserOldPass && validUserPass && validUserRepeatPass && noDuplicatePass;
}
function changePass(fn){
	if(checkVaLidChangePass(fn)){
		fn.method = "POST"; //goi den doPost()
		fn.action = "/"+webModule+"/user/changepass/control.jsp"; //Duong dan
		fn.submit();
	}
}