function sort(fn){
	var sortValue = fn.slcSort.value;
	var tabValue = fn.txtTabActive.value;
	if(sortValue!=0){
		submitForm(fn,"POST","control/?sid="+sortValue+"&tab="+tabValue);
	}
}

function setPaging(countLeft, countRight, maxPage, indexActive, link, id){

	var start = indexActive - countLeft;
	var stop = indexActive + countRight;
	
	if(start<0){
		start = 0;
		stop = start + countRight;
		if(stop >= maxPage){
			stop = maxPage;
		}
	}
	
	if(stop >= maxPage){
		stop = maxPage;
		start = stop - countLeft;
		if(start < 0){
			start = 0;
		}
	}

	var paging = "";

	if(indexActive>0){
		paging += "<li>";
		paging += "<a href=\""+link+(indexActive-1)+"\" class=\"prev link-default\">Prev</a>";
		paging += "</li>";
	}
	
	for(let i=start;i<=stop;i++){
		paging += (i == indexActive) ? "<li class=\"active\">" : "<li class=\"\">";
		paging += (i == indexActive) ? i : "<a href=\""+link+i+"\" class=\"link-default\">"+i+"</a>";

		paging += "</li>";	
	}
	
	if(indexActive<maxPage){
		paging += "<li>";
		paging += "<a href=\""+link+(indexActive+1)+"\" class=\"next link-default\">Next</a>";
		paging += "</li>";
	}
	document.getElementById(id).innerHTML = paging;
}
function generatePaging(id){

	var paging = "<section class=\"paging row\">";
	paging += "<ul id=\""+id+"\" class=\"horizontal-nav list-unstyle\">";
	
	paging += "</ul>";
	paging += "</section>";
	
	document.write(paging);
}