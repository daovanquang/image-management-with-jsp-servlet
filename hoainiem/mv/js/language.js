//ERROR
//VN - EN
var Err_JSON = '{';
Err_JSON += '"EMPTY_FIELD" : ["Bạn không được để trống trường này","You can&#8217t leave this empty"],';
Err_JSON +=' "LENGTH_FIELD" : ["Độ dài của trường này phải là","The length of the field should be"],';
Err_JSON +=' "HAVE_SPACE_FIELD" : ["Trường này không chứa khoảng trắng","This field must not have spaces"],';
Err_JSON +=' "MAIL_STRUCT_FIELD" : ["Cấu trúc hộp thư không chính xác","Incorrect mailbox structure"],';
Err_JSON +=' "DIFFERENT_PASS_FIELD" : ["Hai mật khẩu phải giống nhau","The two passwords must be the same"],';
Err_JSON +=' "CONFIRM_CODE_STRUCT_FIELD" : ["Cấu trúc mã xác nhận không chính xác","Incorrect validation code structure"] ';
Err_JSON +='}';

ERR = JSON.parse(Err_JSON);

CHARACTERS = ["ký tự"," characters"];
GREATER = ["lớn hơn"," greater"];
SMALLER = ["nhỏ hơn"," smaller"];

YEAR = ["Năm","Year"];
MONTH = ["Tháng","Month"];
DATE = ["Ngày","Date"];