function loadImage(inputId, imgId, nameId){
	var file = document.getElementById(inputId).files[0];
	var itemShow = document.getElementById(imgId);
	var itemName = document.getElementById(nameId);
	itemName.innerHTML = file.name;
	readAnImage(itemShow, file);
}
function checkValidVerify(fn,messageText){
	//Lấy dữ liệu trên giao diện
	var fullname = fn.txtUserFullName.value,
	phone = fn.txtUserPhone.value,
	gender = fn.slcUserGender.selectedIndex,
	addressdetails = fn.txtAddressDetails.value;
	var day = fn.slcDay.selectedIndex,
	month = fn.slcMonth.selectedIndex,
	year = fn.slcYear.selectedIndex,
	nation = fn.slcNation.selectedIndex,
	provicecity = fn.slcProviceCity.selectedIndex;
	var images = fn.txtUserImage.files.length;
	//Các hiển thị xác nhận sự hợp lệ
	var validFullName=true,
	validPhone=true,
	validBirthDay=true,
	validGender=true,
	validAddress=true;
	validImage=true;

	//Hiển thị thông báo lỗi
	var message="";

	//kiem tra hop le
	if(nation==0|| provicecity==0|| addressdetails.trim() ==""||addressdetails==null){
    	validAddress = false;
    	message = "Nhập địa chỉ";
    }
	if(day ==0 || month == 0|| year == 0){
    	validBirthDay = false;
    	message = "Chọn ngày sinh";
    }

    if(phone==""||phone==null){
    	validPhone = false;
    	message = "Nhập số điện thoại";
    	if(phone.match(/^\d{10,12}$/g)){
    		message = "Sai cấu trúc số điện thoại";
    	}
    } 

    if(gender == 0){
    	validGender = false;
    	message = "Chọn giới tính";
    } 

	fullname=fullname.trim();
	if(isSpace(fullname)){
		validFullName=false;
		message="Thiếu tên đầy đủ";
	}else{
		if(!checkHaveSpace(fullname)){
			validFullName=false;
		    message="Vui lòng nhập đầy đủ họ tên";
		}else if(!isLessLength(fullname,50)){
			validUserName=false;
		    message="Tên quá dài";
		}
    }	

    if(images == 0){
    	validImage = false;
    	message = "Bạn phải thêm ảnh mới";
    }

    if(message!=""){
    	messageText.innerHTML = message;
    }
    return validGender && validFullName && validAddress && validBirthDay && validAddress && validImage; 

}
function pass(fn){
	submitForm(fn,"POST","/"+webModule+"/user/verify/control.jsp");
}

function verify(fn,messageboxId){
	
	var messageBox = document.getElementById(messageboxId),
	messageText = messageBox.querySelectorAll(".content")[0];
	if(checkValidVerify(fn,messageText)){
		//luu dia chi
		fn.txtUserAddress.value = fn.txtAddressDetails.value +"-"+ fn.slcProviceCity.options[fn.slcProviceCity.selectedIndex].text+"-"+ fn.slcNation.options[fn.slcNation.selectedIndex].text; 
		//luu ngay sinh
		fn.txtUserBirthDay.value = fn.slcYear.options[fn.slcYear.selectedIndex].text +"-"+ fn.slcMonth.options[fn.slcMonth.selectedIndex].text +"-"+ fn.slcDay.options[fn.slcDay.selectedIndex].text;

		submitForm(fn,"POST","/"+webModule+"/user/verify/control.jsp");
		flag = true;
	}else{	
		swapHide(messageboxId,"hide");
	}
}

