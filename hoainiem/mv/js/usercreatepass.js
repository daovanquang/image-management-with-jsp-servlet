function checkCreatePass(fn, lang_value){
	//Lấy dữ liệu trên giao diện
	var userpass = fn.txtUserPass.value,
	repeatpass = fn.txtUserRepeatPass.value;
	
	//Các hiển thị xác nhận sự hợp lệ
	validUserPass = true,
	validRepeatPass = true;
	
	//Hiển thị thông báo lỗi
	var message = "";

	//Kiểm tra 2 mk
	if(userpass != repeatpass){
     	validRepeatPass = false;
     	message = ERR.DIFFERENT_PASS_FIELD[lang_value];
    }
	//Kiểm tra mật khẩu
	if(isSpace(userpass)){
		validUserPass=false;
		message = ERR.EMPTY_FIELD[lang_value];
	}else if(isLessLength(userpass,5) || !isLessLength(userpass,100)){
		validUserPass=false;
		message = ERR.LENGTH_FIELD[lang_value] + " " + 5 +"-"+100+" "+CHARACTERS[lang_value];				
	}

	console.log(message);

	//Thông báo
	if(message != ""){
		if(!validUserPass){
			fn.txtUserPass.focus();
			fn.txtUserPass.select();
			showLabel("label-userpass-error", "hide", message);

		}else if(!validRepeatPass){		
			fn.txtUserRepeatPass.value="";
			fn.txtUserPass.focus();
			fn.txtUserRepeatPass.select();
			
			showLabel("label-userpass-error", "hide", message);
			showLabel("label-userrepeatpass-error", "hide", message);
		}
	}
	/*The logical AND operator,return a if a is falsy, return b if a is truthy*/
	return validUserPass && validRepeatPass;
}

function createPass(fn, lang_value){
	if(checkCreatePass(fn, lang_value)){
		submitForm(fn,"POST","/"+webModule+"/user/createpass/control.jsp");
	}
}