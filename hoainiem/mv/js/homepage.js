/*HOME PAGE*/
/*FOR MAIN SLIDE*/
var mainSlideJSON = '{"slideId":"MainSlider","listClass":"slider__track", "itemClass":"slider__item", "dotClass":"dot" ,"btnClass":"prev-next-btn" ,"index":0, "numSlideChange":1, "numSlideShow":1,"isResponseHeight":true }';
mainSlideObj = JSON.parse(mainSlideJSON);

window.addEventListener("load", function (){
	loadSlide(mainSlideObj);

},false);

/*auto run slide*/
function autoNext(){
	nextSlide(mainSlideObj);
}
var IntervalForNextSlide = setInterval(autoNext,8000);

//add event for slide control
window.addEventListener("load", function(){

	document.getElementById(mainSlideObj.slideId).querySelectorAll("."+mainSlideObj.btnClass)[0].addEventListener("click",function(){
		prevSlide(mainSlideObj);
	}, false); 
	document.getElementById(mainSlideObj.slideId).querySelectorAll("."+mainSlideObj.btnClass)[1].addEventListener("click",function(){
		nextSlide(mainSlideObj);
	}, false);

	var dots=document.getElementById(mainSlideObj.slideId).querySelectorAll("."+mainSlideObj.dotClass);
	for(let i=0;i<dots.length;i++){
		dots[i].addEventListener("click",function(){
			mainSlideObj.index = i;
			changeSlide(mainSlideObj);
			changeDot(mainSlideObj);
		},false);
	}
},false);

//auto change slide
var resizeId;
window.addEventListener("resize", function (){
	var screenWidth = parseInt(window.innerWidth);
	var list = document.getElementById(mainSlideObj.slideId).querySelectorAll("."+mainSlideObj.listClass)[0];
	if(!list.classList.contains('animated')) list.classList.remove('animated');
	clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 300);
},false);
function doneResizing(){
	resizeSlide(mainSlideObj);
}

/*END FOR MAIN SLIDE*/

/*FOR IMPRESSIVE IMAGE SLIDE*/
var impressiveImageJSON = '{"slideId":"ImpressivePhotos","listClass":"slider__track", "itemClass":"slider__item","imageClass":"card__img-cover", "dotClass":null ,"btnClass":"prev-next-btn" ,"index":0, "numSlideShow":5, "numSlideChange":1, "numSlideShow":1,"isResponseHeight":true}';
impressiveSlideObj = JSON.parse(impressiveImageJSON);

/*auto run slide*/
function autoNextImpressiveSlide(){
	nextSlide(impressiveSlideObj);
}
var IntervalForImpressiveSlide = setInterval(autoNextImpressiveSlide,6000);

//add event for slide control
window.addEventListener("load", function(){
	document.getElementById(impressiveSlideObj.slideId).querySelectorAll("."+impressiveSlideObj.btnClass)[0].addEventListener("click",function(){
		prevSlide(impressiveSlideObj);
	}, false); 
	document.getElementById(impressiveSlideObj.slideId).querySelectorAll("."+impressiveSlideObj.btnClass)[1].addEventListener("click",function(){
		nextSlide(impressiveSlideObj);
	}, false); 

},false);


window.addEventListener("load",setImpressiveSlide,false);
window.addEventListener("resize",setImpressiveSlide,false);

function setImpressiveSlide(){
	var screenWidth = parseInt(window.innerWidth);
	impressiveSlideObj.numSlideShow = generateCol(screenWidth,"large");
}
window.addEventListener("load", function (){
	loadSlide(impressiveSlideObj);
},false);
//auto change slide
var resizeImpressId;

function doneResizingImpress(){
	resizeSlide(impressiveSlideObj);
}
window.addEventListener("resize", function (){
	var list = document.getElementById(impressiveSlideObj.slideId).querySelectorAll("."+impressiveSlideObj.listClass)[0];
	if(!list.classList.contains('animated')) list.classList.remove('animated');
	clearTimeout(resizeImpressId);
    resizeImpressId = setTimeout(doneResizingImpress, 100);
},false);


/*FOR multi-purpose*/
var informationJSON = '{"slideId":"Services","listClass":"slider__track", "itemClass":"slider__item", "dotClass":"dot" ,"btnClass":null ,"index":0, "numSlideChange":1, "numSlideShow":1,"isResponseHeight":false}';
informationObj = JSON.parse(informationJSON);

window.addEventListener("load",setInformation,false);
window.addEventListener("resize",resizeInformation,false);
function resizeInformation(){
	var screenWidth = parseInt(window.innerWidth);
	setInformation();
	if(screenWidth<= 600){
		resizeSlide(informationObj);
	}
}
function setInformation(){
	//Kiểm tra xem width = 600 thì thiết lập informationObj slide
	var screenWidth = parseInt(window.innerWidth);
	var dots=document.getElementById(informationObj.slideId).querySelectorAll("."+informationObj.dotClass);
	
	if(screenWidth<= 600){
		loadSlide(informationObj);
		for(let i=0;i<dots.length;i++){
			dots[i].addEventListener("click",function(){
				informationObj.index = i;
				changeSlide(informationObj);
				changeDot(informationObj);
			},false);
		} 
	}/*else{
		clearInterval(IntervalForInformationSlide); clearInterval auto run
	}*/

}
/*auto next Services Slide
function autoNextInformationSlide(){
	nextSlide(informationObj);
}
var IntervalForInformationSlide = setInterval(autoNextInformationSlide,5000);
*/

/*END FOR services-slide*/


/*FOR PRIMARY VIDEO*/
var primaryVideoJSON = '{"slideId":"PrimaryVideoPlayer","listClass":"slider__track", "itemClass":"slider__item", "dotClass":null ,"btnClass":"prev-next-btn" , "index":0, "active":3, "numSlideShow":5, "numSlideChange":1}';
primaryVideoObj = JSON.parse(primaryVideoJSON);

var mainVideoSourceJSON = '{"Id":"MainVideoPlayer","titleClass":"main-video-title", "sourceClass":"main-video-source"}',
mainVideoSourceObj = JSON.parse(mainVideoSourceJSON);

var primaryVideoSourceJSON = '{"Id":"PrimaryVideoPlayer","titleClass":"primary-video-title", "sourceClass":"primary-video-source", "btnClass":"slider__item", "index":3, "numSlideShow":1,"isResponseHeight":false}',
primaryVideoSource = JSON.parse(primaryVideoSourceJSON);

/*auto run slide
function autoNextPrimaryVideo(){
	nextSlide(primaryVideoObj);
}
var IntervalForPrimaryVideo = setInterval(autoNextPrimaryVideo,10000);
*/

//add event for slide control
window.addEventListener("load", function(){
	document.getElementById(primaryVideoObj.slideId).querySelectorAll("."+primaryVideoObj.btnClass)[0].addEventListener("click",function(){
		prevSlide(primaryVideoObj);
		
	}, false); 
	document.getElementById(primaryVideoObj.slideId).querySelectorAll("."+primaryVideoObj.btnClass)[1].addEventListener("click",function(){
		nextSlide(primaryVideoObj);
	}, false); 

},false);

window.addEventListener("load",function (){
	setPrimaryVideo();
	loadSlide(primaryVideoObj);
	setHeightMainVideo();
},false);
window.addEventListener("resize",function (){
	setPrimaryVideo();
	resizeSlide(primaryVideoObj);
	setHeightMainVideo();
},false);

function setHeightMainVideo(){
	var screenWidth = parseInt(window.innerWidth);

	//set height
	if(screenWidth<= 600){
		var mainVideo = document.getElementById(mainVideoSourceObj.Id);

		var mainVideoSource =  mainVideo.querySelectorAll("."+mainVideoSourceObj.sourceClass)[0];
		var height = Math.floor(2*screenWidth/3);
		mainVideoSource.setAttribute("height",height+"px");
	}
}

function setPrimaryVideo(){
	var screenWidth = parseInt(window.innerWidth);
	primaryVideoObj.numSlideShow = generateCol(screenWidth, "primary-video");
	if(screenWidth>= 600){
		primaryVideoObj.active = 3;
	}else if(screenWidth>=400 && screenWidth<600){
		primaryVideoObj.active = 2;
	}else if(screenWidth<200){
		primaryVideoObj.active = 1;
	}
}

window.addEventListener("load",function(){	

	changeVideoSource(mainVideoSourceObj,primaryVideoSource);
	primaryVideo = document.getElementById(primaryVideoSource.Id)
	listBtn =  primaryVideo.querySelectorAll("."+primaryVideoSource.btnClass);
	for(let i=0; i<listBtn.length;i++){
		document.getElementById(primaryVideoSource.Id).querySelectorAll("."+primaryVideoSource.btnClass)[i].addEventListener("click",function(){
			primaryVideoSource.index = i;
			changeVideoSource(mainVideoSourceObj,primaryVideoSource);
		}, false); 
	}

},false)

function changeVideoSource(mainObj,primaryObj){
	var mainVideo = document.getElementById(mainObj.Id),
	mainVideoTitle =  mainVideo.querySelectorAll("."+mainObj.titleClass)[0],
	mainVideoSourceObj =  mainVideo.querySelectorAll("."+mainObj.sourceClass)[0];

	var primaryVideo = document.getElementById(primaryObj.Id),
	listVideoTitle =  primaryVideo.querySelectorAll("."+primaryObj.titleClass),
	listVideoSource =  primaryVideo.querySelectorAll("."+primaryObj.sourceClass);

	mainVideoTitle.innerHTML = listVideoTitle[primaryObj.index].innerHTML;	
	mainVideoSourceObj.src = listVideoSource[primaryObj.index].innerHTML;

}


/*END FOR PRIMARY VIDEO*/
