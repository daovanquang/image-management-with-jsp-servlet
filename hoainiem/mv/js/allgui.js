webModule = "mv";

/*IMAGE SAVED*/
var intervalCountUp;
window.addEventListener("load", function(){
	var elm = document.getElementById('img-saved'),
	num=0,
	speed = 0;
	if(elm!=undefined){
		max = parseInt(elm.innerHTML);
		intervalCountUp = setInterval(function(){
			if(num<max) num++;
			elm.innerHTML = num;
			if(num%4==0) speed+=5;
			if(num>=max) clearInterval(intervalCountUp);
		},1/speed)
	}
},false);

/*SCROLL*/
window.addEventListener("scroll", function(){
	var mainBar = document.getElementById("MainMenu"),
	screenWidth = parseInt(window.innerWidth),
	top = window.pageYOffset;
	if(screenWidth>=800 && top>= 71){
		if(!mainBar.classList.contains("fixed-top")){
			mainBar.classList.add("fixed-top");
		}
	}else{
		mainBar.classList.remove("fixed-top");
	}

}, false)

function generateCol(screen_width, type){
	var card_col = 1;
	if(type=="extra-large"){
		if(screen_width>1200){
			card_col = 7;
		}else if(screen_width>1000 && screen_width<=1200){
			card_col = 6;
		}else if(screen_width>800 && screen_width<=1000){
			card_col = 5;	
		}else if(screen_width>600 && screen_width<=800){
			card_col = 4;
		}else if(screen_width>400 && screen_width<=600){
			card_col = 3;
		}else if(screen_width>200 && screen_width<=400){
			card_col = 2;
		}else{
			card_col = 1;
		}
	}else if(type=="large"){
		if(screen_width>1200){
			card_col = 6;
		}else if(screen_width>1000 && screen_width<=1200){
			card_col = 5;
		}else if(screen_width>800 && screen_width<=1000){
			card_col = 4;	
		}else if(screen_width>600 && screen_width<=800){
			card_col = 4;
		}else if(screen_width>400 && screen_width<=600){
			card_col = 3;
		}else if(screen_width>200 && screen_width<=400){
			card_col = 2;
		}else{
			card_col = 1;
		}
	}else if(type=="small"){
		if(screen_width>1200){
			card_col = 5;
		}else if(screen_width>1000 && screen_width<=1200){
			card_col = 5;
		}else if(screen_width>800 && screen_width<=1000){
			card_col = 4;	
		}else if(screen_width>600 && screen_width<=800){
			card_col = 4;
		}else if(screen_width>400 && screen_width<=600){
			card_col = 3;
		}else if(screen_width>200 && screen_width<=400){
			card_col = 2;
		}else{
			card_col = 1;
		}
	}else if(type=="primary-video"){
		if(screen_width>600){
			card_col = 5;	
		}else if(screen_width>250 && screen_width<=600){
			card_col = 3;
		}else{
			card_col = 1;
		}
	}else if(type=="recent-photos"){
		if(screen_width>1200){
			card_col = 3;
		}else if(screen_width>1000 && screen_width<=1200){
			card_col = 3;
		}else if(screen_width>800 && screen_width<=1000){
			card_col = 6;	
		}else if(screen_width>600 && screen_width<=800){
			card_col = 5;
		}else if(screen_width>400 && screen_width<=600){
			card_col = 6;
		}else if(screen_width>200 && screen_width<=400){
			card_col = 3;
		}else{
			card_col = 2;
		}
	}else if(type=="proposed-groups"){
		if(screen_width>1200){
			card_col = 2;
		}else if(screen_width>1000 && screen_width<=1200){
			card_col = 2;
		}else if(screen_width>800 && screen_width<=1000){
			card_col = 5;	
		}else if(screen_width>600 && screen_width<=800){
			card_col = 4;
		}else if(screen_width>400 && screen_width<=600){
			card_col = 3;
		}else if(screen_width>200 && screen_width<=400){
			card_col = 2;
		}else{
			card_col = 1;
		}
	}
	return card_col;
}

function response(typeCard, listCard){
	var screen_width = window.innerWidth;
	var card_col = 1;
	
	//response
	card_col = generateCol(screen_width,typeCard);

	var list = document.querySelectorAll("."+listCard);
	if(list!=undefined)
	for (var i = 0; i < list.length; i++) {
		var card_margin_left = 5;
		
		var elm_width = getInnerElmWidth(list[i]);
		elm_width = Math.floor(elm_width - card_margin_left*(card_col-1));
		var card_width = Math.floor(elm_width/card_col);

		var card_cover_height = Math.floor(2*card_width/3);	
		var card_cover_style = "height:"+card_cover_height+"px;";

		var card_style ="";

		var cards = list[i].querySelectorAll(".image-card");
		var card_covers = list[i].querySelectorAll(".card__img-cover");

		if(cards!=undefined&&card_covers!=undefined){
			var len = cards.length;
			for(var j = 0; j<len;j++){
				if(cards[j]!=undefined){
					/*set property*/

					if(j==0 || j%card_col==0){
						card_margin_left = 0;		
					}else{
						card_margin_left = 5;	
					}
					card_style = "width:"+ card_width +"px;margin-left:"+card_margin_left+"px;";
					card_style += (card_margin_left > 0) ? "":"margin-right:0;";

					cards[j].setAttribute("style",card_style);

					if(card_covers!=undefined && card_covers[i]!=undefined){
						/*set property*/
						card_covers[j].setAttribute("style",card_cover_style);
					}
				}
			}
		}
	}
}

window.addEventListener("load",function(){

	response("extra-large","image-list--extra-large");
	response("small","image-list--small");
	response("recent-photos","image-list--recent-photos");
	response("proposed-groups","image-list--proposed-groups");
},true);
//var resizeEnd;
window.addEventListener("resize",function(){
	//clearTimeout(resizeEnd);
	//resizeEnd = setTimeout(function() {
		response("extra-large","image-list--extra-large");
		response("small","image-list--small");
		response("recent-photos","image-list--recent-photos");
		response("proposed-groups","image-list--proposed-groups");
	//},5);
},false);

function addRow(event, textarea, beginRow, endRow, isCommentForm){
	var rowAdd = 1;
	var form = textarea.form;
	var text = textarea.value,   
	lines = text.split(/\r|\r\n|\n/),
	presentRow = lines.length;

	if(isShiftEnter(event)){
		if(textarea.rows < endRow) {
			textarea.rows += rowAdd;
		} else {
			textarea.rows = endRow; 
		}
	}else if(isBackspaceOrDel(event)){
		if(textarea.rows >= beginRow + rowAdd && presentRow <= textarea.rows + rowAdd) {
			textarea.rows -= rowAdd;
		}
	}

	if(isCommentForm){
		if(textarea.rows> beginRow) addClass(form, "form-comment--big");
		else removeClass(form, "form-comment--big");
	}
}
function validResetTextarea(elm, rows){
	var text = elm.value,   
	lines = text.split(/\r|\r\n|\n/),
	presentRow = lines.length;
	if(elm.value != "" || presentRow>rows){
		return false;
	}
	return true;
}
function resetTextarea(elm, value, rows,beginRow){
	if(validResetTextarea(elm, beginRow)) setTextareaTitle(elm, value, rows);
}
function validChangeTextarea(elm, beginRow){
	if(elm.rows==beginRow){
		return true;
	}
	return false;
}
function changeTextarea(elm, value, rows, beginRow){
	if(validChangeTextarea(elm, beginRow)) setTextareaTitle(elm, value, rows);
}
function setTextareaTitle(elm, value, rows){
	setTextare(elm, value, rows);
}
function setTextare(elm, value, rows){
	elm.setAttribute("style",value);
	elm.rows = rows;
}

function searchPressEnter(e, fn){
	if(isEnter(e)){
		search(fn);
	}
}

function search(fn){
	var validKeywords = false;
	var keywords = fn.txtKeywords.value;
	keywords = keywords.trim();

	var message = '';
	if(keywords!=null){
		if(keywords!=''){
			validKeywords = true;
		}
	}

	if(validKeywords){
		var url =  '/mv/search/?q='+keywords;
		submitForm(fn, 'POST', url);
	}else{
		message = 'Bạn phải nhập ít nhất một từ khóa';
	}

	if(message != '') {
		alert(message);
	}
}