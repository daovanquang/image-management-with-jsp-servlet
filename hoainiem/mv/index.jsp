<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
  <jsp:include flush="true" page="/master/head.jsp"></jsp:include>
  <body>
    <jsp:include flush="true" page="/master/header.jsp"></jsp:include>
    <jsp:include flush="true" page="/main/slider.jsp"></jsp:include>
    <!--======================= CONTENT ===========================-->
    <div class="main-1200 mb-20">
      <!--newest-photo-shortcuts-->
      <jsp:include flush="true" page="/main/newestphotos.jsp"></jsp:include>
      <!--end newest-photo-shortcuts-->

      <!--impressive-photos-->
      <jsp:include flush="true" page="/main/impressivephotos.jsp"></jsp:include>
      <!--end impressive-photos-->

      <!--all-photo-->
      <jsp:include flush="true" page="/main/allphoto.jsp"></jsp:include>
      <!--end all photo-->

      <!--services & video-->
      <div class="multi-purpose">
        <jsp:include flush="true" page="/main/services.jsp"></jsp:include>
        <jsp:include flush="true" page="/main/newestvideos.jsp"></jsp:include>
      </div>
      <!--end services & video-->
     </div>
     <!--======================= END CONTENT ===========================-->
  <jsp:include flush="true" page="/master/footer.jsp"></jsp:include>
  <jsp:include flush="true" page="/master/scripts.jsp"></jsp:include>
  <script src="js/homepage.js" language="javascript"></script>
</body>
</html>
