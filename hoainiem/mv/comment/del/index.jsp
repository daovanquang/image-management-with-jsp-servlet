<%@page import="hoainiem.*, hoainiem.objects.*, hoainiem.gui.comment.*, hoainiem.library.*" %>
<%
int comment_id = Utilities.getIntParam(request,"cid");
if(comment_id>0){
  String datetime = UtilitiesCalendar.getCurrentTime("yyyy-MM-dd HH:mm");

  ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
  CommentControl cc = new CommentControl(cp);
  CommentObject delComment = new CommentObject();
  delComment.setComment_id(comment_id);
  delComment.setComment_created_date(datetime);
  boolean result = cc.delComment(delComment);

  if(result) {
    String referer = request.getHeader("Referer");
    response.sendRedirect(referer);
  }
}
%>

