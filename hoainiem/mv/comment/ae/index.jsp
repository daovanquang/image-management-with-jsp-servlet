<%@page import="java.util.*, java.io.BufferedReader, java.io.InputStreamReader" %>
<%@page import="org.json.JSONObject.*, org.json.*"%>
<%@page import="hoainiem.gui.comment.*"%>
<%@page import="hoainiem.*,hoainiem.objects.*, hoainiem.library.*" %>
<%
//Tim bo quan ly ket noi
ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");

BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
String strJson = "";
if(br != null){
    strJson = br.readLine();
}
if(Utilities.checkValidString(strJson)){
  JSONObject json = null;
  try{
    json = new JSONObject(strJson);
  }catch(JSONException ex){
    ex.printStackTrace();
  }
  if(json!=null){
    /*============- Comment Info -============*/
    try{
      int articleId =  json.getInt("article_id");
      String authorName = json.getString("user_fullname");
      String authorImage = json.getString("user_image");
      byte authorPermission = (byte)json.getInt("user_permission");
      String commentContent = json.getString("content");

      String datetime = UtilitiesCalendar.getCurrentTime("yyyy-MM-dd HH:mm");
      boolean commentEnable = true;

      CommentObject nComment = new CommentObject();
      nComment.setComment_article_id(articleId);
      nComment.setComment_author_name(authorName);
      nComment.setComment_author_image(authorImage);
      nComment.setComment_author_permission(authorPermission);
      nComment.setComment_created_date(datetime);
      nComment.setComment_content(commentContent);

      nComment.setComment_enable(commentEnable);
      /*============- Add New Comment -============*/
      CommentControl cc = new CommentControl(cp);

      cc.addComment(nComment);
    }catch(JSONException ex){
      ex.printStackTrace();
    }
  }
}
%>

