<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@page import="hoainiem.lang.gui.*, hoainiem.library.*" %>

<%
byte lang_value = Utilities.getLangValue(session);
%>

<section class="newest-videos slide pull-lef\">
<div class="section-heading">
<h5 class="pull-left"><%=HomeLang.WHAT_S_NEW[lang_value]%></h5>
</div>

<%
String newestvideos = (String) session.getAttribute("newestvideos");
if(!newestvideos.equalsIgnoreCase("")){
  out.print(newestvideos);
}
%>


