<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="hoainiem.home.menu.*, hoainiem.library.*"%>
<%@page import="hoainiem.values.gui.*, hoainiem.lang.gui.*" %>

<jsp:include flush="true" page="data.jsp"></jsp:include>
<%
String webModule = AllGUI.WebModule;

byte lang_value = Utilities.getLangValue(session);
String title = HomeLang.COMPANY_NAME[lang_value]+" - "+"website lưu trữ ảnh số 0 Việt Nam";

String pos = Utilities.getURI(request,(byte)2);

/* =============== SET TITLE ================= */
HomeMenuObject[] items = HomeMenu.createHomeMenu();

if(!pos.equalsIgnoreCase("")){
  for(int i=1; i<items.length; i++){
    if(items[i].getHomemenu_path().equalsIgnoreCase(pos)){
      if(lang_value==1){
        //English
        title = items[i].getHomemenu_name()+ "&nbsp;&nbsp;|&nbsp;&nbsp;" +title;
      }else{
        title = items[i].getHomemenu_name_en()+ "&nbsp;&nbsp;|&nbsp;&nbsp;" +title;
      }
    }
  }
}
%>
<head>
  <title><%=title %></title>
  <meta name="viewport" content="width=device-width"/> <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- ======================= STYLES & JS =========================== -->
  <link href="/<%=webModule%>/css/style.css" rel="stylesheet" type="text/css">
  <link href="/<%=webModule%>/imgs/favicon.png" rel="icon" type="image/x-icon">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
