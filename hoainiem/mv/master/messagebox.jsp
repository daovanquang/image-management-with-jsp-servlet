<%@page import="hoainiem.lang.gui.*, hoainiem.library.* "%>

<%
byte lang_value = Utilities.getLangValue(session);
%>

<div id = "messagebox" class="screen-fade absolute absolute-top hide">
<div class="message-box main-250 mt-30">
<div class="message-box__header">
<h3 class="title"><%=HomeLang.MESSAGE[lang_value]%></h3>
</div>
<div class="message-box--bg-sky row">
<div class="message-box__body mt-10">
<h4 class="content text-center"></h4>
</div>
<div class="message-box__footer row mt-15">
<button class="yes-btn btn btn-default" onclick="swapHide('messagebox','hide');"><%=HomeLang.MESSAGE_YES[lang_value]%></button>
<button class="cancel-btn btn btn-default" onclick="swapHide('messagebox','hide');"><%=HomeLang.MESSAGE_CANCEL[lang_value]%></button>
</div>
</div>
</div>
</div>
