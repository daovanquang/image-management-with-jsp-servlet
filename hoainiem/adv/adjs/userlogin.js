/*Các kịch bản xử lý cho login.html*/
function checkVaLidLogin(fn){
	//Lấy dữ liệu trên giao diện
	var username=fn.txtUserName.value;
	var userpass=fn.txtUserPass.value;
	
	//Các hiển thị xác nhận sự hợp lệ
	var validUserName=true;
	var validUserPass=true;
	
	//Hiển thị thông báo lỗi
	var message="";
	
	//kiểm tra mật khẩu	
	if(isSpace(userpass)){
		validUserPass=false;
		message="Thiếu mật khẩu";
	}else if(isLessLength(userpass,5)){
		validUserPass=false;
		message="Mật khẩu lớn hơn 5 kí tự"				
	}
	
	//Kiểm tra tên đăng nhập
	username=username.trim();
	if(isSpace(username)){
		validUserName=false;
		message="Thiếu tên đăng nhập";
	}else{
		if(checkHaveSpace(username)){
			validUserName=false;
		    message="Tên không được có khoảng trắng";
		}else if(!isLessLength(username,50)){
			validUserName=false;
		    message="Tên quá dài";
		}else if(username.indexOf("@")!=-1||username.indexOf(".")!=-1){
			//Cấu trúc hợp thư
			if(checkValidMail(username)) {
				 message="Không đúng cấu trúc hộp thư";
				}
            }	
        }	
	//Thông báo
	if(message!=""){
		if(!validUserName){
			fn.txtUserName.focus();
			fn.txtUserName.select();
			showLabel("label-username-error","hide", message);
		}
		else
		if(!validUserPass){
			fn.txtUserPass.focus();
			fn.txtUserPass.select();
			showLabel("label-userpass-error", "hide", message);
		}
	}
	/*The logical AND operator,return a if a is falsy, return b if a is truthy*/
	return validUserName && validUserPass;
}
function login(fn){
	if(checkVaLidLogin(fn)){
		fn.method = "POST"; //goi den doPost()
		fn.action = "/adv/user/login"; //Duong dan servlet
		fn.submit();
	}
}

function signin(fn){
	if(checkVaLidLogin(fn)){
		fn.method = "POST"; //goi den doPost()
		fn.action = "/adv/user/signin"; //Duong dan servlet
		fn.submit();
	}
}