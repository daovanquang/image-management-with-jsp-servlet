
function swapHide(id, classHide){
	var elm = document.getElementById(id);
	if(elm.classList.contains(classHide)) elm.classList.remove(classHide);
	else elm.classList.add(classHide);
}
/*FOR TOGGLE*/
function swapMenu(id, classHide, elm){
	swapHide(id, classHide);
	if(elm.classList.contains("fa-bars")){
		elm.classList.remove("fa-bars");
		elm.classList.add("fa-times");
	}
	else{
		elm.classList.remove("fa-times");
		elm.classList.add("fa-bars");
		
	}
}
/*END FOR TOGGLE*/
function showOnlyTab(itemsClass, classHide, index){
	var items = document.querySelectorAll("."+itemsClass);
	var len = items.length; 
	for(let i=0;i<len;i++){
		if(!items[i].classList.contains(classHide)) items[i].classList.add(classHide);
	}
	items[index].classList.remove(classHide);
}
function showOnlyTabByObj(objTab){
	var tabContainer = document.getElementById(objTab.tabContainerId),
	items = tabContainer.querySelectorAll("."+objTab.tabItemClass),
	tabs = tabContainer.querySelectorAll("."+objTab.tabControlClass),
	len = items.length; 

	for(let i=0;i<len;i++){
		if(tabs[i].classList.contains(objTab.activeTabControlClass)) tabs[i].classList.remove(objTab.activeTabControlClass);
		if(!items[i].classList.contains(objTab.hideTabItemClass)) items[i].classList.add(objTab.hideTabItemClass);
	}

	tabs[objTab.index].classList.add(objTab.activeTabControlClass);
	items[objTab.index].classList.remove(objTab.hideTabItemClass);
}

function loadTab(objTab){
	var tabContainer = document.getElementById(objTab.tabContainerId),
	items = tabContainer.querySelectorAll("."+objTab.tabItemClass),
	tabs = tabContainer.querySelectorAll("."+objTab.tabControlClass),
	len= items.length;
	for(let i=0;i< len;i++){
		tabs[i].addEventListener("click",function(){
			objTab.index = i;
			showOnlyTabByObj(objTab)
		},false);
		
	}
}
function getNum(value){
	return parseInt(value.replace(/[^\d.]/g, ''));
}
function isUnderfined(value){
	if(!value){
		value=0;
	}else{
		value = getNum(value);
	}
	return value;
}
function getOutOfElm(elm){
	var outOfElm=0;
	outOfElm += isUnderfined(elm.style.marginLeft);
	outOfElm += isUnderfined(elm.style.marginRight);
	outOfElm += isUnderfined(elm.style.paddingLeft);
	outOfElm += isUnderfined(elm.style.paddingRight);
	outOfElm += isUnderfined(elm.style.borderLeftWidth);
	outOfElm += isUnderfined(elm.style.borderRightWidth);
	return outOfElm;
}
function setChildsWidth(item, value){
	item.style.width = value - getOutOfElm(item) + 'px';
	
}
function getOuterElmWidth(elm){
	return Math.ceil(elm.offsetWidth + getOutOfElm(elm));
}
function getOuterParentWidth(items,indexStart, indexStop){
	var sum = 0;
	for(let i= indexStart;i<indexStop;i++){
		sum += getOuterElmWidth(items[i]);
	}
	return sum;
}
/*FOR SLIDE*/
function prevSlide(obj){
	slide= document.getElementById(obj.slideId),
	list= slide.querySelectorAll("."+obj.listClass)[0],
	btns = slide.querySelectorAll("."+obj.btnClass),
	items = slide.querySelectorAll("."+obj.itemClass),
	len = slide.querySelectorAll("."+obj.itemClass).length;
	if(obj.dotClass!=null) dots = slide.querySelectorAll("."+obj.dotClass);
	//change index to active and show new slide
	if(obj.index > 0) {
		if(!list.classList.contains('animated')) list.classList.add('animated');
		obj.index --;
	} else {
		obj.index = len - obj.numSlideChange;
		if(list.classList.contains('animated')) list.classList.remove('animated');
		var width=0;
		for(var i=0; i< obj.index; i++) width += items[i].offsetWidth;

		list.style.webkitTransform = 'translate3d('+(-width)+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+(-width)+ 'px, 0, 0)';
	}
	changeSlide(obj);
	//change dots
	if(obj.dotClass!=null) changeDot(obj);

}
function nextSlide(obj){
	var slide= document.getElementById(obj.slideId), 
	list= slide.querySelectorAll("."+obj.listClass)[0],
	btns = slide.querySelectorAll("."+obj.btnClass),
	items = slide.querySelectorAll("."+obj.itemClass),
	len = slide.querySelectorAll("."+obj.itemClass).length;
	if(obj.dotClass!=null) dots = slide.querySelectorAll("."+obj.dotClass);

	if(obj.index < len - obj.numSlideChange )  {
		if(!list.classList.contains('animated')) list.classList.add('animated');
		obj.index ++;
	} else {
		obj.index = 0;
		if(list.classList.contains('animated')) list.classList.remove('animated');
			
		list.style.webkitTransform = 'translate3d('+0+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+0+ 'px, 0, 0)';
	}
	changeSlide(obj);
	//change dots
	if(obj.dotClass!=null) changeDot(obj);

	

}
function changeDot(obj){
	var slide= document.getElementById(obj.slideId);
	dots = slide.querySelectorAll("."+obj.dotClass);

	for(var i=0;i<dots.length;i++) {
		if(dots[i].classList.contains('active')){
			dots[i].classList.remove('active');
		}
	}
	dots[obj.index].classList.add('active');
}
function changeSlide(obj){
	var slide= document.getElementById(obj.slideId), 
	list= slide.querySelectorAll("."+obj.listClass)[0],
	items = slide.querySelectorAll("."+obj.itemClass);
	
	var widthOfDiv = Math.floor(slide.offsetWidth/ parseInt(obj.numSlideChange)),
	sum = (obj.index)*widthOfDiv,
	maxTranslateX = (items.length - parseInt(obj.numSlideChange) + 1)*widthOfDiv;

	if(!list.classList.contains('animated')) list.classList.add('animated');
	
	if(sum >=0 && sum<maxTranslateX){
		sum=-sum;
		list.style.webkitTransform = 'translate3d('+sum+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+sum+ 'px, 0, 0)';
	}
}

function resizeSlide(obj){
	var slide= document.getElementById(obj.slideId),
	list=  slide.querySelectorAll("."+obj.listClass)[0],
	items = slide.querySelectorAll("."+obj.itemClass);

	if(!list.classList.contains('animated')) list.classList.add('animated');

	//reset width

	var widthOfDiv = Math.floor(slide.offsetWidth/ parseInt(obj.numSlideChange)),
	sum = (obj.index)*widthOfDiv,
	maxTranslateX = (items.length - parseInt(obj.numSlideChange) + 1)*widthOfDiv;

	//reset width
	for(var i=0;i<items.length;i++)
	setChildsWidth(items[i],widthOfDiv);

	list.style.width = items.length * widthOfDiv + 'px';	

	if(sum >= 0 && sum<maxTranslateX){
		sum=-sum;
		list.style.webkitTransform = 'translate3d('+sum+ 'px, 0, 0)';
		list.style.transform = 'translate3d('+sum+ 'px, 0, 0)';
	}	
}

function loadSlide(obj){
	var slide= document.getElementById(obj.slideId),
	list =  slide.querySelectorAll("."+obj.listClass)[0],
	items = slide.querySelectorAll("."+obj.itemClass),
	widthOfDiv = Math.floor(slide.offsetWidth/parseInt(obj.numSlideChange));

	//reset width
	for(var i=0;i<items.length;i++)
	setChildsWidth(items[i],widthOfDiv);

	list.style.width = items.length * widthOfDiv  + 'px';

	if(obj.dotClass!=null) changeDot(obj);
}
/*END FOR SLIDE*/


/*Component form*/
function isSpace(text){
	if(text=="") return true;
	else return false;
}
function isLessLength(text,length){
	if(text.length<length) return true;
	else return false;
}

function checkValidMail(mail){
	var partern=/\w+@\w+[.]\w/;
	if(!mail.match(partern)) return false;
	else return true;
}
function checkHaveSpace(text){
	if(text.indexOf(" ")==-1) return false;
	else return true;
}

function submitForm(fn,method,action){
	fn.method = method; //goi den method
	fn.action = action; //Duong dan
	fn.submit();
}

//message log

function changeLabel(input,labelId){
	if(input.value!="") hideLabel(labelId,"hide");
}
function showLabel(labelId,classHide,text){
	var label = document.getElementById(labelId);
	label.innerHTML = text;
	if(label.classList.contains(classHide)){
		label.classList.remove(classHide);
	}
}
function hideLabel(labelId,classHide){
	var label = document.getElementById(labelId);
	if(!label.classList.contains(classHide)){
		label.classList.add(classHide);
	}
}

//FOR DEL 
function confirmDel(url){
	var message = "Xác nhận xóa";
	if(window.confirm(message)){
		window.location.href=url;
	}else{
		return false;
	}
}

/*Gererate SelectBox*/
function generateSelectBox(name,labels,defaultIndex,className,fnChange){
	var tmp="<select name=\'"+name+"\' class=\'"+className+"\' onchange=\'"+fnChange+"\' >";
	for(let i=0;i<labels.length;i++){
		if(i==defaultIndex) tmp += "<option value=\'"+i+"\' selected>"+labels[i]+"</option>"; 
		else  tmp += "<option value=\'"+i+"\'>"+labels[i]+"</option>"; 
	}
	tmp += "</select>";
	document.write(tmp);
}

/*Generate check box list from select box value*/
function generateCheckBoxs(name, labels){
	var tmp = "";
	for(let i=0;i<labels.length;i++){
		if(i%2==0) {
			tmp+="<div class=\"row\">";
		}
		
		tmp += "<div class=\"col-6\">";
		tmp += "<input type=\"checkbox\" name=\'"+name+"\' id=\"chk"+i+"\" disabled/><label class=\"form-label\" for=\"chk"+i+"\">";
		tmp += "Quản lý "+labels[i];
		tmp += "</div>";
		
		if(i%2==1 || i==labels.length-1){
			tmp+="</div>";
		}
	}
	document.write(tmp);
}

function setCheckbox(fn,checkboxsName,dis,chk){
	for(var i=0;i<fn.elements.length;i++){
		//định vị vào các checkbox
		if(fn.elements[i].type=="checkbox" && fn.elements[i].name==checkboxsName){
			fn.elements[i].disabled = dis;
			fn.elements[i].checked = chk;
		}
	}
}
function generateInput(name,className, placeholder){
	var tmp = "<input type=\'text\' name = \'"+name+"\' class=\'"+className+"\' placeholder=\'"+placeholder+"\' />";
	document.write(tmp);
}
function generateHiddenInput(name){
	var tmp = "<input type=\'hidden\' name = \'"+name+"\' />";
	document.write(tmp);
}

function generateEmptySelectBox(name,firstLabel,className){
	var tmp="<select name=\'"+name+"\' class=\'"+className+"' >";
	tmp += "<option value=\'"+0+"\' selected>"+firstLabel+"</option>"; 
	tmp += "</select>";
	document.write(tmp);
}

function generateSelectBoxDate(fn,inputSaveName,slcYearName,slcMonthName,slcDayName,fromYear,toYear,nationCode){

	var fromYear = parseInt(fromYear),
	toYear = parseInt(toYear);
	var labelYear,labelMonth,labelDay;
	
	generateHiddenInput(inputSaveName);
	
	//set Label
	if(nationCode.toLowerCase() == 'vn'){
		labelYear = "Năm";
		labelMonth = "Tháng";
		labelDay = "Ngày";
	}
	
	var labelsYear = setLabels(fromYear, toYear);
	generateEmptySelectBox(slcYearName,labelYear,'col-3  pull-left');
	var slcYear = document.getElementsByName(slcYearName)[0];
	setSelectBox(slcYear, labelsYear);
	
	var labelsMonth = setLabels(0, 12);
	document.write("<div class=\"col-6 text-center\"><div class=\"col-3\">&nbsp;</div>");
	generateEmptySelectBox(slcMonthName,labelMonth,'col-6');
	document.write("</div>");
	var slcMonth = document.getElementsByName(slcMonthName)[0];
	setSelectBox(slcMonth, labelsMonth);
	
	
	generateEmptySelectBox(slcDayName,labelDay,'col-3  pull-right');
	var slcDay = document.getElementsByName(slcDayName)[0];
	elmResult = document.getElementsByName(inputSaveName)[0];
	
	slcYear.addEventListener("change",function(){
		if(slcYear.selectedIndex != 0 && slcMonth.selectedIndex != 0){
			setSelectBoxDay(slcYear, slcMonth,slcDay, elmResult, nationCode);
		}
	},false);
	
	slcMonth.addEventListener("change",function(){
		if(slcYear.selectedIndex != 0 && slcMonth.selectedIndex != 0){
			setSelectBoxDay(slcYear, slcMonth,slcDay, elmResult, nationCode);
		}
	},false);
}
//chuyen doi ten nhãn dán khi nhỏ hơn 10 thành 0x 
function setLabels(from, to){
	var max = Math.abs(to - from);
	var labels = new Array(max);
	for(let i = 0; i< max; i++){
		if(from+i<9) temp = "0" + (i+1);
		else temp = from+i+1;
		labels[i] = temp;

	}
	
	return labels;
}

//Thiết lập lại thuộc tính cho selectBox
function setSelectBox(elm, optionsLabel){
	var len = elm.childElementCount-1;
	var newlen = optionsLabel.length;
	distLen = newlen - len;
	if(distLen>0){
		for (var i=len ; i< newlen; i++) {
			var option = document.createElement("option");
			option.text = optionsLabel[i];
			option.value = i+1;
			elm.appendChild(option);	
		}
	}else{
		for (var i=len ; i> newlen; i--) {
			elm.removeChild(elm.lastChild);	
		}
	}
}
//don dep


//Thiết lập lại thuộc tính cho selectBox day lấy giá trị tự selectbox year, month
function setSelectBoxDay(elmYear, elmMonth,elmDay, elmResult, nationCode){
	
	var year = parseInt(elmYear.value),
	month = parseInt(elmMonth.value);
	var date = [31,28,31,30,31,30,31,31,30,31,30,31]; 
	//kiem tra nam nhuan
	if(year%4==0 || (year%4==0 && year%100!=0)){
		date[1] = 29;
	}

	var len = date[month-1];

	var labelsDay = setLabels(0, len);

	setSelectBox(elmDay,labelsDay);
}

 function readAnImage(imgElm, inputElm,index) {
    if (inputElm.files) {
        var reader = new FileReader();

        reader.onload = function (e) {
            imgElm.src = e.target.result;
        };

        reader.readAsDataURL(inputElm.files[index]);
    }
}

function clickById(id){
	document.getElementById(id).click();
}

function resetValue(elm){
	elm.value="";
}
function removeLastIndexChar(str,char){
	var at = str.length - 1;
	if(str!=null&&str!=""&&char!=null&&char!=""){
		while(str[at]!=char){
			at--;
		}
	}
	str = str.substring(0,at);
}

