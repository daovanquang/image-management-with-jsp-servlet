function checkValidGroupAE(fn){
	//lấy thông tin trên giao diện
	var name=fn.txtGroupName.value;
	image=fn.txtGroupImage.value;
	

	var permis= fn.slcGroupPermis.value;
	var enable= fn.slcGroupEnable.value;
	//hiển thị message
	var validName=true;
	var validImage=true;
	var validPermis=true;
	var validEnable=true;
	
	var message="";
	if(enable<0){
		validEnable = false;
		message  = "Cần có ít nhất thiết lập mở/đóng";
	}
	//kiểm tra vai trò của tác giả và quản lý
	if(permis<=0){
		validPermis=false;
	}
	if(!validPermis) message="Cần có ít nhất 1 vai trò";
	
	//kiểm tra ảnh
	image=image.trim();
	if(isSpace(image)){
		validPass=false;
		message="Thiếu ảnh";
	}else if(isLessLength(image,5)){
		validPass=false;
		message="Sai đường dẫn"				
	}
	
	//kiểm tra ten nhom
	name=name.trim();
	if(isSpace(name)){
		validName=false;
		message="Thiếu tên nhóm";
	}

	
	//xuất thông báo
	if(message!="") {
		window.alert(message);
		if(!validName){
			fn.txtGroupName.focus();
			fn.txtGroupName.select();
		}else{
			if(!validImage){
			fn.txtGroupImage.focus();
			fn.txtGroupImage.select();
			}else{
				if(!validPermis){
				fn.slcGroupPermis.focus();
				}else{
					if(!validEnable){
						fn.slcGroupEnable.focus();
					}
				}
			}
		}
    }
	return validName && validImage && validPermis && validEnable;
}
function resetGroupAE(fn,isEdit){
	var name=fn.txtGroupName;
	image=fn.txtGroupImage;
	var permis= fn.slcGroupPermis,
	enable= fn.slcGroupEnable;

	var url = fn.txtGroupUrlLink;

	//
	name.value = "";
	image.value = "";
	permis.selectedIndex = "0";
	enable.selectedIndex = "0";
	if(!isEdit){
		url.value = "";
	}
}
function submitGroupAE(fn){
	if(checkValidGroupAE(fn)){
		submitForm(fn,"POST","/adv/group/ae");
	}
}

