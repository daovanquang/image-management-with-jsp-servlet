//set roles
function setRoles(fn,checkboxName){
	var chks = fn.elements[checkboxName]; 
	var str = "";
	var checkAll = true;
	for(let i=0;i<chks.length;i++){
		if(chks[i].checked) str+= '@'+(i+1);
	}
	for(let i=0;i<chks.length;i++){
		if(!chks[i].checked) {
			checkAll = false;
			break;
		}
	}

	if(checkAll) fn.txtUserRoles.value = "all";
	else {
		str.trim();
		len = str.length;
		if(str[0] == '@')  fn.txtUserRoles.value = str.substring(1,len);
	}
}


function setCheckbox(fn,dis,chk, checkboxName){
	for(var i=0;i<fn.elements.length;i++){
		//định vị vào các checkbox
		checkbox = fn.elements[i];
		if(checkbox.type=="checkbox" && checkbox.name==checkboxName){
			checkbox.disabled = dis;
			checkbox.checked = chk;
			checkbox.onchange = function(){
				setRoles(fn,checkboxName);
			}
		}
	}
}




function refeshPermis(fn){
	var permis=parseInt(fn.slcUserPermis.selectedIndex);
	if(permis==4||permis==5)
		this.setCheckbox(fn,true,true,"chks");
	else if(permis==3)
		this.setCheckbox(fn,false,true,"chks");
	else if(permis==2){
		this.setCheckbox(fn,false,false,"chks");
	}else {
		this.setCheckbox(fn,true,false,"chks");
	}
}

function checkValidUserAE(fn){
	//lấy thông tin trên giao diện
	var name=fn.txtUserName.value;
	pass=fn.txtUserPass.value,
	mail=fn.txtUserMail.value;
	

	var permis= fn.slcUserPermis.value;
	var actions= fn.slcUserActions.value;
	//hiển thị message
	var validName=true;
	var validPass=true;
	var validMail=true;
	var validActions=true;
	var validPermis=true;
	
	var message="";
	if(actions<0){
		validActions = false;
		message  = "Cần có ít nhất 1 hành động";
	}
	//kiểm tra vai trò của tác giả và quản lý
	if(permis==2||permis==3)
		for(var i=0;i<fn.elements.length;i++){
			if(fn.elements[i].type=="checkbox" && fn.elements[i].name=="chks"){
				if(fn.elements[i].checked==true){
					validPermis=true;
					break;
				}else{
					validPermis=false;
				}
			}
		}
	if(!validPermis) message="Cần có ít nhất 1 vai trò";
	
	//kiểm tra email
	mail=mail.trim();
	if(!checkValidMail(mail)){
		validMail = false;
		message = "Không đúng cấu trúc mail";

	}
	
	//kiểm tra pass
	pass=pass.trim();
	if(isSpace(pass)){
		validPass=false;
		message="Thiếu mật khẩu";
	}else if(isLessLength(pass,5)){
		validPass=false;
		message="Mật khẩu lớn hơn 5 kí tự"				
	}

		//kiểm tra tên
	name=name.trim();
	if(isSpace(name)){
		validName=false;
		message="Thiếu tên đăng nhập";
	}else{
		if(checkHaveSpace(name)){
			validName=false;
		    message="Tên không được có khoảng trắng";
		}else if(!isLessLength(name,50)){
			validName=false;
		    message="Tên quá dài";
		}else if(name.indexOf("@")!=-1||name.indexOf(".")!=-1){
			//Cấu trúc hợp thư
			if(checkValidMail(name)) {
				 message="Không đúng cấu trúc hộp thư";
				}
            }	
        }	

	//xuất thông báo
	if(message!="") {
		window.alert(message);
		if(!validName){
			fn.txtUserName.focus();
			fn.txtUserName.select();
		}else{
			if(!validPass){
			fn.txtUserPass.focus();
			fn.txtUserPass.select();
			}else{
				if(!validMail){
			    fn.txtUserMail.focus();
			    fn.txtUserMail.select();
				}else{
					if(!validPermis){
					fn.slcUserPermis.focus();
					}else{
						if(!validActions){
							fn.slcUserActions.focus();
						}
					}
				}
			}
		}
    }
	return validName && validPass && validMail && validPermis && validActions;
}

function submitUserAE(fn){
	if(checkValidUserAE(fn)){
		submitForm(fn,"POST","/adv/user/ae");
	}
}

function resetUserAE(fn,isEdit){
	var name=fn.txtUserName,
	fullname=fn.txtUserFullName,
	pass=fn.txtUserPass,
	mail=fn.txtUserMail,
	groupid =fn.txtUserGroupId,
	phone=fn.txtUserPhone,
	birthday=fn.txtUserBirthDay,
	notes=fn.txtUserNotes,
	position=fn.txtUserPosition
	job=fn.txtUserJob;

	var permis= fn.slcUserPermis,
	actions= fn.slcUserActions;
	gender= fn.slcUserGender;

	if(!isEdit) {
		name.value = "";
		groupid.value = "";
	}
	pass.value = "";
	mail.value = "";
	phone.value = "";
	fullname.value = "";
	birthday.value = "",
	notes.value = "",
	position.value = "";
	job.value = "";

	permis.selectedIndex = "0";
	actions.selectedIndex = "0";
	gender.selectedIndex = "0";

	this.setCheckbox(fn,true,false);
}

window.addEventListener("load", function(){
	var frmUserAE = document.forms['frmUserAE'];
	if(frmUserAE!=undefined){
		refeshPermis(frmUserAE);
		setRoles(frmUserAE,'chks');
	}
}, false);


