package hoainiem.library.exception;

public class Math {
  public static float divided(int first_num, int second_num) {
    float result = 0;
    try {
      result = first_num / second_num;
    } catch (ArithmeticException e) {
      System.out.println("Can't be divided by Zero" + e);
    }
    return result;
  }
}
