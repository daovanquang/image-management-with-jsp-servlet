package hoainiem.library;

import java.sql.*;

public class UtilitiesSQL {
  public UtilitiesSQL() {
  }
  public static int getGeneratedKeys(PreparedStatement pre){
    int id = 0;
    try{
        ResultSet rs = pre.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
        rs.close();
    }
    catch (SQLException ex) {
    }
    return id;
  }
}
