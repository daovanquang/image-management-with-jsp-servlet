package hoainiem.library;

import java.awt.RenderingHints;
import java.io.IOException;
import sun.misc.BASE64Decoder;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Graphics2D;
import java.io.FileOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import sun.misc.BASE64Decoder;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

public class UtilitiesMedia {

    public static String[] VIDEO_EXT = {
        "x-m4v", "webm", "x-ms-wmv", "x-msvideo", "3gpp", "flv", "x-flv", "mp4",
        "quicktime", "mpeg", "ogv", "ts", "mkv"};
    public static String[] IMG_EXT = {
        "jpg", "jpeg", "png", "bmp", "svg", "gif", "heic", "heif"};

    public static String classifyFile(String path) {
      String ext = path.substring(path.indexOf(".") + 1);
      byte len = (UtilitiesMedia.VIDEO_EXT.length > UtilitiesMedia.IMG_EXT.length) ?
          (byte) UtilitiesMedia.VIDEO_EXT.length : (byte) UtilitiesMedia.IMG_EXT.length;
      String type = "other";
      for (byte i = 0; i < len; i++) {
        if (i < UtilitiesMedia.VIDEO_EXT.length) {
          if (ext.equalsIgnoreCase(UtilitiesMedia.VIDEO_EXT[i])) {
            type = "video";
            return type;
          }
        }
        if (i < UtilitiesMedia.IMG_EXT.length) {
          if (ext.equalsIgnoreCase(UtilitiesMedia.IMG_EXT[i])) {
            type = "img";
            return type;
          }
        }
      }

      return type;
    }

    public static void decodeBase64(String sourceData, String path) {
      // tokenize the data
      String[] parts = sourceData.split(",");
      String imageString = parts[1];

      // create a buffered image
      BufferedImage image = null;
      byte[] imageByte;

      BASE64Decoder decoder = new BASE64Decoder();
      try {
        imageByte = decoder.decodeBuffer(imageString);
        FileOutputStream fos = new FileOutputStream(new File(path));
        fos.write(imageByte);
        fos.close();

      }
      catch (IOException ex) {
      }
    }

    //get Buffered
    public static BufferedImage createThumb(BufferedImage in, int w, int h) {
      // scale w, h to keep aspect constant
      double outputAspect = 1.0 * w / h;
      double inputAspect = 1.0 * in.getWidth() / in.getHeight();
      if (outputAspect < inputAspect) {
        // width is limiting factor; adjust height to keep aspect
        h = (int) (w / inputAspect);
      }
      else {
        // height is limiting factor; adjust width to keep aspect
        w = (int) (h * inputAspect);
      }
      BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = bi.createGraphics();
      g2.setRenderingHint(
          RenderingHints.KEY_INTERPOLATION,
          RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      g2.drawImage(in, 0, 0, w, h, null);
      g2.dispose();
      return bi;
    }

//Resize Image
    private void createThumbnail(File file, String newAbsolutePath,
                                 int newHight,
                                 int newWidth) {
      try {
        // BufferedImage is the best (Toolkit images are less flexible)
        BufferedImage img = ImageIO.read(file);
        BufferedImage thumb = createEmptyThumbnail(newHight, newWidth);

        // BufferedImage has a Graphics2D
        Graphics2D g2d = (Graphics2D) thumb.getGraphics();
        g2d.drawImage(img, 0, 0, thumb.getWidth() - 1, thumb.getHeight() - 1, 0,
                      0, img.getWidth() - 1, img.getHeight() - 1, null);
        g2d.dispose();
        ImageIO.write(thumb, "PNG", createOutputFile(file, newAbsolutePath));
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    private File createOutputFile(File inputFile, String newAbsolutePath) {
      return new File(newAbsolutePath);
    }

    private BufferedImage createEmptyThumbnail(int newHight, int newWidth) {
      return new BufferedImage(newHight, newWidth, BufferedImage.TYPE_INT_RGB);
    }
    //end resize image
    public static void convertToGrayImage(String inputPath, String outputPath){
      BufferedImage img = null;
      File f = null;

      f = new File(inputPath);
      try {
        img = ImageIO.read(f);
      }
      catch (IOException ex) {
      }
      //get image width and height
      if(img!=null){
        int width = img.getWidth();
        int height = img.getHeight();
        int r = 0;
        int g = 0;
        int b = 0;
        //int a = 0;

        for (int i = 0; i < width; i++) {
          for (int j = 0; j < height; j++) {
            int pixel = (byte) img.getRGB(i,j);
            //a = (pixel>>24)&0xff;
            r = (pixel >> 16) &0xff;
            g = (pixel >> 8) & 0xff;
            b = pixel & 0xff;

            int avg = (r+g+b)/3;
            img.setRGB(i, j, avg);
          }
        }
      }
    }

    public static boolean delFile(String absolutePath){
      File file = new File(absolutePath);
      boolean isDeleted = false;
      if(file.delete())
      {
          isDeleted = true;
      }
      return isDeleted;
    }

    public static File getFile(String path){
      File file = new File(path);
      if (!file.exists()) {
        file.mkdir();
      }
      return file;
    }
  }
