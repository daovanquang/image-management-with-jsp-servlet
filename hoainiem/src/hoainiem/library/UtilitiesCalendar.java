package hoainiem.library;

import java.util.Date;
import java.text.SimpleDateFormat;

public class UtilitiesCalendar {
  public static String getCurrentTime(String format) {
    SimpleDateFormat sdfDate = new SimpleDateFormat(format);
    Date now = new Date();
    String strDate = sdfDate.format(now);
    return strDate;
  }

}
