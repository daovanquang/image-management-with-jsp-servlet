package hoainiem.library;

import javax.servlet.http.HttpSession;
import javax.servlet.ServletRequest;

import java.io.File;

public class UtilitiesExtends {
  public static String encodeSortValue(int sort_fieldIndex, byte sort_typeIndex) {
    return sort_fieldIndex + "@" + sort_typeIndex;
  }

  public static String[] decodeSortValue(ServletRequest request) {
    String[] sort_value = null;
    if (request != null) {
      String value = (String) request.getParameter("sid");
      if (value != null) {
        String delimiter = "@";
        sort_value = value.split(delimiter);
        return sort_value;
      }
    }
    return sort_value;
  }

  public static String getLastName(String fullName,byte lang_value){
    int end = 0;
    int flag = -1;
    int index = 0;
    int len = fullName.length();
    String lastName = "";
    if(lang_value==0){
      //vn
      index = len - 1;
    }else{
      //en
      end = len;
      flag = 1;
    }
    while(index*flag < end*flag){
      if(fullName.charAt(index)==32){
        break;
      }
      if(flag==1){
        index++;
      }else{
        index--;
      }
    }

    if(flag == 1){
      lastName = fullName.substring(0, index);
    }else{
      lastName = fullName.substring(index, len);
    }
    return lastName;
  }

  public static boolean isFileExists(String path){
    File f = new File(path);
    boolean isExists = f.exists();
    return isExists;
  }
}
