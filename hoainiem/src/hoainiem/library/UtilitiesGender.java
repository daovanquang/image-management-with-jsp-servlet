package hoainiem.library;

public interface UtilitiesGender {
  String []VN_GENDERS = {"--Gi\u1EDBi t�nh--",
      "Nam", "N\u1EEF", "Th\u1EB3ng",
      "\u0110\u1ED3ng t�nh", "L\u01B0\u1EE1ng t�nh", "V� t�nh", "Kh�c"};
  String []EN_GENDERS = {"--Gender--",
      "Male", "Female", "Straight",
      "Lesbian / Gay", "Bisexual", "Asexual", "Other"};
}
