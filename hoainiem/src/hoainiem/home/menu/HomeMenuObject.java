package hoainiem.home.menu;

public class HomeMenuObject {
  private byte homemenu_id;
  private String homemenu_name;
  private String homemenu_name_en;
  private String homemenu_active;
  private String homemenu_path;
  private String homemenu_notes;
  public HomeMenuObject(
      byte homemenu_id,
      String homemenu_name,
      String homemenu_name_en,
      String homemenu_path,
      String homemenu_active,
      String homemenu_notes){

    this.homemenu_id = homemenu_id;
    this.homemenu_name = homemenu_name;
    this.homemenu_name_en = homemenu_name_en;
    this.homemenu_path = homemenu_path;
    this.homemenu_active = homemenu_active;
    this.homemenu_notes = homemenu_notes;
  }

  public byte getHomemenu_id() {
    return homemenu_id;
  }

  public String getHomemenu_name() {
    return homemenu_name;
  }

  public String getHomemenu_name_en() {
    return homemenu_name_en;
  }

  public String getHomemenu_path() {
    return homemenu_path;
  }

  public String getHomemenu_active() {
    return homemenu_active;
  }

  public String getHomemenu_notes() {
    return homemenu_notes;
  }

  public void setHomemenu_id(byte homemenu_id) {
    this.homemenu_id = homemenu_id;
  }

  public void setHomemenu_name(String homemenu_name) {
    this.homemenu_name = homemenu_name;
  }

  public void setHomemenu_name_en(String homemenu_name_en) {
    this.homemenu_name_en = homemenu_name_en;
  }

  public void setHomemenu_path(String homemenu_path) {
    this.homemenu_path = homemenu_path;
  }

  public void setHomemenu_active(String homemenu_active) {
    this.homemenu_active = homemenu_active;
  }

  public void setHomemenu_notes(String homemenu_notes) {
    this.homemenu_notes = homemenu_notes;
  }



}
