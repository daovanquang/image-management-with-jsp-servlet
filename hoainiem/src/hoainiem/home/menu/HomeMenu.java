package hoainiem.home.menu;

public class HomeMenu {
  public HomeMenu() {

  }
  public static HomeMenuObject[] createHomeMenu(){
    HomeMenuObject[] list = {
        //ID - Name - Name_En - Path - Active - Notes
        new HomeMenuObject( (byte) 0, "Trang ch\u1EE7", "Homepage", "", "", ""),
        new HomeMenuObject( (byte) 1, "Gi\u1EDBi thi\u1EC7u", "About-us", "about-us", "", ""),
        new HomeMenuObject( (byte) 2, "D\u1ECBch v\u1EE5", "Services", "services", "", ""),
        new HomeMenuObject( (byte) 3, "\u1EA2nh", "Photos", "photos", "", ""),
        new HomeMenuObject( (byte) 4, "C\u1ED9ng \u0111\u1ED3ng", "Community", "community", "", ""),
        new HomeMenuObject( (byte) 5, "Li�n h\u1EC7", "Contact-us", "contact-us", "", "")
    };
    return list;
  }
}
