package hoainiem.home.topbar;

public class HomeTopbarObject {
  private byte topbar_id;
  private String topbar_name;
  private String topbar_name_en;
  private String topbar_path;
  private String topbar_notes;

  public HomeTopbarObject(
      byte topbar_id,
      String topbar_name,
      String topbar_name_en,
      String topbar_path,
      String topbar_notes){

    this.topbar_id = topbar_id;
    this.topbar_name = topbar_name;
    this.topbar_name_en = topbar_name_en;
    this.topbar_path = topbar_path;
    this.topbar_notes = topbar_notes;
  }


  public void setTopbar_id(byte topbar_id) {
    this.topbar_id = topbar_id;
  }

  public void setTopbar_name(String topbar_name) {
    this.topbar_name = topbar_name;
  }

  public void setTopbar_name_en(String topbar_name_en) {
    this.topbar_name_en = topbar_name_en;
  }

  public void setTopbar_path(String topbar_path) {
    this.topbar_path = topbar_path;
  }

  public void setTopbar_notes(String topbar_notes) {
    this.topbar_notes = topbar_notes;
  }

  public byte getTopbar_id() {
    return topbar_id;
  }

  public String getTopbar_name() {
    return topbar_name;
  }

  public String getTopbar_name_en() {
    return topbar_name_en;
  }

  public String getTopbar_path() {
    return topbar_path;
  }

  public String getTopbar_notes() {
    return topbar_notes;
  }

}
