package hoainiem.home.topbar;

public class HomeTopbar {
  public static HomeTopbarObject[] createTopbar(){
    HomeTopbarObject[] list = {
        //ID - Name - Name_En - Path - Left - Notes
        new HomeTopbarObject( (byte) 0, "C�u h\u1ECFi th\u01B0\u1EDDng g\u1EB7p", "FAQS", "faqs", ""),
        new HomeTopbarObject( (byte) 1, "Li�n h\u1EC7", "Contact-us", "contact-us", ""),
        new HomeTopbarObject( (byte) 2, "\u0110\u0103ng k�", "SignUp", "signup", ""),
        new HomeTopbarObject( (byte) 3, "\u0110\u0103ng nh\u1EADp", "Signin", "signin", ""),
        new HomeTopbarObject( (byte) 4, "Ti\u1EBFng Vi\u1EC7t", "vietnamese", "vi", ""),
        new HomeTopbarObject( (byte) 5, "English", "Ti\u1EBFng Anh", "en", "")
    };
    return list;
  }
}
