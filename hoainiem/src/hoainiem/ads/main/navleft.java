package hoainiem.ads.main;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

import hoainiem.objects.UserObject;

public class navleft
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html;charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();

    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");

    //String role = user.getUser_roles();

    out.print("<section class=\"section-navleft pull-left\">");
    out.print("<div class=\"toggle btn btn-default btn-sm mb-10\" onclick=\"swapHide('section-navleft__management','section-navleft__management--hide');\">");
    out.print("<span class=\"text-uppercase text-center\">Qu\u1EA3n l�</span>");
    out.print("<i class=\"pull-right fas fa-angle-down\"></i>");
    out.print("</div>");
    out.print("<ul id=\"section-navleft__management\" class=\"list-unstyle section-navleft__management section-navleft__management--hide\" class=\"list-unstyle\">");

    out.print("<li class = \"section-navleft__info list-unstyle\">");
    out.print("<div>");
    out.print("<a href = \"/adv/user/logout\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_logout-md pull-left mr-5\"></i>");
    out.print("<span>\u0110\u0103ng xu\u1EA5t</span></a>");
    out.print("</div>");
    out.print("</li>");

    out.print("<li class=\"heading\">");
    out.print("Qu\u1EA3n l� ng\u01B0\u1EDDi s\u1EED d\u1EE5ng");
    out.print("</li>");
    out.print("<li>");
    out.print("<a href=\"/adv/user/view\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_user-md pull-left mr-5\"></i>");
    out.print("<span>Danh s�ch</span>");
    out.print("</a>");
    out.print("</li>");

    out.print("<li  class=\"heading\">");
    out.print("Qu\u1EA3n l� nh�m");
    out.print("</li>");
    out.print("<li class=\"\">");
    out.print("<a href=\"/adv/group/view\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_members-md pull-left mr-5\"></i>");
    out.print("<span>Danh s�ch nh�m</span>");
    out.print("</a>");
    out.print("</li>");

    out.print("<li  class=\"heading\">");
    out.print("Qu\u1EA3n l� \u1EA3nh");
    out.print("</li>");
    out.print("<li>");
    out.print("<a href=\"/adv/article/view\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_article-md pull-left mr-5\"></i>");
    out.print("<span>Danh s�ch b�i vi\u1EBFt</span>");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a href=\"/adv/image/view\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_photos-md pull-left mr-5\"></i>");
    out.print("<span>Danh s�ch \u1EA3nh</span>");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a href=\"/adv/comment/view\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_comments-md pull-left mr-5\"></i>");
    out.print("<span>B�nh lu\u1EADn</span>");
    out.print("</a>");
    out.print("</li>");

    out.print("<li class=\"heading\">");
    out.print("Th\u1ED1ng k�");
    out.print("</li>");
    out.print("<li class=\"\">");
    out.print("<a href=\"\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_chart-md pull-left mr-5\"></i>");
    out.print("<span>T�i kho\u1EA3n</span>");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a href=\"\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_chart-md pull-left mr-5\"></i>");
    out.print("<span>\u1EA2nh</span>");
    out.print("</a>");
    out.print("</li>");
    out.print("<li  class=\"heading\">");
    out.print("C�u h\u1ECFi h\u1ED7 tr\u1EE3");
    out.print("</li>");
    out.print("<li class=\"\">");
    out.print("<a href=\"\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_new-md pull-left mr-5\"></i>");
    out.print("<span>C�u h\u1ECFi m\u1EDBi</span>");
    out.print("</a>");
    out.print("</li>");
    out.print("<li class=\"\">");
    out.print("<a href=\"\" class=\"row\">");
    out.print("<i class=\"ic ic-md ic_adv-md ic_faqs-md pull-left mr-5\"></i>");
    out.print("<span>Danh s�ch c�u h\u1ECFi</span>");
    out.print("</a>");
    out.print("</li>");
    out.print("</ul>");
    out.print("</section>");

  }

  //Clean up resources
  public void destroy() {
  }
}
