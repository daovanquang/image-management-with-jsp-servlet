package hoainiem.ads.main;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class scripts
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    out.print("<script language=\"javascript\" src=\"/adv/adjs/userlogin.js\"></script>");
    out.print("<script language=\"javascript\" src=\"/adv/adjs/user.js\"></script>");
    out.print("<script language=\"javascript\" src=\"/adv/adjs/management.js\"></script>");
    out.print("<script language=\"javascript\" src=\"/adv/adjs/group.js\"></script>");

  }

  //Clean up resources
  public void destroy() {
  }
}
