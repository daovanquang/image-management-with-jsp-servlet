package hoainiem.ads.main;

import hoainiem.objects.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class view
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);

    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");

    if (user != null) {
       if(user.getUser_permission() > 2) view(request, response);
       else{
         response.sendRedirect("/home/community");
       }
    }
    else {
      response.sendRedirect("/adv/user/login");
    }
  }

  private void view(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();

    RequestDispatcher h = request.getRequestDispatcher("/header");
    if(h!=null){
      h.include(request, response);
    }

    out.print("<div class=\"body\">");
    out.print("<div class=\"main-1200\">");
    RequestDispatcher nl = request.getRequestDispatcher("/navleft");
    if(nl!=null){
      nl.include(request, response);
    }
    out.print("</div>");
    out.print("</div>");

    RequestDispatcher f = request.getRequestDispatcher("/footer");
    if(f!=null){
      f.include(request, response);
    }

  }

  //Clean up resources
  public void destroy() {
  }
}
