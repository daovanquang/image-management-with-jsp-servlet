package hoainiem.ads.main;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class footer extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html;charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    out.print("<div class=\"footer\">");

    out.print("<section class=\"copyright text-center row\">");
    out.print("<div class=\"container main-1200\">");
    out.print("<p class=\"pull-left\">Copyright &copy; 2018 COMPANY. All Rights Reserved.</p>");
    out.print("<p class=\"pull-right\">Author <a href=\"#\">Quang Dao Van</a></p>");
    out.print("</div>");
    out.print("</section>");

    out.print("</div>");

    RequestDispatcher s = request.getRequestDispatcher("/scripts");
    if (s != null) {
      s.include(request, response);
    }

    out.print("</body>");
    out.print("</html>");
    out.close();
  }

  //Clean up resources
  public void destroy() {
  }
}
