package hoainiem.ads.main;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class mainbar
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html;charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();

    out.print("<section id=\"mainBar\" class=\"main-bar main-bar--hide\">");
    out.print("<div class=\"main-1200\">");
    out.print("<ul class=\"horizontal-nav list-unstyle pull-left\">");
    out.print("<li>");
    out.print("<a href=\"/home/index.jsp\">");
    out.print("<i class=\"fa fa-home\"></i>&nbspTrang ch\u1EE7");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a class=\"\" href=\"\">");
    out.print("Gi\u1EDBi thi\u1EC7u");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a class=\"\" href=\"\">");
    out.print("H\u1EE3p t�c");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a class=\"\" href=\"/home/index.jsp\">");
    out.print("\u1EA2nh");
    out.print("</a>");
    out.print("</li>");
    out.print("<li class=\"active\">");
    out.print("<a class=\"\" href=\"\">");
    out.print("C\u1ED9ng \u0110\u1ED3ng");
    out.print("</a>");
    out.print("</li>");
    out.print("<li>");
    out.print("<a class=\"\" href=\"\">");
    out.print("Li�n H\u1EC7");
    out.print("</a>");
    out.print("</li>");
    out.print("</ul>");
    out.print("</div>");
    out.print("</section>");

  }

  //Clean up resources
  public void destroy() {
  }
}
