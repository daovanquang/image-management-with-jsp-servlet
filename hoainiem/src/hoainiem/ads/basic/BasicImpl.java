package hoainiem.ads.basic;

import java.sql.*;

import hoainiem.*;

public class BasicImpl implements Basic {

    //Bo quan ly ket noi de Basic su dung
    private ConnectionPool cp;
    //Ket noi de Basic lam viec voi CSDL
    protected Connection con;
    //Ten doi tuong lam viec voi Basic
    private String objectName;

    public BasicImpl(ConnectionPool cp, String objectName) {
        //Xac dinh doi tuong lam viec voi Basic
        this.objectName = objectName;

        //Xac dinh bo quan ly ket noi
        if(cp==null){
            this.cp = new ConnectionPoolImpl();
        }else{
            this.cp = cp;
        }

        //Hoi xin ket noi
        try {
            this.con = this.cp.getConnection(this.objectName);

            //Kiem tra che do thuc thi cua ket noi
            if(this.con.getAutoCommit()){
                //Cham dut che do thuc thi tu dong
                this.con.setAutoCommit(false);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private synchronized boolean exe(PreparedStatement pre){
        if(pre!=null){
            //Thuc thi cap nhat
            try {
                int results = pre.executeUpdate();

                if(results==0){
                    this.con.rollback();
                    return false;
                }

                //Xac nhan thuc thi sau cung
                this.con.commit();
                return true;
            } catch (SQLException ex) {
                //In loi
                ex.printStackTrace();
                //Tro lai trang thai an toan
                try {
                    this.con.rollback();
                } catch (SQLException ex1) {
                    ex1.printStackTrace();

                }
            }finally{
                pre = null;
            }
        }
        return false;
    }

    public boolean add(PreparedStatement pre) {
        return this.exe(pre);
    }

    public boolean del(PreparedStatement pre) {
        return this.exe(pre);
    }

    public boolean edit(PreparedStatement pre) {
        return this.exe(pre);
    }

    /**
     * get
     *
     * @param sql String
     * @param value int
     * @return ResultSet
     * @todo Implement this jsoft.ads.basic.Basic method
     */
    public synchronized ResultSet get(String sql, int value) {

        //Bien dich cau lenh
        try {
            PreparedStatement pre = this.con.prepareStatement(sql);

            if(value>0){
                pre.setInt(1, value);
            }
            return pre.executeQuery();

        } catch (SQLException ex) {

            ex.printStackTrace();

            try {
                this.con.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
        }finally{
            sql = null;
        }


        return null;
    }

    public ResultSet get(String sql, String name, String pass) {

        try {
            PreparedStatement pre = this.con.prepareStatement(sql);

            pre.setString(1, name);
            pre.setString(2, pass);

            return pre.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.con.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
        }

        return null;
    }

    public ConnectionPool getCP() {
        return this.cp;
    }

    public ResultSet gets(String sql) {
        return this.get(sql, 0);
    }

    public ResultSet[] gets(String[] sqls) {
        ResultSet[] tmp = new ResultSet[sqls.length];
        for(int i=0; i<tmp.length; i++){
            tmp[i] = this.get(sqls[i],0);
        }
        return null;
    }

    public void releaseConnection() {
        try {
            this.cp.releaseConnection(this.con, this.objectName);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
