package hoainiem.ads.sql;

import hoainiem.objects.*;

public class MakeConditions {
  public MakeConditions() {
  }

  public static String createConditions(UserObject similar){
    String tmp="";
    if(similar!=null){
      byte permis = similar.getUser_permission();
      tmp += "WHERE (user_permission <= " + permis + ") ";

      //for search
      String key = similar.getUser_name();
      if(key!=null && !key.equalsIgnoreCase("")){
        tmp += " AND (user_name LIKE '%" + key + "%' OR user_fullname LIKE '%" + key + "%' ";
        tmp += "OR user_mail LIKE '%" + key + "%' OR user_phone LIKE '%" + key + "%' ";
        tmp += "OR user_notes LIKE '%" + key + "%' OR user_position LIKE '%" + key + "%') ";
      }

    }
    return tmp;
  }
}
