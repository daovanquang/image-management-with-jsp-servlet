package hoainiem.ads.user;

import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class UserDel
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    //Tim th�ng tin \u0111\u0103ng nh\u1EADp
    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");
    if (user != null) {
      int id = Utilities.getIntParam(request,"id");
      String name = request.getParameter("name");
      if (id > 0 && id != user.getUser_id()) {

        ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
        //Tao doi tuong thuc thi chuc nang
        UserControl uc = new UserControl(cp);
        //set ConnectionPool if it is null
        if (cp == null) {
          getServletContext().setAttribute("CPool", uc.getCP());
        }



        //Tao doi tuong luu tru thong tin xoa
        UserObject dUser = new UserObject();
        dUser.setUser_id(id);
        dUser.setUser_name(name);

        boolean result = uc.delUser(dUser);

        //tra ve ket noi
        uc.releaseConnection();

        //kiem tra ket qua xoa
        if (result) {
          response.sendRedirect("/adv/user/view");
        }
        else {
          response.sendRedirect("/adv/user/view?err=notok");
        }

      }
      else {
        response.sendRedirect("/adv/user/view");
      }
    }
    else {
      response.sendRedirect("/adv/user/login");
    }

  }

  //Clean up resources
  public void destroy() {
  }
}
