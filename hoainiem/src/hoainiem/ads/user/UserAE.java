package hoainiem.ads.user;

import hoainiem.*;
import hoainiem.library.*;
import hoainiem.objects.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public class UserAE
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html;charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    UserObject user = (UserObject) request.getSession().getAttribute(
        "UserLogined");
    if (user != null) {
      view(request, response, user);
    }
    else {
      response.sendRedirect("/adv/user/login");
    }

  }

  public void view(HttpServletRequest request, HttpServletResponse response,
                   UserObject user) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    String isEditText = "Th�m";
    boolean isEdit = false;
    String readonly = "";

    int index;
    try {
      index = Utilities.getIntParam(request, "id");
    }
    finally {
    }

    ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute(
        "CPool");
    UserControl uc = new UserControl(cp);
    //set ConnectionPool if it is null
    if (cp == null) {
      getServletContext().setAttribute("CPool", uc.getCP());
    }

    UserObject item = null;

    if (index > 0) {
      item = uc.getUserObject(index);
    }
    //String userpass="";
    String username = "", userapplydate = "", usermail = "", userphone = "",
        userfullname = "", usernotes = "", userbirthday = "", userposition = "",
        userjob = "",
        usercreateddate = "", useraddress = "", userimage = "";
    int userid = 0, usergroupid = 0, userparentid = 0;
    byte userpermission = 0, usergender = 0, useractions = 0;
    if (item != null) {
      userid = item.getUser_id();
      userparentid = item.getUser_parent_id();
      userimage = Utilities.getValue(item.getUser_image());
      username = Utilities.getValue(item.getUser_name());
      //userpass = Utilities.getValue(item.getUser_pass());
      usergroupid = Utilities.getIntValue(item.getUser_group_id());
      userapplydate = Utilities.getValue(item.getUser_applydate());
      usermail = Utilities.getValue(item.getUser_mail());
      userphone = Utilities.getValue(item.getUser_phone());
      userfullname = Utilities.getValue(item.getUser_fullname());
      usernotes = Utilities.getValue(item.getUser_notes());
      userbirthday = Utilities.getValue(item.getUser_birthday());
      userposition = Utilities.getValue(item.getUser_position());
      userjob = Utilities.getValue(item.getUser_job());
      userpermission = Utilities.getByteValue(item.getUser_permission());
      usergender = Utilities.getByteValue(item.getUser_gender());
      useractions = Utilities.getByteValue(item.getUser_actions());
      usercreateddate = Utilities.getValue(item.getUser_created_date());
      useraddress = Utilities.getValue(item.getUser_address());

      readonly = "readonly";
      isEditText = "C\u1EADp nh\u1EADt";
      isEdit = true;
    }

    RequestDispatcher h = request.getRequestDispatcher("/header");
    if (h != null) {
      h.include(request, response);
    }


    out.print("<div class=\"main-1200\">");
    RequestDispatcher nl = request.getRequestDispatcher("/navleft");
    if (nl != null) {
      nl.include(request, response);
    }

    out.print("<div class=\"section-right pull-left\">");

    out.print("<div class=\"splash-container main-500\">");
    out.print( "<div class=\"panel panel-border-color panel-border-color-primary mt-10\">");
    out.print("<div class=\"panel-body\">");
    out.print("<form class=\"row\" action=\"\" method=\"\">");
    out.print("<div class=\"form-group form-heading text-center\">");
    out.print("<h3>Th�ng tin t�i kho\u1EA3n</h3>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("T�n t�i kho\u1EA3n");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<input type=\"text\" class=\"form-control " + readonly + "\" name=\"txtUserName\" value=\"" + username + "\" placeholder=\"\" " + readonly + ">");
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("M\u1EADt kh\u1EA9u");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<input type=\"text\" class=\"form-control\" name=\"txtUserPass\"  value=\"\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Nh�m");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<input type=\"text\" class=\"form-control " + readonly +"\" name=\"txtUserGroupId\" value=\"" + usergroupid + "\" placeholder=\"\" " + readonly + ">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("H\u1ED9p th\u01B0");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print( "<input type=\"text\" class=\"form-control\" name=\"txtUserMail\"  value=\"" + usermail + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Quy\u1EC1n th\u1EF1c thi");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    //script generate permission selectbox
    out.print("<script>");
    out.print("var permissions = [\"--Ch\u1ECDn quy\u1EC1n--\",\"Th�nh vi�n\",\"Tr\u1EE3 l� nh�m\",\"Qu\u1EA3n l� nh�m\",\"Qu\u1EA3n tr\u1ECB vi�n\",\"Qu\u1EA3n tr\u1ECB c\u1EA5p cao\"];");
    out.print("generateSelectBox(\'slcUserPermis\',permissions," +
              userpermission +
              ",\'form-control\',\'refeshPermis(this.form)\');");
    out.print("</script>");
    //script
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Quy\u1EC1n");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    //script generate roles checkbox
    out.print("<script>");
    out.print(
        "var roles = [\"ng\u01B0\u1EDDi s\u1EED d\u1EE5ng\",\"\u1EA3nh\",\"nh�m\"];");
    out.print("generateCheckBoxs(\'chks\',roles,\'\');");
    out.print("</script>");
    //script
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("H�nh \u0111\u1ED9ng");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");

    //script generate actions selectbox
    out.print("<script>");
    out.print("var actions = [\"--Ch\u1EC9 xem--\",\"Th�m\",\"Th�m, S\u1EEDa\",\"Th�m, S\u1EEDa, X�a\",\"Th�m, S\u1EEDa, X�a, Th\u1ED1ng K�\"];");
    out.print("generateSelectBox(\'slcUserActions\',actions," + useractions +
              ",\'form-control\',\'\');");
    out.print("</script>");
    //script
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Ng�y �p d\u1EE5ng");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<input type=\"text\" class=\"form-control\" name=\"txtUserApplyDate\"  value=\"" + userapplydate + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group form-heading text-center\">");
    out.print("<h3>Th�ng tin chi ti\u1EBFt</h3>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("\u1EA2nh");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print( "<input  type=\"text\" class=\"form-control\"  name=\"txtUserImage\" value=\'" +userimage + "\'/>");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("T�n \u0111\u1EA7y \u0111\u1EE7");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print( "<input type=\"text\" class=\"form-control\" name=\"txtUserFullName\" value=\"" + userfullname + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("\u0110i\u1EC7n tho\u1EA1i");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print(
        "<input type=\"text\" class=\"form-control\" name=\"txtUserPhone\" value=\"" +
        userphone + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Ng�y sinh");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print(
        "<input type=\"text\" class=\"form-control\" name=\"txtUserBirthDay\" value=\"" +
        userbirthday + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Gi\u1EDBi t�nh");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    //script generate gender selectbox
    out.print("<script>");
    out.print("var genders = [\"--Ch\u1ECDn gi\u1EDBi t�nh--\",\"Nam\",\"N\u1EEF\",\"Straight\",\"Lesbian/Gay\",\"Bisexual\",\"Asexual\",\"Other\"];");
    out.print("generateSelectBox(\'slcUserGender\',genders," + usergender +
              ",\'form-control\',\'\');");
    out.print("</script>");
    //script
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("\u0110\u1ECBa ch\u1EC9");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print(
        "<input type=\"text\" class=\"form-control\" name=\"txtUserAddress\" value=\"" +
        useraddress + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Ghi ch�");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<textarea type=\"text\" class=\"form-control\" row=\"4\" name=\"txtUserNotes\" placeholder=\"\">" +
              usernotes + "</textarea>");
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group form-heading text-center\">");
    out.print("<h3>Th�ng tin kh�c</h3>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Ch\u1EE9c v\u1EE5");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print(
        "<input type=\"text\" class=\"form-control\" name=\"txtUserPosition\" value=\"" +
        userposition + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("C�ng vi\u1EC7c");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print(
        "<input type=\"text\" class=\"form-control\" name=\"txtUserJob\" value=\"" +
        userjob + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group text-center\">");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-success\" onclick=\"resetUserAE(this.form," + isEdit + ")\">Nh\u1EADp l\u1EA1i</button>");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-primary\" onclick=\"submitUserAE(this.form)\">" + isEditText + "</button>");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-warning\">H\u1EE7y</button>");
    out.print("<a href=\"/adv/user/view\">");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-danger\">Tho�t</button>");
    out.print("</a>");
    out.print("</div>");
    out.print("</div>");
    out.print("</form>");
    out.print("</div>");
    out.print("</div>");

    //Luu tru userroles tu checkbox
    out.print("<input type=\"hidden\" name=\"txtUserRoles\" value=\"\"/>");
    //Truyen id cho doPost de sua chua user neu co
    if (isEdit) {
      /*truyen id,parentcreatedid,imageid cho user khi sua*/
      out.print("<input type=\"hidden\" name=\"txtUserId\" value=\'" + userid +
                "\'/>");
      out.print("<input type=\"hidden\" name=\"txtUserParentId\" value=\'" +
                userparentid + "\'/>");
      out.print("<input type=\"hidden\" name=\"txtUserCreatedDate\" value=\'" +
                usercreateddate + "\'/>");
    }

    out.print("</form>");

    out.print("</div>");

    out.print("</div>");
    out.print("</div>");

    RequestDispatcher f = request.getRequestDispatcher("/footer");
    if (f != null) {
      f.include(request, response);
    }

    out.close();

  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();

    //lay ve id neu la sua se ton tai & >0
    int id = Utilities.getIntParam(request, "txtUserId");

    String name = request.getParameter("txtUserName");
    String pass = request.getParameter("txtUserPass");
    String mail = request.getParameter("txtUserMail");
    if (name != null && pass != null && mail != null) {

      //Cat bo khoang trong
      name = name.trim();
      pass = pass.trim();
      mail = mail.trim();

      if (!name.equalsIgnoreCase("") &&
          !pass.equalsIgnoreCase("") &&
          !mail.equalsIgnoreCase("")) {

        String fullname = request.getParameter("txtUserFullName");
        String phone = request.getParameter("txtUserPhone");
        int groupid = Utilities.getIntParam(request, "txtUserGroupId");
        String birthday = request.getParameter("txtUserBirthDay");
        String position = request.getParameter("txtUserPosition");
        String job = request.getParameter("txtUserJob");
        String notes = request.getParameter("txtUserNotes");
        String roles = request.getParameter("txtUserRoles");
        String address = request.getParameter("txtUserAddress");
        String applydate = request.getParameter("txtUserApplyDate");
        byte permis = Utilities.getByteParam(request, "slcUserPermis");
        byte actions = Utilities.getByteParam(request, "slcUserActions");
        byte gender = Utilities.getByteParam(request, "slcUserGender");
        String lastmodified = "";
        String createddate = "";
        int parentid = 0;
        String image = "";

        Date datetime = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(datetime);

        //ma hoa
        fullname = UtilitiesHelper.toENTITIES(fullname);
        job = UtilitiesHelper.toENTITIES(job);
        address = UtilitiesHelper.toENTITIES(address);
        notes = UtilitiesHelper.toENTITIES(notes);

        System.out.println(roles);

        if (id > 0) {
          //neu sua
          lastmodified = date.toString();
          parentid = Utilities.getIntParam(request, "txtUserParentId");
          image = request.getParameter("txtUserImage");
          createddate = request.getParameter("txtUserCreatedDate");
        }
        else {
          //neu them moi
          createddate = date.toString();

          //lay ve id cua parent khi tao moi
          UserObject parent = (UserObject) request.getSession().getAttribute(
              "UserLogined");
          parentid = parent.getUser_id();
        }
        //Tao doi tuong luu tru

        UserObject user = new UserObject();
        user.setUser_parent_id(user.getUser_id());
        user.setUser_name(name);
        user.setUser_pass(pass);
        user.setUser_permission(permis);
        user.setUser_roles(roles);
        user.setUser_actions(actions);
        user.setUser_mail(mail);
        user.setUser_phone(phone);
        user.setUser_gender(gender);
        user.setUser_group_id(groupid);
        user.setUser_fullname(fullname);
        user.setUser_birthday(birthday);
        user.setUser_position(position);
        user.setUser_job(job);
        user.setUser_address(address);
        user.setUser_notes(notes);
        user.setUser_last_modified(lastmodified);
        user.setUser_created_date(createddate);
        user.setUser_parent_id(parentid);
        user.setUser_image(image);
        user.setUser_applydate(applydate);
        //Tim bo quan ly ket noi
        ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute(
            "CPool");
        UserControl uc = new UserControl(cp);

        //set ConnectionPool if it is null
        if (cp == null) {
          getServletContext().setAttribute("CPool", uc.getCP());
        }

        //Thuc hien cap nhat
        boolean result;

        if (id > 0) {
          //sua chua
          user.setUser_id(id);
          result = uc.editUser(user);
        }
        else {
          //Them moi
          result = uc.addUser(user);
        }
        uc.releaseConnection();

        if (result) {
          response.sendRedirect("/adv/user/view");
        }
        else {
          response.sendRedirect("/adv/user/ae?err=notok");
        }

      }
      else {
        response.sendRedirect("/adv/user/ae?arr=value");
      }
    }
    else {
      response.sendRedirect("/adv/user/ae?arr=param");
    }

  }

  //Clean up resources
  public void destroy() {
  }
}
