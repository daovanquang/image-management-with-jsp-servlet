package hoainiem.ads.user;

import hoainiem.ShareControl;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface User extends ShareControl{
  boolean addUser(UserObject item);
  boolean editUser(UserObject item);
  boolean delUser(UserObject item);

  //Cac chuc nang truy van du lieu
  public ResultSet getUser(int id);
  public ResultSet getUser(String username, String userpass);
  public ResultSet getUsers(UserObject similar, int at, byte total);

}
