package hoainiem.ads.user;

import hoainiem.objects.*;
import hoainiem.library.*;

import java.util.*;

public class UserLibrary {
  public UserLibrary() {
  }

  public static String viewUsers(ArrayList<UserObject> list, UserObject user) {

    String tmp ="<table class=\"datatable table-border-gray\" style=\"width:100%\">";
    tmp += "<tr class=\"title text-center\">";
    tmp += "<th class=\"text-uppercase\">STT</th>";
    tmp += "<th>T�n t�i kho\u1EA3n</th>";
    tmp += "<th>T�n \u0111\u1EA7y \u0111\u1EE7</th>";
    tmp += "<th>GroupId</th>";
    tmp += "<th>Permission</th>";
    tmp += "<th>Quy\u1EC1n</th>";
    tmp += "<th>H�nh \u0111\u1ED9ng</th>";

    tmp += "<th>Ng�y �p d\u1EE5ng</th>";
    tmp +="<th class=\"datatable__control\" colspan=\"2\">H�nh \u0111\u1ED9ng</th>";
    tmp += "<th>Id</th>";
    tmp += "</tr>";

    byte i = 0;
    for (UserObject item : list) {

      tmp += (i % 2 == 0) ? "<tr>" : "<tr class=\"even\">";

      tmp += "<td class=\"text-center\">" + i + "</td>";
      tmp += "<td>" + item.getUser_name() + "</td>";
      tmp += "<td>" + item.getUser_fullname() + "</td>";
      tmp += "<td class=\"text-center\">" + item.getUser_group_id() + "</td>";
      tmp += "<td class=\"text-center\">" + item.getUser_permission() + "</td>";
      tmp += "<td>" + item.getUser_roles() + "</td>";
      tmp += "<td class=\"text-center\">" + item.getUser_actions() + "</td>";
      tmp += "<td>" + Utilities.getValue(item.getUser_applydate()) + "</td>";
      tmp += "<td class=\"control__edit text-center\">";
      tmp += "<a class = \"link-default\" href=\"/adv/user/ae?id=" +
          item.getUser_id() + "\">";
      tmp +=
          "<button type = \"button\" class=\"btn btn-xs btn-primary\">S\u1EEDa</button>";
      tmp += "</a>";
      tmp += "</td>";
      tmp += "<td class=\"control_del text-center\">";
      if (item.getUser_id() != user.getUser_id()) {
        tmp += "<a href=\"javascript:confirmDel('/adv/user/del?id=" + item.getUser_id() + "&name=" + item.getUser_name() + "');void(0);\">";
        tmp += "<button type=\"button\"  class=\"btn btn-xs btn-danger\">X�a</button>";
        tmp += "</a>";
      }
      else {
        tmp += "-----";
      }

      tmp += "</td>";

      tmp += "<td class=\"text-center\">" + item.getUser_id() + "</td>";

      tmp += "</tr>";
      i++;
    }

    tmp += "</table>";
    return tmp;
  }

}
