package hoainiem.ads.user;

import hoainiem.*;
import hoainiem.ads.user.*;
import hoainiem.objects.*;
import java.util.*;
import java.sql.SQLException;

public class UserControl {
  UserModel um;
  public UserControl(ConnectionPool cp) {
    um = new UserModel(cp);
  }
  public ConnectionPool getCP(){
       return this.um.getCP();
   }

   //ra lenh tra ve ket noi
   public void releaseConnection(){
       this.um.releaseConnection();
   }
  //chuyen dieu khien
  public boolean addUser(UserObject user) {
    return this.um.addUser(user);
  }

  public boolean editUser(UserObject user) {
    return this.um.editUser(user);
  }

  public boolean delUser(UserObject user) {
    return this.um.delUser(user);
  }
  public UserObject getUserObject(int id){
    return this.um.getUserObject(id);
  }
  public UserObject getUserObject(String username, String password){
    return this.um.getUserObject(username, password);
  }

  public String getUserOjects(UserObject similar, int page, byte total,UserObject user) throws SQLException {
    ArrayList<UserObject> items = null;
    try {
        items = this.um.getUserOjects(similar, page, total);
    } catch (SQLException ex) {
        ex.printStackTrace();
    }

    return UserLibrary.viewUsers(items, user);
  }

}
