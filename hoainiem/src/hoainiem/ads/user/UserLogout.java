package hoainiem.ads.user;

import hoainiem.objects.*;
import hoainiem.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;
public class UserLogout
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    HttpSession session=request.getSession();
    session.invalidate();
    response.sendRedirect("/adv/user/login");
  }

  //Clean up resources
  public void destroy() {
  }
}
