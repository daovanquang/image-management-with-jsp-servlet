package hoainiem.ads.user;

import hoainiem.*;
import hoainiem.library.*;
import hoainiem.objects.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import java.sql.SQLException;

public class UserView
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);

    PrintWriter out = response.getWriter();

    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");
    if (user != null) {
      view(request, response, user);
    }
    else {
      response.sendRedirect("/adv/user/login");
    }

  }

  private void view(HttpServletRequest request, HttpServletResponse response, UserObject user) throws ServletException, IOException {
    response.setContentType(CONTENT_TYPE);

    PrintWriter out = response.getWriter();

    ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
    UserControl uc = new UserControl(cp);
    //set ConnectionPool if it is null
    if (cp == null) {
         getServletContext().setAttribute("CPool", uc.getCP());
     }

    //Tao doi tuong luu tru bo loc
    UserObject similar = new UserObject();
    similar.setUser_permission(user.getUser_permission());
    String keyword = request.getParameter("key");
    if(keyword!=null && !keyword.equalsIgnoreCase("")){
      similar.setUser_name(keyword);
    }

    request.getSession().removeAttribute("key");
    //het doi tuong bo loc

    //loc

    String view="";
    try{
      view = uc.getUserOjects(similar, 1, (byte) 15,user);

    }catch(SQLException ex){
      ex.printStackTrace();
    }
    uc.releaseConnection();


    RequestDispatcher h = request.getRequestDispatcher("/header");
    if (h != null) {
      h.include(request, response);
    }

    out.print("<div class=\"main-1200\">");
    RequestDispatcher nl = request.getRequestDispatcher("/navleft");
    if (nl != null) {
      nl.include(request, response);
    }

    out.print("<div class=\"section-right pull-left\">");

    out.print("<section class=\"section-control row mb-10\">");
    out.print("<a class=\"link-default\" href=\"/adv/user/ae\">");
    out.print("<button class=\"btn btn-sm btn-success\">+Th�m m\u1EDBi</button>");
    out.print("</a>");
    out.print("</section>");

    out.print("<section class=\"section-view pull-left\">");
    out.print(view);
    out.print("</section>");

    out.print("</div>");
    out.print("</div>");

    RequestDispatcher f = request.getRequestDispatcher("/footer");
    if (f != null) {
      f.include(request, response);
    }

  }

  //Process the HTTP Post request
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    PrintWriter out = response.getWriter();

  }

  //Clean up resources
  public void destroy() {
  }
}
