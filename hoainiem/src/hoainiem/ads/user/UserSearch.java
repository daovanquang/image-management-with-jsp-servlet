package hoainiem.ads.user;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class UserSearch
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    String keyword = request.getParameter("txtKey");
    response.sendRedirect("/adv/user/view?key=" + keyword);

  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

  }

  //Clean up resources
  public void destroy() {
  }
}
