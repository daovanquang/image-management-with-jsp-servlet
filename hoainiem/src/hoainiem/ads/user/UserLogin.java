package hoainiem.ads.user;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class UserLogin
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html;charset=utf-8;";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);

    PrintWriter out = response.getWriter();
    /*Tra ve cau truc html*/
    out.print("<html>");
    out.print("<head>");
    out.print("<title>Login</title>");
    out.print("<link rel=\"icon\" type=\"image/x-icon\" href=\"/adv/imgs/favicon.png\">");
    out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"/adv/adcss/style.css\">");
    out.print("<script language=\"javascript\" src=\"/adv/adjs/component.js\"></script>");
    out.print("<script language=\"javascript\" src=\"/adv/adjs/userlogin.js\"></script>");
    out.print("</head>");
    out.print("<body>");
    out.print("<div class=\"splash-screen\">");
    out.print("<div class=\"splash-container main-300\">");
    out.print("<div class=\"panel mt-50\">");
    out.print("<div class=\"panel-heading text-center\">");
    out.print("<h3>Qu\u1EA3n tr\u1ECB vi�n \u0111\u0103ng nh\u1EADp</h3>");
    out.print("</div>");
    out.print("<div class=\"panel-body\">");
    out.print("<form action=\"\" method=\"\" name=\"frmLogin\" id=\"frmLogin\">");
    out.print("<div class=\"form-log\">");
    out.println("<label id=\"label-username-error\" class=\"label-bold label-danger hide\"></label>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<input type=\"text\" class=\"form-control\" name=\"txtUserName\" placeholder=\"T�n \u0111\u0103ng nh\u1EADp\" onkeypress=\"changeLabel(this,'label-username-error')\" >");
    out.print("</div>");
    out.print("<div class=\"form-log\">");
    out.println("<label id=\"label-userpass-error\" class=\"label-bold label-danger hide\"></label>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<input type=\"password\" class=\"form-control\" name=\"txtUserPass\" placeholder=\"M\u1EADt kh\u1EA9u\" onkeypress=\"changeLabel(this,'label-userpass-error')\" >");
    out.print("</div>");
    out.print("<div class=\"form-group row\">");
    out.print("<label class=\"pull-left\">");
    out.print("<input type=\"checkbox\" name=\"chkRemember\">");
    out.print("<span class=\"form-label\">Ghi nh\u1EDB</span>");
    out.print("</label>");
    out.print("<a href=\"/adv/user/forgotpass\" class=\"pull-right\"><label class=\"label-bold label-hover\">Qu�n m\u1EADt kh\u1EA9u</label></a>");
    out.print("</div>");
    out.print("<div class=\"form-group form-submit mt-15\">");
    out.print("<button type=\"button\" class=\"btn btn-xl btn-primary\" onclick=\"login(this.form)\">\u0110\u0103ng nh\u1EADp</button>");
    out.print("</div>");
    out.print("</form>");
    out.print("</div>");
    out.print("<div class=\"splash-footer text-center main-350\">");
    out.print("<div class=\"row mt-30\">");
    out.print("B\u1EA1n ch\u01B0a c� t�i kho\u1EA3n, li�n h\u1EC7&nbsp;<label class=\"label-bold label-hover\">0xxxxxxxxx</label>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</body>");
    out.print("</html>");

    /*end*/
    out.close();

  }

  //Process the HTTP Post request
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String username = request.getParameter("txtUserName");
    String password = request.getParameter("txtUserPass");

    if(username!=null&&password!=null){

      username = username.trim();
      password = password.trim();

      if(!username.equalsIgnoreCase("")&&!password.equalsIgnoreCase("")){

        //Tham chieu ngu canh ung dung
        ServletContext application = getServletConfig().getServletContext();
        ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");

        //Tao doi tuong thuc thi chuc nang
        UserControl uc = new UserControl(cp);

        //set ConnectionPool if it is null
        if (cp == null) {
          getServletContext().setAttribute("CPool", uc.getCP());
        }

        password = Utilities.md5(password);
        UserObject user = uc.getUserObject(username,password);
        uc.releaseConnection();

        if(user!=null){
          //tham chieu phien
          HttpSession session = request.getSession(true);
          session.setAttribute("UserLogined",user);
          if(user.getUser_permission()>=3){
            response.sendRedirect("/adv/view");
          }else{
            response.sendRedirect("/home/community/");
          }
        }else{
          response.sendRedirect("/adv/user/login?err=notok");
        }

      }else{
          response.sendRedirect("/adv/user/login?err=value");
      }


    }else{
          response.sendRedirect("/adv/user/login?err=param");
      }

  }

}
