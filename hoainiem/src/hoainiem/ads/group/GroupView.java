package hoainiem.ads.group;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import hoainiem.*;
import hoainiem.library.*;
import hoainiem.objects.*;


public class GroupView
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);

    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");

    if (user != null) {
        view(request, response);
    }
    else {
      response.sendRedirect("/adv/user/login");
    }
  }

  private void view(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    PrintWriter out = response.getWriter();
    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");

    ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
    GroupControl gc  = new GroupControl(cp);
    if (cp == null) {
        getServletContext().setAttribute("CPool", gc.getCP());
    }

    gc.releaseConnection();

    String view = gc.getGroupObjects(null, 0, (byte) 15, user);

    RequestDispatcher h = request.getRequestDispatcher("/header");
    if (h != null) {
      h.include(request, response);
    }

    out.print("<div class=\"main-1200\">");
    RequestDispatcher nl = request.getRequestDispatcher("/navleft");
    if (nl != null) {
      nl.include(request, response);
    }

    out.print("<div class=\"section-right pull-left\">");

    out.print("<section class=\"section-control row\">");
    out.print("<a class=\"link-default\" href=\"/adv/group/ae\">");
    out.print("<button class=\"btn btn-sm btn-success\">+Th�m m\u1EDBi</button>");
    out.print("</a>");
    out.print("</section>");

    out.print("<section class=\"row\">");
    out.print(view);
    out.print("</section>");

    out.print("</div>");
    out.print("</div>");

    RequestDispatcher f = request.getRequestDispatcher("/footer");
    if (f != null) {
      f.include(request, response);
    }

  }

  //Clean up resources
  public void destroy() {
  }
}
