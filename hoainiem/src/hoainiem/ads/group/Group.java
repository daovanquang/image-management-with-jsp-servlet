package hoainiem.ads.group;

import hoainiem.objects.*;
import hoainiem.*;
import java.sql.*;

public abstract interface Group extends ShareControl{
  boolean addGroup(GroupObject item);
  boolean editGroup(GroupObject item);
  boolean delGroup(GroupObject item);

  ResultSet getGroup(int id);
  ResultSet getGroups(GroupObject similar, int at, byte total);
}
