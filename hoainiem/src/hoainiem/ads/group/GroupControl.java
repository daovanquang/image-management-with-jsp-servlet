package hoainiem.ads.group;

import hoainiem.objects.*;
import hoainiem.*;

import java.util.*;

public class GroupControl {
  private GroupModel gm;
  public GroupControl(ConnectionPool cp) {
    gm = new GroupModel(cp);
  }

  public ConnectionPool getCP() {
    return this.gm.getCP();
  }

//ra lenh tra ve ket noi
  public void releaseConnection() {
    this.gm.releaseConnection();
  }

  public boolean addGroup(GroupObject item) {
    return this.gm.addGroup(item);
  }

  public boolean editGroup(GroupObject item) {
    return this.gm.editGroup(item);
  }

  public boolean delGroup(GroupObject item) {
    return this.gm.delGroup(item);
  }
  public GroupObject getGroupObject (int id){
    return this.gm.getGroupObject(id);
  }
  public String getGroupObjects (GroupObject similar, int at, byte total, UserObject user){
    ArrayList<GroupObject> items = null;
    items = this.gm.getGroupObjects(similar, at, total);
    return GroupLibrary.viewGroups(items,user);
  }
}
