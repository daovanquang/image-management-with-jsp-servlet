package hoainiem.ads.group;
import hoainiem.*;
import hoainiem.objects.*;

import java.util.*;
import java.sql.*;

public class GroupModel {
  private Group g;
  public GroupModel(ConnectionPool cp) {
     g = new GroupImpl(cp);
  }
  public ConnectionPool getCP() {
    return this.g.getCP();
  }

  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.g.releaseConnection();
  }

  public boolean addGroup(GroupObject item){
    return this.g.addGroup(item);
  }
  public boolean editGroup(GroupObject item){
     return this.g.editGroup(item);
  }
  public boolean delGroup(GroupObject item){
     return this.g.delGroup(item);
  }


  public GroupObject getGroupObject (int id){
    ResultSet rs = this.g.getGroup(id);
    GroupObject item = null;
    if(rs!=null){
      item = new GroupObject();
      try {
        if (rs.next()) {
          item.setGroup_id(rs.getInt("group_id"));
          item.setGroup_name(rs.getString("group_name"));
          item.setGroup_author_name(rs.getString("group_author_name"));
          item.setGroup_created_date(rs.getString("group_created_date"));
          item.setGroup_enable(rs.getBoolean("group_enable"));
          item.setGroup_image(rs.getString("group_image"));
          item.setGroup_permission(rs.getByte("group_permission"));
          item.setGroup_url_link(rs.getString("group_url_link"));
          item.setGroup_notes(rs.getString("group_notes"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return item;
  }
  public ArrayList<GroupObject> getGroupObjects (GroupObject similar,int at , byte total ){
    ResultSet rs = this.g.getGroups(similar, at, total);
    ArrayList<GroupObject> items = new ArrayList<GroupObject>();
    GroupObject item = null;
    if(rs!=null){

      try {
        while (rs.next()) {
          item = new GroupObject();
          item.setGroup_id(rs.getInt("group_id"));
          item.setGroup_name(rs.getString("group_name"));
          item.setGroup_author_name(rs.getString("group_author_name"));
          item.setGroup_created_date(rs.getString("group_created_date"));
          item.setGroup_enable(rs.getBoolean("group_enable"));
          item.setGroup_image(rs.getString("group_image"));
          item.setGroup_permission(rs.getByte("group_permission"));
          item.setGroup_url_link(rs.getString("group_url_link"));
          item.setGroup_notes(rs.getString("group_notes"));

          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return items;

  }


}
