package hoainiem.ads.group;

import hoainiem.*;
import hoainiem.library.*;
import hoainiem.objects.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public class GroupAE
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html;charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    UserObject user = (UserObject) request.getSession().getAttribute(
        "UserLogined");
    if (user != null) {
      view(request, response, user);
    }
    else {
      response.sendRedirect("/adv/user/login");
    }

  }

  public void view(HttpServletRequest request, HttpServletResponse response,
                   UserObject user) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    String isEditText = "Th�m";
    boolean isEdit = false;
    String readonly = "";

    int id;
    try {
      id = Utilities.getIntParam(request, "id");
    }
    finally {
    }

    ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute( "CPool");
    GroupControl gc = new GroupControl(cp);
    //set ConnectionPool if it is null
    if (cp == null) {
      getServletContext().setAttribute("CPool", gc.getCP());
    }

    GroupObject item = null;

    if (id > 0) {
      item = gc.getGroupObject(id);
    }

    String name = "", createddate = "", authorname = "",
        image = "", urllink = "", notes = "", lastmodified = "";
    byte member = 0;
    byte permission = 0;
    byte enable = 0;

    if (item != null) {
      id = item.getGroup_id();
      name = Utilities.getValue(item.getGroup_name());
      authorname = Utilities.getValue(item.getGroup_author_name());
      image = Utilities.getValue(item.getGroup_image());
      urllink = Utilities.getValue(item.getGroup_url_link());
      notes = Utilities.getValue(item.getGroup_notes());
      permission = Utilities.getByteValue(item.getGroup_permission());
      createddate = Utilities.getValue(item.getGroup_created_date());
      member = 0;
      byte membervalue = Utilities.getByteValue(item.getGroup_member_sum());
      if (membervalue > 0)
        member = membervalue;

      lastmodified = Utilities.getValue(item.getGroup_last_modified());
      if (item.isGroup_enable())
        enable = 1;
      else
        enable = 0;

      readonly = "readonly";
      isEditText = "C\u1EADp nh\u1EADt";
      isEdit = true;
    }
    else {
      //neu la them moi
      authorname = user.getUser_name();
    }

    RequestDispatcher h = request.getRequestDispatcher("/header");
    if (h != null) {
      h.include(request, response);
    }

    out.print("<div class=\"main-1200\">");
    RequestDispatcher nl = request.getRequestDispatcher("/navleft");
    if (nl != null) {
      nl.include(request, response);
    }

    out.print("<section class=\"section-right pull-left\">");

    out.print("<form action=\"\" method=\"\" class=\"form\" name=\"frmGroupAE\">");
    out.print("<div class=\"splash-container main-500\">");
    out.print("<div class=\"panel panel-border-color panel-border-color-primary mt-10\">");
    out.print("<div class=\"panel-body\">");
    out.print("<div class=\"form-group form-heading text-center\">");
    out.print("<h3>Th�ng tin nh�m</h3>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("T�n nh�m");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print( "<input type=\"text\" class=\"form-control\" name=\"txtGroupName\"  value=\"" + name + "\" placeholder=\"\">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Image");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print( "<input type=\"text\" class=\"form-control \" name=\"txtGroupImage\" value=\"" +image + "\" placeholder=\"\" >");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("URL link");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<input type=\"text\" class=\"form-control " + readonly + "\" name=\"txtGroupUrlLink\" value=\"" + urllink +"\" placeholder=\"\" " + readonly + ">");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Vai tr� th\u1EF1c thi");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    //script generate permission selectbox
    out.print("<script>");
    out.print("var permissions = [\"--Ch\u1ECDn quy\u1EC1n--\",\"Th�nh vi�n\",\"Tr\u1EE3 l� nh�m\",\"Qu\u1EA3n l� nh�m\",\"Qu\u1EA3n tr\u1ECB vi�n\",\"Qu\u1EA3n tr\u1ECB c\u1EA5p cao\"];");
    out.print("generateSelectBox(\'slcGroupPermis\',permissions," + permission + ",\'form-control\',\'\');");
    out.print("</script>");
    //script
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("\u0110�ng/m\u1EDF");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    //script generate roles checkbox
    out.print("<script>");
    out.print("var enables = [\"\u0110�ng\",\"M\u1EDF\"];");
    out.print("generateSelectBox(\'slcGroupEnable\',enables," + enable +
              ",\'form-control\',\'\');");
    out.print("</script>");
    //script
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\"form-group\">");
    out.print("<div class=\"lc col-3\">");
    out.print("Ghi ch�");
    out.print("</div>");
    out.print("<div class=\"rc col-9\">");
    out.print("<textarea type=\"text\" class=\"form-control\" row=\"4\" name=\"txtGroupNotes\" placeholder=\"\">" + notes + "</textarea>");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\"form-group text-center\">");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-success\" onclick=\"resetGroupAE(this.form," +isEdit + ")\">Nh\u1EADp l\u1EA1i</button>");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-primary\" onclick=\"submitGroupAE(this.form)\">" + isEditText + "</button>");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-warning\">H\u1EE7y</button>");
    out.print("<a href=\"/adv/group/view\">");
    out.print("<button type=\"button\" class=\"btn btn-link btn-sm btn-danger\">Tho�t</button>");
    out.print("</a>");
    out.print("</div>");
    out.print("</div>");

    //Truyen id cho doPost de sua chua group neu co
    out.print("<input type=\"hidden\" name=\"txtGroupAuthorName\" value=\"" + authorname + "\">");
    if (isEdit) {
      /*truyen id,authorcreated cho groupae khi sua*/
      out.print("<input type=\"hidden\" name=\"txtGroupId\" value=\'" + id + "\'/>");
      out.print("<input type=\"hidden\" name=\"txtGroupMember\" value=\'" + member + "\'/>");
      out.print("<input type=\"hidden\" name=\"txtGroupCreatedDate\" value=\'" + createddate + "\'/>");
      out.print("<input type=\"hidden\" name=\"txtGroupLastModified\" value=\'" +lastmodified + "\'/>");
    }
    out.print("</form>");

    out.print("</section>");
    out.print("</div>");

    RequestDispatcher f = request.getRequestDispatcher("/footer");
    if (f != null) {
      f.include(request, response);
    }

    out.close();

  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();

    //lay ve id neu la sua se ton tai & >0
    int id = Utilities.getIntParam(request, "txtGroupId");
    String name = request.getParameter("txtGroupName");

    if (name != null) {

      //Cat bo khoang trong
      name = name.trim();

      if (!name.equalsIgnoreCase("")) {

        String notes = request.getParameter("txtGroupNotes");
        byte permis = Utilities.getByteParam(request, "slcGroupPermis");
        byte enable = Utilities.getByteParam(request, "slcGroupEnable");
        String createddate = "";
        String authorname = request.getParameter("txtGroupAuthorName"); ;
        byte groupmember = 0;
        byte membervalue = Utilities.getByteParam(request, "txtGroupMember");
        if (membervalue > 0)
          groupmember = membervalue;
        String image = request.getParameter("txtGroupImage");
        String lastmodified = "";
        Date datetime = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(datetime);
        String urllink = request.getParameter("txtGroupUrlLink"); ;
        if (id > 0) {
          //neu sua
          lastmodified = date.toString();
          createddate = request.getParameter("txtGroupCreatedDate");

        }
        else {
          //neu them moi
          createddate = date.toString();

          //lay ve id cua parent khi tao moi
          UserObject parent = (UserObject) request.getSession().getAttribute(
              "UserLogined");
          authorname = parent.getUser_name();
        }
        //Tao doi tuong luu tru

        GroupObject nGroup = new GroupObject();
        nGroup.setGroup_name(name);
        nGroup.setGroup_author_name(authorname);
        nGroup.setGroup_created_date(createddate);
        nGroup.setGroup_last_modified(lastmodified);
        nGroup.setGroup_image(image);
        if (enable == 1)
          nGroup.setGroup_enable(true);
        else
          nGroup.setGroup_enable(false);
        nGroup.setGroup_member_sum(groupmember);
        nGroup.setGroup_notes(notes);
        nGroup.setGroup_permission(permis);
        nGroup.setGroup_url_link(urllink);

        //Tim bo quan ly ket noi
        ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
        GroupControl gc = new GroupControl(cp);

        //set ConnectionPool if it is null
        if (cp == null) {
          getServletContext().setAttribute("CPool", gc.getCP());
        }

        //Thgc hien cap nhat
        boolean result;
        if (id > 0) {
          //sua chua
          nGroup.setGroup_id(id);
          result = gc.editGroup(nGroup);
        }
        else {
          //Them moi
          result = gc.addGroup(nGroup);
        }
        gc.releaseConnection();

        if (result) {
          response.sendRedirect("/adv/group/view");
        }
        else {
          response.sendRedirect("/adv/group/ae?err=notok");
        }

      }
      else {
        response.sendRedirect("/adv/group/ae?arr=value");
      }
    }
    else {
      response.sendRedirect("/adv/group/ae?arr=param");
    }

  }

  //Clean up resources
  public void destroy() {
  }
}
