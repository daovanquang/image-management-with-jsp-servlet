package hoainiem.ads.group;

import hoainiem.objects.*;
import java.util.ArrayList;

public class GroupLibrary {
  public GroupLibrary() {
  }
  public static String viewGroups(ArrayList<GroupObject> list, UserObject user) {
    String tmp = "<div class=\"image-list image-list--datatable\">";
    for (GroupObject item:list) {
      if(item.getGroup_permission()<=user.getUser_permission()){
        tmp += "<div class=\"image-card card pull-left image-card--bg-skin\">";
        tmp += "<div class=\"card__img-cover\">";
        tmp += "<img class=\"img-responsive\" src=\"" + item.getGroup_image() + "\" alt=\"" + item.getGroup_image() + "\">";
        tmp += "</div>";
        tmp += "<div class=\"card__padding\">";
        tmp += "<div class=\"card__details text-center\">";
        tmp += "<h3 class=\"details__title\">";
        tmp += "<a class=\"link-default link-simple\" href=\"/adv/user/view/?id=" + item.getGroup_id() + "\" >" + item.getGroup_name() + "</a>";
        tmp += "</h3>";
        tmp += "<div class=\"details__description\">";
        tmp += "<p>Ng\u01B0\u1EDDi t\u1EA1o: " + item.getGroup_author_name() +"</p>";
        tmp += "<p>S\u1ED1 th�nh vi�n: " + item.getGroup_member_sum() + "</p>";
        tmp += "<p>" + item.getGroup_created_date() + "</p>";
        tmp += "</div>";
        tmp += "</div>";
        tmp += "<div class=\"card__control mt-10 text-center\">";
        tmp += "<a class=\"btn btn-primary btn-xs\" href=\"/adv/group/ae?id=" + item.getGroup_id() + "\">S\u1EEDa</a>";
        tmp += "<a class=\"btn btn-xs btn-danger\" href=\"javascript:confirmDel('/adv/group/del?id=" + item.getGroup_id() + "');void(0);\">X�a</a>";
        tmp += "</div>";
        tmp += "</div>";
        tmp += "</div>";
      }
    }

    tmp += "</div>";

    return tmp;
  }

}
