package hoainiem.ads.group;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import hoainiem.objects.*;
import hoainiem.*;
import hoainiem.library.*;

public class GroupDel
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    //Tim th�ng tin \u0111\u0103ng nh\u1EADp
    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");
    if (user != null) {
      int id = Utilities.getIntParam(request, "id");

      if (id > 0 && id != user.getUser_id()) {

        ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute(
            "CPool");
        //Tao doi tuong thuc thi chuc nang
        GroupControl ic = new GroupControl(cp);
        if (cp == null) {
          getServletContext().setAttribute("CPool", ic.getCP());
        }

        //Tao doi tuong luu tru thong tin xoa
        GroupObject dGroup = new GroupObject();
        dGroup.setGroup_id(id);
        boolean result = ic.delGroup(dGroup);

        //tra ve ket noi
        ic.releaseConnection();

        //kiem tra ket qua xoa
        if (result) {
          response.sendRedirect("/adv/group/view");
        }
        else {
          response.sendRedirect("/adv/group/view?err=notok");
        }

      }
      else {
        response.sendRedirect("/adv/group/view?err=param");
      }
    }
    else {
      response.sendRedirect("/adv/user/login");
    }

    out.close();
  }

  //Clean up resources
  public void destroy() {
  }
}
