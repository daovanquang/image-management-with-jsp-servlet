package hoainiem.ads.group;

import java.sql.*;

import hoainiem.*;
import hoainiem.ads.basic.*;
import hoainiem.objects.*;

public class GroupImpl extends BasicImpl implements Group {
  public GroupImpl(ConnectionPool cp) {
    super(cp, "Group");
  }

  /**
   * addGroup
   *
   * @return boolean
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public boolean addGroup(GroupObject item) {
    String sql = "INSERT INTO tblgroup(";
    sql += "group_name, ";
    sql += "group_created_date, group_permission, ";
    sql += "group_author_name, group_notes , ";
    sql += "group_image ,group_enable , ";
    sql += "group_url_link ,group_member_sum , ";
    sql += "group_last_modified )";
    sql += "VALUES(?,?,?,?,?,?,?,?,?,?)";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getGroup_name());
      pre.setString(2, item.getGroup_created_date());
      pre.setByte(3, item.getGroup_permission());
      pre.setString(4, item.getGroup_author_name());
      pre.setString(5, item.getGroup_notes());
      pre.setString(6, item.getGroup_image());
      pre.setBoolean(7, item.isGroup_enable());
      pre.setString(8, item.getGroup_url_link());
      pre.setByte(9, item.getGroup_member_sum());
      pre.setString(10, item.getGroup_last_modified());
      //thuc thi
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;

  }

  /**
   * delGroup
   *
   * @return boolean
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public boolean delGroup(GroupObject item) {
    String sql = "DELETE FROM tblgroup WHERE group_id = ? ";

   //Doi tuong thuc thi
   try {
     PreparedStatement pre = this.con.prepareStatement(sql);
     pre.setInt(1, item.getGroup_id());

     //thuc thi
     return this.del(pre);
   }
   catch (SQLException ex) {
     ex.printStackTrace();
   }
   return false;

  }

  /**
   * editGroup
   *
   * @return boolean
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public boolean editGroup(GroupObject item) {
    String sql = "UPDATE tblgroup SET ";
    sql += "group_name = ?, ";
    sql += "group_created_date = ?, group_permission = ?, ";
    sql += "group_author_name =? , group_notes =?, ";
    sql += "group_image = ?, group_enable = ?, ";
    sql += "group_url_link =?, group_member_sum =?, ";
    sql += "group_last_modified = ? ";
    sql += "WHERE group_id = ?";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getGroup_name());
      pre.setString(2, item.getGroup_created_date());
      pre.setByte(3, item.getGroup_permission());
      pre.setString(4, item.getGroup_author_name());
      pre.setString(5, item.getGroup_notes());
      pre.setString(6, item.getGroup_image());
      pre.setBoolean(7, item.isGroup_enable());
      pre.setString(8, item.getGroup_url_link());
      pre.setByte(9, item.getGroup_member_sum());
      pre.setString(10, item.getGroup_last_modified());
      pre.setInt(11, item.getGroup_id());
      //thuc thi
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;

  }

  /**
   * getGroup
   *
   * @param id int
   * @return ResultSet
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public ResultSet getGroup(int id) {
    String sql ="SELECT * FROM tblgroup WHERE group_id = ?";
    return this.get(sql,id);
  }

  /**
   * getGroups
   *
   * @param similar GroupObject
   * @param at int
   * @param total byte
   * @return ResultSet[]
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public ResultSet getGroups(GroupObject similar, int at, byte total) {
    String sqls ="SELECT * FROM tblgroup ";
    sqls += "";
    sqls += "ORDER BY group_id ASC ";
    sqls += "LIMIT "+at+","+total;
    return this.gets(sqls);
  }

}
