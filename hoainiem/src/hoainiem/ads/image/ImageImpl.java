package hoainiem.ads.image;

import java.sql.*;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.ads.basic.*;

public class ImageImpl
    extends BasicImpl implements Image {
  public ImageImpl(ConnectionPool cp) {
    super(cp, "Image");
  }

  public boolean addImage(ImageObject item) {
    String sql = "INSERT INTO tblimage(";
    sql += "image_title, image_created_date, ";
    sql += "image_author_name, image_title_en, ";
    sql += "image_tag, image_tag_en, ";
    sql += "image_visited, image_language, ";
    sql += "image_author_permission, image_notes, ";
    sql += "image_article_id, image_enable,  ";
    sql += "image_last_modified, image_source ";
    sql += ") ";
    sql += "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?.?)";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getImage_title());
      pre.setString(2, item.getImage_created_date());
      pre.setString(3, item.getImage_author_name());
      pre.setString(4, item.getImage_title_en());
      pre.setString(5, item.getImage_tag());
      pre.setString(6, item.getImage_tag_en());
      pre.setInt(7, item.getImage_visited());
      pre.setString(8, item.getImage_language());
      pre.setInt(9, item.getImage_author_permission());
      pre.setString(10, item.getImage_notes());
      pre.setInt(11,item.getImage_article_id());
      pre.setBoolean(12,item.isImage_enable());
      pre.setString(13,item.getImage_last_modified());
      pre.setString(14, item.getImage_source());
      //thuc thi
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean delImage(ImageObject item) {
    String sql = "DELETE FROM tblimage WHERE image_id = ? ";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setInt(1, item.getImage_id());

      //thuc thi
      return this.del(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean editImage(ImageObject item) {
    String sql = "UPDATE tblimage SET ";
    sql += "image_title = ?, image_created_date = ?, ";
    sql += "image_author_name = ?, image_title_en = ?, ";
    sql += "image_tag = ?, image_tag_en = ?, ";
    sql += "image_visited = ?, image_language = ?, ";
    sql += "image_author_permission = ?, image_notes = ?, ";
    sql += "image_article_id = ?, image_enable = ?,  ";
    sql += "image_last_modified = ?, image_source = ? ";
    sql += "WHERE image_id = ?";


    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getImage_title());
       pre.setString(2, item.getImage_created_date());
       pre.setString(3, item.getImage_author_name());
       pre.setString(4, item.getImage_title_en());
       pre.setString(5, item.getImage_tag());
       pre.setString(6, item.getImage_tag_en());
       pre.setInt(7, item.getImage_visited());
       pre.setString(8, item.getImage_language());
       pre.setInt(9, item.getImage_author_permission());
       pre.setString(10, item.getImage_notes());
       pre.setInt(11,item.getImage_article_id());
       pre.setBoolean(12,item.isImage_enable());
       pre.setString(13,item.getImage_last_modified());
       pre.setString(14, item.getImage_source());
       pre.setInt(15, item.getImage_article_id());
      //thuc thi
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public ResultSet getImage(int id) {
    String sql = "SELECT * FROM tblimage WHERE image_id = ?";
    return this.get(sql, id);
  }

  public ResultSet getImages(ImageObject similar, int at, byte total) {
    String sql = "SELECT * FROM tblimage ";
    sql += "";
    sql += "";
    sql += "";
    sql += "ORDER BY image_id ASC ";
    sql += "LIMIT " + at + "," + total;

    return this.gets(sql);
  }

}
