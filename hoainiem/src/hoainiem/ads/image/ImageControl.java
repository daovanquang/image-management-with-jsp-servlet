package hoainiem.ads.image;

import hoainiem.objects.*;
import hoainiem.*;
import java.util.*;
import java.sql.*;

public class ImageControl {
  private ImageModel im;
  public ImageControl(ConnectionPool cp) {
    im = new ImageModel(cp);
  }

  public ConnectionPool getCP() {
    return this.im.getCP();
  }

  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.im.releaseConnection();
  }

//chuyen dieu khien
  public boolean addUser(ImageObject item) {
    return this.im.addImage(item);
  }

  public boolean editImage(ImageObject item) {
    return this.im.editImage(item);
  }

  public boolean delImage(ImageObject item) {
    return this.im.delImage(item);
  }

  public ImageObject getImageObject(int id) {
    return this.im.getImageObject(id);
  }

  public String getImageOjects(ImageObject similar, int page, byte total) throws
      SQLException {
    ArrayList<ImageObject> items =  this.im.getImageObjects(similar,page,total);
    return ImageLibrary.viewImages(items);
  }

}
