package hoainiem.ads.image;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface Image extends ShareControl{
   boolean addImage(ImageObject item);
   boolean editImage(ImageObject item);
   boolean delImage(ImageObject item);

   public ResultSet getImage(int id);
   public ResultSet getImages(ImageObject similar, int at, byte total);
}
