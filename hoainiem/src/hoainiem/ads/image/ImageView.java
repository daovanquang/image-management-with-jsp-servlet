package hoainiem.ads.image;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import java.sql.SQLException;
import hoainiem.objects.*;
import hoainiem.*;

public class ImageView
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

  //Initialize global variables
  public void init() throws ServletException {
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    UserObject user = (UserObject) request.getSession().getAttribute("UserLogined");

    if (user != null) {
      view(request, response);
    }
    else {
      response.sendRedirect("/adv/user/login");
    }
  }

  private void view(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();

    ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
    ImageControl ic  = new ImageControl(cp);
    if (cp == null) {
        getServletContext().setAttribute("CPool", ic.getCP());
    }


    String view="";
    try {
      view = ic.getImageOjects(null, 0, (byte) 15);
    }
    catch (SQLException ex) {
    }
    ic.releaseConnection();

    RequestDispatcher h = request.getRequestDispatcher("/header");
    if (h != null) {
      h.include(request, response);
    }

    out.print("<div class=\"main-1200\">");
    RequestDispatcher nl = request.getRequestDispatcher("/navleft");
    if (nl != null) {
      nl.include(request, response);
    }

    out.print("<div class=\"section-right pull-left\">");
    out.print(view);
    out.print("</div>");

    out.print("</div>");

    RequestDispatcher f = request.getRequestDispatcher("/footer");
   if (f != null) {
     f.include(request, response);
   }

  }

  //Clean up resources
  public void destroy() {
  }
}
