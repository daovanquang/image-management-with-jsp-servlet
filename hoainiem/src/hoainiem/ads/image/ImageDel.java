package hoainiem.ads.image;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ImageDel
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    //Tim th�ng tin \u0111\u0103ng nh\u1EADp
    UserObject user = (UserObject) request.getSession().getAttribute(
        "UserLogined");
    if (user != null) {
      int id = Utilities.getIntParam(request, "id");
      String title = request.getParameter("title");

      if (id > 0 && id != user.getUser_id()) {

        ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
        //Tao doi tuong thuc thi chuc nang
        ImageControl ic = new ImageControl(cp);
        if (cp == null) {
          getServletContext().setAttribute("CPool", ic.getCP());
        }

        //Tao doi tuong luu tru thong tin xoa
        ImageObject dImage = new ImageObject();
        dImage.setImage_id(id);
        dImage.setImage_title(title);
        boolean result = ic.delImage(dImage);

        //tra ve ket noi
        ic.releaseConnection();

        //kiem tra ket qua xoa
        if (result) {
          response.sendRedirect("/adv/image/view");
        }
        else {
          response.sendRedirect("/adv/image/view?err=notok");
        }

      }
      else {
        response.sendRedirect("/adv/image/view");
      }
    }
    else {
      response.sendRedirect("/adv/user/login");
    }

  }

  //Clean up resources
  public void destroy() {
  }
}
