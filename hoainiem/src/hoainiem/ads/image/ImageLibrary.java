package hoainiem.ads.image;

import hoainiem.objects.*;
import java.util.*;

public class ImageLibrary {
  public ImageLibrary() {
  }

  public static String viewImages(ArrayList<ImageObject> list) {
    String tmp = "<div class=\"image-list image-list--datatable\">";
    for (ImageObject item:list) {
      tmp += "<div class=\"image-card card  image-card--bg-skin\">";
      tmp += "<div class=\"card__img-cover\">";
      tmp += "<img class=\"img-responsive\" src=\""+item.getImage_source()+"\" alt=\""+item.getImage_title()+"\">";
      tmp += "</div>";
      tmp += "<div class=\"card__padding\">";
      tmp += "<div class=\"card__details text-center\">";
      tmp += "<h3 class=\"details__title\">";
      tmp += item.getImage_title();
      tmp += "</h3>";
      tmp += "<div class=\"details__description\">";
      tmp += "<p>Ng\u01B0\u1EDDi t\u1EA1o:"+item.getImage_author_name()+"</p>";
      tmp += "<p>"+item.getImage_created_date()+"</p>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "<div class=\"card__control mt-10 text-center\">";
      tmp += "<a class=\"btn btn-primary btn-xs\" href=\"/adv/image/ae?id="+item.getImage_id()+"&name="+item.getImage_title()+"\">S\u1EEDa</a>";
      tmp += "<a class=\"btn btn-xs btn-danger\" href=\"javascript:confirmDel('/adv/image/del?id="+ item.getImage_id() + "&title="+ item.getImage_title() +"');void(0);\">X�a</a>";

      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";
    }

    tmp += "</div>";

    return tmp;
  }

}
