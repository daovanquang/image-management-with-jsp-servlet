package hoainiem.lang.gui;

public interface FormLang {
  String []USERNAME = {"T�n t�i kho\u1EA3n","Username"};
  String []USERMAIL = {"H\u1ED9p th\u01B0","Mail address"};
  String []USERPHONE = {"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i","Phone number"};
  String []PASSWORD = {"M\u1EADt kh\u1EA9u","Password"};
  String []REPEAT_PASSWORD = {"Nh\u1EADp l\u1EA1i m\u1EADt kh\u1EA9u","Repeat password"};
  String []REMEMBER_ME = {"Ghi nh\u1EDB","Remember me"};
  String []HAVE_AN_ACCOUNT = {"\u0110� c� t�i kho\u1EA3n?","Have an account?"};
  String []HERE = {"t\u1EA1i \u0111�y","here"};
  String []OR = {"ho\u1EB7c","or"};
  String []NEW_MEMBER = {"Th�nh vi�n m\u1EDBi?","New member?"};

  String []OLD_PASS = {"M\u1EADt kh\u1EA9u c\u0169","Old password"};
  String []NEW_PASS = {"M\u1EADt kh\u1EA9u m\u1EDBi","New password"};
  String []REPEAT_NEW_PASS = {"Nh\u1EADp l\u1EA1i m\u1EADt kh\u1EA9u m\u1EDBi","Repeat new password"};
  String []CONTINUE = {"Ti\u1EBFp t\u1EE5c","Coninue"};
  String []AGREE_PRIVACY_POLICY = {"T�i \u0111\u1ED3ng � v\u1EDBi","By clicking \"<b>Sign Up</b>\", I agree to"};

  String []CONFIRM = {"X�c nh\u1EADn","Confirm"};
  String []VERIFY = {"X�c minh","Verify"};
  String []VERIFY_ACCOUNT = {"X�c minh t�i kho\u1EA3n","Account verify"};
  String []VERIFY_CODE = {"M� x�c minh c\u1EE7a b\u1EA1n l�:","Your code to confirm is:"};

  String []A_CONFIRM_CODE_SEND_TO = {"M\u1ED9t m� x�c minh \u0111� \u0111\u01B0\u1EE3c g\u1EEDi \u0111\u1EBFn ","A confirm code send to "};
  String []PLS_CHECK_MAIL = {"Vui l�ng ki\u1EC3m tra h\u1ED9p th\u01B0!", "Pls! check your mail."};

  //FIELD USER INFO
  String []ADDRESS_DETAIL = {"\u0110\u1ECBa ch\u1EC9 chi ti\u1EBFt", "Address detail"};
  String []USERAVATAR = {"\u1EA2nh \u0111\u1EA1i di\u1EC7n", "Avatar"};
  String []SELECT_AN_AVATAR = {"Ch\u1ECDn \u1EA3nh \u0111\u1EA1i di\u1EC7n", "Select an image"};
  String []USERFULLNAME = {"T�n \u0111\u1EA7y \u0111\u1EE7", "Fullname"};
  String[] USERBIRTHDAY = {"Ng�y sinh", "Birthday"};
  String[] USERGENDER = {"Gi\u1EDBi t�nh", "Gender"};
  String[] USERADDRESS = {"\u0110\u1ECBa ch\u1EC9", "Address"};

  String []NATION_LANG = {"Qu\u1ED1c gia","Nation"};
  String []PROVICECITY_LANG = {"T\u1EC9nh/TP","Provice/City"};

  String []CREATE_PASS = {"T\u1EA1o m\u1EADt kh\u1EA9u", "Create password"};
  String []CONFIRM_CODE = {"M� x�c nh\u1EADn","Confirm code"};
  String []SEND_CONFIRMCODE_AGAIN = {"G\u1EEDi l\u1EA1i m� x�c minh", "Pls! Send confirm code again!"};
  String []WILL_VERIFY_AFTER = {"T�i s\u1EBD x�c minh sau. \u0110\u0103ng nh\u1EADp !", "I will verify after. Login now!"};
}

