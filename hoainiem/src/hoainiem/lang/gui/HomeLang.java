package hoainiem.lang.gui;

public interface HomeLang extends MenuLang, KeywordLang, FormLang{
  String []LANG = {"English", "Ti\u1EBFng Vi\u1EC7t"};
  String []FLAG = {"en", "vn"};

  //ACCOUNT
  String []CONNECT = {"K\u1EBFt n\u1ED1i", "Connect"};
  String []ACCOUNT = {"T�i kho\u1EA3n", "Account"};
  String []CREATE_AN_ACCOUNT = {"T\u1EA1o t�i kho\u1EA3n", "Create an account"};
  String []LOGIN = {"\u0110\u0103ng nh\u1EADp", "Login"};
  String []LOGOUT = {"\u0110\u0103ng xu\u1EA5t", "Logout"};
  String []SIGNUP = {"\u0110\u0103ng k�", "Sign Up"};
  String []FORGOTPASS = {"Qu�n m\u1EADt kh\u1EA9u", "Forgot my password"};
  String []CHANGEPASS = {"\u0110\u1ED5i m\u1EADt kh\u1EA9u", "Change my password"};

  String []GROUP = {"Nh�m","Group"};
  String []HELP_CENTER= {"Trung t�m tr\u1EE3 gi�p","Help Center"};
  String []FAQS= {"C�u h\u1ECFi th\u01B0\u1EDDng g\u1EB7p","FAQS"};
  String []CONTACT_US = {"Li�n h\u1EC7", "Contact us"};
  String []TERMS_OF_SERVICE = {"\u0110i\u1EC1u kho\u1EA3n d\u1ECBch v\u1EE5","Terms of service"};
  String []PRIVACY_POLICY = {"Ch�nh s�ch b\u1EA3o m\u1EADt","Privacy policy"};
  String []FOLLOW_US = {"Theo d�i","Follow us"};
  //company
  String []COMPANY_NAME = {"Tri K\u1EF7", "Best Friends"};
  String []COMPANY_INTRO = {"<h3>Tri k\u1EF7 - N\u0103m th�ng v\u1ED9i v�</h3><p>Tr\u01B0\u1EDBc t\u1EA5t c\u1EA3 nh\u1EEFng h\u1ED3i \u1EE9c v\u1EE5n v\u1EE1, b\u1EA1n c� th\u1EC3 nh\u1EDB l\u1EA1i \u0111\u01B0\u1EE3c g�?</p><p>Nh\u1EEFng b�ng r�m um t�m t\u1ECFa n\u1EAFng tr�n s�n tr\u01B0\u1EDDng</p><p>Hay l� \u0111�m c\u1ECF non th\u01A1m ng�t d\u01B0\u1EDBi �nh n\u1EAFng m\u1EB7t tr\u1EDDi?</p>","Best Friends"};
  String []COPYRIGHT = {"Copyright � 2018 | All rights reserved","Copyright � 2018 | All rights reserved"};
  String []AUTHOR_TITLE = {"Develop by ","Develop by "};
  String []AUTHOR_NAME = {"\u0110�o V\u0103n Quang","Quang Dao Van"};
  //INPUT
  String []SEARCH_INPUT = {"Nh\u1EADp t\u1EEB kh�a...", "Enter keyword..."};
  String []COMMENT_INPUT = {"Nh\u1EADp b�nh lu\u1EADn...", "Enter comment..."};
  //Views
  String []MESSAGE = {"Th�ng b�o", "Message"};
  String []MESSAGE_YES = {"OK", "OK"};
  String []MESSAGE_CANCEL = {"CANCEL", "CANCEL"};
  String []SEEMORE = {"Xem th�m", "See more"};
  String []SEEALL = {"Xem t\u1EA5t c\u1EA3", "See all"};
  String []NEWEST_PHOTOS = {"\u1EA2nh m\u1EDBi nh\u1EA5t","Newest photos"};
  String []IMPRESSIVE_PHOTOS = {"\u1EA2nh n\u1ED5i b\u1EADt","Impressive photos"};
  String []RECENT_PHOTOS = {"\u1EA2nh g\u1EA7n \u0111�y","Recent photos"};
  String []ALL_PHOTO = {"T\u1EA5t c\u1EA3 \u1EA3nh","All photo"};
  String []MORE_THAN = {"H\u01A1n", "More than"};
  String []PHOTO = {"\u1EA2nh", "Photo"};
  String []PHOTOS = {"\u1EA2nh", "Photos"};
  String []ADD_PHOTOS = {"Th�m \u1EA3nh", "Add photos"};
  String []ADD_FOLDER_PHOTO = {"Th\u01B0 m\u1EE5c \u1EA3nh", "Select folder photo"};
  String []VIDEO = {"Video", "Video"};
  String []ADD_VIDEO = {"Th�m video", "Add video"};
  String []WHAT_S_NEW = {"C� g� m\u1EDBi ?","What's new ?"};
  String []SERVICES = {"D\u1ECBch v\u1EE5","Services"};
  String []CATEGORIES = {"Danh m\u1EE5c","Categories"};
  String []DOTS = {"&bull;&nbsp;&bull;&nbsp;&bull;","&bull;&nbsp;&bull;&nbsp;&bull;"};
  String []NEWS = {"Tin t\u1EE9c","News"};
  String []MEMBERS = {"Th�nh vi�n","Members"};
  String []COMMENTS = {"B�nh lu\u1EADn","Comments"};
  String []MANAGEMENT = {"Qu\u1EA3n l�","Management"};
  String []TAG = {"Th\u1EBB", "tags"};
  String []ADD_TAG = {"Th�m th\u1EBB", "Add tags"};
  String []TAG_CONTENT = {"Ph�n t�ch b\u1EB1ng d\u1EA5u ph\u1EA9y","Tags separated by commas"};
  String []TAG_TITLE = {"G\u1EAFn th\u1EBB", "Tags"};
  String []POST = {"\u0110\u0103ng tin", "Post"};
  String []SHARE_INTERESTING = {"Chia s\u1EBB nh\u1EEFng \u0111i\u1EC1u th� v\u1ECB !","Share interesting things !"};
  String []OTHER_FILE = {"T\u1EC7p kh�c","Other files"};
  String []PROPOSED_GROUPS = {"Nh�m \u0111\u01B0\u1EE3c \u0111\u1EC1 xu\u1EA5t","Proposed groups"};
  //Actions
  String []ADD = {"Th�m", "Add"};
  String []EDIT = {"S\u1EEDa", "Edit"};
  String []DEL = {"X�a", "Del"};
  String []COMMENT = {"B�nh lu\u1EADn", "Comment"};
  String []CARE = {"Quan t�m", "Care"};
  //SORT
  String []SORT = {"S\u1EAFp x\u1EBFp","Sort by"};
  String []NEWEST = {"M\u1EDBi nh\u1EA5t","Newest"};
  String []OLDEST = {"C\u0169 nh\u1EA5t","Oldest"};
  String []ASC_VISITED = {"L\u01B0\u1EE3t xem t\u0103ng d\u1EA7n","ASC Visited"};
  String []DESC_VISITED = {"L\u01B0\u1EE3t xem gi\u1EA3m d\u1EA7n","DESC Visited"};

  //404
  String []PAGE_NOT_FOUND = {"R\u1EA5t ti\u1EBFc! \u0110� x\u1EA3y ra l\u1ED7i!","Oops! Something went wrong!"};
  String []RETURN_HOME = {"Quay v\u1EC1 trang ch\u1EE7", "Return to Home"};
}
