package hoainiem.lang.adv;

public interface AdvGUI {
  //company
  String []COMPANY_NAME = {"Tri K\u1EF7", "Best Friends"};
  String []COMPANY_INTRO = {"<h3>Tri k\u1EF7 - N\u0103m th�ng v\u1ED9i v�</h3><p>Tr\u01B0\u1EDBc t\u1EA5t c\u1EA3 nh\u1EEFng h\u1ED3i \u1EE9c v\u1EE5n v\u1EE1, b\u1EA1n c� th\u1EC3 nh\u1EDB l\u1EA1i \u0111\u01B0\u1EE3c g�?</p><p>Nh\u1EEFng b�ng r�m um t�m t\u1ECFa n\u1EAFng tr�n s�n tr\u01B0\u1EDDng</p><p>Hay l� \u0111�m c\u1ECF non th\u01A1m ng�t d\u01B0\u1EDBi �nh n\u1EAFng m\u1EB7t tr\u1EDDi?</p>","Best Friends"};
  String []COPYRIGHT = {"Copyright � 2018 | All rights reserved","Copyright � 2018 | All rights reserved"};
  String []AUTHOR_TITLE = {"Develop by ","Develop by "};
  String []AUTHOR_NAME = {"\u0110�o V\u0103n Quang","Quang Dao Van"};
}
