package hoainiem;

public abstract interface ShareControl {
  ConnectionPool getCP();
  void releaseConnection();
}
