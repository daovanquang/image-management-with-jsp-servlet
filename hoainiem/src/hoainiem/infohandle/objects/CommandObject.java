package hoainiem.infohandle.objects;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CommandObject {
  private int command_id;
  private String command_struct_format;
  private String command_name;
  private String command_notes;
  private String command_section;
  private int command_crawl_id;
  public CommandObject() {
  }

  public void setCommand_id(int command_id) {
    this.command_id = command_id;
  }

  public void setCommand_struct_format(String command_struct_format) {
    this.command_struct_format = command_struct_format;
  }

  public void setCommand_name(String command_name) {
    this.command_name = command_name;
  }

  public void setCommand_notes(String command_notes) {
    this.command_notes = command_notes;
  }

  public void setCommand_section(String command_section) {
    this.command_section = command_section;
  }

  public void setCommand_crawl_id(int command_crawl_id) {
    this.command_crawl_id = command_crawl_id;
  }

  public int getCommand_id() {
    return command_id;
  }

  public String getCommand_struct_format() {
    return command_struct_format;
  }

  public String getCommand_name() {
    return command_name;
  }

  public String getCommand_notes() {
    return command_notes;
  }

  public String getCommand_section() {
    return command_section;
  }

  public int getCommand_crawl_id() {
    return command_crawl_id;
  }
}
