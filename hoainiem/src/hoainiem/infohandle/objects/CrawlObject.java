package hoainiem.infohandle.objects;



/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CrawlObject {
  private int crawl_id;
  private String crawl_url;
  private String crawl_created_date;
  private String crawl_author_created;
  private String crawl_notes;
  private boolean crawl_deleted;
  public CrawlObject() {

  }

  public void setCrawl_id(int crawl_id) {

    this.crawl_id = crawl_id;
  }

  public void setCrawl_url(String crawl_url) {
    this.crawl_url = crawl_url;
  }

  public void setCrawl_created_date(String crawl_created_date) {
    this.crawl_created_date = crawl_created_date;
  }

  public void setCrawl_author_created(String crawl_author_created) {
    this.crawl_author_created = crawl_author_created;
  }

  public void setCrawl_notes(String crawl_notes) {
    this.crawl_notes = crawl_notes;
  }

  public void setCrawl_deleted(boolean crawl_deleted) {
    this.crawl_deleted = crawl_deleted;
  }

  public int getCrawl_id() {

    return crawl_id;
  }

  public String getCrawl_url() {
    return crawl_url;
  }

  public String getCrawl_created_date() {
    return crawl_created_date;
  }

  public String getCrawl_author_created() {
    return crawl_author_created;
  }

  public String getCrawl_notes() {
    return crawl_notes;
  }

  public boolean isCrawl_deleted() {
    return crawl_deleted;
  }
}
