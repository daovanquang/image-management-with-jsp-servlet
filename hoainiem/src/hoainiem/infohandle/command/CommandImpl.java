package hoainiem.infohandle.command;

import java.sql.*;

import hoainiem.*;
import hoainiem.infohandle.objects.*;
import hoainiem.gui.basic.*;
import hoainiem.infohandle.sql.*;

public class CommandImpl extends BasicImpl implements Command {
  public CommandImpl(ConnectionPool cp) {
    super(cp,"CommandInfoHandle");
  }

  public boolean addCommand(CommandObject item) {
    String sql = "INSERT INTO tblcommand";
    sql += "command_name, command_notes, ";
    sql += "command_section, command_struct_format, ";
    sql += "command_crawl_id ";
    sql += ") ";
    sql += "VALUES(?,?,?,?)";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getCommand_name());
      pre.setString(2, item.getCommand_notes());
      pre.setString(3, item.getCommand_section());
      pre.setString(4, item.getCommand_struct_format());
      pre.setInt(5, item.getCommand_crawl_id());
      //thuc thi
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean delCommand(CommandObject similar) {
    String sql = "DELETE FROM tblcommand WHERE ";
    sql += InfoHanleConditions.conditionsDelCommand(similar);
    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);

      int id = (similar.getCommand_crawl_id()> 0) ? similar.getCommand_crawl_id(): similar.getCommand_id();
      pre.setInt(1, id);

      //thuc thi
      return this.del(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean editCommand(CommandObject item) {
    String sql = "UPDATE tblcommand SET ";
    sql += "command_name = ?, command_notes = ?, ";
    sql += "command_section = ?, command_struct_format = ?, ";
    sql+= "command_crawl_id = ? ";
    sql += "WHERE command_id = ?";


    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getCommand_name());
      pre.setString(2, item.getCommand_notes());
      pre.setString(3, item.getCommand_section());
      pre.setString(4, item.getCommand_struct_format());
      pre.setInt(5, item.getCommand_crawl_id());
      pre.setInt(6, item.getCommand_id());
      //thuc thi
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public ResultSet getCommand(int id) {
    String sql = "SELECT * FROM tblcommand WHERE command_id = ?";
    return this.get(sql, id);
  }

  public ResultSet getCommands(CommandObject similar, int at, byte total, boolean isDESC) {
    String sql = "SELECT * FROM tblcommand ";
    sql += InfoHanleConditions.getCommandConditions(similar);
    sql += "ORDER BY command_id ";
    sql += (isDESC) ? " DESC ":" ASC ";
    sql += "LIMIT " + at + "," + total;

    return this.gets(sql);
  }
}
