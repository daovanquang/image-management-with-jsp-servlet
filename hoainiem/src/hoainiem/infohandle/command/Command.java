package hoainiem.infohandle.command;

import hoainiem.*;
import hoainiem.infohandle.objects.*;
import java.sql.*;

public abstract interface Command extends ShareControl{
  boolean addCommand(CommandObject item);
  boolean editCommand(CommandObject item);
  boolean delCommand(CommandObject item);

  public ResultSet getCommand(int id);
  public ResultSet getCommands(CommandObject similar, int at, byte total, boolean isDESC);
}
