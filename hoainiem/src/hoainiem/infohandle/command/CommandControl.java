package hoainiem.infohandle.command;

import hoainiem.infohandle.objects.CommandObject;
import hoainiem.ConnectionPool;
import java.util.ArrayList;

public class CommandControl {
  private CommandModel cm;
  public CommandControl(ConnectionPool cp) {
    cm = new CommandModel(cp);
  }

  public ConnectionPool getConnection() {
    return this.cm.getConnection();
  }

  public void releaseConection() {
    this.cm = null;
  }

  public boolean addCommand(CommandObject item) {
    return this.cm.addCommand(item);
  }

  public boolean delCommand(CommandObject item) {
    return this.cm.delCommand(item);
  }

  public boolean editCommand(CommandObject item) {
    return this.cm.editCommand(item);
  }

  public CommandObject getCommand(int id) {
    return this.cm.getCommand(id);
  }

  public ArrayList<CommandObject> getCommands(CommandObject similar, int at,  byte total, boolean isDESC) {
    return this.cm.getCommands(similar, at, total, isDESC);
  }

}
