package hoainiem.infohandle.command;

import hoainiem.*;
import hoainiem.infohandle.objects.CommandObject;
import hoainiem.infohandle.sql.*;

import java.sql.*;
import java.util.*;

public class CommandModel {
  private Command c;
  public CommandModel(ConnectionPool cp) {
    c = new CommandImpl(cp);
  }

  public ConnectionPool getConnection(){
    return this.c.getCP();
  }

  public void releaseConection(){
    this.c = null;
  }

  public boolean addCommand(CommandObject item) {
    return this.c.addCommand(item);
  }

  public boolean delCommand(CommandObject item) {
    return this.c.delCommand(item);
  }

  public boolean editCommand(CommandObject item) {
    return this.c.editCommand(item);
  }

  public CommandObject getCommand(int id) {
    ResultSet rs = this.c.getCommand(id);
    CommandObject item = new CommandObject();

    if(rs!=null){
      try {
        if (rs.next()) {
          item.setCommand_id(rs.getInt("command_id"));
          item.setCommand_crawl_id(rs.getInt("command_crawl_id"));
          item.setCommand_name(rs.getString("command_name"));
          item.setCommand_section(rs.getString("command_section"));
          item.setCommand_struct_format(rs.getString("command_struct_format"));
          item.setCommand_notes(rs.getString("command_notes"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return item;
  }

  public ArrayList<CommandObject> getCommands(CommandObject similar, int at, byte total, boolean isDESC) {
    ResultSet rs = this.c.getCommands(similar, at, total, isDESC);
    ArrayList<CommandObject> items = new ArrayList<CommandObject>();


    if(rs!=null){
      try {
        while (rs.next()) {
          CommandObject item = new CommandObject();
          item.setCommand_id(rs.getInt("command_id"));
          item.setCommand_crawl_id(rs.getInt("command_crawl_id"));
          item.setCommand_name(rs.getString("command_name"));
          item.setCommand_section(rs.getString("command_section"));
          item.setCommand_struct_format(rs.getString("command_struct_format"));
          item.setCommand_notes(rs.getString("command_notes"));

          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return items;
  }


}
