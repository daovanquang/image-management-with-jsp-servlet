package hoainiem.infohandle.crawl;

import hoainiem.*;
import hoainiem.infohandle.objects.*;
import java.sql.*;

public abstract interface Crawl extends ShareControl{
  boolean addCrawl(CrawlObject item);
  boolean editCrawl(CrawlObject item);
  boolean delCrawl(int id);

  public ResultSet getCrawl(int id);
  public ResultSet getCrawls(CrawlObject similar, int at, byte total, boolean isDESC);
}
