package hoainiem.infohandle.crawl;

import hoainiem.*;
import hoainiem.infohandle.objects.*;
import java.sql.*;
import java.util.ArrayList;

public class CrawlModel {

  private Crawl c;
  public CrawlModel(ConnectionPool cp) {
    c = new CrawlImpl(cp);
  }

  public ConnectionPool getConnection() {
    return this.c.getCP();
  }

  public void releaseConection() {
    this.c = null;
  }

  public boolean addCrawl(CrawlObject item) {
    return this.c.addCrawl(item);
  }

  public boolean delCrawl(int id) {
    return this.c.delCrawl(id);
  }

  public boolean editCrawl(CrawlObject item) {
    return this.c.editCrawl(item);
  }

  public CrawlObject getCrawl(int id) {
    ResultSet rs = this.c.getCrawl(id);
    CrawlObject item = new CrawlObject();

    if (rs != null) {
      try {
        if (rs.next()) {
          item.setCrawl_id(rs.getInt("crawl_id"));
          item.setCrawl_url(rs.getString("crawl_url"));
          item.setCrawl_created_date(rs.getString("crawl_created_date"));
          item.setCrawl_author_created(rs.getString("crawl_author_created"));
          item.setCrawl_deleted(rs.getBoolean("crawl_deleted"));
          item.setCrawl_notes(rs.getString("crawl_notes"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return item;
  }

  public ArrayList<CrawlObject> getCrawls(CrawlObject similar, int at, byte total, boolean isDESC) {
    ResultSet rs = this.c.getCrawls(similar, at, total, isDESC);
    ArrayList<CrawlObject> items = new ArrayList<CrawlObject> ();

    if (rs != null) {
      try {
        while (rs.next()) {

          CrawlObject item = new CrawlObject();
          item.setCrawl_id(rs.getInt("crawl_id"));
          item.setCrawl_url(rs.getString("crawl_url"));
          item.setCrawl_created_date(rs.getString("crawl_created_date"));
          item.setCrawl_author_created(rs.getString("crawl_author_created"));
          item.setCrawl_deleted(rs.getBoolean("crawl_deleted"));
          item.setCrawl_notes(rs.getString("crawl_notes"));

          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return items;
  }

}
