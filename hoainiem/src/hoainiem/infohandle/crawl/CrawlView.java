package hoainiem.infohandle.crawl;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class CrawlView
    extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html";

  //Initialize global variables
  public void init() throws ServletException {
  }

  //Process the HTTP Get request
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    out.print("<html>");
    out.print("<head>");
    out.print("<title>	");
    out.print("</title>");
    out.print("<link rel=\'stylesheet\' type=\'text/css\' href=\'/adv/adcss/crawl.css\'>");
    out.print("<link rel=\'stylesheet\' type=\'text/css\' href=\'/adv/adjs/crawl.js\'>");
    out.print("</head>");
    out.print("<body class=\'editor\'>");
    out.print("<form method=\'POST\' action=\'/adv/crawl/view\'>");
    out.print("<section class=\'main-header\'>");
    out.print("<div class=\'pen-title pull-left\'>");
    out.print("<div class=\'pen-text\'>");
    out.print("<h1>Crawler tool</h1>");
    out.print("<p>by Quang Dao Van</p>");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\'secondary-nav horizontal-nav pull-right\'>");
    out.print("<ul class=\'controls\'>");
    out.print("<li>");
    out.print("<button class=\'controls-default\' onclick='getSourcePage(this.form)'>");
    out.print("Get source page");
    out.print("</button>");
    out.print("<button class=\'controls-default\' onclick='submitCrawlAE(this.form)'>");
    out.print("Run");
    out.print("</button>");
    out.print("</li>");
    out.print("<li>");
    out.print("<button class=\'controls-default\'>");
    out.print("Reset");
    out.print("</button>");
    out.print("</li>");
    out.print("<li>");
    out.print("<button class=\'controls-default\'>");
    out.print("Exit");
    out.print("</button>");
    out.print("</li>");
    out.print("</ul>");
    out.print("</div>");
    out.print("</section>");
    out.print("<section class=\'main-max\'>");
    out.print("<div class=\'top-codeboxes row\'>");
    out.print("<div class=\'codebox codebox-col codebox-html pull-left\' style=\'width: 33.33%; height: 60%;\'>");
    out.print( "<div class=\'codebox-resize resize resize-horizontal resize-fixed pull-left\'>");
    out.print("</div>");
    out.print("<div class=\'codebox-content pull-left\'>");
    out.print("<div class=\'code-wrap\'>");
    out.print("<div class=\'code-title row pull-left\'>");
    out.print("<h3>HTML Or Link</h3>");
    out.print("</div>");
    out.print("<div class=\'code row\'>");
    out.print("<div class=\'code-line pull-left\'>");
    out.print("<span>1</span>");
    out.print("<span>2</span>");
    out.print("<span>3</span>");
    out.print("</div>");
    out.print("<div class=\'code-content pull-left\'>");
    out.print("<textarea name=\"txtHTMLOrLink\" autocorrect=\'off\' autocapitalize=\'off\' spellcheck=\'false\' style=\'width: 100%; height: 100%; outline: none; border: 0; background: transparent;\' tabindex=\'0\'></textarea>");
    out.print("</div>");
    out.print("<div class=\'code-scroll\'>");

    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\'codebox codebox-col codebox-struct pull-left\' style=\'width: 33.33%; height: 60%;\'>");
    out.print("<div class=\'codebox-resize resize  resize-horizontal pull-left\'>");
    out.print("</div>");

    out.print("<div class=\'codebox-content pull-left\'>");
    out.print("<div class=\'code-title row pull-left\'>");
    out.print("<h3>Struct Format</h3>");
    out.print("</div>");
    out.print("<div class=\'code-wrap\'>");
    out.print("<div class=\'code row\'>");
    out.print("<div class=\'code-line pull-left\'>");
    out.print("<span>1</span>");
    out.print("<span>2</span>");
    out.print("<span>3</span>");
    out.print("</div>");
    out.print("<div class=\'code-content pull-left\'>");
    out.print("<textarea name=\"txtStruct\" autocorrect=\'off\' autocapitalize=\'off\' spellcheck=\'false\' style=\'width: 100%; height: 100%; outline: none; border: 0; background: transparent;\' tabindex=\'0\'></textarea>");
    out.print("</div>");
    out.print("<div class=\'code-scroll\'>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");

    out.print("<div class=\'codebox codebox-col codebox-command pull-left\' style=\'width: 33.33%; height: 60%;\'>");

    out.print("<div class=\'codebox-resize resize resize-horizontal pull-left\'>");
    out.print("</div>");

    out.print("<div class=\'codebox-content pull-left\'>");
    out.print("<div class=\'code-wrap\'>");
    out.print("<div class=\'code-title row pull-left\'>");
    out.print("<h3>Excute statement</h3>");
    out.print("</div>");
    out.print("<div class=\'code row\'>");
    out.print("<div class=\'code-line pull-left\'>");
    out.print("<span>1</span>");
    out.print("<span>2</span>");
    out.print("<span>3</span>");
    out.print("</div>");
    out.print("<div class=\'code-content pull-left\'>");
    out.print("<textarea name=\"txtStatement\" autocorrect=\'off\' autocapitalize=\'off\' spellcheck=\'false\' style=\'width: 100%; height: 100%; outline: none; border: 0; background: transparent;\' tabindex=\'0\'></textarea>");
    out.print("</div>");
    out.print("<div class=\'code-scroll\'>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("<div class=\'bottom-codeboxes row\'>");
    out.print("<div class=\'codebox codebox-result codebox-row pull-left\'>");
    out.print("<div class=\'codebox-resize resize resize-vertical\'>");
    out.print("</div>");
    out.print("<div class=\'codebox-content pull-left\' style=\'height: calc(40% - 70px);\'>");
    out.print("<div class=\'code-wrap row\'>");
    out.print("<div class=\'code row pull-left\'>");
    out.print("<div class=\'code-line pull-left\'>");
    out.print("<span>1</span>");
    out.print("<span>2</span>");
    out.print("<span>3</span>");
    out.print("</div>");
    out.print("<div class=\'code-content pull-left\'>");
    out.print("<textarea autocorrect=\'off\' autocapitalize=\'off\' spellcheck=\'false\' style=\'width: 100%; height: 100%; outline: none; border: 0; background: transparent;\' tabindex=\'0\'></textarea>");
    out.print("</div>");
    out.print("</div>");

    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</div>");
    out.print("</section>");
    out.print("</form>");

    out.print("</body>");
    out.print("</html>");

    out.close();
  }

  //Clean up resources
  public void destroy() {
  }
}
