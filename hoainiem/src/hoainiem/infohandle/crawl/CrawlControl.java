package hoainiem.infohandle.crawl;

import hoainiem.*;
import hoainiem.infohandle.objects.*;
import java.sql.*;
import java.util.ArrayList;

public class CrawlControl {

  private CrawlModel cm;
  public CrawlControl(ConnectionPool cp) {
    cm = new CrawlModel(cp);
  }

  public ConnectionPool getConnection() {
    return this.cm.getConnection();
  }

  public void releaseConection() {
    this.cm = null;
  }

  public boolean addCrawl(CrawlObject item) {
    return this.cm.addCrawl(item);
  }

  public boolean delCrawl(int id) {
    return this.cm.delCrawl(id);
  }

  public boolean editCrawl(CrawlObject item) {
    return this.cm.editCrawl(item);
  }

  public CrawlObject getCrawl(int id) {
    return this.cm.getCrawl(id);
  }

  public ArrayList<CrawlObject> getCrawls(CrawlObject similar, int at, byte total, boolean isDESC) {
    return this.cm.getCrawls(similar, at, total, isDESC);
  }

}
