package hoainiem.infohandle.crawl;

import java.sql.*;

import hoainiem.*;
import hoainiem.infohandle.objects.*;
import hoainiem.gui.basic.*;

public class CrawlImpl extends BasicImpl implements Crawl {
  public CrawlImpl(ConnectionPool cp) {
    super(cp,"CrawlInfoHandle");
  }

  public boolean addCrawl(CrawlObject item) {
    String sql = "INSERT INTO tblcrawl(";
    sql += "crawl_url, crawl_created_date, ";
    sql += "crawl_deleted, crawl_author_created, ";
    sql += "crawl_notes ";
    sql += ") ";
    sql += "VALUES(?,?,?,?)";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getCrawl_url());
      pre.setString(2, item.getCrawl_author_created());
      pre.setBoolean(3, item.isCrawl_deleted());

      pre.setString(4, item.getCrawl_author_created());
      pre.setString(5, item.getCrawl_notes());
      //thuc thi
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean delCrawl(int id) {
    String sql = "DELETE FROM tblcrawl WHERE crawl_id = ? ";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setInt(1, id);

      //thuc thi
      return this.del(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean editCrawl(CrawlObject item) {
    String sql = "UPDATE tblCrawl SET ";
    sql += "crawl_url = ?, crawl_created_date = ?, ";
    sql += "crawl_deleted = ?, crawl_author_created = ?, ";
    sql+= "crawl_notes = ? ";
    sql += "WHERE crawl_id = ?";


    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getCrawl_url());
      pre.setString(2, item.getCrawl_author_created());
      pre.setBoolean(3, item.isCrawl_deleted());
      pre.setString(4, item.getCrawl_author_created());
      pre.setString(5, item.getCrawl_notes());

      pre.setInt(6, item.getCrawl_id());
      //thuc thi
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public ResultSet getCrawl(int id) {
    String sql = "SELECT * FROM tblcrawl WHERE crawl_id = ?";
    return this.get(sql, id);
  }

  public ResultSet getCrawls(CrawlObject similar, int at, byte total, boolean isDESC) {
    String sql = "SELECT * FROM tblcrawl ";

    sql += "ORDER BY crawl_id ";
    sql += (isDESC) ? " DESC " : " ASC ";
    sql += "LIMIT " + at + "," + total;

    return this.gets(sql);

  }

}
