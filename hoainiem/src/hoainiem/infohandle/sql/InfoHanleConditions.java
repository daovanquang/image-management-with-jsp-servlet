package hoainiem.infohandle.sql;

import hoainiem.infohandle.objects.*;

public class InfoHanleConditions {
  public static String getCommandConditions(CommandObject similar){
    String tmp =  "";
    if(similar==null) return tmp;
    tmp += (similar.getCommand_crawl_id()>0) ? " WHERE command_crawl_id = ? " : " ";
    return tmp;
  }
  public static String conditionsDelCommand(CommandObject similar){
    String tmp =  "";
    if(similar==null) return tmp;
    tmp += (similar.getCommand_crawl_id()>0) ? " WHERE command_crawl_id = ? " : " WHERE command_id = ? ";
    return tmp;
  }

}
