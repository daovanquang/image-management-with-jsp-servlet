package hoainiem.mail;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage;

public class EmailSender {
  private static final String fromMail = "daovanquang1997hy@gmail.com";
  private static final String pass = "Daovanquang1";

  public static String randomCode(byte length){
    String code="";
    for(byte i=0;i<length;i++){
      //random code
      Random rand = new Random();
      int num = rand.nextInt(9) + 1;
      code += num;
    }
    return code;
  }

  public static boolean sendMail(String toMail, String subject, String message) {
    if(toMail==null){
      return false;
    }else{
      if(fromMail.equalsIgnoreCase(toMail)){
        return false;
      }
    }

    toMail = toMail.trim().toLowerCase();

    boolean isSuccess = true;

    String defaultSubject = "This mail is not spam";
    subject = (subject==null)? defaultSubject : (subject=="")? defaultSubject : subject;

    try {
      String host = "smtp.gmail.com";
      boolean sessionDebug = false;

      Properties props = System.getProperties();

      props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", host);
      props.put("mail.smtp.port", "587");
      props.put("mail.smtp.auth", "true");

      java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
      Session mailSession = Session.getDefaultInstance(props, null);
      mailSession.setDebug(sessionDebug);
      Message msg = new MimeMessage(mailSession);
      msg.setFrom(new InternetAddress(fromMail));
      InternetAddress[] address = {new InternetAddress(toMail)};
      msg.setRecipients(Message.RecipientType.TO, address);
      msg.setSubject(subject); msg.setSentDate(new Date());
      msg.setText(message);

     Transport transport=mailSession.getTransport("smtp");
     transport.connect(host, fromMail, pass);
     transport.sendMessage(msg, msg.getAllRecipients());
     transport.close();
    }
    catch (Exception ex) {
      isSuccess = false;
      ex.printStackTrace();
    }
    return isSuccess;
  }
}
