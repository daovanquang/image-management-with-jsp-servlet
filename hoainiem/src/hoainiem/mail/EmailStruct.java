package hoainiem.mail;

import hoainiem.lang.gui.HomeLang;

public class EmailStruct {
  public static String create_Mail_Confirm_Code_Title(byte lang_value){
    String title = HomeLang.VERIFY_ACCOUNT[lang_value];
    return title;
  };

  public static String create_Mail_Confirm_Code_Content(String code, byte lang_value){
    String tmp = "Dear Customer!";
    tmp += HomeLang.VERIFY_CODE[lang_value] + code;
    tmp += "Thankyou !";
    tmp += "Supporter";
    tmp += "@2018 - www.trikys.com All Right Reserved.";
    return tmp;
  }
}
