package hoainiem;

import java.sql.Connection;
import java.sql.SQLException;

public abstract interface ConnectionPool {
  //Mot connection la 1 bo dem duy tri ket noi toi co so du lieu,
  //Cac ket noi khong duoc dong ngay ma duoc luu de goi lai
  Connection getConnection(String string) throws SQLException;
  void releaseConnection(Connection connection, String string) throws SQLException;
}
