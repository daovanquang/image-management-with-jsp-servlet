package hoainiem.machinelearning.objects;

import java.util.ArrayList;

public class Tree {
  private Node root;
  // Constructors
  public Tree(int key, String name) {
    root = new Node(key, name, null, null, null);
  }

  public Tree() {
    root = null;
  }

  public Node getNode(int key, Node parentNode) {
    if (key <= 0) {
      return this.root;
    }
    else {
      Node searchNode = (parentNode == null) ?  this.root : parentNode;
      for (Node node : searchNode.getChilds()) {
        if (node.getKey() == key) {
          return node;
        }
        else {
          getNode(key, node);
        }
      }

    }
    return null;
  }

  public void show(Node node) {
    Node selectedNode = new Node();
    if(this.root==null){
      System.out.println("tree is empty!");
    }else{
      selectedNode = (node == null) ?  this.root : node;
      System.out.println(selectedNode.toString());
      for (Node child : selectedNode.getChilds()) {
        show(child);
      }
    }
  }

  public String getPropertyByValues(ArrayList<String> names, ArrayList<String>values, Node node) {

    if(names == null || values == null) return null;

    String result = "";

    if(this.root==null){
      System.out.println("tree is empty!");
      return null;

    }else{

      int names_size = names.size();
      int values_size = values.size();

      if(node == null && names_size != values_size) return null;
      Node selectedNode = (node == null) ?  this.root : node;

      if(names_size > 0 &&values.size()>=0){

        if(selectedNode.getName() == names.get(0)){
          names.remove(0);
          for (Node child : selectedNode.getChilds()) {
            if (child.getParent_line_connection_value() == values.get(0)) {
              values.remove(0);
            }

            String tmp = child.getResult();

            if(values.size()>0){
              if (tmp != null) {
                tmp = tmp.trim();
                if (!tmp.equalsIgnoreCase("")) {
                  return tmp;
                }
              }

            }

            getPropertyByValues(names, values, child);
          }
        }
      }

    }

    return result;
  }

  public void setNode(int key, String name, String line_connect_value, String result ,Node parentNode) {
    Node nNode = new Node(key, name,line_connect_value,result, parentNode);
    if(parentNode==null){
      parentNode.getChilds().add(nNode);
    }else{
      this.root.getChilds().add(nNode);
    }
  }

  public void removeNode(int key,Node parentNode) {
    if (key == 0) {
      System.out.println("Can't delete root");
    }
    else {
      Node delNode = new Node();
      if (parentNode == null) {
        delNode = this.root;
      }else{
        delNode = parentNode;
      }
      for (Node node : delNode.getChilds()) {
        if (node.getKey() == key) {
          parentNode.getChilds().remove(node);
          break;
        }
        else {
          removeNode(key, node);
        }
      }

    }
  }

  public void removeNodes(int parentKey,Node parentNode) {
    if (parentKey == 0) {
      System.out.println("Can't delete root");
    }
    else {
      Node delNode = new Node();
      if (parentNode == null) {
        delNode = this.root;
      }else{
        delNode = parentNode;
      }
      for (Node node : delNode.getChilds()) {
        if (node.getKey() == parentKey) {
          parentNode.getChilds().remove(node);
        }
        else {
          removeNodes(parentKey, node);
        }
      }
    }

  }

}
