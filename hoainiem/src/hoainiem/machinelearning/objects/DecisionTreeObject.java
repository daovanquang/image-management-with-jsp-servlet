package hoainiem.machinelearning.objects;

import java.util.*;

public class DecisionTreeObject {

  private String decision_independent_title;
  private ArrayList<Boolean> decision_independent_values;

  private ArrayList<String> decision_dependent_titles;
  private String [][]decision_dependent_values;
  //contructor
  public DecisionTreeObject(ArrayList<String> dependent_titles, String [][] dependent_values,String independent_title, ArrayList<Boolean> independent_values) {
    this.decision_independent_title = independent_title;
    this.decision_independent_values = independent_values;

    this.decision_dependent_titles = dependent_titles;
    this.decision_dependent_values = dependent_values;
  }

  public ArrayList<Boolean> getDecision_independent_values() {
    return decision_independent_values;
  }

  public String[][] getDecision_dependent_values() {
    return decision_dependent_values;
  }

  public String getDecision_independent_title() {

    return decision_independent_title;
  }

  public ArrayList getDecision_dependent_titles() {

    return decision_dependent_titles;
  }

  public void setDecision_independent_values(ArrayList<Boolean> decision_independent_values) {
    this.decision_independent_values = decision_independent_values;
  }

  public void setDecision_dependent_values(String[][] decision_dependent_values) {
    this.decision_dependent_values = decision_dependent_values;
  }

  public void setDecision_independent_title(String decision_independent_title) {

    this.decision_independent_title = decision_independent_title;
  }

  public void setDecision_dependent_titles(ArrayList<String> decision_dependent_titles) {

    this.decision_dependent_titles = decision_dependent_titles;
  }
}
