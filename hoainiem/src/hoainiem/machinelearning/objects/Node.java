package hoainiem.machinelearning.objects;

import java.util.*;

public class Node {
  private int key;
  private int parent_key;
  private String name;
  private String parent_line_connection_value;
  private ArrayList<Node> childs;
  private String result;

  public Node(int key, String name ,String parent_line_connection_value, String result, Node parent_node)
   {
       this.key = key;
       this.name = name;
       this.childs = new ArrayList<Node>();

       if(parent_node!=null){
         this.result = result;
         this.parent_key = parent_node.getKey();
         this.parent_line_connection_value = parent_line_connection_value;
       }else{
         this.result = "";
         this.parent_key = -1;
         this.parent_line_connection_value = "none";
       }

   }
   public Node(){
      this.key = -1;
      this.name = "empty";
      this.parent_key = -1;
      this.parent_line_connection_value = "none";
      this.result = "";
      this.childs = new ArrayList<Node>();
   }

  public String toString(){
    return  "key: " + this.key + ", name: " + this.name + ", parent_key: " + this.parent_key + ", line: "+ this.parent_line_connection_value + ", child size: "+this.childs.size() +" ,result: "+this.result;
  }

  public void setKey(int key) {
    this.key = key;
  }

  public void setParent_key(int parent_key) {

    this.parent_key = parent_key;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setParent_line_connection_value(String  parent_line_connection_value) {
    this.parent_line_connection_value = parent_line_connection_value;
  }

  public void setChilds(ArrayList<Node> childs) {
    this.childs = childs;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public int getKey() {
    return key;
  }

  public int getParent_key() {

    return parent_key;
  }

  public String getName() {
    return name;
  }

  public String getParent_line_connection_value() {
    return parent_line_connection_value;
  }

  public ArrayList<Node> getChilds() {
    return childs;
  }

  public String getResult() {
    return result;
  }

}
