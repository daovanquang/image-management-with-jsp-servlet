package hoainiem.machinelearning.objects;

public class TrainingDataObject {

  private int trainingdata_id;
  private boolean trainingdata_insert;
  private String trainingdata_avg_search_month;
  private String trainingdata_word_num;
  private String trainingdata_compete_level;

  public int getTrainingdata_id() {
    return trainingdata_id;
  }

  public boolean isTrainingdata_insert() {
    return trainingdata_insert;
  }

  public String getTrainingdata_avg_search_month() {
    return trainingdata_avg_search_month;
  }

  public String getTrainingdata_word_num() {
    return trainingdata_word_num;
  }

  public String getTrainingdata_compete_level() {
    return trainingdata_compete_level;
  }

  public void setTrainingdata_id(int trainingdata_id) {
    this.trainingdata_id = trainingdata_id;
  }

  public void setTrainingdata_insert(boolean trainingdata_insert) {
    this.trainingdata_insert = trainingdata_insert;
  }

  public void setTrainingdata_avg_search_month(String
                                               trainingdata_avg_search_month) {
    this.trainingdata_avg_search_month = trainingdata_avg_search_month;
  }

  public void setTrainingdata_word_num(String trainingdata_word_num) {
    this.trainingdata_word_num = trainingdata_word_num;
  }

  public void setTrainingdata_compete_level(String trainingdata_compete_level) {
    this.trainingdata_compete_level = trainingdata_compete_level;
  }

}
