package hoainiem.machinelearning.decisiontree;

import hoainiem.machinelearning.objects.DecisionTreeObject;
import hoainiem.machinelearning.objects.Tree;

public class DecisionTreeControl {
  private DecisionTreeModel dtm;
  public DecisionTreeControl(){
    dtm = new DecisionTreeModel();
  }

  public Tree generateTree(DecisionTreeObject item){
    return this.dtm.generateTree(item);
  }

}
