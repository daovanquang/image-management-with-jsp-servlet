package hoainiem.machinelearning.decisiontree;

import hoainiem.machinelearning.objects.*;

public abstract interface DecisionTree {
  Tree generateTree(DecisionTreeObject item);
}
