package hoainiem.machinelearning.decisiontree;

import hoainiem.machinelearning.objects.*;
import hoainiem.machinelearning.library.*;
import java.util.*;

public class DecisionTreeImpl {
  public static int key = 0;

  private Tree setTree(Node parent_node, String parent_line_connection_value, String result, ArrayList<String> dependent_titles ,String[][] mx_dependent_values, ArrayList<Boolean> indipendent_values, Tree tree) {
    /*check valid*/
    if(dependent_titles==null || mx_dependent_values == null || indipendent_values==null) return null;

    int rows = mx_dependent_values.length;
    int cols = mx_dependent_values[0].length;

    /*check valid*/
    if(dependent_titles.size()==0 || rows==0 || cols==0 || indipendent_values.size()==0){
      return null;
    }

    if(parent_node!=null){
      key ++;
    }

    if(rows>0 && cols>0){

      /*
      System.out.println("Key: "+key);
      System.out.println("Title: ");
      DecisionTreeUtilities.showStringList(dependent_titles);
      System.out.println("\nValues: ");
      DecisionTreeUtilities.showMatrix(mx_dependent_values);
      System.out.println("Results: ");
      DecisionTreeUtilities.showBooleanList(indipendent_values);
      System.out.println("---------------------");
      */

      ArrayList<Float> IGs = new ArrayList<Float> ();

      for (int i = 0; i < cols; i++) {
        /*============- group by value of matrix -==============*/
        if(mx_dependent_values.length < 1 || mx_dependent_values[0].length < 1)  break;
        ArrayList<String> groupValues = DecisionTreeUtilities.groupValues(mx_dependent_values,i);
        ArrayList<Float> entropyChilds = new ArrayList<Float> ();

        if(groupValues.size()>0){
          /*============- calculate entropy after group values -==============*/
          int yesCountChild = 0;
          float entropyChild = 0;
          for (int k = 0; k < groupValues.size(); k++) {

            String valueAfterGroup = groupValues.get(k);
            int sum = DecisionTreeUtilities.countByValue(mx_dependent_values,i,valueAfterGroup);
            for (int j = 0; j < rows; j++) {
              if (valueAfterGroup.equalsIgnoreCase(mx_dependent_values[j][i]) && indipendent_values.get(j) == true) {
                yesCountChild++;
              }
            }
            entropyChild = DecisionTreeUtilities.getEntropyNode(yesCountChild, sum, (byte) 5);
            entropyChilds.add(entropyChild);

            yesCountChild = 0;
            entropyChild = 0;
          }

          /*============- Information Gain -============*/

          //get coefficients
          ArrayList<Float> coefficients = new ArrayList<Float> ();

          for (String valueGroup:groupValues) {
            int countValue = DecisionTreeUtilities.countByValue(mx_dependent_values,i,valueGroup);
            float coefficient = (float) countValue / rows;
            coefficients.add(coefficient);
          }
          //get count yes dependent value

          int yesCount = DecisionTreeUtilities.countByTrueValue(indipendent_values);

          float entropyParent = DecisionTreeUtilities.getEntropyNode(yesCount, rows,(byte) 5);
          //get infor gain
          float IG = DecisionTreeUtilities.getInformationGain(entropyParent, coefficients, entropyChilds, (byte)5);
          IGs.add(IG);

          groupValues.clear();
          entropyChilds.clear();
          coefficients.clear();
        }
      }

      // get elm with max IG value
      float max = -1;
      int selected_index = 0;

      if(IGs.size()>0){
        max = DecisionTreeUtilities.getMax(IGs);
      }
      if(max>0){
        selected_index = DecisionTreeUtilities.getIndexByFloatValue(IGs, max);
      }

      IGs.clear();


      //generate tree
      String value = dependent_titles.get(selected_index);

      if(tree == null) {
       tree = new Tree(key, value);
      }else{
        if(dependent_titles.size() <= 1) result = String.valueOf(indipendent_values.get(0));

        tree.setNode(key, value, parent_line_connection_value, result, parent_node);
      }

      parent_node = (tree == null) ? new Node(key, value,"","",null) : new Node(key, value, parent_line_connection_value, result, parent_node);

      //group by value of independent selected
      ArrayList<String> parentValues = new ArrayList<String>();
      if(selected_index >= 0){
        parentValues = DecisionTreeUtilities.groupValues(mx_dependent_values, selected_index);
      }

      //recursive by parent row value
      if(parentValues.size() > 0){

        for (String parentValue : parentValues) {

          //update indipendent and dependent, dependent title
          int countByValue = DecisionTreeUtilities.countByValue(mx_dependent_values, selected_index, parentValue);

          if (countByValue > 0 && cols >= 1) {

            ArrayList<Boolean> independentsAfterGroup = new ArrayList<Boolean> ();
            ArrayList<String> independentTitlesAfterGroup = new ArrayList<String> ();
            String[][] dependentsAfterGroup = new String[countByValue][cols -1];

            int n_row_index = 0;

            for (int i = 0; i < rows; i++) {
              String matrixValue = mx_dependent_values[i][selected_index];

              if (n_row_index < countByValue && parentValue.equalsIgnoreCase(matrixValue)) {
                for (int j = 0; j < cols; j++) {
                  if (j != selected_index) {
                    DecisionTreeUtilities.addValueInStringList(independentTitlesAfterGroup, dependent_titles.get(j));
                    String mx_value = mx_dependent_values[i][j];
                    if(j <= selected_index){
                      dependentsAfterGroup[n_row_index][j] = mx_value;
                    }else{
                      dependentsAfterGroup[n_row_index][j-1] = mx_value;
                    }
                  }
                }
                independentsAfterGroup.add(indipendent_values.get(i));

                n_row_index++;
              }

            }

            //recursive
            setTree(parent_node, parentValue, result, independentTitlesAfterGroup, dependentsAfterGroup, independentsAfterGroup, tree);

            independentsAfterGroup.clear();
            independentTitlesAfterGroup.clear();
            dependentsAfterGroup = null;
          }
        }
      }
    }

    return tree;
  }

  public Tree generateTree(DecisionTreeObject item) {
    Tree tree = null;
    return setTree(null, "", "", item.getDecision_dependent_titles(), item.getDecision_dependent_values(), item.getDecision_independent_values(), tree);
  }

  /*
  public static void main(String []args){

    //============- ID3 ALGROTHIRM -============

    //=============- DEVELOP BY -===============

    //===========- QUANG DAO VAN -==============

    //=============- 2018/11/30 -===============

    ArrayList<Boolean> dependents = new ArrayList<Boolean>();
    dependents.add(true);
    dependents.add(false);
    dependents.add(false);
    dependents.add(false);
    dependents.add(false);

    String dependent_title = "trainingdata_insert";

    ArrayList<String> independent_titles = new ArrayList<String>();
    independent_titles.add("avg");
    independent_titles.add("word_num");
    independent_titles.add("compete_level");

    String [][]independent_values = new String[5][3];
    independent_values[0][0] = "big";
    independent_values[1][0] = "big";
    independent_values[2][0] = "normal";
    independent_values[3][0] = "very-small";
    independent_values[4][0] = "very-big";

    independent_values[0][1] = "extra-large";
    independent_values[1][1] = "small";
    independent_values[2][1] = "extra-large";
    independent_values[3][1] = "small";
    independent_values[4][1] = "extra-small";

    independent_values[0][2] = "hight";
    independent_values[1][2] = "low";
    independent_values[2][2] = "medium";
    independent_values[3][2] = "hight";
    independent_values[4][2] = "low";

    DecisionTreeObject item = new DecisionTreeObject(independent_titles,independent_values,dependent_title,dependents);
    DecisionTreeImpl dti = new DecisionTreeImpl();
    Tree tree = dti.generateTree(item);

    //=============- CALL METHOD SHOW TREE -===============
    tree.show(null);

    //=============- GET DECISION -===============
    ArrayList<String> names = new ArrayList<String>();
    names.add("avg");
    names.add("word_num");
    names.add("compete_level");


    ArrayList<String> values = new ArrayList<String>();
    values.add("big");
    values.add("extra-large");
    values.add("hight");

    int size = names.size();
    String message = "\nLog: Decision of dependent values ";
    for(int i=0; i<size; i++){
      message += names.get(i) + "=" + values.get(i);
      //sperator
      message += (i<size-1) ? ", " : " ";
    }
    message += ":" + tree.getPropertyByValues(names, values, null);

    System.out.println(message);
  }
  */
}
