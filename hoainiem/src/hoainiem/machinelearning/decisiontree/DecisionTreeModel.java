package hoainiem.machinelearning.decisiontree;

import hoainiem.machinelearning.objects.DecisionTreeObject;
import hoainiem.machinelearning.objects.Tree;

public class DecisionTreeModel {
  private DecisionTreeImpl dt;
  public DecisionTreeModel(){
    this.dt = new DecisionTreeImpl();
  }

  public Tree generateTree(DecisionTreeObject item){
    return this.dt.generateTree(item);
  }
}
