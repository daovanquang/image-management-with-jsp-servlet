package hoainiem.machinelearning.trainingdata;

import hoainiem.machinelearning.objects.*;
import hoainiem.ShareControl;
import java.sql.*;

public abstract interface TrainingData extends ShareControl{
  boolean addTrainingData(TrainingDataObject item);
  boolean editTrainingData(TrainingDataObject item);
  boolean delTrainingData(int id);
  ResultSet getTrainingDataObject(int id);
  ResultSet getTrainingDataObjects(TrainingDataObject similar, int at, byte total);
}

