package hoainiem.machinelearning.trainingdata;

import java.sql.*;

import hoainiem.*;
import hoainiem.gui.basic.*;
import hoainiem.machinelearning.objects.*;

public class TrainingDataImpl extends BasicImpl implements TrainingData {

  public TrainingDataImpl(ConnectionPool cp) {
    super(cp, "TrainingData");
  }

  public ConnectionPool getCP() {
    return this.getCP();
  }

  public void releaseConnection(){
    this.releaseConnection();
  }


  public boolean addTrainingData(TrainingDataObject item) {
    return false;
  }

  public boolean delTrainingData(int id) {
    return false;
  }

  public boolean editTrainingData(TrainingDataObject item) {
    return false;
  }

  public ResultSet getTrainingDataObject(int id) {
    String sql = "SELECT * FROM tbltrainingdata WHERE trainingdata_id = ?";
    return this.get(sql,id);

  }

  public ResultSet getTrainingDataObjects(TrainingDataObject similar, int at, byte total) {
    String sql = "SELECT * FROM tbltrainingdata ";
    sql += "ORDER BY trainingdata_id DESC ";
    sql += "LIMIT " + at + "," + total;
    return this.gets(sql);
  }
}
