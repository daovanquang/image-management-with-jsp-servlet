package hoainiem.machinelearning.trainingdata;

import hoainiem.*;
import hoainiem.machinelearning.objects.TrainingDataObject;
import java.util.ArrayList;

public class TrainingDataControl {
  private TrainingDataModel tdm;
  public TrainingDataControl(ConnectionPool cp) {
    tdm = new TrainingDataModel(cp);
  }

  public ConnectionPool getCP() {
    return this.tdm.getCP();
  }

  public void releaseConnection() {
    this.tdm.releaseConnection();
  }

  public boolean addTrainingData(TrainingDataObject item) {
    return this.tdm.addTrainingData(item);
  }

  public boolean delTrainingData(int id) {
    return this.tdm.delTrainingData(id);
  }

  public boolean editTrainingData(TrainingDataObject item) {
    return this.tdm.editTrainingData(item);
  }

  public TrainingDataObject getTrainingDataObject(int id) {
    return this.tdm.getTrainingDataObject(id);
  }

  public ArrayList<TrainingDataObject> getTrainingDataObjects(TrainingDataObject similar, int at, byte total) {
    return this.tdm.getTrainingDataObjects(similar,at,total);
  }

}
