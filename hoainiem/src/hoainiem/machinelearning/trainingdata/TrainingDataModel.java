package hoainiem.machinelearning.trainingdata;

import hoainiem.machinelearning.objects.*;
import hoainiem.*;
import java.util.*;
import java.sql.*;

public class TrainingDataModel {
  private TrainingData tdi;
  public TrainingDataModel(ConnectionPool cp) {
    tdi = new TrainingDataImpl(cp);
  }

  public ConnectionPool getCP() {
    return this.tdi.getCP();
  }

  public void releaseConnection() {
    this.tdi.releaseConnection();
  }

  public boolean addTrainingData(TrainingDataObject item) {
    return this.tdi.addTrainingData(item);
  }

  public boolean delTrainingData(int id) {
    return this.tdi.delTrainingData(id);
  }

  public boolean editTrainingData(TrainingDataObject item) {
    return this.tdi.editTrainingData(item);
  }

  public TrainingDataObject getTrainingDataObject(int id) {
    ResultSet rs =  this.tdi.getTrainingDataObject(id);
    TrainingDataObject item = new TrainingDataObject();

    if(rs!=null){
      try {
        if(rs.next()) {
          item.setTrainingdata_id(rs.getInt("trainingdata_id"));
          item.setTrainingdata_avg_search_month(rs.getString("trainingdata_avg_search_month"));
          item.setTrainingdata_compete_level(rs.getString("trainingdata_compete_level"));
          item.setTrainingdata_insert(rs.getBoolean("trainingdata_insert"));
          item.setTrainingdata_word_num(rs.getString("trainingdata_word_num"));
        }
      }
      catch (SQLException ex) {
      }
    }
    return item;
  }

  public ArrayList<TrainingDataObject> getTrainingDataObjects(TrainingDataObject similar, int at, byte total) {
    ArrayList<TrainingDataObject> items = new ArrayList<TrainingDataObject>();
    ResultSet rs = this.tdi.getTrainingDataObjects(similar, at, total);

    if(rs!=null){
      try {
        while (rs.next()) {
          TrainingDataObject item = new TrainingDataObject();
          item.setTrainingdata_id(rs.getInt("trainingdata_id"));
          item.setTrainingdata_avg_search_month(rs.getString("trainingdata_avg_search_month"));
          item.setTrainingdata_compete_level(rs.getString("trainingdata_compete_level"));
          item.setTrainingdata_insert(rs.getBoolean("trainingdata_insert"));
          item.setTrainingdata_word_num(rs.getString("trainingdata_word_num"));

          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }

    }
    return items;
  }

}
