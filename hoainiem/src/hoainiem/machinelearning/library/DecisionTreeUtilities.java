package hoainiem.machinelearning.library;

import java.util.*;

public class DecisionTreeUtilities {

  public static float getEntropyNode(int yesCount, int sum, byte round_after_dot){
    if(sum <= 0 || sum < yesCount) return 0;
    int noCount = sum - yesCount;
    if(yesCount==noCount) return 1;
    if(yesCount==sum||noCount==sum) return 0;

    float yes_instance_sum = (float)yesCount/sum;
    float no_instance_sum = (float)noCount/sum;
    double Hs = -(yes_instance_sum)*DecisionTreeUtilities.log2(yes_instance_sum) - (no_instance_sum)*DecisionTreeUtilities.log2(no_instance_sum);
    int round_pow = (int)Math.pow(10, round_after_dot);
    float rounding = (float) Math.ceil(Hs*round_pow)/round_pow;
    return rounding;
  }

  public static float getInformationGain(float entropyParent, ArrayList<Float> coefficients, ArrayList<Float> entropys, byte round_after_dot){
      if(entropys == null || coefficients == null) return 0;
      int len = entropys.size();
      int cof_len = coefficients.size();
      if(len ==0 || cof_len != len){
          return 0;
      }

      float IG = entropyParent;

      for(int i = 0; i< len;i++){
        IG -= coefficients.get(i) * entropys.get(i);
      }
      return IG;
  }

  public static double log2(float num){
    return (Math.log(num) / Math.log(2));
  }
  public static int countByValue(String [][]matrix, int colIndex, String value){
    int rows = matrix.length;
    int cols = matrix[0].length;
    if(value!=null && rows>0 && cols >0){
      int count = 0;
      for (int i = 0; i < rows; i++) {
        if (value.equalsIgnoreCase(matrix[i][colIndex]))
          count++;
      }
      return count;
    }
    return 0;
  }
  public static int countByTrueValue(ArrayList<Boolean> arr){
    int rows = arr.size();
    if(rows >0){
      int count = 0;
      for (boolean item : arr) {
        if(item==true)
          count++;
      }
      return count;
    }
    return 0;
  }


  public static float getMax(ArrayList<Float> arr){
    if(arr!=null){
      int len = arr.size();
      if(len>0){
        float max = arr.get(0);
        for(int i=1; i<len; i++){
         if(max < arr.get(i)) {
           max = arr.get(i);
         }
        }
        return max;
      }
    }
    return -1;
  }


  public static int getIndexByFloatValue(ArrayList<Float> arr, float value){
    if(arr!=null){
      int len = arr.size();
      if(len>0){
        int index = 0;
        for(float item:arr){
         if(item == value) {
           return index;
         }else {
           index ++;
         }
        }
        return index;
      }
    }
    return -1;
  }

  public static int getIndexByStringValue(ArrayList<String> arr, String value){
    if(arr!=null){
      int len = arr.size();
      if(len!=0){
        int index = 0;
        for(String item:arr){
         if(item == value) {
           return index;
         }else {
           index ++;
         }
        }
        return index;
      }
    }
    return 0;
  }

  public static ArrayList<String> groupValues(String [][]matrix, int colIndex){
    ArrayList<String> groupValues = new ArrayList<String>();
    if(matrix == null) return null;
    int rows = matrix.length;


    groupValues.add(matrix[0][colIndex]);
    for (int j = 1; j < rows; j++) {
      String value = matrix[j][colIndex];
      boolean isExisting = false;
      for (int k = 0; k < groupValues.size(); k++) {
        if (value.equalsIgnoreCase(groupValues.get(k))) {
          isExisting = true;
          break;
        }
      }
      if (!isExisting) {
        groupValues.add(value);
      }
    }
    return groupValues;
  }

  public static String[][] removeCol(String [][]matrix, int colRemoveIndex){
    int rows = matrix.length;
    int cols = matrix[0].length;
    String [][]newMatrix = null;
    if(rows>0 && cols>0) {
      newMatrix = new String[rows][cols - 1];
    }

    for(int i=0; i<cols; i++){
       if(i != colRemoveIndex){
         for (int j = 0; j < rows; j++) {
           boolean isLess = (i < colRemoveIndex)? true : false;
           if(isLess){
             newMatrix[j][i] = matrix[j][i];
           }else if(!isLess){
             newMatrix[j][i-1] = matrix[j][i];
           }
         }
       }
    }
    return newMatrix;
  }

  public static void showMatrix(String [][]matrix){
    int rows = matrix.length;
    int cols = matrix[0].length;
    for(int i=0; i<rows; i++){
       for(int j=0; j<cols; j++){
         System.out.print(matrix[i][j]+"\t");
       }
       System.out.println();
    }
  }
  public static synchronized ArrayList<String> removeValueInStringList(ArrayList<String> items, String value){
    if(items!=null && value!=null){
      if(items.size()>0){
        ArrayList<String> toRemove = new ArrayList<String>();
        for (String item : items) {
          if (item == value) {
            toRemove.add(value);
          }
        }
        items.removeAll(toRemove);
        return items;
      }
    }
    return items;
  }
  public static synchronized ArrayList<String> addValueInStringList(ArrayList<String> items, String value){
    if(items!=null && value!=null){
      boolean valid = true;
        if(items.size()>0){
          for (String item : items) {
            if (item == value) {
              valid = false;
              break;
            }
        }
      }
      if(valid){
        items.add(value);
      }
    }
    return items;
  }

  public static void showBooleanList(ArrayList<Boolean> items){
    for(boolean item: items){
      System.out.print(item+"\n");
    }
  }

  public static void showStringList(ArrayList<String> items){
    for(String item: items){
      System.out.print(item+"\t");
    }
  }
  public static void  showFloatList(ArrayList<Float> items){
    for(Float item: items){
      System.out.print(item+"\t");
    }
  }

}
