package hoainiem.gui.user;

import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.lang.gui.*;
import java.util.*;
import hoainiem.values.gui.*;

public class UserLibrary {
  public UserLibrary() {
  }
  public static String viewSectionSignin(byte lang_value) {

    String tmp = "";
    tmp += "<section>";
    tmp += "<div class=\"splash-container main-350\">";
    tmp += "<div class=\"panel mt-20\">";
    tmp += "<div class=\"panel-body\">";
    tmp += "<form action=\"\" method=\"\" name=\"frmSignin\" id=\"frmSignin\" class=\"row\">";
    tmp += "<div class=\"form-log\">";
    tmp += "<label id=\"label-username-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"text\" class=\"form-control\" name=\"txtUserName\" placeholder=\'"+HomeLang.USERNAME[lang_value]+"\' autocomplete=\"off\" onkeypress=\"changeLabel(this,'label-username-error')\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-log\">";
    tmp += "<label id=\"label-userpass-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"password\" class=\"form-control\" name=\"txtUserPass\" placeholder=\'"+HomeLang.PASSWORD[lang_value]+"\' autocomplete=\"off\" onkeypress=\"changeLabel(this,'label-userpass-error')\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-group row\">";
    tmp += "<label class=\"pull-left\">";
    tmp += "<input type=\"checkbox\" name=\"chkRemember\" onclick=\"isChecked(this.form);\">";
    tmp += "<span class=\"form-label\">"+HomeLang.REMEMBER_ME[lang_value]+"</span>";
    tmp += "</label>";
    tmp += "<a href=\"/"+AllGUI.WebModule+"/user/forgotpass\" class=\"pull-right\"><label class=\"label-bold label-hover\">"+HomeLang.FORGOTPASS[lang_value]+"</label></a>";
    tmp += "</div>";
    tmp += "<div class=\"form-group form-submit mt-15\">";
    tmp += "<button type=\"button\" name=\"btnSignin\" class=\"btn btn-xl btn-primary\" onclick=\"signin(this.form,"+lang_value+")\">"+HomeLang.LOGIN[lang_value]+"</button>";
    tmp += "</div>";
    tmp += "</form>";

    tmp += "<div class=\"form-group third-party-login\">";
    tmp += "<div class=\"third-party-login-line text-center no-select\">"+HomeLang.OR[lang_value]+"</div>";
    tmp += "<a href=\"#\">";
    tmp += "<button type=\"button\" class=\"btn btn-lg google-btn text-center label-bold\">Google</button>";
    tmp += "</a>";
    tmp += "<a href=\"#\">";
    tmp += "<button type=\"button\" class=\"btn  btn-lg facebook-btn text-center label-bold\">Facebook</button>";
    tmp += "</a>";
    tmp += "</div>";

    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"splash-footer text-center main-350 mt-10 mb-30\">";
    tmp += "<label  class=\"label-default\">"+HomeLang.NEW_MEMBER[lang_value]+"&nbsp;";
    tmp += "<a href=\"/"+AllGUI.WebModule+"/user/signup\" class=\"label-bold label-hover\">"+HomeLang.SIGNUP[lang_value]+"</a>";
    tmp += "&nbsp;"+HomeLang.HERE[lang_value]+"</label>";
    tmp += "</div>";
    tmp += "</section>";
    return tmp;
  }

  public static String viewSectionSignup(byte lang_value) {
    String tmp = "";
    tmp += "<section>";
    tmp += "<div class=\"splash-container main-350\">";
    tmp += "<div class=\"panel mt-20\">";
    tmp += "<div class=\"panel-heading mb-20 no-select\">";
    tmp += "<div class=\"progress-bar relative row\">";
    tmp += "<span class=\"dot absolute absolute-left active\">1</span>";
    tmp += "<span class=\"dot absolute absolute-center\">2</span>";
    tmp += "<span class=\"dot absolute absolute-right\">3</span>";
    tmp += "<div class=\"relative progress-line\">";
    tmp += "<div class=\"absolute absolute-center\"></div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"relative row\">";
    tmp += "<div class=\"pull-left\">";
    tmp += "<span>"+HomeLang.SIGNUP[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"absolute absolute-center text-center\">";
    tmp += "<span>"+HomeLang.CONFIRM[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"pull-right\">";
    tmp += "<span>"+HomeLang.CREATE_AN_ACCOUNT[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"panel-body\">";
    tmp += "<form action=\"\" method=\"\">";
    tmp += "<section class=\"row\">";
    tmp += "<div class=\"form-log\">";
    tmp += "<label id=\"label-username-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"text\" class=\"form-control\" name=\"txtUserName\" placeholder=\'"+HomeLang.USERNAME[lang_value]+"...\' onkeypress=\"changeLabel(this,'label-username-error')\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-log\">";
    tmp += "<label id=\"label-usermail-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"text\" class=\"form-control\" name=\"txtUserMail\" placeholder=\'"+HomeLang.USERMAIL[lang_value]+"...\' onkeypress=\"changeLabel(this,'label-usermail-error')\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-group form-submit mt-15\">";
    tmp += "<button type=\"button\" class=\"btn btn-xl btn-primary\" onclick=\"signup(this.form,"+lang_value+");\">"+HomeLang.SIGNUP[lang_value]+"</button>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<p>"+HomeLang.AGREE_PRIVACY_POLICY[lang_value]+"&nbsp;";

    String companyPriacyPolicy = (lang_value == 1) ? HomeLang.COMPANY_NAME[lang_value]+"&nbsp;"+HomeLang.PRIVACY_POLICY[lang_value] : HomeLang.PRIVACY_POLICY[lang_value]+"&nbsp;"+ HomeLang.COMPANY_NAME[lang_value];

    tmp += "<a class=\"label-bold label-default label-hover link-no-color\"  href=\"/"+AllGUI.WebModule+"/privacy-policy/\">"+companyPriacyPolicy+"</a></p>";
    tmp += "</div>";
    tmp += "</section>";
    tmp += "</form>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"splash-footer text-center main-350 mt-10 mb-30\">";
    tmp += "<label>"+HomeLang.HAVE_AN_ACCOUNT[lang_value]+"&nbsp;<a  class=\"label-bold label-hover\" href=\"/"+AllGUI.WebModule+"/user/signin/\">"+HomeLang.LOGIN[lang_value]+"</a>&nbsp;"+HomeLang.HERE[lang_value]+"</label>";
    tmp += "</div>";
    tmp += "</section>";

    return tmp;
  }

  public static String viewSectionConfirm(UserObject userSignup, byte lang_value) {
    if(userSignup==null) return null;

    String tmp = "";
    tmp += "<section>";
    tmp += "<div class=\"splash-container main-350\">";
    tmp += "<div class=\"panel mt-20\">";
    tmp += "<div class=\"panel-heading\">";
    tmp += "<div class=\"progress-bar relative row\">";
    tmp += "<span class=\"dot absolute absolute-left active\">&#10004;</span>";
    tmp += "<span class=\"dot absolute absolute-center active\">2</span>";
    tmp += "<span class=\"dot absolute absolute-right\">3</span>";
    tmp += "<div class=\"relative progress-line\">";
    tmp += "<div class=\"absolute absolute-left progress-line__value progress-line__value--50\"></div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"relative row\">";
    tmp += "<div class=\"pull-left\">";
    tmp += "<span>"+HomeLang.SIGNUP[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"absolute absolute-center text-center\">";
    tmp += "<span>"+HomeLang.CONFIRM[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"pull-right\">";
    tmp += "<span>"+HomeLang.CREATE_AN_ACCOUNT[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"panel-body\">";
    tmp += "<form action=\"\" method=\"\">";
    tmp += "<section class=\"row\">";
    tmp += "<div class=\"form-group text-center\">";
    tmp += "<h3 class=\"label-bold\">"+HomeLang.A_CONFIRM_CODE_SEND_TO[lang_value]+"<span class=\"link-primary\">" +userSignup.getUser_mail() + "</span>. "+HomeLang.PLS_CHECK_MAIL[lang_value]+"</h3>";
    tmp += "</div>";
    tmp += "<div class=\"form-log\">";
    tmp +="<label id=\"label-code-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"text\" class=\"form-control\" name=\"txtConfirmCode\" placeholder=\""+HomeLang.CONFIRM_CODE[lang_value]+"...\" autocomplete=\"off\">";
    tmp += "</div>";
    tmp += "<div class=\"form-group text-center mt-20\">";
    tmp += "<button type=\"button\" class=\"btn btn-lg btn-primary\" onclick=\"confirmForm(this.form,'label-code-error',"+lang_value+");\">"+HomeLang.CONFIRM[lang_value]+"</button>";
    tmp += "</div>";
    tmp += "</section>";
    tmp += "</form>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"splash-footer text-center main-350 mt-10 mb-30\">";
    tmp += "<a href=\"activecode.jsp\" class=\"label-bold label-hover label-bold\">"+HomeLang.SEND_CONFIRMCODE_AGAIN[lang_value]+"</a>";
    tmp += "</form>";
    tmp += "</div>";
    tmp += "</section>";

    return tmp;
  }

  public static String viewSectionCreatePass(byte lang_value) {

    String tmp = "";
    tmp += "<section class=\"row relative\">";
    tmp += "<div class=\"splash-container main-350\">";
    tmp += "<form name=\"frmUserVerify\" action=\'\' method=\'\' >";
    tmp += "<div class=\"panel mt-20\">";
    tmp += "<div class=\"panel-heading\">";
    tmp += "<div class=\"progress-bar relative row\">";
    tmp += "<span class=\"dot absolute absolute-left active\">&#10004;</span>";
    tmp += "<span class=\"dot absolute absolute-center active\">&#10004;</span>";
    tmp += "<span class=\"dot absolute absolute-right active\">3</span>";
    tmp += "<div class=\"relative progress-line\">";
    tmp += "<div class=\"absolute absolute-left progress-line__value progress-line__value--100\"></div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"relative row\">";
    tmp += "<div class=\"pull-left\">";
    tmp += "<span>"+HomeLang.SIGNUP[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"absolute absolute-center text-center\">";
    tmp += "<span>"+HomeLang.CONFIRM[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"pull-right\">";
    tmp += "<span>"+HomeLang.CREATE_AN_ACCOUNT[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"panel-body\">";
    tmp += "<form action=\"\" method=\"\">";
    tmp += "<section class=\"row\">";
    tmp += "<div class=\"form-log\">";
    tmp += "<label id = \"label-userpass-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"password\" class=\"form-control\" name=\"txtUserPass\" placeholder=\'"+HomeLang.NEW_PASS[lang_value]+"...\' onkeypress=\"changeLabel(this,'label-userpass-error')\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-log\">";
    tmp += "<label id=\"label-userrepeatpass-error\" class=\"label-bold label-danger hide\"></label>";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"password\" class=\"form-control\" name=\"txtUserRepeatPass\" placeholder=\'"+HomeLang.REPEAT_NEW_PASS[lang_value]+"...\' onkeypress=\"changeLabel(this,'label-userrepeatpass-error')\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-group text-center mt-20\">";
    tmp += "<button type=\"button\" class=\"btn btn-lg btn-primary\" onclick=\"createPass(this.form,"+lang_value+");\">"+HomeLang.CREATE_PASS[lang_value]+"</button>";
    tmp += "</div>";
    tmp += "</section>";
    tmp += "</form>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</section>";

    return tmp;
  }


  public static String viewSectionVerify(UserObject userConfirmed, byte lang_value) {
    if(userConfirmed==null) return null;

    String tmp = "";
    tmp += "<section class=\"row relative\">";
    tmp += "<div class=\"splash-container main-350\">";
    tmp += "<form name=\"frmUserVerify\" action=\'\' method=\'\' enctype=\"multipart/form-data\">";
    tmp += "<div class=\"panel mt-20\">";
    tmp += "<div class=\"panel-heading\">";
    tmp += "<div class=\"progress-bar relative row\">";
    tmp += "<span class=\"dot absolute absolute-left active\">&#10004;</span>";
    tmp += "<span class=\"dot absolute absolute-center active\">&#10004;</span>";
    tmp += "<span class=\"dot absolute absolute-right active\">3</span>";
    tmp += "<div class=\"relative progress-line\">";
    tmp += "<div class=\"absolute absolute-left progress-line__value progress-line__value--100\"></div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"relative row\">";
    tmp += "<div class=\"pull-left\">";
    tmp += "<span>"+HomeLang.SIGNUP[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"absolute absolute-center text-center\">";
    tmp += "<span>"+HomeLang.CONFIRM[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "<div class=\"pull-right\">";
    tmp += "<span>"+HomeLang.CREATE_AN_ACCOUNT[lang_value]+"</span>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"panel-body\">";
    tmp += "<div class=\"form-group row\">";
    tmp += "<div class=\"quickview quickview-lg quickview--center\" onclick=\"selectElmById('inputUpload');\">";
    tmp += "<img id=\"previewImage\" src=\"/"+AllGUI.WebModule+"/imgs/avatar.jpg\" alt=\"\">";
    tmp += "</div>";
    tmp += "<h4 id=\"imageName\" class=\"text-center label-bold\">"+HomeLang.SELECT_AN_AVATAR[lang_value]+"</h4>";
    tmp += "<input type=\"file\" id=\"inputUpload\" accept=\"image/*\" name=\"txtUserImage\" hidden=\"true\" onchange=\"loadImage('inputUpload','previewImage','imageName');\">";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"text\" class=\"form-control\" name=\"txtUserFullName\" placeholder=\""+HomeLang.USERFULLNAME[lang_value]+"...\" >";
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    //script generate gender selectbox
    tmp += "<script>";
    tmp += "var genders = [";
    if(lang_value==0){
      int gender_len = UtilitiesGender.VN_GENDERS.length;
      for(int i=0; i<gender_len; i++){
        tmp += (i==0) ? "'"+UtilitiesGender.VN_GENDERS[i]+"'" : ",'"+UtilitiesGender.VN_GENDERS[i]+"'";
      }
    }else{
       int gender_len = UtilitiesGender.EN_GENDERS.length;
       for(int i=0; i<gender_len; i++){
         tmp += (i==0) ? "'"+UtilitiesGender.EN_GENDERS[i]+"'" : ",'"+UtilitiesGender.EN_GENDERS[i]+"'";
       }
    }
    tmp += "];";
    tmp +="generateSelectBox(\'slcUserGender\',genders,null,0,\'form-control\',null);";
    tmp += "</script>";
    //script
    tmp += "</div>";
    tmp += "<div class=\"form-group\">";
    tmp += "<input type=\"text\" class=\"form-control\" name=\"txtUserPhone\" placeholder=\""+HomeLang.USERPHONE[lang_value]+"...\">";
    tmp += "</div>";
    tmp += "<div class=\"form-log\">";
    tmp += "<label class=\"label-bold\">"+HomeLang.USERBIRTHDAY[lang_value]+"</label>";
    tmp += "</div>";

    tmp += "<div class=\"form-group slc-date-time\">";
    //generate datetime box
    tmp += "<script>";
    tmp += "generateSelectBoxDate(this.form,'txtUserBirthDay','slcYear','slcMonth','slcDay',1990,2018,"+lang_value+");";
    tmp += "</script>";
    //end generate datetime box
    tmp += "</div>";
    tmp += "<div class=\"form-log\">";
    tmp += "<label class=\"label-bold\">"+HomeLang.USERADDRESS[lang_value]+"</label>";
    tmp += "</div>";

    tmp += "<div class=\"form-group\">";
    //generate provices/city box
    tmp += "<script>";

    tmp += "var provincecity = '{\"title\":\"";
    tmp += HomeLang.PROVICECITY_LANG[lang_value]+'\"';
    tmp += ",\"nations\":[";
    int vn_pv_len = UtilitiesAddress.VN_PROVICES_CITYS.length;
    if(vn_pv_len>0){
      tmp += "{\"nation_code\":\"vn\",";
      String pv_values = "\"values\":[";
      for (int i = 0; i < vn_pv_len; i++) {
        pv_values += (i==0) ? "\"" + UtilitiesAddress.VN_PROVICES_CITYS[i] + "\"": ",\"" + UtilitiesAddress.VN_PROVICES_CITYS[i] + "\"";
      }
      pv_values += "]";
      tmp += pv_values;
      tmp += "}";
    }

    int en_pv_len = UtilitiesAddress.EN_PROVICES_CITYS.length;
    if(en_pv_len>0){
      tmp += ",{\"nation_code\":\"en\",";
      String pv_values = "\"values\":[";
      for (int i = 0; i < en_pv_len; i++) {
        pv_values += (i==0) ? "\"" + UtilitiesAddress.EN_PROVICES_CITYS[i] + "\"": ",\"" + UtilitiesAddress.EN_PROVICES_CITYS[i] + "\"";
      }
      pv_values += "]";
      tmp += pv_values;
      tmp += "}";
    }
    tmp += "]";
    tmp += "}';";

    tmp += "var nations = '{";
    int nations_len = UtilitiesAddress.NATION_VALUES.length;
    if(nations_len>0){
      String nation_values = "\"values\" : [\""+HomeLang.NATION_LANG[lang_value]+"\",";
      String nation_codes = "\"codes\" : [\"default\",";
      for (int i = 0; i < nations_len; i++) {
        nation_values += (i==0) ? '\"'+UtilitiesAddress.NATION_VALUES[i] + '\"' : ",\"" + UtilitiesAddress.NATION_VALUES[i]+'\"';
        nation_codes += (i==0) ? '\"'+UtilitiesAddress.NATION_CODES[i] + '\"' : ",\"" +  UtilitiesAddress.NATION_CODES[i]+'\"';
      }
      nation_values += "],";
      nation_codes += "]";

      tmp += nation_values + nation_codes;
    }
    tmp += "}';";

    tmp += "generateSelectBoxNation(\'slcNation\', nations, 0, \'col-5 pull-left\', null);";
    tmp += "generateSelectBoxProviceCity(\'slcProviceCity\', provincecity, \'col-5 pull-right\');";

    tmp += "generateHiddenInput('txtUserAddress');";
    tmp += "</script>";
    //end generate address box
    tmp += "</div>";

    tmp += "<div class=\"form-group\">";
    tmp += "<script>";
    tmp += "generateInput('txtAddressDetails','form-control','" +HomeLang.ADDRESS_DETAIL[lang_value] + "...');";
    tmp += "</script>";
    tmp += "</div>";

    tmp += "<div class=\"form-group form-submit mt-15\">";
    tmp += "<button type=\"button\" class=\"btn btn-xl btn-primary\" onclick=\"verify(this.form,'messagebox',"+lang_value+");\">"+HomeLang.CONFIRM[lang_value]+"</button>";
    tmp += "</div>";

    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"splash-footer text-center main-350 mt-10 mb-30\">";
    tmp += "<label class=\"label-bold label-hover label-bold\" onclick=\"pass(this.form);\">"+HomeLang.WILL_VERIFY_AFTER[lang_value]+"</label>";
    tmp += "</form>";
    tmp += "</div>";
    tmp += "</section>";
    return tmp;
  }

  public static String viewSectionChangePass(UserObject userSigned,byte lang_value) {
   if(userSigned==null) return null;

   String tmp = "";
   tmp += "<div class=\"splash-screen\">";
   tmp += "<form action=\"\" method=\"\" name=\"frmResetPass\">";
   tmp += "<div class=\"splash-container main-350\">";
   tmp += "<div class=\"panel panel-border-color panel-border-color-primary mt-20 mb-30\">";
   tmp += "<div class=\"panel-body\">";
   tmp += "<div class=\"form-log\">";
   tmp += "<label id=\"label-useroldpass-error\" class=\"label-bold label-danger hide\"></label>";
   tmp += "</div>";
   tmp += "<div class=\"form-group\">";
   tmp += "<input type=\"text\" class=\"form-control\" name=\"txtUserOldPass\" placeholder=\'"+HomeLang.OLD_PASS[lang_value]+"\' onkeypress=\"changeLabel(this,'label-useroldpass-error')\" >";
   tmp += "</div>";
   tmp += "<div class=\"form-log\">";
   tmp += "<label id=\"label-userpass-error\" class=\"label-bold label-danger hide\"></label>";
   tmp += "</div>";
   tmp += "<div class=\"form-group\">";
   tmp += "<input type=\"password\" class=\"form-control\" name=\"txtUserPass\" placeholder=\'"+HomeLang.NEW_PASS[lang_value]+"\' onkeypress=\"changeLabel(this,'label-userpass-error')\">";
   tmp += "</div>";
   tmp += "<div class=\"form-log\">";
   tmp += "<label id=\"label-userrepeatpass-error\" class=\"label-bold label-danger hide\"></label>";
   tmp += "</div>";
   tmp += "<div class=\"form-group\">";
   tmp += "<input type=\"password\" class=\"form-control\" name=\"txtUserRepeatPass\" placeholder=\'"+HomeLang.REPEAT_NEW_PASS[lang_value]+"\' onkeypress=\"changeLabel(this,'label-userrepeatpass-error')\">";
   tmp += "</div>";
   tmp += "<div class=\"form-group form-submit\">";
   tmp += "<button type=\"button\" class=\"btn btn-xl btn-primary\" onclick=\"changePass(this.form)\">"+HomeLang.CHANGEPASS[lang_value]+"</button>";
   tmp += "</div>";
   tmp += "</div>";
   tmp += "</div>";
   tmp += "</div>";
   tmp += "</form>";
   tmp += "</div>";

    return tmp;
  }

}
