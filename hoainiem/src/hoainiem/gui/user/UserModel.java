package hoainiem.gui.user;

import hoainiem.objects.*;
import hoainiem.*;
import java.sql.*;
import java.util.*;

public class UserModel {
  private User u;
  public UserModel(ConnectionPool cp) {
    this.u = new UserImpl(cp);
  }
  public ConnectionPool getCP(){
         return this.u.getCP();
   }

   //ra lenh tra ve ket noi
   public void releaseConnection(){
       this.u.releaseConnection();
  }

  protected void finalize() throws Throwable {
    this.u = null;
    super.finalize();
  };


  //chuyen dieu khien
  public boolean addUser(UserObject user) {
    return this.u.addUser(user);
  }
  public boolean editUser(UserObject user) {
    return this.u.editUser(user);
  }

  /*LAY RA CAC GIA TRI THUOC TINH CUA USER CAN LAY */
  public UserObject getUserObject(int id) {
    UserObject item = null;
    ResultSet rs = this.u.getUser(id);
    if (rs != null) {
      try {
        if (rs.next()) {
          item = new UserObject();

          item.setUser_id(rs.getInt("user_id"));
          item.setUser_name(rs.getString("user_name"));
          item.setUser_pass(rs.getString("user_pass"));
          item.setUser_fullname(rs.getString("user_fullname"));
          item.setUser_birthday(rs.getString("user_birthday"));
          item.setUser_mail(rs.getString("user_mail"));
          item.setUser_phone(rs.getString("user_phone"));
          item.setUser_created_date(rs.getString("user_created_date"));
          item.setUser_roles(rs.getString("user_roles"));
          item.setUser_permission(rs.getByte("user_permission"));
          item.setUser_notes(rs.getString("user_notes"));
          item.setUser_group_id(rs.getInt("user_group_id"));
          item.setUser_image(rs.getString("user_image"));
          item.setUser_last_logined(rs.getString("user_last_logined"));
          item.setUser_applydate(rs.getString("user_applydate"));
          item.setUser_gender(rs.getByte("user_gender"));
          item.setUser_actions(rs.getByte("user_actions"));
          item.setUser_address(rs.getString("user_address"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return item;
  }
  public UserObject getUserObject(String username) {
      UserObject item = null;
      ResultSet rs = this.u.getUser(username);
      if (rs != null) {
        try {
          if (rs.next()) {
            item = new UserObject();
            item.setUser_id(rs.getInt("user_id"));
            item.setUser_name(rs.getString("user_name"));
            item.setUser_fullname(rs.getString("user_fullname"));
            item.setUser_birthday(rs.getString("user_birthday"));
            item.setUser_mail(rs.getString("user_mail"));
            item.setUser_phone(rs.getString("user_phone"));
            item.setUser_created_date(rs.getString("user_created_date"));
            item.setUser_notes(rs.getString("user_notes"));
            item.setUser_group_id(rs.getInt("user_group_id"));
            item.setUser_image(rs.getString("user_image"));
            item.setUser_last_logined(rs.getString("user_last_logined"));
            item.setUser_applydate(rs.getString("user_applydate"));
            item.setUser_gender(rs.getByte("user_gender"));
            item.setUser_address(rs.getString("user_address"));
          }
        }
        catch (SQLException ex) {
          ex.printStackTrace();
        }
      }

      return item;
  }
public UserObject getUserObject(String username, String password) {

    UserObject item=null;
    ResultSet rs = this.u.getUser(username, password);
    if (rs != null) {
      try {
        if (rs.next()) {
          item = new UserObject();

          item.setUser_id(rs.getInt("user_id"));
          item.setUser_name(rs.getString("user_name"));
          item.setUser_pass(rs.getString("user_pass"));
          item.setUser_fullname(rs.getString("user_fullname"));
          item.setUser_birthday(rs.getString("user_birthday"));
          item.setUser_mail(rs.getString("user_mail"));
          item.setUser_phone(rs.getString("user_phone"));
          item.setUser_permission(rs.getByte("user_permission"));
          item.setUser_created_date(rs.getString("user_created_date"));
          item.setUser_roles(rs.getString("user_roles"));
          item.setUser_permission(rs.getByte("user_permission"));
          item.setUser_notes(rs.getString("user_notes"));
          item.setUser_group_id(rs.getInt("user_group_id"));
          item.setUser_image(rs.getString("user_image"));
          item.setUser_last_logined(rs.getString("user_last_logined"));
          item.setUser_applydate(rs.getString("user_applydate"));
          item.setUser_gender(rs.getByte("user_gender"));
          item.setUser_actions(rs.getByte("user_actions"));
          item.setUser_address(rs.getString("user_address"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return item;
  }

  public ArrayList getUserOjects(UserObject similar, int page, byte total) throws
      SQLException {
    ArrayList items = new ArrayList();
    UserObject item = null;
    //get Obj
    //lay danh sach doi tuong
    int at = (page - 1) * total;
    ResultSet rs = this.u.getUsers(similar, at, total);
    if (rs != null) {
      try{
        while (rs.next()) {
          item = new UserObject();

          item.setUser_id(rs.getInt("user_id"));
          item.setUser_name(rs.getString("user_name"));
          item.setUser_pass(rs.getString("user_pass"));
          item.setUser_fullname(rs.getString("user_fullname"));
          item.setUser_birthday(rs.getString("user_birthday"));
          item.setUser_mail(rs.getString("user_mail"));
          item.setUser_phone(rs.getString("user_phone"));
          item.setUser_permission(rs.getByte("user_permission"));
          item.setUser_created_date(rs.getString("user_created_date"));
          item.setUser_roles(rs.getString("user_roles"));
          item.setUser_permission(rs.getByte("user_permission"));
          item.setUser_notes(rs.getString("user_notes"));
          item.setUser_group_id(rs.getInt("user_group_id"));
          item.setUser_image(rs.getString("user_image"));
          item.setUser_last_logined(rs.getString("user_last_logined"));
          item.setUser_applydate(rs.getString("user_applydate"));
          item.setUser_gender(rs.getByte("user_gender"));
          item.setUser_actions(rs.getByte("user_actions"));
          item.setUser_address(rs.getString("user_address"));

          items.add(item);
        }
        rs.close();
      }catch(SQLException ex){
        ex.printStackTrace();
      }
    }

    return items;
  }

}
