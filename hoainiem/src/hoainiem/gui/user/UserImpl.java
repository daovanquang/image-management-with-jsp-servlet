package hoainiem.gui.user;

import hoainiem.*;
import hoainiem.gui.sql.*;
import hoainiem.objects.*;
import hoainiem.gui.basic.*;
import java.sql.*;

import java.sql.*;

public class UserImpl
    extends BasicImpl implements User {
  public UserImpl(ConnectionPool cp) {
    super(cp, "User");
  }

  public boolean addUser(UserObject user) {
    if (isExisting(user)) {
      return false;
    }
    String sql = "INSERT INTO tbluser(";
    sql += "user_name, user_pass, ";
    sql += "user_fullname, user_birthday, ";
    sql += "user_mail, user_phone, ";

    sql += "user_created_date, ";
    sql += " user_gender, ";
    sql += "user_image, ";

    sql += "user_parent_id , ";
    sql += "user_job, ";
    sql += "user_address ) ";

    sql += " VALUES(?,md5(?),?,?,?,?,?,?,?,?,?,?)";
    //Bien dich
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, user.getUser_name());
      pre.setString(2, user.getUser_pass());
      pre.setString(3, user.getUser_fullname());
      pre.setString(4, user.getUser_birthday());
      pre.setString(5, user.getUser_mail());
      pre.setString(6, user.getUser_phone());

      pre.setString(7, user.getUser_created_date());


      pre.setByte(8, user.getUser_gender());

      pre.setString(9, user.getUser_image());

      pre.setInt(10, user.getUser_parent_id());

      pre.setString(11, user.getUser_job());

      pre.setString(12, user.getUser_address());
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
    return false;
  }

  private boolean isExisting(UserObject item) {
    boolean flag = false;
    String sql = "SELECT * FROM tbluser WHERE ";
    sql += "user_name = '" + item.getUser_name() + "' ";
    ResultSet rs = this.gets(sql);
    if (rs != null) {
      try {
        if (rs.next()) {
          flag = true;
        }
        rs.close();
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
      finally {
        rs = null;
      }
    }
    return flag;
  }

  public boolean editUser(UserObject user) {
    String sql = "UPDATE tbluser SET ";
    sql += "user_name = ?, user_pass = md5(?), ";
    sql += "user_fullname = ?, user_birthday = ?, ";
    sql += "user_mail = ?, user_phone = ?, ";

    sql += " user_notes = ?, ";
    sql += "user_group_id =?, user_gender = ?, ";
    sql += "user_last_logined = ?, user_image =?, ";
    sql += "user_permission = ?, user_actions =? ";

    sql += "WHERE user_id = ?";

    //Bien dich
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, user.getUser_name());
      pre.setString(2, user.getUser_pass());
      pre.setString(3, user.getUser_fullname());
      pre.setString(4, user.getUser_birthday());
      pre.setString(5, user.getUser_mail());
      pre.setString(6, user.getUser_phone());
      pre.setString(7, user.getUser_notes());
      pre.setInt(8, user.getUser_group_id());
      pre.setByte(9, user.getUser_gender());
      pre.setString(10, user.getUser_last_logined());
      pre.setString(11, user.getUser_image());

      pre.setByte(12, user.getUser_permission());

      pre.setByte(13, user.getUser_actions());

      pre.setInt(14, user.getUser_id());

      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
    return false;

  }

  public ResultSet getUser(int id) {
    String sql = "SELECT * FROM tbluser WHERE user_id = "+id+"";
    return this.gets(sql);
  }

  /*==========- info for signup -========*/
  public ResultSet getUser(String username) {
    String sql = "SELECT * FROM tbluser WHERE user_name = '"+username+"'";
    return this.gets(sql);
  }

  public ResultSet getUser(String username, String userpass) {
    String sql =
        "SELECT * FROM tbluser WHERE (user_name = ?) AND  (user_pass = ?)";
    return this.get(sql, username, userpass);
  }

  public ResultSet getUsers(UserObject similar, int at, byte total) {
    String sql = "SELECT * FROM tbluser ";
    sql += "ORDER BY user_id ASC ";
    sql += "LIMIT " + at + "," + total;
    return this.gets(sql);
  }

}
