package hoainiem.gui.user;

import hoainiem.*;
import hoainiem.gui.user.*;
import hoainiem.objects.*;
import java.util.*;
import java.sql.SQLException;

public class UserControl {
  UserModel um;
  public UserControl(ConnectionPool cp) {
    um = new UserModel(cp);
  }
  public ConnectionPool getCP(){
       return this.um.getCP();
   }

   //ra lenh tra ve ket noi
   public void releaseConnection(){
       this.um.releaseConnection();
   }
  //chuyen dieu khien
  public boolean addUser(UserObject user) {
      return this.um.addUser(user);
  }
  public boolean editUser(UserObject user) {
    return this.um.editUser(user);
  }

  public UserObject getUserObject(int id){
    return this.um.getUserObject(id);
  }
  public UserObject getUserObject(String username){
    return this.um.getUserObject(username);
  }
  public UserObject getUserObject(String username, String password){
    return this.um.getUserObject(username, password);
  }
  public String viewSectionSignin(byte lang_value){
    return UserLibrary.viewSectionSignin(lang_value);
  }
  public String viewSectionSignup(byte lang_value){
    return UserLibrary.viewSectionSignup(lang_value);
  }
  public String viewSectionConfirm(UserObject nUser, byte lang_value){
    return UserLibrary.viewSectionConfirm(nUser, lang_value);
  }
  public String viewSectionCreatePass(byte lang_value){
    return UserLibrary.viewSectionCreatePass(lang_value);
  }
  public String viewSectionVerify(UserObject userConfirmed, byte lang_value){
    return UserLibrary.viewSectionVerify(userConfirmed, lang_value);
  }
  public String viewSectionChangePass(UserObject userSigned, byte lang_value){
    return UserLibrary.viewSectionChangePass(userSigned, lang_value);
  }

}
