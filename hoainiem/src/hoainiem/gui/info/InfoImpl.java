package hoainiem.gui.info;

import java.sql.*;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.basic.*;
public class InfoImpl extends BasicImpl implements Info {
  public InfoImpl(ConnectionPool cp) {
    super(cp, "InfoGUI");
  }

  public ResultSet getInfos(InfoObject similar, int at, byte total) {
    String sql = "SELECT * FROM tblinfo ";
    sql += "WHERE info_enable = true ";
    sql += "ORDER BY info_id ";
    sql += "LIMIT "+at+","+total;
    return this.gets(sql);
  }

}
