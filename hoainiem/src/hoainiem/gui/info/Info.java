package hoainiem.gui.info;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface Info extends ShareControl{
  public ResultSet getInfos(InfoObject similar, int at, byte total);
}
