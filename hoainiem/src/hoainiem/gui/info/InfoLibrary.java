package hoainiem.gui.info;

import hoainiem.objects.*;
import hoainiem.lang.gui.*;
import java.util.*;

public class InfoLibrary {
  public InfoLibrary() {
  }

  public static String viewServices(ArrayList<InfoObject> items, byte lang_value) {
    String tmp = "";
    tmp += "<section class='services slider pull-left'>";
    tmp += "<div class = 'section-heading'>";
    tmp += "<h5 class ='pull-left'>"+HomeLang.SERVICES[lang_value]+"</h5>";
    tmp += "</div >";

    tmp += "<div id=\"Services\">";
    tmp += "<div class=\"slider__container relative\">";
    tmp += "<div class=\"slider__track\" style=\"width: auto;transform: translate3d(0,0,0);\">";
    tmp += "<div class=\"boxs\">";
    for( InfoObject item:items){
      tmp += "<div class=\"box slider__item text-center\" style=\"width: calc(50% - 2px)\">";
      tmp += "<div class=\"box-padding\">";
      tmp += "<h3 class=\"text-uppercase\">";
      tmp += (lang_value == 1) ? item.getInfo_title_en() : item.getInfo_title();
      tmp += "</h3>";
      tmp += "<h4>";
      tmp += (lang_value == 1) ? item.getInfo_primary_title_en() : item.getInfo_primary_title();
      tmp += "</h4>";
      tmp += "<p>";
      tmp += (lang_value == 1) ? item.getInfo_content_en() : item.getInfo_content();
      tmp += "</p>";
      tmp += "<div class=\"link-padding\">";
      tmp += "<a href=\'"+item.getInfo_link_source()+"\' class=\'text-link\'>";
      tmp += (lang_value == 1) ? item.getInfo_link_text_en() : item.getInfo_link_text();
      tmp += "</a>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";
    }
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"slider__dots text-center\">";
    for (InfoObject item : items) {
      tmp += "<div class=\"dot\"></div>";
    }
    tmp += "</div>";
    tmp += "</div>";

    return tmp;
  }
}
