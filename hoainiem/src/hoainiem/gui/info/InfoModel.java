package hoainiem.gui.info;

import hoainiem.*;
import hoainiem.objects.*;

import java.sql.*;
import java.util.*;

public class InfoModel {
  private Info i;
  public InfoModel(ConnectionPool cp) {
    i = new InfoImpl(cp);
  }
  public ConnectionPool getCP(){
    return this.i.getCP();
  }
  public void releaseConnection(){
    this.i = null;
  }
  public ArrayList<InfoObject> getInfoObjects(InfoObject similar, int at, byte total){
    ArrayList<InfoObject> items = new ArrayList<InfoObject> ();
    InfoObject item = null;
    ResultSet rs = this.i.getInfos(similar, at, total);
    if (rs != null) {
      try {
        while (rs.next()) {
          item = new InfoObject();
          item.setInfo_title(rs.getString("info_title"));
          item.setInfo_primary_title(rs.getString("info_primary_title"));
          item.setInfo_content(rs.getString("info_content"));
          item.setInfo_link_text(rs.getString("info_link_text"));
          item.setInfo_link_source(rs.getString("info_link_source"));
          item.setInfo_link_text_en(rs.getString("info_link_text_en"));
          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return items;
  }
}
