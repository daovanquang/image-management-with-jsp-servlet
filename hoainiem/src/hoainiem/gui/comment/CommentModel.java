package hoainiem.gui.comment;

import hoainiem.*;
import hoainiem.objects.*;
import java.util.*;
import java.sql.*;

public class CommentModel {
  private Comment c;
  public CommentModel(ConnectionPool cp) {
    this.c = new CommentImpl(cp);
  }

  //lay chuoi ket noi
  public ConnectionPool getCP() {
    return this.c.getCP();
  }

  public void finalize() throws Throwable {
    this.c = null;
  }

  public void releaseConnection() {
    this.c = null;
  }

  //chuyen dkhien
  public boolean addComment(CommentObject item) {
    return this.c.addComment(item);
  }

  public boolean editComment(CommentObject item) {
    return this.c.editComment(item);
  }

  public boolean delComment(CommentObject item) {
    return this.c.delComment(item);
  }

  public CommentObject getComment(int id) {
    CommentObject item = null;
    ResultSet rs = this.c.getComment(id);
    if (rs != null) {
      item = new CommentObject();
      try {
        if (rs.next()) {
          item.setComment_article_id(rs.getInt("comment_article_id"));
          item.setComment_author_image(rs.getString("comment_author_image"));
          item.setComment_author_name(rs.getString("comment_author_name"));
          item.setComment_content(rs.getString("comment_content"));
          item.setComment_content_en(rs.getString("comment_content_en"));
          item.setComment_created_date(rs.getString("comment_created_date"));
          item.setComment_id(rs.getInt("comment_id"));
          item.setComment_last_modified(rs.getString("comment_last_modified"));
          item.setComment_tag(rs.getString("comment_tag"));
          item.setComment_enable(rs.getBoolean("comment_enable"));
          item.setComment_author_permission(rs.getByte("comment_author_permission"));
          item.setComment_parent_id(rs.getInt("comment_parent_id"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return item;
  }

  public ArrayList<CommentObject> getComments(CommentObject similar, int at, byte total) {
    ArrayList<CommentObject> items = new ArrayList<CommentObject>();
    CommentObject item = null;
    ResultSet rs = this.c.getComments(similar, at, total);
    if (rs != null) {
      try {
        while (rs.next()) {
          item = new CommentObject();
          item.setComment_article_id(rs.getInt("comment_article_id"));
          item.setComment_author_image(rs.getString("comment_author_image"));
          item.setComment_author_name(rs.getString("comment_author_name"));
          item.setComment_content(rs.getString("comment_content"));
          item.setComment_content_en(rs.getString("comment_content_en"));
          item.setComment_created_date(rs.getString("comment_created_date"));
          item.setComment_id(rs.getInt("comment_id"));
          item.setComment_last_modified(rs.getString("comment_last_modified"));
          item.setComment_tag(rs.getString("comment_tag"));
          item.setComment_enable(rs.getBoolean("comment_enable"));
          item.setComment_author_permission(rs.getByte("comment_author_permission"));
          item.setComment_parent_id(rs.getInt("comment_parent_id"));
          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return items;
  }

}
