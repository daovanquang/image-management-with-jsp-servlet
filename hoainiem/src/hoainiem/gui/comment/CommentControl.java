package hoainiem.gui.comment;

import hoainiem.*;
import hoainiem.objects.*;
import java.util.*;
import java.sql.*;

public class CommentControl {
  private CommentModel cm;
  public CommentControl(ConnectionPool cp) {
    this.cm = new CommentModel(cp);
  }

  //lay chuoi ket noi
  public ConnectionPool getCP() {
    return this.cm.getCP();
  }

  public void finalize() throws Throwable {
    this.cm = null;
  }

  public void releaseConnection() {
    this.cm = null;
  }

  //chuyen dkhien
  public boolean addComment(CommentObject item) {
    return this.cm.addComment(item);
  }

  public boolean editComment(CommentObject item) {
    return this.cm.editComment(item);
  }

  public boolean delComment(CommentObject item) {
    return this.cm.delComment(item);
  }

  public CommentObject getComment(int id) {
   return this.cm.getComment(id);
  }

  public ArrayList<CommentObject> getComments(CommentObject similar, int at, byte total) {
    return this.cm.getComments(similar, at, total);
  }

}
