package hoainiem.gui.comment;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface Comment extends ShareControl{
  boolean addComment(CommentObject item);
  boolean editComment(CommentObject item);
  boolean delComment(CommentObject item);

  public ResultSet getComment(int id);
  public ResultSet getComments(CommentObject similar, int at, byte total);

}
