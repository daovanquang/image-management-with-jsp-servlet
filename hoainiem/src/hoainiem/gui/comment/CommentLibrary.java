package hoainiem.gui.comment;

import hoainiem.objects.*;
import java.util.*;
import hoainiem.library.*;
import hoainiem.lang.gui.*;

public class CommentLibrary {
  public CommentLibrary() {
  }

  public static String viewComments(ArrayList<CommentObject> items, UserObject user, byte lang_value) {
    String tmp ="";
    tmp += "<div class=\"comment__footer\">";
    //child comment
    for(CommentObject item: items){

      tmp += "<div class=\"rows comment parent-comment\">";

      tmp += "<div class=\"comment__heading\">";
      tmp += "<div class=\'pull-left quickview quickview-md\'>";
      tmp += "<img src=\'"+item.getComment_author_image()+"\' alt=\"\">";
      tmp += "</div>";

      tmp += "<div class=\'quickview-properties quickview-properties-md\'>";
      tmp += "<div class=\"row\">";
      tmp += "<a href=\"\">";
      tmp += "<h3>"+item.getComment_author_name()+"</h3>";
      tmp += "</a>";
      tmp += "</div>";

      tmp += "<div class=\"comment__control row\">";

      byte authorpermis = Utilities.getByteValue(item.getComment_author_permission());
      byte userpermis = 0;
      String authorname = Utilities.getValue(item.getComment_author_name());
      String username = "";
      if(user !=null) {
        userpermis = Utilities.getByteValue(user.getUser_permission());
        username = Utilities.getValue(user.getUser_name());
      }
      if((authorpermis > 0 && Utilities.getByteValue(user.getUser_permission()) >= authorpermis)||(authorname != "" && username== authorname)){
        tmp += "<a href=\'/home/comment/ae?cid==" + item.getComment_id() +"\' class=\"\"><span class=\"comment-edit label-bold mr-5\">"+HomeLang.EDIT[lang_value]+"</span></a>";
        tmp += "<a href=\'/home/comment/del?cid=" + item.getComment_id() + "\' class=\"link-no-color link-danger\"><span class=\"comment-delete label-bold mr-5\">"+HomeLang.DEL[lang_value]+"</span></a>";
      }
      tmp += "<span class=\"link-gray\">"+item.getComment_created_date()+"</span>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";

      tmp += "<div class=\"comment__body\">";

      String content = (lang_value == 1) ? item.getComment_content_en():item.getComment_content();
      tmp += "<p>" + content + "</p>";
      tmp += "</div>";

      tmp += "<div class=\"comment__images images\">";
      tmp += "</div>";

      tmp += "</div>";
    }
    tmp+="</div>";

    return tmp;
  }

}
