package hoainiem.gui.comment;

import java.sql.*;
import hoainiem.*;
import hoainiem.gui.basic.*;
import hoainiem.objects.*;
import hoainiem.gui.sql.*;

public class CommentImpl
    extends BasicImpl implements Comment {
  public CommentImpl(ConnectionPool cp) {
    super(cp, "CommentGUI");
  }

  /**
   * addComment
   *
   * @param item CommentObject
   * @return boolean
   * @todo Implement this hoainiem.gui.comment.Comment method
   */
  public boolean addComment(CommentObject item) {
    String sql = "INSERT INTO tblcomment ( ";
    sql += "comment_content, ";
    sql += "comment_created_date, comment_author_name, ";
    sql += "comment_last_modified , comment_tag, ";
    sql += "comment_parent_id , comment_article_id, ";
    sql += "comment_enable ,comment_author_image, ";
    sql += "comment_author_permission, comment_content_en ";
    sql += ") ";
    sql += "VALUES(?,?,?,?,?,?,?,?,?,?,?)";

    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getComment_content());
      pre.setString(2, item.getComment_created_date());
      pre.setString(3, item.getComment_author_name());
      pre.setString(4, item.getComment_last_modified());
      pre.setString(5, item.getComment_tag());
      pre.setInt(6, item.getComment_parent_id());
      pre.setInt(7, item.getComment_article_id());
      pre.setBoolean(8, item.isComment_enable());
      pre.setString(9, item.getComment_author_image());
      pre.setByte(10, item.getComment_author_permission());
      pre.setString(11, item.getComment_content_en());

      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  /**
   * editComment
   *
   * @param item CommentObject
   * @return boolean
   * @todo Implement this hoainiem.gui.comment.Comment method
   */
  public boolean editComment(CommentObject item) {
    String sql = "UPDATE tblcomment SET";
    sql += "comment_content = ?, ";
    sql += "comment_created_date = ?, comment_author_name = ?, ";
    sql += "comment_last_modified =?, comment_tag = ?, ";
    sql += "comment_parent_id =?, comment_article_id = ?, ";
    sql += "comment_enable =?,comment_author_image = ?, ";
    sql += "comment_author_permission =?, comment_content_en = ? ";
    sql += "WHERE comment_id = ?";

    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getComment_content());
      pre.setString(2, item.getComment_created_date());
      pre.setString(3, item.getComment_author_name());
      pre.setString(4, item.getComment_last_modified());
      pre.setString(5, item.getComment_tag());
      pre.setInt(6, item.getComment_parent_id());
      pre.setInt(7, item.getComment_article_id());
      pre.setBoolean(8, item.isComment_enable());
      pre.setString(9, item.getComment_author_image());
      pre.setByte(10, item.getComment_author_permission());
      pre.setString(11, item.getComment_content_en());
      pre.setInt(12, item.getComment_id());
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;

  }

  /**
   * delComment
   *
   * @param item CommentObject
   * @return boolean
   * @todo Implement this hoainiem.gui.comment.Comment method
   */
  public boolean delComment(CommentObject item) {
    String sql = "UPDATE tblcomment SET comment_deleted = 1, comment_deleted_date = ? WHERE ";
    sql += MakeConditions.conditionsDelComment(item);
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getComment_deleted_date());
      return this.del(pre);
    }
    catch (SQLException ex) {
    }

    return false;
  }

  /**
   * getComment
   *
   * @param id int
   * @return ResultSet
   * @todo Implement this hoainiem.gui.comment.Comment method
   */
  public ResultSet getComment(int id) {
    String sql = "SELECT * FROM tblcomment WHERE (comment_deleted = 0) comment_id = ?";
    return this.get(sql, id);
  }

  /**
   * getComments
   *
   * @param similar CommentObject
   * @param at int
   * @param total byte
   * @return ResultSet
   * @todo Implement this hoainiem.gui.comment.Comment method
   */
  public ResultSet getComments(CommentObject similar, int at, byte total) {
    String conds = MakeConditions.conditionsForComment(similar);
    String sql = "SELECT * FROM tblcomment ";
    sql += "WHERE  (comment_deleted = 0) AND ( comment_enable = 1 ) ";
    if(!conds.equalsIgnoreCase("")){
      sql+= conds;
    }
    sql+= "ORDER BY comment_id DESC ";
    sql+= "LIMIT "+at+","+total;

    return this.gets(sql);
  }
}
