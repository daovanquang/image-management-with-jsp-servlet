package hoainiem.gui.image;

import java.util.*;

import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.lang.gui.*;
import hoainiem.values.gui.*;

public class ImageLibrary {
  public ImageLibrary() {
  }

  //for articles
  public static String viewImages(ArrayList<ImageObject> items) {
    if(items.size() == 0){
      return null;
    }

    String tmp = "";
    tmp += "<div class=\"article__images\">";
    tmp += ImageLibrary.viewListCardComponent(items, "image-list--small",true,(byte)0);
    tmp += "</div>";
    return tmp;
  }

  public static String viewSlider(ArrayList<ImageObject> items) {
    String tmp = "";
    byte size = (byte)items.size();
    int count = 0;
    if(size == 0){
          return tmp;
    }
    int wrap_size = size*1200;
    String item_size = "width: 1200px; height:440px;";

    tmp += "<section class=\"slider\">";
    tmp += "<div id=\"MainSlider\" class=\"main-1200 row mt-20\">";
    tmp += "<div class=\"slider__container relative\">";
    tmp += "<div class=\"slider__track\" style=\"width: "+wrap_size+"px;transform: translate3d(0,0,0);\">";
    for(ImageObject item:items){
      tmp += "<a href=\"#\">";
      tmp += "<img class=\"slider__item\" src=\'"+item.getImage_source()+"\' style=\'"+ item_size +"\' alt=\"\">";
      tmp += "</a>";

      count++;
    }
    tmp += "</div>";

    tmp += "<div class=\"slider__control slider__control--center\">";
    tmp += "<i class=\"prev-next-btn absolute prev-btn ic ic_utl-md ic_arwleft-lg ic-lg\"></i>";
    tmp += "<i class=\"prev-next-btn absolute next-btn ic ic_utl-md ic_arwright-lg ic-lg\"></i>";
    tmp += "</div>";

    tmp += "<div class=\"slider__dots text-center\">";
    for(int i = 0; i < count; i++){
      tmp += "<span class=\"dot\"></span>";
    }
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</section>";

    return tmp;
  }

  //for image card component
  public static String viewListCardComponent(ArrayList<ImageObject> items, String typeListClass, boolean onlyImage, byte lang_value){
    String tmp = "";
    String webModule = AllGUI.WebModule;

    tmp += "<div class=\"image-list "+typeListClass+"\">";
    for (ImageObject item : items) {
     tmp += (!onlyImage)? "<div class=\"image-card image-card--bg-skin\">" : "<div class=\"image-card\">";
     tmp += (!onlyImage)? "<a href=\"/"+webModule+"/photos/?id="+item.getImage_id()+"\">":"";
     tmp += "<div class=\"card__img-cover\">";
     tmp += "<img class=\"img-fill\" src=\'" + item.getImage_source() +"\' alt=\'"+item.getImage_name()+"\'>";
     tmp += "</div>";
     if(!onlyImage){
       String title = (lang_value == 0) ? item.getImage_title() : item.getImage_title_en();
       String tag = (lang_value == 0) ? item.getImage_tag() : item.getImage_tag_en();
       tag = Utilities.getValue(tag);
       tmp += "<div class=\"card__padding\">";
       tmp += "<div class=\"card__details\">";
       tmp += "<h3 class=\"details__title\">";
       tmp += title;
       tmp += "</h3>";
       tmp += "<div class=\"details__description\">";
       tmp += "<span class=\"pull-left\">"+ tag +"</span>";
       tmp += "<span class=\"pull-right\">"+ item.getImage_visited() +"</span>";
       tmp += "</div>";
       tmp += "</div>";
       tmp += "</div>";
     }
     tmp += (!onlyImage)? "</a>":"";
     tmp += "</div>";
   }
   tmp += "</div>";

   return tmp;
  }

  public static String viewNewestPhotos(ArrayList<ImageObject> items, byte lang_value) {
    String tmp = "";
    tmp += "<section class = \"newest-photos row\">";
    tmp += "<div class=\"section-heading text-center\">";
    tmp += "<h5 class=\"pull-left\">" + HomeLang.NEWEST_PHOTOS[lang_value] +"</h5>";
    tmp += "<a href=\"photos/control/?tab=0\" class=\"see-more pull-left\">"+HomeLang.SEEMORE[lang_value]+"</a>";
    tmp += "</div>";
    tmp += ImageLibrary.viewListCardComponent(items, "image-list--extra-large",false,lang_value);
    tmp += "</section>";
    return tmp;
  }

  public static String viewAllPhoto(ArrayList<ImageObject> items, byte lang_value) {
    String tmp = "";
    if(items.size()==0) return tmp;
    tmp += "<section class=\"all-image row\">";
    tmp += "<div class=\"section-heading text-center\">";
    tmp += "<h5 class=\"pull-left\">"+HomeLang.ALL_PHOTO[lang_value]+"</h5>";
    tmp += "</div>";

    tmp += ImageLibrary.viewListCardComponent(items, "image-list--extra-large",false,lang_value);

    tmp += "</section>";

    tmp += "<section class=\"section-seemore row text-center mt-10 mb-20\">";
    tmp += "<a href=\"photos/control/?tab=0\">";
    tmp += "<button type=\"button\" class=\"btn btn-outline label-bold\">" + HomeLang.SEEMORE[lang_value] + "</button>";
    tmp += "</a>";
    tmp += "</section>";

    return tmp;
  }

  public static String viewImpressivePhotos(ArrayList<ImageObject> items, byte lang_value) {
    String tmp = "";
    if (items.size() == 0) {
      return tmp;
    }

    tmp += "<section id=\"ImpressivePhotos\" class=\"impressive-photos slider\">";
    tmp += "<section class=\"section-heading text-center\">";
    tmp +="<h5 class=\"pull-left\">"+HomeLang.IMPRESSIVE_PHOTOS[lang_value]+"</h5>";
    tmp += "<a href=\"photos/control/?tab=1\" class=\"see-more pull-left\">"+HomeLang.SEEALL[lang_value]+"</a>";
    tmp += "</section>";

    tmp += "<div class=\"slider__container  relative\">";
    tmp += "<div class=\"slider__track image-list\" style=\"width: 9600px;transform: translate3d(0,0,0);\">";

    for (ImageObject item : items) {
      String tag = (lang_value == 0) ? item.getImage_tag() : item.getImage_tag_en();
      tag = Utilities.getValue(tag);

      tmp += "<div class=\"slider__item\" style=\"width: 300px;margin: 0 2px;\">";
      tmp += "<a href=\"\">";
      tmp += "<div class=\"image-card image-card--bg-skin\">";
      tmp += "<div class=\"card__img-cover\">";
      tmp += "<img class=\"img-fill\" src=\'" + item.getImage_source() + "\' alt=\"\">";
      tmp += "</div>";
      tmp += "<div class=\"card__padding\">";
      tmp += "<div class=\"card__details\">";
      tmp += "<h3 class=\"details__title\">";
      tmp += item.getImage_title();
      tmp += "</h3>";
      tmp += "<div class=\"details__description\">";
      tmp += "<span class=\"pull-left\">" + UtilitiesExtends.getLastName(item.getImage_author_name(),lang_value) + "</span>";
      tmp += "<span class=\"pull-right\">" + item.getImage_created_date() + "</span>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "<div class=\"card__footer row\">";
      tmp += "<div class=\"card-carer pull-left\">";
      tmp += "<i class=\"ic ic-md ic_gui-md ic_heart-md pull-left\"></i>";
      tmp += "<span  class=\'ml-5\'>" + item.getImage_visited() + "</span>";
      tmp += "</div>";
      tmp += "<div class=\"card-tag pull-right\">";
      tmp += "<span class=\'ml-5\'>"+ tag +"</span>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</a>";
      tmp += "</div>";
    }
    tmp += "</div>";

    tmp += "<div class=\"slider__control slider__control--center\">";
    tmp += "<i class=\"prev-next-btn absolute prev-btn ic ic_utl-md ic_arwleft-md ic-md\"></i>";
    tmp += "<i class=\"prev-next-btn absolute next-btn ic ic_utl-md ic_arwright-md ic-md\"></i>";
    tmp += "</div>";

    tmp += "</div>";

    return tmp;
  }

  public static String viewRecentPhotos(ArrayList<ImageObject> items, byte lang_value){
    String tmp = "";
    if(items.size()==0) return tmp;

    tmp += "<section class=\"recent-photos\">";
    tmp += "<div class=\"heading\">";
    tmp += "<h3>"+HomeLang.RECENT_PHOTOS[lang_value]+"</h3>";
    tmp += "</div>";
    tmp += "<div class=\"recent-list row\">";
    tmp += ImageLibrary.viewListCardComponent(items, "image-list--recent-photos",true,lang_value);
    tmp += "</div>";
    tmp += "</section>";

    return tmp;
  }

  public static String viewAlbum(ArrayList<ImageObject>  items, byte tab_index, byte maxPage, byte page_index, byte lang_value){

    page_index = (page_index<=maxPage) ? page_index : 0;
    //declare paths
    String pathControl = "control/";
    //declare list type sort
    String []listSortField = PhotosPage.SORT_FIELDS;
    byte lenField = (byte)listSortField.length;
    boolean []listSortType = PhotosPage.SORT_TYPE;
    byte lenStype = (byte)listSortType.length;
    int len =  lenField * lenStype;
    byte index = 0;
    byte isDESC = 0;

    String tmp = "";

    tmp += "<section id=\"section-album\" class=\"section-album mb-20\">";tmp += "<div class=\"main-1200\">";
    tmp += "<nav class=\"section-heading relative\">";
    tmp += "<a href=\""+pathControl+"?tab=1&page="+page_index+"\">";
    tmp += "<span class=\"tab-control btn btn-outline btn-sm";
    tmp += (tab_index == 1) ? " active":"";
    tmp += "\">" +HomeLang.IMPRESSIVE_PHOTOS[lang_value]+ "</span>";
    tmp += "</a>";
    tmp += "<a href=\""+pathControl+"?tab=0&page="+page_index+"\">";
    tmp += "<span class=\"tab-control btn btn-outline btn-sm";
    tmp += (tab_index == 0) ? " active":"";
    tmp += "\">" +HomeLang.ALL_PHOTO[lang_value]+ "</span>";
    tmp += "</a>";
    tmp += "<span class=\"section-heading__sort pull-right\">";
    tmp += "<form action=\"\" method=\"\">";

    tmp += "<select name=\"slcSort\" class=\"select\" onchange=\"sort(this.form);\">";
    tmp += "<option value='0'>" +HomeLang.SORT[lang_value]+ "</option>";

    for(byte i=0; i<len;i++){
      if(i%2==0) isDESC = 1;
      else isDESC = 0;
      if(index == lenField) index = 0;

      tmp += "<option value='"+UtilitiesExtends.encodeSortValue(index,isDESC)+"'>";
      tmp += (i==0) ? HomeLang.NEWEST[lang_value] : (i==1) ? HomeLang.OLDEST[lang_value] : (i==2) ? HomeLang.DESC_VISITED[lang_value] : HomeLang.ASC_VISITED[lang_value];
      tmp += "</option>";

      if(i + 1 == lenStype) index++;
    }

    tmp += "</select>";
    tmp += "<input type='hidden' name='txtTabActive' value='"+tab_index+"'>";
    tmp += "</form>";
    tmp += "</span>";
    tmp += "</nav>";

    tmp += ImageLibrary.viewListCardComponent(items, "tab-item image-list--extra-large",false,lang_value);

    tmp += "</div>";
    tmp += "</section>";

    /*paging*/
    tmp += "<script>";
    tmp += "generatePaging(\"paging-content\");";
    tmp += "setPaging(3, 3, "+maxPage+", "+page_index+", '"+pathControl+"?tab="+tab_index+"&page=', 'paging-content');";
    tmp += "</script>";
    return tmp;
  }
}
