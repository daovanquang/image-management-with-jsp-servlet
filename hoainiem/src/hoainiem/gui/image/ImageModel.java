package hoainiem.gui.image;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;
import java.util.*;

public class ImageModel {
  private Image i;

  public ImageModel(ConnectionPool cp) {
    this.i = new ImageImpl(cp);
  }

  public ConnectionPool getCP() {
    return this.i.getCP();
  }
  public void finalize() throws Throwable{
    this.i = null;
  };
  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.i = null;
  }

  public boolean addImage(ImageObject item) {
    return this.i.addImage(item);
  }

  public boolean delImage(ImageObject item) {
    return this.i.delImage(item);
  }

  public boolean editImage(ImageObject item) {
    return this.i.editImage(item);
  }

  public int getTotal(ImageObject similar) {
    int total = 0;

    ResultSet rs = this.i.getTotal(similar);
    if (rs != null) {
      try {
        if (rs.next()) {
          total = rs.getInt(1);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return total;
  }
  /*LAY RA CAC GIA TRI THUOC TINH CUA IMAGE CAN LAY */
  public ImageObject getImageObject(int id) {
    ImageObject item = null;

    ResultSet rs = this.i.getImage(id);
    if (rs != null) {
      try {
        if (rs.next()) {
          item = new ImageObject();
          item.setImage_id(rs.getInt("image_id"));
          item.setImage_title(rs.getString("image_title"));
          item.setImage_source(rs.getString("image_source"));
          item.setImage_created_date(rs.getString("image_created_date"));
          item.setImage_author_name(rs.getString("image_author_name"));
          item.setImage_title_en(rs.getString("image_title_en"));
          item.setImage_tag(rs.getString("image_tag"));
          item.setImage_tag_en(rs.getString("image_tag_en"));
          item.setImage_visited(rs.getInt("image_visited"));
          item.setImage_language(rs.getString("image_language"));
          item.setImage_last_modified(rs.getString("image_last_modified"));
          item.setImage_author_permission(rs.getByte("image_author_permission"));
          //item.setImage_notes(rs.getString("image_notes"));
          item.setImage_article_id(rs.getInt("image_article_id"));
          item.setImage_enable(rs.getBoolean("image_enable"));
          item.setImage_forslide(rs.getBoolean("image_forslide"));
          item.setImage_name(rs.getString("image_name"));
          item.setImage_author_image(rs.getString("image_author_image"));
          item.setImage_forimpressive(rs.getBoolean("image_forimpressive"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return item;
  }

  public ArrayList<ImageObject> getImageObjects(ImageObject similar, int at, byte total, String field_value, boolean isDESC){
    ArrayList<ImageObject> items = new ArrayList<ImageObject>();
    ImageObject item = null;

    ResultSet rs = this.i.getImages(similar, at, total, field_value,isDESC);
    if (rs != null) {
      try {

        while (rs.next()) {
          item = new ImageObject();
          item.setImage_id(rs.getInt("image_id"));
          item.setImage_title(rs.getString("image_title"));
          item.setImage_source(rs.getString("image_source"));
          item.setImage_created_date(rs.getString("image_created_date"));
          item.setImage_author_name(rs.getString("image_author_name"));
          item.setImage_title_en(rs.getString("image_title_en"));
          item.setImage_tag(rs.getString("image_tag"));
          item.setImage_tag_en(rs.getString("image_tag_en"));
          item.setImage_visited(rs.getInt("image_visited"));
          item.setImage_language(rs.getString("image_language"));
          item.setImage_last_modified(rs.getString("image_last_modified"));
          item.setImage_author_permission(rs.getByte("image_author_permission"));
          //item.setImage_notes(rs.getString("image_notes"));
          item.setImage_article_id(rs.getInt("image_article_id"));
          item.setImage_enable(rs.getBoolean("image_enable"));
          item.setImage_forslide(rs.getBoolean("image_forslide"));
          item.setImage_name(rs.getString("image_name"));
          item.setImage_author_image(rs.getString("image_author_image"));
          item.setImage_forimpressive(rs.getBoolean("image_forimpressive"));
          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return items;
  }



}
