package hoainiem.gui.image;

import java.sql.*;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.basic.*;
import hoainiem.gui.sql.*;
import hoainiem.sql.*;
import hoainiem.library.*;

public class ImageImpl
    extends BasicImpl implements Image {
  public ImageImpl(ConnectionPool cp) {
    super(cp, "ImageGUI");
  }

  public boolean addImage(ImageObject item) {
    String []fields = ImageSQL.FIELDS;
    int []notInsert = {0,20,21,22};
    String sql = BasicSQL.sqlINSERT("image",fields,notInsert);

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getImage_title());
      pre.setString(2, item.getImage_source());
      pre.setString(3, item.getImage_created_date());
      pre.setString(4, item.getImage_author_name());
      pre.setString(5, item.getImage_title_en());
      pre.setString(6, item.getImage_tag());
      pre.setString(7, item.getImage_tag_en());
      pre.setString(8, item.getImage_language());
      pre.setInt(9, item.getImage_author_permission());
      pre.setString(10, item.getImage_notes());
      pre.setInt(11, item.getImage_article_id());
      pre.setBoolean(12, item.isImage_enable());
      pre.setInt(13, item.getImage_visited());
      pre.setString(14, item.getImage_last_modified());
      pre.setInt(15, item.getImage_group_id());
      pre.setBoolean(16, item.isImage_forslide());
      pre.setString(17, item.getImage_name());
      pre.setString(18, item.getImage_author_image());
      pre.setBoolean(19, item.isImage_forimpressive());

      //thuc thi
      return this.add(pre);
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return false;
  }

  public boolean delImage(ImageObject item) {
    String sql = "UPDATE tblimage SET image_deleted = 1, image_deleted_date = ? WHERE ";
    sql += MakeConditions.conditionsDelImage(item);
    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1,item.getImage_deleted_date());
      //thuc thi
      return this.del(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean editImage(ImageObject item) {
    String sql = "UPDATE tblimage SET ";
    sql += "image_title = ?, image_created_date = ?, ";
    sql += "image_author_name = ?, image_title_en = ?, ";
    sql += "image_tag = ?, image_tag_en = ?, ";
    sql += "image_visited = ?, image_language = ?, ";
    sql += "image_author_permission = ?, image_notes = ?, ";
    sql += "image_article_id = ?, image_enable = ?,  ";
    sql += "image_last_modified = ?, image_source = ?, ";
    sql += "image_group_id = ?, image_forslide = ?, ";
    sql += "image_name = ?, image_author_image = ?, ";
    sql += "image_forimpressive = ? ";
    sql += "WHERE image_id = ?";


    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getImage_title());
       pre.setString(2, item.getImage_created_date());
       pre.setString(3, item.getImage_author_name());
       pre.setString(4, item.getImage_title_en());
       pre.setString(5, item.getImage_tag());
       pre.setString(6, item.getImage_tag_en());
       pre.setInt(7, item.getImage_visited());
       pre.setString(8, item.getImage_language());
       pre.setInt(9, item.getImage_author_permission());
       pre.setString(10, item.getImage_notes());
       pre.setInt(11,item.getImage_article_id());
       pre.setBoolean(12,item.isImage_enable());
       pre.setString(13,item.getImage_last_modified());
       pre.setString(14, item.getImage_source());
       pre.setInt(15, item.getImage_group_id());
       pre.setBoolean(16, item.isImage_forslide());
       pre.setString(17, item.getImage_name());
       pre.setString(18, item.getImage_author_image());
       pre.setBoolean(19, item.isImage_forimpressive());
       pre.setInt(20, item.getImage_article_id());

      //thuc thi
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public ResultSet getTotal(ImageObject similar) {
    String []fields = ImageSQL.FIELDS;
    //conditions to select
    int []conditions = null;
    String []values = null;
    if(similar!=null){
      conditions = new int[1];
      values = new String[1];
      if(similar.isImage_forimpressive()){
        conditions[0] = 19;
        values[0] = "1";
      }
    }

    String sql = BasicSQL.sqlCOUNT_BY_CONDS(null, "image", fields, conditions, values);
    return this.gets(sql);
  }

  public ResultSet getImage(int id) {
    String []fields = ImageSQL.FIELDS;
    int []notSelect = {10,21,22};
    String conds = "(image_deleted = 0) AND (image_id = ?)";
    String sql = BasicSQL.sqlSELECT_FIELDS("image", fields, notSelect,conds);
    return this.get(sql, id);
  }

  public ResultSet getImages(ImageObject similar, int at, byte total, String field_name , boolean isDESC){
    String fieldName = "image_id";
    String sortType = "DESC";

    if(field_name!=null){
      if(!field_name.equalsIgnoreCase("")){
        fieldName = field_name;
      }
      if(!isDESC){
        sortType = "ASC";
      }
    }

    String conds = MakeConditions.conditionsForImage(similar);
    int art_id = (similar!=null) ? Utilities.getIntValue(similar.getImage_article_id()) : 0;

    String sql = "SELECT * FROM tblimage ";
    sql+= (art_id>0) ? "LEFT JOIN tblarticle ON article_id = image_article_id " : "";
    sql += "WHERE (image_deleted = 0) AND (image_enable = 1) ";
    if(!conds.equalsIgnoreCase("")){
       sql += conds;
    }

    sql += "ORDER BY "+ fieldName +" " + sortType + " ";
    sql += "LIMIT " + at + "," + total;

    return this.gets(sql);
  }

}
