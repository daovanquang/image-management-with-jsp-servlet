package hoainiem.gui.image;

import hoainiem.objects.*;
import hoainiem.*;
import java.util.*;
import java.sql.SQLException;

public class ImageControl {
  private ImageModel im;
  public ImageControl(ConnectionPool cp) {
    im = new ImageModel(cp);
  }

  public ConnectionPool getCP() {
    return this.im.getCP();
  }

  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.im.releaseConnection();
  }

//chuyen dieu khien
  public boolean addImage(ImageObject item) {
    return this.im.addImage(item);
  }

  public boolean editImage(ImageObject item) {
    return this.im.editImage(item);
  }

  public boolean delImage(ImageObject item) {
    return this.im.delImage(item);
  }

  public ImageObject getImageObject(int id) {
    return this.im.getImageObject(id);
  }
  public int getTotal(ImageObject similar){
    return this.im.getTotal(similar);
  }
  public ArrayList<ImageObject> getImageOjects(ImageObject similar, int page, byte total, String field_name , boolean isDESC){
    return this.im.getImageObjects(similar,page,total, field_name, isDESC);
  }

  public String viewSlider(int page, byte total){
    ImageObject similar = new ImageObject();
    similar.setImage_forslide(true);
    similar.setImage_group_id(0);
    similar.setImage_article_id(0);
    ArrayList<ImageObject> items = this.im.getImageObjects(similar, page, total, null, true);
    return ImageLibrary.viewSlider(items);
  }
  public String viewNewestPhotos(byte lang_value){
    ArrayList<ImageObject> items = this.im.getImageObjects(null,0,(byte)7, null, true);
    return ImageLibrary.viewNewestPhotos(items, lang_value);
  }
  public String viewAllPhoto(ImageObject similar, int page, byte total, byte lang_value){
    ArrayList<ImageObject> items = this.im.getImageObjects(similar,page,total, null, true);
    return ImageLibrary.viewAllPhoto(items, lang_value);
  }

 public String viewImpressivePhotos(int page, byte total, byte lang_value){
    ImageObject similar = new ImageObject();
    similar.setImage_group_id(0);
    similar.setImage_article_id(0);
    similar.setImage_forimpressive(true);
    ArrayList<ImageObject> items = this.im.getImageObjects(similar,page,total, null, true);
    return ImageLibrary.viewImpressivePhotos(items,lang_value);
 }
 public String viewRecentPhotos(ImageObject similar, byte lang_value){
    ArrayList<ImageObject> items = this.im.getImageObjects(similar,0,(byte)6, null, true);
    return ImageLibrary.viewRecentPhotos(items, lang_value);
 }

 /*========- TABS -=========*/
  public String viewAlbum(ImageObject similar,int page, byte total, String field_name , boolean isDESC, byte tab_index, byte page_index ,byte lang_value){

    int totalPhotos = this.im.getTotal(similar);
    byte maxPage = (byte) (totalPhotos / total);

    page = (page<total)? page:total;

    ArrayList<ImageObject> items = this.im.getImageObjects(similar,page,total, field_name, isDESC);
    return ImageLibrary.viewAlbum(items, tab_index,maxPage, page_index ,lang_value);
  }
}
