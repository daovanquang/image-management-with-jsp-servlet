package hoainiem.gui.group;

import hoainiem.objects.*;
import hoainiem.*;
import java.sql.*;

public abstract interface Group extends ShareControl{
  int addGroup(GroupObject item);
  boolean editGroup(GroupObject item);
  boolean delGroup(GroupObject item);
  public ResultSet getGroupObject(int id);
  public ResultSet getGroupObjects(GroupObject similar, int page, byte total);
}
