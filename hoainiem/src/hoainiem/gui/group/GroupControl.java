package hoainiem.gui.group;

import hoainiem.objects.*;
import hoainiem.*;

import java.util.*;

public class GroupControl {
  private GroupModel gm;
  public GroupControl(ConnectionPool cp) {
    gm = new GroupModel(cp);
  }

  public ConnectionPool getCP() {
    return this.gm.getCP();
  }

//ra lenh tra ve ket noi
  public void releaseConnection() {
    this.gm.releaseConnection();
  }

  public int addGroup(GroupObject item) {
    return this.gm.addGroup(item);
  }

  public boolean editGroup(GroupObject item) {
    return this.gm.editGroup(item);
  }

  public boolean delGroup(GroupObject item) {
    return this.gm.delGroup(item);
  }
  public GroupObject getGroupObject (int id){
    return this.gm.getGroupObject(id);
  }
  public ArrayList<GroupObject> getGroupObjects (GroupObject similar, int at, byte total, byte lang_value){
    ArrayList<GroupObject> items = this.gm.getGroupObjects(similar, at, total);
    return items;
  }

  public String viewProposedGroups(byte lang_value){
    ArrayList<GroupObject> items = this.gm.getGroupObjects(null, 0, (byte)6);
    return GroupLibrary.viewProposedGroups(items, lang_value);
  }
}
