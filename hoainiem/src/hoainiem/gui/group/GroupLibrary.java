package hoainiem.gui.group;

import hoainiem.objects.*;
import java.util.ArrayList;
import hoainiem.lang.gui.*;
import hoainiem.values.gui.*;

public class GroupLibrary {
  public GroupLibrary() {
  }
  public static String viewProposedGroups(ArrayList<GroupObject> items, byte lang_value) {
    String tmp = "";

    String webModule = AllGUI.WebModule;

    if(items.size()==0) return tmp;

    tmp += "<section class=\"proposed-group\">";
    tmp += "<div class=\"heading\">";
    tmp += "<h3>"+HomeLang.PROPOSED_GROUPS[lang_value]+"</h3>";
    tmp += "</div>";
    tmp += "<div class=\"proposed-list image-list image-list--proposed-groups row\">";
    for(GroupObject item : items){
      tmp += "<a href=\"/"+webModule+"/community/?gid="+item.getGroup_id()+"\">";
      tmp += "<div class=\"proposed-group-item image-card image-card--bg-skin\">";
      tmp += "<div class=\"card__img-cover\">";
      tmp += "<img class=\"img-fill\" src=\'"+item.getGroup_image()+"\' alt=\'"+item.getGroup_name()+"\'>";
      tmp += "</div>";
      tmp += "<div class=\"preview-image-details\">";
      tmp += "<div class=\"preview-image-details--padding\">";
      tmp += "<h3>"+item.getGroup_name()+"</h3>";
      tmp += "<p>"+item.getGroup_created_date()+"</p>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</a>";
    }
    tmp += "</div>";
    tmp += "</section>";
    return tmp;
  }

}
