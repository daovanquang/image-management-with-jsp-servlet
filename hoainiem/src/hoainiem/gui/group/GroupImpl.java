package hoainiem.gui.group;

import java.sql.*;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.basic.*;
import hoainiem.library.*;

public class GroupImpl extends BasicImpl implements Group {
  public GroupImpl(ConnectionPool cp) {
    super(cp, "GroupGUI");
  }

  public int addGroup(GroupObject item) {

    int id = 0;

    String sql = "INSERT INTO tblgroup( ";
    sql += "group_name, group_created_date, ";
    sql += "group_permission, group_notes, ";
    sql += "group_author_name, group_image, ";
    sql += "group_enable, group_url_link, ";
    sql += "group_member_sum, group_last_modified ";
    sql += ") ";
    sql += "VALUES(?,?,?,?,?,?,?,?,?,?)";

    try {
      PreparedStatement pre = this.con.prepareStatement(sql,
      Statement.RETURN_GENERATED_KEYS);
      pre.setString(1, item.getGroup_name());
      pre.setString(2, item.getGroup_created_date());
      pre.setByte(3, item.getGroup_permission());
      pre.setString(4, item.getGroup_notes());
      pre.setString(5, item.getGroup_author_name());
      pre.setString(6, item.getGroup_image());
      pre.setBoolean(7, item.isGroup_enable());
      pre.setString(8, item.getGroup_url_link());
      pre.setInt(9, item.getGroup_member_sum());
      pre.setString(10, item.getGroup_last_modified());
      this.add(pre);

      id = UtilitiesSQL.getGeneratedKeys(pre);
      return id;
    }
    catch (SQLException ex) {
    }
    return id;
  }

  public boolean editGroup(GroupObject item) {
    String sql = "UPDATE tblgroup SET ";
    sql += "group_name = ?, group_created_date = ?, ";
    sql += "group_permission = ?, group_notes = ?, ";
    sql += "group_author_name = ?, group_image = ?, ";
    sql += "group_enable = ?, group_url_link = ?, ";
    sql += "group_member_sum = ?, group_last_modified = ? ";
    sql += "WHERE group_id = ?";
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getGroup_name());
      pre.setString(2, item.getGroup_created_date());
      pre.setByte(3, item.getGroup_permission());
      pre.setString(4, item.getGroup_notes());
      pre.setString(5, item.getGroup_author_name());
      pre.setString(6, item.getGroup_image());
      pre.setBoolean(7, item.isGroup_enable());
      pre.setString(8, item.getGroup_url_link());
      pre.setInt(9, item.getGroup_member_sum());
      pre.setString(10, item.getGroup_last_modified());
      pre.setInt(11, item.getGroup_id());

      return this.edit(pre);

    }
    catch (SQLException ex) {
    }
    return false;
  }

  public boolean delGroup(GroupObject item) {
    String sql = "DELETE tblgroup ";
    sql += "WHERE group_id = ?";
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setInt(1, item.getGroup_id());

      return this.del(pre);

    }
    catch (SQLException ex) {
    }
    return false;

  }

  /**
   * getGroup
   *
   * @param id int
   * @return ResultSet
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public ResultSet getGroupObject(int id) {
    String sql = "SELECT * FROM tblgroup WHERE group_id = ? ";
    return this.get(sql, id);
  }

  /**
   * getGroups
   *
   * @param similar GroupObject
   * @param at int
   * @param total byte
   * @return ResultSet
   * @todo Implement this hoainiem.ads.group.Group method
   */
  public ResultSet getGroupObjects(GroupObject similar, int at, byte total) {
    String sql = "SELECT * FROM tblgroup ";
    sql += "WHERE (group_enable = 1) ";

    sql += "ORDER BY group_id DESC ";
    sql += "LIMIT " + at + "," + total;
    return this.gets(sql);
  }
}
