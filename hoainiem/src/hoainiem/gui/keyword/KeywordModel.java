package hoainiem.gui.keyword;

import hoainiem.*;
import hoainiem.objects.*;
import java.util.*;
import java.sql.*;

public class KeywordModel {
  private Keyword k;

  public KeywordModel(ConnectionPool cp) {
    this.k = new KeywordImpl(cp);
  }

  public ConnectionPool getConnection() {
    return this.k.getCP();
  }

  public void releaseConnection() {
    this.k = null;
  }

  public boolean addKeyword(KeywordObject item){
    return this.k.addKeyword(item);
  }

  public boolean editKeyword(KeywordObject item, String command){
    return this.k.editKeyword(item, command);
  }

  private KeywordObject getKeyword(ResultSet rs){
    KeywordObject item = new KeywordObject();
    if(rs!=null){
      try {
        if (rs.next()) {
          item.setKeyword_author_created(rs.getString("keyword_author_created"));
          item.setKeyword_author_permission(rs.getByte("keyword_author_permission"));
          item.setKeyword_created_date(rs.getString("keyword_created_date"));
          item.setKeyword_deleted(rs.getBoolean("keyword_deleted"));
          item.setKeyword_enable(rs.getBoolean("keyword_enable"));
          item.setKeyword_id(rs.getInt("keyword_id"));
          item.setKeyword_restored_date(rs.getString("keyword_restored_date"));
          item.setKeyword_category(rs.getString("keyword_category"));
          item.setKeyword_search_count(rs.getInt("keyword_search_count"));
          item.setKeyword_url(rs.getString("keyword_url"));
          item.setKeyword_value(rs.getString("keyword_value"));
          item.setKeyword_title(rs.getString("keyword_title"));
          item.setKeyword_summary(rs.getString("keyword_summary"));
        }
      }
      catch (SQLException ex) {
      }
    }
    return item;
  }
  public KeywordObject getKeyword(int id){
    ResultSet rs = this.k.getKeyword(id);
    KeywordObject item = getKeyword(rs);
    return item;
  }

  public KeywordObject getKeyword(String value){
    ResultSet rs = this.k.getKeyword(value);
    KeywordObject item = getKeyword(rs);
    return item;
  }

  public ArrayList<KeywordObject> getKeywords(KeywordObject similar, int at, byte total, boolean isExactly){
    ArrayList<KeywordObject> items = new ArrayList<KeywordObject>();
    ResultSet rs = this.k.getKeywords(similar, at, total, isExactly);
    if (rs != null) {
      try {
        while(rs.next()) {
          KeywordObject item = new KeywordObject();

          item.setKeyword_author_created(rs.getString("keyword_author_created"));
          item.setKeyword_author_permission(rs.getByte("keyword_author_permission"));
          item.setKeyword_created_date(rs.getString("keyword_created_date"));
          item.setKeyword_deleted(rs.getBoolean("keyword_deleted"));
          item.setKeyword_enable(rs.getBoolean("keyword_enable"));
          item.setKeyword_id(rs.getInt("keyword_id"));
          item.setKeyword_restored_date(rs.getString("keyword_restored_date"));
          item.setKeyword_category(rs.getString("keyword_category"));
          item.setKeyword_search_count(rs.getInt("keyword_search_count"));
          item.setKeyword_url(rs.getString("keyword_url"));
          item.setKeyword_value(rs.getString("keyword_value"));
          item.setKeyword_title(rs.getString("keyword_title"));
          item.setKeyword_summary(rs.getString("keyword_summary"));

          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return items;
  }
}

