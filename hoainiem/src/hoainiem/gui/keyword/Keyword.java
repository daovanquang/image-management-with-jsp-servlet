package hoainiem.gui.keyword;
import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface Keyword extends ShareControl {
  boolean addKeyword(KeywordObject item);
  boolean editKeyword(KeywordObject item, String command);

  public ResultSet getKeyword(int id);
  public ResultSet getKeyword(String value);
  public ResultSet getKeywords(KeywordObject similar, int at, byte total, boolean isExactly);
}
