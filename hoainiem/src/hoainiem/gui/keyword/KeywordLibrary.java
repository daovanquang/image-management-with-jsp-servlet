package hoainiem.gui.keyword;

import hoainiem.objects.*;
import java.util.ArrayList;
import hoainiem.library.*;
import hoainiem.lang.gui.*;

public class KeywordLibrary {
  public static String viewNotFoundResult(byte lang_value){
    String tmp = "<section class=\"result mt-20 mb-20\">";
    tmp += HomeLang.NOT_FOUND_RESULTS[lang_value];
    tmp+= "</section>";
    return tmp;
  }
  public static String viewResults(ArrayList<KeywordObject> items){
    String tmp = "";
    if(items!=null){
      for(KeywordObject item : items){
        tmp += "<div class=\"row mt-20\">";
        tmp += "<a class=\"link-simple link-hvr-underline\" href=\'" + item.getKeyword_url() + "\'>";
        tmp += "<h2 class=\"keyword-title\">"+ Utilities.getValue(item.getKeyword_title()) +"</h2>";
        tmp += "</a>";
        tmp += "<p class=\"keyword-url link-success\">"+ item.getKeyword_url() +"</p>";
        tmp += "<p class=\"keyword-summary\">"+ Utilities.getValue(item.getKeyword_summary()) +"</p>";
        tmp += "</div>";
      }
    }
    return tmp;
  }
  public static String viewSectionResult(ArrayList<KeywordObject> items, boolean isExactly, byte lang_value){

    String notFound = viewNotFoundResult(lang_value);
     if(items==null) return notFound;
     if(items.size()< 1) return notFound;

     String tmp = "";
     String className = (isExactly) ? "exact-result" : "approximate-result";
     String secTitle = (isExactly) ? HomeLang.EXACTLY_RESULTS[lang_value] : HomeLang.APPROXIMATE_RESULTS[lang_value];
     tmp += "<section class=\"result mt-20 mb-20\">";
     tmp += "<div class=\'"+className+" result-wrap row\'>";
     tmp += "<h3 class=\"result-title\">"+ secTitle +"</h3>";
     tmp += "<div class=\"result-content\">";
     tmp += KeywordLibrary.viewResults(items);
     tmp += "</div>";
     tmp += "</div>";
     tmp += "</section>";

     return tmp;
  }
}
