package hoainiem.gui.keyword;

import java.sql.*;

import hoainiem.gui.basic.*;
import hoainiem.sql.*;
import hoainiem.gui.sql.*;
import hoainiem.*;
import hoainiem.library.*;
import hoainiem.objects.*;

public class KeywordImpl extends BasicImpl implements Keyword {

  public KeywordImpl(ConnectionPool cp) {
    super(cp, "KeywordGUI");
  }

  public boolean addKeyword(KeywordObject item) {
    String []fields = KeywordSQL.FIELDS;
    int []notInsert = {0};
    String sql = BasicSQL.sqlINSERT("keyword",fields,notInsert);

    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getKeyword_value());
      pre.setString(2, item.getKeyword_created_date());
      pre.setBoolean(3, item.isKeyword_deleted());
      pre.setString(4, item.getKeyword_restored_date());
      pre.setBoolean(5, item.isKeyword_enable());
      pre.setInt(6, item.getKeyword_search_count());
      pre.setString(7, item.getKeyword_category());
      pre.setString(8, item.getKeyword_created_date());
      pre.setString(9, item.getKeyword_url());
      pre.setByte(10, item.getKeyword_author_permission());
      pre.setString(11,item.getKeyword_title());
      pre.setString(12,item.getKeyword_summary());
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  /**
   * editKeyword
   *
   * @param item KeywordObject
   * @return boolean
   * @todo Implement this hoainiem.gui.keyword.Keyword method
   */
  public boolean editKeyword(KeywordObject item, String command) {
    String sql = "";

    if(command == KeywordSQL.COMMAND_UPDATE_SEARCH_COUNT){
      int []fields_conditions = {1};
      sql = BasicSQL.sqlCountUp(KeywordSQL.NAME, KeywordSQL.FIELDS,6, fields_conditions);
    }else{
      sql = BasicSQL.sqlUPDATE(KeywordSQL.NAME , KeywordSQL.FIELDS, null, 0);
    }

    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      if(command == KeywordSQL.COMMAND_UPDATE_SEARCH_COUNT){
        pre.setString(1, item.getKeyword_value());
      }else{
        pre.setString(1, item.getKeyword_value());
        pre.setString(2, item.getKeyword_created_date());
        pre.setBoolean(3, item.isKeyword_deleted());
        pre.setString(4, item.getKeyword_restored_date());
        pre.setBoolean(5, item.isKeyword_enable());
        pre.setInt(6, item.getKeyword_search_count());
        pre.setString(7, item.getKeyword_category());
        pre.setString(8, item.getKeyword_created_date());
        pre.setString(9, item.getKeyword_url());
        pre.setByte(10, item.getKeyword_author_permission());
        pre.setString(11, item.getKeyword_title());
        pre.setString(12, item.getKeyword_summary());

        pre.setInt(13, item.getKeyword_id());
      }

      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;

  }

  /**
   * getKeyword
   *
   * @param value String
   * @return ResultSet
   * @todo Implement this hoainiem.gui.keyword.Keyword method
   */
  public ResultSet getKeyword(String value) {
    String sql = "SELECT * FROM tblkeyword WHERE (keyword_deleted = 0) AND (keyword_enable = 1) AND keyword_value = "+ value;
    return this.gets(sql);
  }

  /**
   * getKeyword
   *
   * @param id int
   * @return ResultSet
   * @todo Implement this hoainiem.gui.keyword.Keyword method
   */
  public ResultSet getKeyword(int id) {
    String sql = "SELECT * FROM tblkeyword WHERE (keyword_deleted = 0) AND (keyword_enable = 1) AND keyword_id = ?";
    return this.get(sql, id);
  }

  /**
   * getKeywords
   *
   * @param similar KeywordObject
   * @param at int
   * @param total byte
   * @return ResultSet
   * @todo Implement this hoainiem.gui.keyword.Keyword method
   */
  public ResultSet getKeywords(KeywordObject similar, int at, byte total, boolean isExactly) {
    String sql = "SELECT * FROM tblkeyword ";
    sql += "WHERE (keyword_deleted = 0) AND (keyword_enable = 1) ";
    sql += MakeConditions.conditionsForSearch(similar, isExactly);
    sql += "ORDER BY keyword_search_count DESC ";
    sql += "LIMIT "+at+","+total;

    return this.gets(sql);
  }
}
