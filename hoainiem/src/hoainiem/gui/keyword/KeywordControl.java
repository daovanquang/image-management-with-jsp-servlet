package hoainiem.gui.keyword;

import hoainiem.*;
import hoainiem.objects.*;
import java.util.*;

public class KeywordControl {
  private KeywordModel km;

  public KeywordControl(ConnectionPool cp) {
    this.km = new KeywordModel(cp);
  }

  public ConnectionPool getConnection() {
    return this.km.getConnection();
  }

  public void releaseConnection() {
    this.km = null;
  }

  public boolean addKeyword(KeywordObject item){
    return this.km.addKeyword(item);
  }

  public boolean editKeyword(KeywordObject item, String command){
    return this.km.editKeyword(item, command);
  }

  public KeywordObject getKeyword(int id){
    return this.km.getKeyword(id);
  }

  public KeywordObject getKeyword(String value){
    return this.km.getKeyword(value);
  }
  public ArrayList<KeywordObject> getKeywords(KeywordObject similar, int at, byte total, boolean isExactly){
    return this.km.getKeywords(similar, at, total, isExactly);
  }
  public String getKeywords(KeywordObject similar, int at, byte total, boolean isExactly, byte lang_value){
    ArrayList<KeywordObject> items = this.km.getKeywords(similar,at,total, isExactly);
    return KeywordLibrary.viewSectionResult(items, isExactly, lang_value);
  }
}

