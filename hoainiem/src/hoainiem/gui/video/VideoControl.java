package hoainiem.gui.video;

import hoainiem.objects.*;
import hoainiem.*;
import java.util.*;
import java.sql.SQLException;

public class VideoControl {
  private VideoModel vm;
  public VideoControl(ConnectionPool cp) {
    vm = new VideoModel(cp);
  }

  public ConnectionPool getCP() {
    return this.vm.getCP();
  }

  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.vm.releaseConnection();
  }

//chuyen dieu khien
  public boolean addVideo(VideoObject item) {
    return this.vm.addVideo(item);
  }

  public boolean editVideo(VideoObject item) {
    return this.vm.editVideo(item);
  }

  public boolean delVideo(VideoObject item) {
    return this.vm.delVideo(item);
  }

  public VideoObject getVideoObject(int id) {
    return this.vm.getVideoObject(id);
  }

  public ArrayList<VideoObject> getVideoOjects(VideoObject similar, int page, byte total) throws
      SQLException {
    return this.vm.getVideoObjects(similar,page,total);
  }

  public String viewNewestVideos(VideoObject similar, int page, byte total){
    ArrayList<VideoObject> items = this.vm.getVideoObjects(similar, page, total);
    return VideoLibrary.viewNewestVideos(items);
  }

}
