package hoainiem.gui.video;
import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface Video extends ShareControl {
  boolean addVideo(VideoObject item);
  boolean editVideo(VideoObject item);
  boolean delVideo(VideoObject item);

  public ResultSet getVideo(int id);
  public ResultSet getVideos(VideoObject similar, int at, byte total);
}
