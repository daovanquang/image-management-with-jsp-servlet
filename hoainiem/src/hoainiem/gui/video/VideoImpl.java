package hoainiem.gui.video;

import java.sql.*;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.basic.*;
import hoainiem.gui.sql.*;

public class VideoImpl
    extends BasicImpl implements Video {
  public VideoImpl(ConnectionPool cp) {
    super(cp, "VideoGUI");
  }

  public boolean addVideo(VideoObject item) {
    String sql = "INSERT INTO tblvideo(";
    sql += "video_title, video_source, ";
    sql += "video_image, video_created_date, ";
    sql += "video_author_name, video_timmer, ";
    sql += "video_notes, video_enable, ";
    sql += "video_formultipurpose, video_author_permission, ";
    sql += "video_author_image,  video_article_id,  ";
    sql += "video_tag, video_last_modified, ";
    sql += "video_group_id,video_title_en, ";
    sql += "video_language, video_tag_en ) ";
    sql += "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getVideo_title());
      pre.setString(2, item.getVideo_source());
      pre.setString(3, item.getVideo_image());
      pre.setString(4, item.getVideo_created_date());
      pre.setString(5, item.getVideo_author_name());
      pre.setString(6, item.getVideo_timmer());
      pre.setString(7, item.getVideo_notes());
      pre.setBoolean(8, item.isVideo_enable());
      pre.setBoolean(9, item.isVideo_formultipurpose());
      pre.setByte(10, item.getVideo_author_permission());
      pre.setString(11, item.getVideo_author_image());
      pre.setInt(12, item.getVideo_article_id());
      pre.setString(13, item.getVideo_tag());
      pre.setString(14, item.getVideo_last_modified());
      pre.setInt(15, item.getVideo_group_id());
      pre.setString(16, item.getVideo_title_en());
      pre.setString(17, item.getVideo_language());
      pre.setString(18, item.getVideo_tag_en());
      //thuc thi
      return this.add(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean delVideo(VideoObject item) {
    String sql = "UPDATE tblvideo SET video_deleted = 1, video_deleted_date = ? WHERE ";
    sql += MakeConditions.conditionsDelVideo(item);
    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getVideo_deleted_date());
      //thuc thi
      return this.del(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean editVideo(VideoObject item) {
    String sql = "UPDATE SET tblvideo ";
    sql += "video_title = ?, video_source = ?, ";
    sql += "video_image = ?, video_created_date = ?, ";
    sql += "video_author_name = ?, video_timmer = ?, ";
    sql += "video_notes = ?, video_enable = ?, ";
    sql += "video_formultipurpose = ?, video_author_permission = ?, ";
    sql += "video_author_image = ?,  video_article_id = ?,  ";
    sql += "video_tag = ?, video_last_modified = ?, ";
    sql += "video_group_id = ?,video_title_en = ?, ";
    sql += "video_language, video_tag_en = ? ";
    sql += "WHERE video_id = ?";

    //Doi tuong thuc thi
    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getVideo_title());
      pre.setString(2, item.getVideo_source());
      pre.setString(3, item.getVideo_image());
      pre.setString(4, item.getVideo_created_date());
      pre.setString(5, item.getVideo_author_name());
      pre.setString(6, item.getVideo_timmer());
      pre.setString(7, item.getVideo_notes());
      pre.setBoolean(8, item.isVideo_enable());
      pre.setBoolean(9, item.isVideo_formultipurpose());
      pre.setByte(10, item.getVideo_author_permission());
      pre.setString(11, item.getVideo_author_image());
      pre.setInt(12, item.getVideo_article_id());
      pre.setString(13, item.getVideo_tag());
      pre.setString(14, item.getVideo_last_modified());
      pre.setInt(15, item.getVideo_group_id());
      pre.setString(16, item.getVideo_title_en());
      pre.setString(17, item.getVideo_language());
      pre.setString(18, item.getVideo_tag_en());
      pre.setInt(19, item.getVideo_id());
      //thuc thi
      return this.edit(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;

  }

  public ResultSet getVideo(int id) {
    String sql = "SELECT * FROM tblvideo WHERE (video_deleted = 0) AND video_id = ?";
    return this.get(sql, id);
  }

  public ResultSet getVideos(VideoObject similar, int at, byte total) {
    String conds = MakeConditions.conditionsForVideo(similar);
    String sql = "SELECT * FROM tblvideo ";

    if (!conds.equalsIgnoreCase("")) {
      sql += "LEFT JOIN tblarticle ON article_id = video_article_id ";
      sql += "WHERE (video_deleted = 0) " + conds;
    }
    sql += "ORDER BY video_id DESC ";
    sql += "LIMIT " + at + "," + total;

    return this.gets(sql);
  }

}
