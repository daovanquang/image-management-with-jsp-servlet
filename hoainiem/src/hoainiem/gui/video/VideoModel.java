package hoainiem.gui.video;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;
import java.util.*;

public class VideoModel {
  private Video v;

  public VideoModel(ConnectionPool cp) {
    this.v = new VideoImpl(cp);
  }

  public ConnectionPool getCP() {
    return this.v.getCP();
  }

  public void finalize() throws Throwable {
    this.v = null;
  };
  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.v = null;
  }

  public boolean addVideo(VideoObject item) {
    return this.v.addVideo(item);
  }

  public boolean delVideo(VideoObject item) {
    return this.v.delVideo(item);
  }

  public boolean editVideo(VideoObject item) {
    return this.v.editVideo(item);
  }

  /*LAY RA CAC GIA TRI THUOC TINH CUA Video CAN LAY */
  public VideoObject getVideoObject(int id) {
    VideoObject item = null;

    ResultSet rs = this.v.getVideo(id);
    if (rs != null) {
      try {
        if (rs.next()) {
          item = new VideoObject();
          item.setVideo_id(rs.getInt("video_id"));
          item.setVideo_title(rs.getString("video_title"));
          item.setVideo_image(rs.getString("video_image"));
          item.setVideo_source(rs.getString("video_source"));
          item.setVideo_created_date(rs.getString("video_created_date"));
          item.setVideo_author_name(rs.getString("video_author_name"));
          item.setVideo_title_en(rs.getString("video_title_en"));
          item.setVideo_tag(rs.getString("video_tag"));
          item.setVideo_tag_en(rs.getString("video_tag_en"));
          item.setVideo_visited(rs.getShort("video_visited"));
          item.setVideo_language(rs.getString("video_language"));
          item.setVideo_last_modified(rs.getString("video_last_modified"));
          item.setVideo_author_permission(rs.getByte("video_author_permission"));
          item.setVideo_notes(rs.getString("video_notes"));
          item.setVideo_article_id(rs.getInt("video_article_id"));
          item.setVideo_enable(rs.getBoolean("video_enable"));
          item.setVideo_formultipurpose(rs.getBoolean("video_formultipurpose"));
          item.setVideo_timmer(rs.getString("video_timmer"));
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return item;
  }

  public ArrayList<VideoObject> getVideoObjects(VideoObject similar, int at,
                                                byte total) {
    ArrayList<VideoObject> items = new ArrayList<VideoObject> ();
    VideoObject item = null;

    ResultSet rs = this.v.getVideos(similar, at, total);
    if (rs != null) {
      try {

        while (rs.next()) {
          item = new VideoObject();
          item.setVideo_id(rs.getInt("video_id"));
          item.setVideo_title(rs.getString("video_title"));
          item.setVideo_source(rs.getString("video_source"));
          item.setVideo_image(rs.getString("video_image"));
          item.setVideo_created_date(rs.getString("video_created_date"));
          item.setVideo_author_name(rs.getString("video_author_name"));
          item.setVideo_title_en(rs.getString("video_title_en"));
          item.setVideo_tag(rs.getString("video_tag"));
          item.setVideo_tag_en(rs.getString("video_tag_en"));
          item.setVideo_visited(rs.getShort("video_visited"));
          item.setVideo_language(rs.getString("video_language"));
          item.setVideo_last_modified(rs.getString("video_last_modified"));
          item.setVideo_author_permission(rs.getByte("video_author_permission"));
          item.setVideo_notes(rs.getString("video_notes"));
          item.setVideo_article_id(rs.getInt("video_article_id"));
          item.setVideo_enable(rs.getBoolean("video_enable"));
          item.setVideo_formultipurpose(rs.getBoolean("video_formultipurpose"));
          item.setVideo_timmer(rs.getString("video_timmer"));

          items.add(item);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }
    return items;
  }

}
