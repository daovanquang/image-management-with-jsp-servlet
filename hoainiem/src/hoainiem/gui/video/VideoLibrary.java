package hoainiem.gui.video;

import hoainiem.objects.*;
import java.util.*;

public class VideoLibrary {
  public VideoLibrary() {
  }

  public static String viewNewestVideos(ArrayList<VideoObject> items) {
    String tmp = "";
    tmp += "<div class=\"container\">";
    tmp += "<div id=\"MainVideoPlayer\" class=\"main-video-player\">";
    tmp += "<div class=\"main-video-player__content\">";
    tmp += "<iframe width=\"100%\" src=\"\" class=\"main-video-source\" autostart=\"false\" preload=\"none\" allow=\"encrypted-media\" frameborder=\"0\" allowfullscreen>";
    tmp += "</iframe>";
    tmp += "</div>";
    tmp += "<div class=\"main-video-player__title text-center\">";
    tmp += "<h5 class=\"main-video-title\"></h5>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div  id=\"PrimaryVideoPlayer\" class=\"primary-video-player relative\">";
    tmp += "<div class=\"prev-next-btn prev-btn absolute text-center\">";
    tmp += "<i class=\"ic ic_utl-md ic_arwleft-lg ic-lg\"></i>";
    tmp += "</div>";

    tmp += "<div class=\"primary-video-player__content relative\">";
    tmp += "<div class=\"slider__track\" style=\"width: 600px;transform: translate3d(0,0,0);\">";
    for(VideoObject item:items){
      tmp += "<div class=\"slider__item image-hover-blur\" style=\"width: 110px; margin:0 2px;\">";
      tmp += "<img src=\'"+item.getVideo_image()+"\' alt=\"\">";
      tmp += "<h6 class=\"primary-video-title\" hidden=\"true\">";
      tmp += item.getVideo_title();
      tmp += "</h6>";
      tmp += "<p class=\"primary-video-source\" hidden=\"true\">";
      tmp += item.getVideo_source();
      tmp += "</p>";
      tmp += "</div>";
    }
    tmp += "</div>";
    tmp += "</div>";

    tmp += "<div class=\"prev-next-btn next-btn absolute text-center\">";
    tmp += "<i class=\"ic ic_utl-md ic_arwright-lg ic-lg\"></i>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "</div>";

    return tmp;
  }
}
