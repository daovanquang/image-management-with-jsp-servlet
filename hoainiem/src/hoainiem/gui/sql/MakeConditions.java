package hoainiem.gui.sql;

import hoainiem.objects.*;
import hoainiem.library.*;

public class MakeConditions {
  public MakeConditions() {
  }

  public static String conditionsForArticle(ArticleObject similar){
    String tmp="";
    if(similar!=null){
      int groupid = similar.getArticle_group_id();
      if(groupid>=0) tmp += "AND (article_group_id = " + groupid + ") ";
    }else{
      //select default article
      tmp += "AND (article_group_id = " + 0 + ") ";
    }
    return tmp;
  }
  public static String conditionsForVideo(VideoObject similar){
      String tmp="";
      if(similar!=null){
        int articleId = similar.getVideo_article_id();
        boolean formultipurpose = similar.isVideo_formultipurpose();

        if(articleId>0) tmp += "AND (video_article_id = " + articleId + ") ";
        if(formultipurpose) tmp += "AND (video_formultipurpose = " + formultipurpose + ") ";
      }

      return tmp;
  }

  public static String conditionsDelVideo(VideoObject similar){
    String tmp = "";
    if(similar!=null){
      int videoArticleId = similar.getVideo_article_id();
      tmp += (videoArticleId>0) ? "video_article_id = "+ videoArticleId : "video_id = "+ similar.getVideo_id();
    }
    return tmp;
  }


  public static String conditionsForImage(ImageObject similar){
    String tmp="";
    if(similar!=null){
      int articleId = Utilities.getIntValue(similar.getImage_article_id());
      boolean forslide = similar.isImage_forslide();
      boolean forimpressive= similar.isImage_forimpressive();
      int groupid = Utilities.getIntValue(similar.getImage_group_id());

      if(articleId > 0) tmp += "AND (image_article_id = " + articleId + ") ";
      if(forslide == true) tmp += "AND (image_forslide = 1) ";
      if(forimpressive == true) tmp+= "AND (image_forimpressive = 1) ";
      if(groupid > 0) tmp+= "AND (image_group_id = "+groupid+") ";
    }
    return tmp;
  }
  public static String conditionsDelImage(ImageObject similar){
    String tmp = "";
    if(similar!=null){
      int imageArticleId = similar.getImage_article_id();
      tmp += (imageArticleId>0) ? "image_article_id = "+ imageArticleId : "image_id = "+ similar.getImage_id();
    }
    return tmp;
  }

  public static String conditionsForComment(CommentObject similar){
   String tmp="";
   if(similar!=null){
     int articleId = similar.getComment_article_id();
     byte authorPermis = similar.getComment_author_permission();

     if (articleId > 0) {
       tmp += "AND (comment_article_id = " + articleId + ") ";
     }
     if(authorPermis > 0){
       tmp += "AND (comment_author_permission = " + authorPermis + ") ";
     }
   }
   return tmp;
  }

  public static String conditionsDelComment(CommentObject similar){
    String tmp = "";
     if(similar!=null){
       int commentArticleId = similar.getComment_article_id();
       tmp += (commentArticleId>0) ? "comment_article_id = "+ commentArticleId : "comment_id = "+ similar.getComment_id();
     }
     return tmp;
  }

  public static String conditionsForSearch(KeywordObject similar, boolean isExactly){
    String tmp = "";
    if(similar!=null){
      String value = Utilities.getValue(similar.getKeyword_value());
      if(!value.equalsIgnoreCase("")) {
        tmp += " AND keyword_value LIKE ";
        tmp+= (isExactly) ? "'"+value + "' " : " '%" + value + "%' ";
      }
    }
    return tmp;
  }
}
