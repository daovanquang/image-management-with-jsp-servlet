package hoainiem.gui.basic;

import hoainiem.ShareControl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract interface Basic extends ShareControl{
  //trinh dieu khien doi tuong thuc thi
  boolean add(PreparedStatement prepapredStatement);
  boolean edit(PreparedStatement preparedStatement);
  boolean del(PreparedStatement preparedStatement);

  //doi tuong thuc thi voi co so du lieu
  ResultSet get(String string, int _int);
  ResultSet get(String string1, String string2, String string3);
  ResultSet gets(String string);
  ResultSet[] gets(String []Arrays);
}
