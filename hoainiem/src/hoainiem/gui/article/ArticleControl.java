package hoainiem.gui.article;

import java.util.*;

import hoainiem.*;
import hoainiem.objects.*;

public class ArticleControl {
  private ArticleModel am;
  public ArticleControl(ConnectionPool cp) {
    this.am = new ArticleModel(cp);
  }
  public ConnectionPool getCP() {
        return this.am.getCP();
  }
  public void releaseConnection() {
         this.am = null;
  }

  //chuyen dieu khien
  public int addArticle(ArticleObject item) {
    return this.am.addArticle(item);
  }

  public boolean editArticle(ArticleObject item) {
    return this.am.editArticle(item);
  }

  public boolean delArticle(ArticleObject item) {
    return this.am.delArticle(item);
  }
  public ArticleObject getArticleObject(int id){
    return this.am.getArticle(id);
  }
  public ArrayList<ArticleObject> getArticleObjects(ArticleObject similar, int at, byte total){
     return this.am.getArticles(similar, at, total);
  }

  public String viewPost(UserObject user, byte lang_value){
   return ArticleLibrary.viewPost(user, lang_value);
  }

  public String viewArticle(ArticleObject item,ArrayList<ImageObject> imgs,ArrayList<CommentObject> cmts, UserObject user, byte lang_value){
      return ArticleLibrary.viewArticle(item,imgs,cmts, user, lang_value);
  }
}
