package hoainiem.gui.article;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;

public abstract interface Article extends ShareControl{
  int addArticle(ArticleObject item);
  boolean editArticle(ArticleObject item);
  boolean delArticle(ArticleObject item);

  public ResultSet getArticle(int id);
  public ResultSet getArticles(ArticleObject similar, int at, byte total);
}
