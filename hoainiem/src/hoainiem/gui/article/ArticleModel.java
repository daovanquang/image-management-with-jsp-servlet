package hoainiem.gui.article;

import hoainiem.*;
import hoainiem.objects.*;
import java.sql.*;
import java.util.*;

public class ArticleModel {
  private Article a;
  public ArticleModel(ConnectionPool cp) {
    this.a = new ArticleImpl(cp);
  }

  public ConnectionPool getCP() {
    return this.a.getCP();
  }

  //ra lenh tra ve ket noi
  public void releaseConnection() {
    this.a = null;
  }

  //chuyen dieu khien
  public int addArticle(ArticleObject item) {
    return this.a.addArticle(item);
  }

  public boolean editArticle(ArticleObject item) {
    return this.a.editArticle(item);
  }

  public boolean delArticle(ArticleObject item) {
    return this.a.delArticle(item);
  }

  public ArticleObject getArticle(int id){
    ArticleObject item = null;
    ResultSet rs = this.a.getArticle(id);
    if(rs!=null){
      item = new ArticleObject();
      try {
        if (rs.next()) {
          item.setArticle_id(rs.getInt("article_id"));
          item.setArticle_title(rs.getString("article_title"));
          item.setArticle_created_date(rs.getString("article_created_date"));
          item.setArticle_author_name(rs.getString("article_author_name"));
          item.setArticle_title_en(rs.getString("article_title_en"));
          item.setArticle_deleted_date(rs.getString("article_deleted_date"));
          item.setArticle_restored_date(rs.getString("article_restored_date"));
          item.setArticle_author_permission(rs.getByte("article_author_permission"));
          item.setArticle_notes(rs.getString("article_notes"));
          item.setArticle_enable(rs.getBoolean("article_enable"));
          item.setArticle_deleted(rs.getBoolean("article_deleted"));
          item.setArticle_visited(rs.getInt("article_visited"));
          item.setArticle_last_modified(rs.getString("article_last_modified"));
          item.setArticle_comment_sum(rs.getByte("article_comment_sum"));
          item.setArticle_image_sum(rs.getByte("article_image_sum"));
          item.setArticle_author_image(rs.getString("article_author_image"));
          item.setArticle_tag(rs.getString("article_tag"));
          item.setArticle_tag_en(rs.getString("article_tag_en"));
          item.setArticle_language(rs.getString("article_language"));
        }
      }
      catch (SQLException ex) {
      }
    }

    return item;
  }
  public ArrayList<ArticleObject> getArticles(ArticleObject similar, int at, byte total){
    ArticleObject item = null;
    ArrayList<ArticleObject> items = new ArrayList<ArticleObject>();
    ResultSet rs = this.a.getArticles(similar, at, total);

    if(rs!=null){
      try {
        while (rs.next()) {
          item = new ArticleObject();
          item.setArticle_id(rs.getInt("article_id"));
          item.setArticle_title(rs.getString("article_title"));
          item.setArticle_created_date(rs.getString("article_created_date"));
          item.setArticle_author_name(rs.getString("article_author_name"));
          item.setArticle_title_en(rs.getString("article_title_en"));
          item.setArticle_deleted_date(rs.getString("article_deleted_date"));
          item.setArticle_restored_date(rs.getString("article_restored_date"));
          item.setArticle_author_permission(rs.getByte("article_author_permission"));
          item.setArticle_notes(rs.getString("article_notes"));
          item.setArticle_enable(rs.getBoolean("article_enable"));
          item.setArticle_deleted(rs.getBoolean("article_deleted"));
          item.setArticle_visited(rs.getInt("article_visited"));
          item.setArticle_last_modified(rs.getString("article_last_modified"));
          item.setArticle_comment_sum(rs.getByte("article_comment_sum"));
          item.setArticle_image_sum(rs.getByte("article_image_sum"));
          item.setArticle_author_image(rs.getString("article_author_image"));
          item.setArticle_tag(rs.getString("article_tag"));
          item.setArticle_tag_en(rs.getString("article_tag_en"));
          item.setArticle_language(rs.getString("article_language"));

          items.add(item);

        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
    }

    return items;
  }

}
