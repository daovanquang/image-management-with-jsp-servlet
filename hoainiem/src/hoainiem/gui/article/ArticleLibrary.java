package hoainiem.gui.article;

import java.util.*;

//thu vien imgs,comments
import hoainiem.gui.image.*;
import hoainiem.gui.comment.*;
import hoainiem.library.*;
import hoainiem.objects.*;
import hoainiem.lang.gui.*;
import hoainiem.values.gui.*;

public class ArticleLibrary {
  public ArticleLibrary() {
  }

  public static String viewPost(UserObject user, byte lang_value) {
    String tmp = "";
    if(user==null) return null;
    String styleTxtTitle = "line-height: 2.5rem;font-size: 1rem;font-weight:540;";
    String changeStyleTxtTitle ="line-height: 1rem;font-size: 0.9rem;font-weight:500;";
    byte beginRow = 1, resetRow = 3, endRow = 10;

    if (user != null && user.getUser_permission()>1) {
      String fullname = user.getUser_fullname();
      String image = user.getUser_image();
      int groupid = Utilities.getIntValue(user.getUser_group_id());
      byte permission = Utilities.getByteValue(user.getUser_permission());

      tmp += "<section class=\"post mt-10 mb-10\" enctype='multipart/form-data' accept-charset='utf-8'>";
      tmp += "<form name=\"frmPost\" action=\"\" method=\"\" enctype=\"multipart/form-data\">";
      tmp += "<div class=\"post__control\">";
      tmp += "<ul class=\"list-unstyle horizontal-nav\">";
      tmp += "<li class=\"tabs\" onclick=\"selectFilePost(0);\">"+HomeLang.ADD_PHOTOS[lang_value] + "</li>";
      tmp += "<li class=\"separator\">|</li>";
      tmp += "<li class=\"tabs\" onclick=\"selectFilePost(1);\">"+HomeLang.ADD_FOLDER_PHOTO[lang_value] + "</li>";
      tmp += "<li class=\"separator\">|</li>";
      tmp += "<li class=\"tabs\" onclick=\"selectFilePost(2);\">" + HomeLang.ADD_VIDEO[lang_value] + "</li>";
      tmp += "<li class=\"separator\">|</li>";
      tmp += "<li class=\"tabs\" onclick=\"selectFilePost(3);\">" +  HomeLang.OTHER_FILE[lang_value] + "</li>";
      tmp += "<li class=\"separator\">|</li>";
      tmp += "<li class=\"tabs\" onclick=\"activePostControl(4);swapHide('tags','hide');\">" + HomeLang.ADD_TAG[lang_value] + "</li>";
      tmp += "</ul>";
      tmp += "</div>";
      tmp += "<div class=\"post__body\">";
      tmp += "<div class=\"title row\">";
      tmp += "<div class=\"quickview quickview-md pull-left\">";
      tmp += "<img src=\"" + image + "\" alt=\"" + fullname + "\">";
      tmp += "</div>";
      tmp += "<div class=\"quickview-properties quickview-properties-md\">";
      tmp += "<textarea name=\"txtArticleTitle\" rows='"+beginRow+"' value=\"\" placeholder=\"" + HomeLang.SHARE_INTERESTING[lang_value] + "\" onkeyup=\"addRow(event, this, "+resetRow+","+endRow+", true)\" onclick=\"changeTextarea(this,'"+changeStyleTxtTitle+"',"+resetRow+","+beginRow+")\" onmouseleave=\"resetTextarea(this,'"+styleTxtTitle+"',"+beginRow+","+beginRow+")\" style='"+styleTxtTitle+"'></textarea>";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "<div id=\"tags\" class=\"tag row hide\">";
      tmp += "<div class=\"tag__title pull-left text-center\" >" + HomeLang.TAG_TITLE[lang_value] + "</div>";
      tmp += "<input type=\"text\" name=\"txtArticleTags\" class=\"tag__content border-none pull-left\" placeholder=\"" + HomeLang.TAG_CONTENT[lang_value] + "\">";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "<div id=\"listOfPost\" class=\"post__images images hide\">";
      tmp += "<div class=\"image-list image-list--small-not-auto-response\">";
      tmp += "<div id=\"listCard\">";
      tmp += "</div>";
      tmp += "<div id=\"virtualCard\" class=\"image-card no-select\">";
      tmp += "<div class=\"relative virtual-image card__img-cover\">";
      tmp +="<img class=\"absolute\" src=\"/"+AllGUI.WebModule+"/imgs/plus-icon.png\" alt=\"\" >";
      tmp += "</div>";
      tmp += "</div>";
      tmp += "</div>";

      tmp += "<label id=\"post__label-error\" class=\"label-danger\"></label>";
      tmp += "</div>";
      tmp += "<div class=\"post__footer\">";
      tmp += "<i class=\"ic ic-sm ic_utl-sm ic_img-sm pull-left\" onclick=\"selectFilePost(0);selectVirtualCard()\"></i>";
      tmp += "<i class=\"ic ic-sm ic_utl-sm ic_img-dir-sm pull-left\" onclick=\"selectFilePost(1);selectVirtualCard()\"></i>";
      tmp += "<i class=\"ic ic-sm ic_utl-sm ic_video-sm pull-left\" onclick=\"selectFilePost(2);selectVirtualCard()\"></i>";
      tmp += "<button type=\"button\" class=\"btn btn-sm btn-primary pull-right\" onclick=\"submitArticleAE(this.form);\">" +  HomeLang.POST[lang_value] + "</button>";
      tmp += "</div>";
      tmp += "<div id=\"listInput\" class=\"\" hidden>";
      tmp += "</div>";
      tmp += "<input type=\"file\" id=\"imagePost\" hidden=\"true\">";
      tmp += "<input type=\"hidden\" name=\"txtArticleAuthorFullName\" value=\"" + fullname + "\">";
      tmp += "<input type=\"hidden\" name=\"txtArticleAuthorImage\" value=\"" + image + "\">";
      tmp += "<input type=\"hidden\" name=\"txtArticleAuthorGroupId\" value=\"" + groupid + "\">";
      tmp += "<input type=\"hidden\" name=\"txtArticleAuthorPermission\" value=\"" + permission + "\">";
      tmp += "</form>";
      tmp += "</section>";
    }
    return tmp;
  }

  public static String viewArticle(ArticleObject item, ArrayList<ImageObject> imgs, ArrayList<CommentObject> cmts, UserObject user, byte lang_value) {
    String tmp = "";
    byte authorpermis = Utilities.getByteValue(item.getArticle_author_permission());
    String authorname = Utilities.getValue(item.getArticle_author_name());
    int articleId = item.getArticle_id();
    byte userpermis = 0;
    String userfullname = "";
    String userimage = "";
    if (user != null) {
      userpermis = Utilities.getByteValue(user.getUser_permission());
      userfullname = Utilities.getValue(user.getUser_fullname());
      userimage = Utilities.getValue(user.getUser_image());
    }

    tmp += "<section class=\"article\">";
    tmp += "<div class=\"article-item mt-10\">";
    tmp += "<div class=\"article__heading\">";
    tmp += "<div class=\"quickview quickview-md pull-left\">";
    tmp += "<img src=\'" + item.getArticle_author_image() + "\' alt=\"\">";
    tmp += "</div>";
    tmp += "<div class=\"relative quickview-properties quickview-properties-md\">";
    tmp += "<a href=\"\">";
    tmp += "<h3>" + item.getArticle_author_name() + "</h3>";
    tmp += "</a>";
    tmp += "<p>";

    if ( (authorpermis > 0 && userpermis >= authorpermis) ||
        (authorname != "" && userfullname == authorname)) {
      tmp += "<a href=\'/"+AllGUI.WebModule+"/article/ae?aid=" + item.getArticle_id() +"\' class=\"\"><span class=\"label-bold mr-5 \">"+HomeLang.EDIT[lang_value]+"</span></a>";
      tmp += "<a href=\"javascript:confirmDel('/"+AllGUI.WebModule+"/article/del?aid=" + item.getArticle_id() + "');void(0);\" class=\"link-no-color link-danger mr-5 \"><span class=\"label-bold ml-5\">"+HomeLang.DEL[lang_value]+"</span></a>";
    }
    tmp += "<span class=\"link-gray\">" + item.getArticle_created_date() +"</span>";
    tmp += "</p>";

    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class=\"article__body\">";

    String title = (lang_value == 1) ? item.getArticle_title_en() : item.getArticle_title();
    tmp += "<h5>" + title + "</h5>";

    tmp += "</div>";

    //lay ra list image
    if (imgs != null) {
      tmp += ImageLibrary.viewImages(imgs);
    }
    tmp += "<div class=\"article__properties\">";
    tmp += "<div class=\"like-count pull-left\">";
    tmp += "<i class=\"ic ic-md ic_gui-md ic_heart-md pull-left mr-5\"></i>";
    tmp += "<span>(" + item.getArticle_visited() + ")</span>";
    tmp += "</div>";
    tmp += "<div class=\"comment-count pull-right\">";
    tmp +=
        "<i class=\"ic ic-md ic_gui-md ic_comment-md pull-left mr-5\"></i>";
    tmp += "<span>(" + item.getArticle_comment_sum() + ")</span>";
    tmp += "</div>";
    tmp += "</div>";

    if (user != null) {
      tmp += "<div class=\"article__control\">";
      tmp += "<div class=\"like-btn pull-left ml-5\">";
      tmp += "<div class=\"btn\" type=\"button\">"+HomeLang.CARE[lang_value]+"</div>";
      tmp += "</div>";

      tmp += "<div class=\"comment-btn pull-right mr-5\">";
      tmp += "<div class=\"btn\" type=\"button\">"+HomeLang.COMMENT[lang_value]+"</div>";
      tmp += "</div>";
      tmp += "</div>";
    }

    tmp += "<div class=\"article__footer\">";
    if (user != null) {
      tmp +="<form  name=\"frmComment\" class=\"form-comment parent-form\" method=\"\" action=\"\">";
      tmp += "<textarea type=\"text\" name=\"txtCommentContent\" rows=\"1\" class=\"input-text\" placeholder=\'"+HomeLang.COMMENT_INPUT[lang_value]+"\' onkeyup=\"addRow(event, this, 1, 5, true)\"></textarea>";
      tmp +="<input  class=\"input-file\" type=\"file\" name=\"\"  hidden=\"true\">";
      tmp += "<div class=\"form-comment__control\">";
      tmp += "<div class=\"attachment-icons\">";
      tmp += "</div>";
      tmp += "<button type=\"button\" class=\"btn-submit\" onclick=\"submitCommentAE(this.form);\">";
      tmp += "<i class=\"ic ic-sm ic_utl-sm ic_send-sm\"></i>";
      tmp += "</button>";
      tmp += "</div>";

      tmp += "<input type=\"hidden\" name=\"txtArticleId\" value=\"" + articleId + "\">";
      tmp += "<input type=\"hidden\" name=\"txtCommentUserFullName\" value=\"" +userfullname + "\">";
      tmp += "<input type=\"hidden\" name=\"txtCommentUserImage\" value=\"" +userimage + "\">";
      tmp +="<input type=\"hidden\" name=\"txtCommentUserPermission\" value=\"" +userpermis + "\">";
      tmp += "</form>";
    }
    //String  list comment
    if (cmts != null) {
      tmp += CommentLibrary.viewComments(cmts, user, lang_value);
    }
    tmp += "</div>";
    //end article content
    tmp += "</div>";

    tmp += "</section>";

    return tmp;
  }

}
