package hoainiem.gui.article;

import java.sql.*;

import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.basic.*;
import hoainiem.library.*;
import hoainiem.gui.sql.*;

public class ArticleImpl extends BasicImpl implements Article {
  public ArticleImpl(ConnectionPool cp) {
    super(cp,"ArticleGUI");
  }

  /**
   * addArticle
   *
   * @param item ArticleObject
   * @return boolean
   * @todo Implement this hoainiem.gui.article.Article method
   */
  public int addArticle(ArticleObject item) {
    int id = 0;

    String sql ="INSERT INTO tblArticle(";
    sql+="article_title, article_created_date, ";
    sql+="article_author_name, article_title_en, ";
    sql+="article_deleted_date, article_restored_date,";
    sql+="article_author_permission, article_notes, ";
    sql+="article_enable, article_deleted, ";
    sql+="article_visited, article_last_modified, ";
    sql+="article_comment_sum, article_image_sum, ";
    sql+="article_author_image, article_group_id, ";
    sql+="article_tag, article_tag_en, ";
    sql+="article_language ";
    sql+= ")";
    sql+="VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    try {
      PreparedStatement pre = this.con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
      pre.setString(1,item.getArticle_title());
      pre.setString(2,item.getArticle_created_date());
      pre.setString(3,item.getArticle_author_name());
      pre.setString(4,item.getArticle_title_en());
      pre.setString(5,item.getArticle_deleted_date());
      pre.setString(6,item.getArticle_restored_date());
      pre.setByte(7,item.getArticle_author_permission());
      pre.setString(8,item.getArticle_notes());
      pre.setBoolean(9,item.isArticle_enable());
      pre.setBoolean(10,item.isArticle_deleted());
      pre.setInt(11,item.getArticle_visited());
      pre.setString(12,item.getArticle_last_modified());
      pre.setInt(13,item.getArticle_comment_sum());
      pre.setInt(14,item.getArticle_image_sum());
      pre.setString(15, item.getArticle_author_image());
      pre.setInt(16, item.getArticle_group_id());
      pre.setString(17,item.getArticle_tag());
      pre.setString(18, item.getArticle_tag_en());
      pre.setString(19, item.getArticle_language());

      this.add(pre);
      id = UtilitiesSQL.getGeneratedKeys(pre);
      return id;
    }
    catch (SQLException ex) {
    }

    return id;
  }

  /**
   * delArticle
   *
   * @param item ArticleObject
   * @return boolean
   * @todo Implement this hoainiem.gui.article.Article method
   */
  public boolean delArticle(ArticleObject item) {

    String sql = "UPDATE tblarticle SET article_deleted = 1, article_deleted_date = ? WHERE article_id = ?";
    //tao doi tuong thuc thi
    PreparedStatement pre = null;
    try {
      pre = this.con.prepareStatement(sql);
      pre.setString(1,item.getArticle_deleted_date());
      pre.setInt(2,item.getArticle_id());

      return this.del(pre);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
    return false;
  }

  /**
   * editArticle
   *
   * @param item ArticleObject
   * @return boolean
   * @todo Implement this hoainiem.gui.article.Article method
   */
  public boolean editArticle(ArticleObject item) {

    String sql = "UPDATE SET tblArticle ";
    sql += "article_title =?, article_created_date = ?, ";
    sql += "article_author_name = ?, article_title_en = ?, ";
    sql += "article_deleted_date = ?, article_restored_date = ?,";
    sql += "article_author_permission = ?, article_notes = ?, ";
    sql += "article_enable = ?, article_deleted = ?, ";
    sql += "article_visited = ?, article_last_modified = ?, ";
    sql += "article_comment_sum = ?, article_image_sum = ?, ";
    sql += "article_author_image = ?, article_group_id = ? ";
    sql += "article_tag = ?, article_tag_en = ?, ";
    sql += "article_language = ? ";
    sql += "WHERE article_id = ?";

    try {
      PreparedStatement pre = this.con.prepareStatement(sql);
      pre.setString(1, item.getArticle_title());
      pre.setString(2, item.getArticle_created_date());
      pre.setString(3, item.getArticle_author_name());
      pre.setString(4, item.getArticle_title_en());
      pre.setString(5, item.getArticle_deleted_date());
      pre.setString(6, item.getArticle_restored_date());
      pre.setByte(7, item.getArticle_author_permission());
      pre.setString(8, item.getArticle_notes());
      pre.setBoolean(9, item.isArticle_enable());
      pre.setBoolean(10, item.isArticle_deleted());
      pre.setInt(11, item.getArticle_visited());
      pre.setString(12, item.getArticle_last_modified());
      pre.setInt(13, item.getArticle_comment_sum());
      pre.setInt(14, item.getArticle_image_sum());
      pre.setString(15, item.getArticle_author_image());
      pre.setInt(16, item.getArticle_group_id());
      pre.setString(17, item.getArticle_tag());
      pre.setString(18, item.getArticle_tag_en());
      pre.setString(19, item.getArticle_language());

      pre.setInt(20, item.getArticle_id());
      return this.add(pre);

    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }

    return false;
  }

  /**
   * getArticle
   *
   * @param id int
   * @return ResultSet
   * @todo Implement this hoainiem.gui.article.Article method
   */
  public ResultSet getArticle(int id) {
    String sql = "SELECT * FROM tblarticle WHERE (article_deleted = 0) AND article_id = ?";
    return this.get(sql, id);
  }

  /**
   * getArticles
   *
   * @param similar ArticleObject
   * @param at int
   * @param total byte
   * @return ResultSet
   * @todo Implement this hoainiem.gui.article.Article method
   */
  public ResultSet getArticles(ArticleObject similar, int at, byte total) {
    String conds = MakeConditions.conditionsForArticle(similar);
    String sql = "SELECT * FROM tblarticle ";
    if(!conds.equalsIgnoreCase("")){
      sql += "WHERE (article_deleted = 0) AND (article_enable = 1) " + conds;
    }

    sql += "ORDER BY article_id DESC ";
    sql +="LIMIT "+at+","+total;
    return this.gets(sql);
  }

}
