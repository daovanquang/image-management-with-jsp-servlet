package hoainiem.objects;

public class ArticleObject {
  private int article_id;
  private String article_title;
  private String article_created_date;
  private String article_author_name;
  private String article_title_en;
  private boolean article_deleted;
  private String article_deleted_date;
  private String article_restored_date;
  private byte article_author_permission;
  private String article_notes;
  private boolean article_enable;
  private int article_visited;
  private String article_last_modified;
  private byte article_comment_sum;
  private byte article_image_sum;
  private String article_author_image;
  private int article_group_id;
  private String article_tag;
  private String article_tag_en;
  private String article_language;
  public ArticleObject() {
  }

  public void setArticle_id(int article_id) {
    this.article_id = article_id;
  }

  public void setArticle_title(String article_title) {
    this.article_title = article_title;
  }

  public void setArticle_created_date(String article_created_date) {
    this.article_created_date = article_created_date;
  }

  public void setArticle_author_name(String article_author_name) {

    this.article_author_name = article_author_name;
  }

  public void setArticle_title_en(String article_title_en) {
    this.article_title_en = article_title_en;
  }

  public void setArticle_deleted(boolean article_deleted) {

    this.article_deleted = article_deleted;
  }

  public void setArticle_deleted_date(String article_deleted_date) {
    this.article_deleted_date = article_deleted_date;
  }

  public void setArticle_restored_date(String article_restored_date) {
    this.article_restored_date = article_restored_date;
  }

  public void setArticle_author_permission(byte article_author_permission) {
    this.article_author_permission = article_author_permission;
  }

  public void setArticle_notes(String article_notes) {
    this.article_notes = article_notes;
  }

  public void setArticle_enable(boolean article_enable) {

    this.article_enable = article_enable;
  }

  public void setArticle_visited(int article_visited) {
    this.article_visited = article_visited;
  }

  public void setArticle_last_modified(String article_last_modified) {
    this.article_last_modified = article_last_modified;
  }

  public void setArticle_comment_sum(byte article_comment_sum) {
    this.article_comment_sum = article_comment_sum;
  }

  public void setArticle_image_sum(byte article_image_sum) {
    this.article_image_sum = article_image_sum;
  }

  public void setArticle_author_image(String article_author_image) {
    this.article_author_image = article_author_image;
  }

  public void setArticle_group_id(int article_group_id) {
    this.article_group_id = article_group_id;
  }

  public void setArticle_tag(String article_tag) {
    this.article_tag = article_tag;
  }

  public void setArticle_tag_en(String article_tag_en) {
    this.article_tag_en = article_tag_en;
  }

  public void setArticle_language(String article_language) {
    this.article_language = article_language;
  }

  public int getArticle_id() {
    return article_id;
  }

  public String getArticle_title() {
    return article_title;
  }

  public String getArticle_created_date() {
    return article_created_date;
  }

  public String getArticle_author_name() {

    return article_author_name;
  }

  public String getArticle_title_en() {
    return article_title_en;
  }

  public boolean isArticle_deleted() {

    return article_deleted;
  }

  public String getArticle_deleted_date() {
    return article_deleted_date;
  }

  public String getArticle_restored_date() {
    return article_restored_date;
  }

  public byte getArticle_author_permission() {
    return article_author_permission;
  }

  public String getArticle_notes() {
    return article_notes;
  }

  public boolean isArticle_enable() {

    return article_enable;
  }

  public int getArticle_visited() {
    return article_visited;
  }

  public String getArticle_last_modified() {
    return article_last_modified;
  }

  public byte getArticle_comment_sum() {
    return article_comment_sum;
  }

  public byte getArticle_image_sum() {
    return article_image_sum;
  }

  public String getArticle_author_image() {
    return article_author_image;
  }

  public int getArticle_group_id() {
    return article_group_id;
  }

  public String getArticle_tag() {
    return article_tag;
  }

  public String getArticle_tag_en() {
    return article_tag_en;
  }

  public String getArticle_language() {
    return article_language;
  }
}
