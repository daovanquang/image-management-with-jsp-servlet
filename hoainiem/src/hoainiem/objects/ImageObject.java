package hoainiem.objects;

public class ImageObject {
  private int image_id;
  private String image_title;
  private String image_source;
  private String image_created_date;
  private String image_author_name;
  private String image_title_en;
  private String image_tag;
  private String image_tag_en;
  private String image_language;
  private byte image_author_permission;
  private String image_notes;
  private int image_article_id;
  private boolean image_enable;
  private int image_visited;
  private String image_last_modified;
  private int image_group_id;
  private boolean image_forslide;
  private String image_name;
  private String image_author_image;
  private boolean image_forimpressive;
  private boolean image_deleted;
  private String image_restored_date;
  private String image_deleted_date;
  public ImageObject() {
  }

  public void setImage_id(int image_id) {
    this.image_id = image_id;
  }

  public void setImage_title(String image_title) {
    this.image_title = image_title;
  }

  public void setImage_source(String image_source) {
    this.image_source = image_source;
  }

  public void setImage_created_date(String image_created_date) {
    this.image_created_date = image_created_date;
  }

  public void setImage_author_name(String image_author_name) {

    this.image_author_name = image_author_name;
  }

  public void setImage_title_en(String image_title_en) {
    this.image_title_en = image_title_en;
  }

  public void setImage_tag(String image_tag) {
    this.image_tag = image_tag;
  }

  public void setImage_tag_en(String image_tag_en) {
    this.image_tag_en = image_tag_en;
  }

  public void setImage_language(String image_language) {
    this.image_language = image_language;
  }

  public void setImage_author_permission(byte image_author_permission) {
    this.image_author_permission = image_author_permission;
  }

  public void setImage_notes(String image_notes) {

    this.image_notes = image_notes;
  }

  public void setImage_article_id(int image_article_id) {
    this.image_article_id = image_article_id;
  }

  public void setImage_enable(boolean image_enable) {
    this.image_enable = image_enable;
  }

  public void setImage_visited(int image_visited) {
    this.image_visited = image_visited;
  }

  public void setImage_last_modified(String image_last_modified) {
    this.image_last_modified = image_last_modified;
  }

  public void setImage_group_id(int image_group_id) {
    this.image_group_id = image_group_id;
  }

  public void setImage_forslide(boolean image_forslide) {
    this.image_forslide = image_forslide;
  }

  public void setImage_name(String image_name) {
    this.image_name = image_name;
  }

  public void setImage_author_image(String image_author_image) {
    this.image_author_image = image_author_image;
  }

  public void setImage_forimpressive(boolean image_forimpressive) {
    this.image_forimpressive = image_forimpressive;
  }

  public void setImage_deleted(boolean image_deleted) {
    this.image_deleted = image_deleted;
  }

  public void setImage_restored_date(String image_restored_date) {
    this.image_restored_date = image_restored_date;
  }

  public void setImage_deleted_date(String image_deleted_date) {
    this.image_deleted_date = image_deleted_date;
  }

  public int getImage_id() {
    return image_id;
  }

  public String getImage_title() {
    return image_title;
  }

  public String getImage_source() {
    return image_source;
  }

  public String getImage_created_date() {
    return image_created_date;
  }

  public String getImage_author_name() {

    return image_author_name;
  }

  public String getImage_title_en() {
    return image_title_en;
  }

  public String getImage_tag() {
    return image_tag;
  }

  public String getImage_tag_en() {
    return image_tag_en;
  }

  public String getImage_language() {
    return image_language;
  }

  public byte getImage_author_permission() {
    return image_author_permission;
  }

  public String getImage_notes() {

    return image_notes;
  }


  public int getImage_article_id() {
    return image_article_id;
  }

  public boolean isImage_enable() {
    return image_enable;
  }

  public int getImage_visited() {
    return image_visited;
  }

  public String getImage_last_modified() {
    return image_last_modified;
  }

  public int getImage_group_id() {
    return image_group_id;
  }

  public boolean isImage_forslide() {
    return image_forslide;
  }

  public String getImage_name() {
    return image_name;
  }

  public String getImage_author_image() {
    return image_author_image;
  }

  public boolean isImage_forimpressive() {
    return image_forimpressive;
  }

  public boolean isImage_deleted() {
    return image_deleted;
  }

  public String getImage_restored_date() {
    return image_restored_date;
  }

  public String getImage_deleted_date() {
    return image_deleted_date;
  }
}
