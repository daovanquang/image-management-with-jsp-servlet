package hoainiem.objects;

public class InfoObject {
  public InfoObject() {
  }

  private int info_id;
  private boolean info_enable;
  private String info_title;
  private String info_primary_title;
  private String info_content;
  private String info_link_text;
  private String info_link_source;
  private String info_notes;
  private String info_created_date;
  private String info_author_name;
  private byte info_author_permission;
  private String info_language;
  private String info_title_en;
  private String info_primary_title_en;
  private String info_content_en;
  private String info_link_text_en;

  public void setInfo_id(int info_id) {
    this.info_id = info_id;
  }

  public void setInfo_enable(boolean info_enable) {
    this.info_enable = info_enable;
  }

  public void setInfo_title(String info_title) {
    this.info_title = info_title;
  }

  public void setInfo_primary_title(String info_primary_title) {
    this.info_primary_title = info_primary_title;
  }

  public void setInfo_content(String info_content) {
    this.info_content = info_content;
  }

  public void setInfo_link_text(String info_link_text) {
    this.info_link_text = info_link_text;
  }

  public void setInfo_link_source(String info_link_source) {
    this.info_link_source = info_link_source;
  }

  public void setInfo_notes(String info_notes) {
    this.info_notes = info_notes;
  }

  public void setInfo_created_date(String info_created_date) {
    this.info_created_date = info_created_date;
  }

  public void setInfo_author_name(String info_author_name) {
    this.info_author_name = info_author_name;
  }

  public void setInfo_author_permission(byte info_author_permission) {
    this.info_author_permission = info_author_permission;
  }

  public void setInfo_language(String info_language) {
    this.info_language = info_language;
  }

  public void setInfo_title_en(String info_title_en) {
    this.info_title_en = info_title_en;
  }

  public void setInfo_primary_title_en(String info_primary_title_en) {
    this.info_primary_title_en = info_primary_title_en;
  }

  public void setInfo_content_en(String info_content_en) {

    this.info_content_en = info_content_en;
  }

  public void setInfo_link_text_en(String info_link_text_en) {
    this.info_link_text_en = info_link_text_en;
  }

  public int getInfo_id() {
    return info_id;
  }

  public boolean isInfo_enable() {
    return info_enable;
  }

  public String getInfo_title() {
    return info_title;
  }

  public String getInfo_primary_title() {
    return info_primary_title;
  }

  public String getInfo_content() {
    return info_content;
  }

  public String getInfo_link_text() {
    return info_link_text;
  }

  public String getInfo_link_source() {
    return info_link_source;
  }

  public String getInfo_notes() {
    return info_notes;
  }

  public String getInfo_created_date() {
    return info_created_date;
  }

  public String getInfo_author_name() {
    return info_author_name;
  }

  public byte getInfo_author_permission() {
    return info_author_permission;
  }

  public String getInfo_language() {
    return info_language;
  }

  public String getInfo_title_en() {
    return info_title_en;
  }

  public String getInfo_primary_title_en() {
    return info_primary_title_en;
  }

  public String getInfo_content_en() {

    return info_content_en;
  }

  public String getInfo_link_text_en() {
    return info_link_text_en;
  }

}
