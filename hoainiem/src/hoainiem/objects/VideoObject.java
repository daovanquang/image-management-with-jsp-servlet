package hoainiem.objects;

public class VideoObject {
  public VideoObject() {
  }

  private int video_id;
  private String video_title;
  private String video_source;
  private String video_image;
  private String video_created_date;
  private String video_author_name;
  private String video_timmer;
  private String video_notes;
  private boolean video_enable;
  private boolean video_formultipurpose;
  private byte video_author_permission;
  private String video_author_image;
  private int video_article_id;
  private String video_tag;
  private String video_title_en;
  private String video_language;
  private String video_last_modified;
  private int video_group_id;
  private String video_tag_en;
  private short video_visited;
  private String video_restored_date;
  private String video_deleted_date;
  private boolean video_deleted;

  public void setVideo_id(int video_id) {
    this.video_id = video_id;
  }

  public void setVideo_title(String video_title) {
    this.video_title = video_title;
  }

  public void setVideo_source(String video_source) {
    this.video_source = video_source;
  }

  public void setVideo_image(String video_image) {
    this.video_image = video_image;
  }

  public void setVideo_created_date(String video_created_date) {
    this.video_created_date = video_created_date;
  }

  public void setVideo_author_name(String video_author_name) {
    this.video_author_name = video_author_name;
  }

  public void setVideo_timmer(String video_timmer) {
    this.video_timmer = video_timmer;
  }

  public void setVideo_notes(String video_notes) {
    this.video_notes = video_notes;
  }

  public void setVideo_enable(boolean video_enable) {
    this.video_enable = video_enable;
  }

  public void setVideo_formultipurpose(boolean video_formultipurpose) {
    this.video_formultipurpose = video_formultipurpose;
  }

  public void setVideo_author_permission(byte video_author_permission) {
    this.video_author_permission = video_author_permission;
  }

  public void setVideo_author_image(String video_author_image) {
    this.video_author_image = video_author_image;
  }

  public void setVideo_article_id(int video_article_id) {
    this.video_article_id = video_article_id;
  }

  public void setVideo_tag(String video_tag) {
    this.video_tag = video_tag;
  }

  public void setVideo_title_en(String video_title_en) {
    this.video_title_en = video_title_en;
  }

  public void setVideo_language(String video_language) {
    this.video_language = video_language;
  }

  public void setVideo_last_modified(String video_last_modified) {
    this.video_last_modified = video_last_modified;
  }

  public void setVideo_group_id(int video_group_id) {
    this.video_group_id = video_group_id;
  }

  public void setVideo_tag_en(String video_tag_en) {
    this.video_tag_en = video_tag_en;
  }

  public void setVideo_visited(short video_visited) {
    this.video_visited = video_visited;
  }

  public void setVideo_restored_date(String video_restored_date) {
    this.video_restored_date = video_restored_date;
  }

  public void setVideo_deleted_date(String video_deleted_date) {

    this.video_deleted_date = video_deleted_date;
  }

  public void setVideo_deleted(boolean video_deleted) {
    this.video_deleted = video_deleted;
  }

  public int getVideo_id() {
    return video_id;
  }

  public String getVideo_title() {
    return video_title;
  }

  public String getVideo_source() {
    return video_source;
  }

  public String getVideo_image() {
    return video_image;
  }

  public String getVideo_created_date() {
    return video_created_date;
  }

  public String getVideo_author_name() {
    return video_author_name;
  }

  public String getVideo_timmer() {
    return video_timmer;
  }

  public String getVideo_notes() {
    return video_notes;
  }

  public boolean isVideo_enable() {
    return video_enable;
  }

  public boolean isVideo_formultipurpose() {
    return video_formultipurpose;
  }

  public byte getVideo_author_permission() {
    return video_author_permission;
  }

  public String getVideo_author_image() {
    return video_author_image;
  }

  public int getVideo_article_id() {
    return video_article_id;
  }

  public String getVideo_tag() {
    return video_tag;
  }

  public String getVideo_title_en() {
    return video_title_en;
  }

  public String getVideo_language() {
    return video_language;
  }

  public String getVideo_last_modified() {
    return video_last_modified;
  }

  public int getVideo_group_id() {
    return video_group_id;
  }

  public String getVideo_tag_en() {
    return video_tag_en;
  }

  public short getVideo_visited() {
    return video_visited;
  }

  public String getVideo_restored_date() {
    return video_restored_date;
  }

  public String getVideo_deleted_date() {

    return video_deleted_date;
  }

  public boolean isVideo_deleted() {
    return video_deleted;
  }
}
