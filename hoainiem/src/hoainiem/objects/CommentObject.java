package hoainiem.objects;

public class CommentObject {
  private int comment_id;
  private String comment_content;
  private String comment_created_date;
  private String comment_author_name;
  private String comment_last_modified;
  private String comment_tag;
  private int comment_article_id;
  private boolean comment_enable;
  private String comment_author_image;
  private byte comment_author_permission;
  private String comment_content_en;
  private int comment_parent_id;
  private String comment_deleted_date;
  private String comment_restored_date;
  private boolean comment_deleted;

  public CommentObject() {
  }
  public void setComment_id(int comment_id) {
    this.comment_id = comment_id;
  }

  public void setComment_content(String comment_content) {
    this.comment_content = comment_content;
  }

  public void setComment_created_date(String comment_created_date) {
    this.comment_created_date = comment_created_date;
  }

  public void setComment_author_name(String comment_author_name) {

    this.comment_author_name = comment_author_name;
  }

  public void setComment_last_modified(String comment_last_modified) {

    this.comment_last_modified = comment_last_modified;
  }

  public void setComment_tag(String comment_tag) {
    this.comment_tag = comment_tag;
  }

  public void setComment_article_id(int comment_article_id) {
    this.comment_article_id = comment_article_id;
  }

  public void setComment_enable(boolean comment_enable) {

    this.comment_enable = comment_enable;
  }

  public void setComment_author_image(String comment_author_image) {
    this.comment_author_image = comment_author_image;
  }

  public void setComment_author_permission(byte comment_author_permission) {
    this.comment_author_permission = comment_author_permission;
  }

  public void setComment_content_en(String comment_content_en) {
    this.comment_content_en = comment_content_en;
  }

  public void setComment_parent_id(int comment_parent_id) {
    this.comment_parent_id = comment_parent_id;
  }

  public void setComment_deleted_date(String comment_deleted_date) {
    this.comment_deleted_date = comment_deleted_date;
  }

  public void setComment_restored_date(String comment_restored_date) {
    this.comment_restored_date = comment_restored_date;
  }

  public void setComment_deleted(boolean comment_deleted) {
    this.comment_deleted = comment_deleted;
  }

  public int getComment_id() {
    return comment_id;
  }

  public String getComment_content() {
    return comment_content;
  }

  public String getComment_created_date() {
    return comment_created_date;
  }

  public String getComment_author_name() {

    return comment_author_name;
  }

  public String getComment_last_modified() {

    return comment_last_modified;
  }

  public String getComment_tag() {
    return comment_tag;
  }

  public int getComment_article_id() {
    return comment_article_id;
  }

  public boolean isComment_enable() {

    return comment_enable;
  }

  public String getComment_author_image() {
    return comment_author_image;
  }

  public byte getComment_author_permission() {
    return comment_author_permission;
  }

  public String getComment_content_en() {
    return comment_content_en;
  }

  public int getComment_parent_id() {
    return comment_parent_id;
  }

  public String getComment_deleted_date() {
    return comment_deleted_date;
  }

  public String getComment_restored_date() {
    return comment_restored_date;
  }

  public boolean isComment_deleted() {
    return comment_deleted;
  }
}
