package hoainiem.objects;

public class GroupObject {
  private int group_id;
  private String group_name;
  private String group_created_date;
  private byte group_permission;
  private String group_notes;
  private String group_author_name;
  private String group_image;
  private boolean group_enable;
  private String group_url_link;
  private byte group_member_sum;
  private String group_last_modified;
  public GroupObject() {
  }

  public void setGroup_id(int group_id) {
    this.group_id = group_id;
  }

  public void setGroup_name(String group_name) {
    this.group_name = group_name;
  }

  public void setGroup_created_date(String group_created_date) {
    this.group_created_date = group_created_date;
  }

  public void setGroup_permission(byte group_permission) {
    this.group_permission = group_permission;
  }

  public void setGroup_notes(String group_notes) {
    this.group_notes = group_notes;
  }

  public void setGroup_author_name(String group_author_name) {

    this.group_author_name = group_author_name;
  }

  public void setGroup_image(String group_image) {

    this.group_image = group_image;
  }

  public void setGroup_enable(boolean group_enable) {
    this.group_enable = group_enable;
  }

  public void setGroup_url_link(String group_url_link) {
    this.group_url_link = group_url_link;
  }

  public void setGroup_member_sum(byte group_member_sum) {
    this.group_member_sum = group_member_sum;
  }

  public void setGroup_last_modified(String group_last_modified) {
    this.group_last_modified = group_last_modified;
  }

  public int getGroup_id() {
    return group_id;
  }

  public String getGroup_name() {
    return group_name;
  }

  public String getGroup_created_date() {
    return group_created_date;
  }

  public byte getGroup_permission() {
    return group_permission;
  }

  public String getGroup_notes() {
    return group_notes;
  }

  public String getGroup_author_name() {

    return group_author_name;
  }

  public String getGroup_image() {

    return group_image;
  }

  public boolean isGroup_enable() {
    return group_enable;
  }

  public String getGroup_url_link() {
    return group_url_link;
  }

  public byte getGroup_member_sum() {
    return group_member_sum;
  }

  public String getGroup_last_modified() {
    return group_last_modified;
  }

}
