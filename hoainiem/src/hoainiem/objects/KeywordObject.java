package hoainiem.objects;

public class KeywordObject {
  private int keyword_id;
  private String keyword_value;
  private String keyword_created_date;
  private boolean keyword_deleted;
  private String keyword_restored_date;
  private boolean keyword_enable;
  private int keyword_search_count;
  private String keyword_category;
  private String keyword_author_created;
  private String keyword_url;
  private byte keyword_author_permission;
  private String keyword_title;
  private String keyword_summary;

  public int getKeyword_id() {
    return keyword_id;
  }

  public String getKeyword_value() {
    return keyword_value;
  }

  public String getKeyword_created_date() {
    return keyword_created_date;
  }

  public boolean isKeyword_deleted() {
    return keyword_deleted;
  }

  public String getKeyword_restored_date() {
    return keyword_restored_date;
  }

  public boolean isKeyword_enable() {
    return keyword_enable;
  }

  public int getKeyword_search_count() {
    return keyword_search_count;
  }

  public String getKeyword_category() {

    return keyword_category;
  }

  public String getKeyword_author_created() {
    return keyword_author_created;
  }

  public String getKeyword_url() {
    return keyword_url;
  }

  public byte getKeyword_author_permission() {
    return keyword_author_permission;
  }

  public String getKeyword_title() {
    return keyword_title;
  }

  public String getKeyword_summary() {
    return keyword_summary;
  }

  public void setKeyword_id(int keyword_id) {
    this.keyword_id = keyword_id;
  }

  public void setKeyword_value(String keyword_value) {
    this.keyword_value = keyword_value;
  }

  public void setKeyword_created_date(String keyword_created_date) {
    this.keyword_created_date = keyword_created_date;
  }

  public void setKeyword_deleted(boolean keyword_deleted) {
    this.keyword_deleted = keyword_deleted;
  }

  public void setKeyword_restored_date(String keyword_restored_date) {
    this.keyword_restored_date = keyword_restored_date;
  }

  public void setKeyword_enable(boolean keyword_enable) {
    this.keyword_enable = keyword_enable;
  }

  public void setKeyword_search_count(int keyword_search_count) {
    this.keyword_search_count = keyword_search_count;
  }

  public void setKeyword_category(String keyword_category) {

    this.keyword_category = keyword_category;
  }

  public void setKeyword_author_created(String keyword_author_created) {
    this.keyword_author_created = keyword_author_created;
  }

  public void setKeyword_url(String keyword_url) {
    this.keyword_url = keyword_url;
  }

  public void setKeyword_author_permission(byte keyword_author_permission) {
    this.keyword_author_permission = keyword_author_permission;
  }

  public void setKeyword_title(String keyword_title) {
    this.keyword_title = keyword_title;
  }

  public void setKeyword_summary(String keyword_summary) {
    this.keyword_summary = keyword_summary;
  }

}
