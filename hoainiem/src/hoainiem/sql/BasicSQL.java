package hoainiem.sql;

import hoainiem.values.options.*;
import hoainiem.library.*;

public class BasicSQL {
  public static String sqlSELECT(String name, String fieldsSELECT, String conds){
    if (name == null)
      return null;

    String sql = "SELECT ";

    boolean valid = false;
    if (fieldsSELECT != null) {
      if(!fieldsSELECT.equalsIgnoreCase("")){
        valid = true;
      }
    }

    if(valid){
      sql += fieldsSELECT;
    }else {
      sql += "*";
    }

    String tblname = SQL.PRE_TABLE_NAME.toLowerCase() + name;
    sql += "FROM " + tblname;

    if (conds != null) {
      sql += " WHERE " + conds;
    }

    return sql;
  }

  public static String sqlSELECT_FIELDS(String name, String []fields, int []notSELECT, String conds){
    boolean flag = true;
    if(name == null || fields == null){
      flag = false;
    }

    int len = fields.length;
    if(len==0){
      flag = true;
    }
    if(!flag) return null;

    String field_select = "";

    String pre = name + "_";
    if(notSELECT!=null){
      if (notSELECT.length != 0) {
        for (int i = 0; i < len; i++) {
          boolean valid = true;
          for (int nSelect : notSELECT){
            if(i==nSelect){
              valid = false;
              break;
            }
          }

          if (valid) {
            field_select += (i == 0) ? pre + fields[i] : "," + pre + fields[i];
          }
        }
      }
      field_select = "*";
    }else{
      field_select = "*";
    }
    return sqlSELECT(name, field_select,conds);

  }


  public static String sqlSELECT_BY_CONDS(String name, String fieldsSELECT, String []fields, int []cond_fields){
    if(name == null) return null;
    String conds = "";

    String pre = name + "_";

    int len = fields.length;


    if (cond_fields != null) {
      int cf_len = cond_fields.length;
      for (int i = 0; i < cf_len; i++) {
        boolean valid = false;
        int index = cond_fields[i];
        if (index < len) {
          valid = true;
        }

        if (valid) {
          conds += (i != 0) ? " " : "AND";
          conds += pre + fields[index] + "= ?";
          conds += (i != 0) ? "," : "";
        }
      }
    }

    return sqlSELECT(name, fieldsSELECT,conds);

  }
  public static String sqlSELECT_FIELDS_BY_CONDS(String name, int []fieldsSELECT, String []fields, int []cond_fields){
    String fieldSELECT = "";
    int len = fields.length;
    String pre = name + "_";

    if(fieldsSELECT!=null){
      int cond_len = fieldsSELECT.length;
      for (int i = 0; i < cond_len; i++) {
        boolean valid = false;
        int index = fieldsSELECT[i];
        if(index<len){
          valid = true;
        }

        if(valid){
          fieldSELECT += pre+fields[index];
          fieldSELECT += (i!=0) ? ",":"";
        }
      }
    }

    return sqlSELECT_BY_CONDS(name, fieldSELECT, fields, cond_fields);
  }

  public static String sqlINSERT(String name, String[] fields, int[] notInsert){
    byte fields_len = (byte) fields.length;
    boolean notUpd_isValid = Utilities.checkIntArray(notInsert);

    String pre = name + "_";

    String values = " VALUES(";

    String tblname = SQL.PRE_TABLE_NAME.toLowerCase()+name;
    String sql = "INSERT INTO " + tblname + "(";

    boolean notFirst = true;
    for(int i=0;i<fields_len;i++){
      boolean valid = true;
      if (notUpd_isValid) {
        for (int notUpd_index : notInsert) {
          if (i == notUpd_index) {
            valid = false;
            break;
          }
        }
      }
      else {
        valid = false;
      }

      if (valid) {

        String field = pre + fields[i];
        if(!notFirst){
          sql += " ,"+field ;
          values += ",?";
        }else{
          sql += field;
          values += "?";
        }
        notFirst = false;
      }
    }
    sql += " ) "+values+" ) ";;

    return sql;

  }

  public static String sqlUPDATE(String name, String[] fields, int[] notUpdate, int fieldIndex) {
    byte fields_len = (byte) fields.length;
    boolean notUpd_isValid = Utilities.checkIntArray(notUpdate);

    String pre = name + "_";
    String field_upd = pre + fields[fieldIndex];

    String tblname = SQL.PRE_TABLE_NAME.toLowerCase()+name;
    String sql = "UPDATE " + tblname + " SET ";

    boolean notFirst = true;
    for (int i = 0; i < fields_len; i++) {
      boolean valid = true;
      if(i!=fieldIndex){
        if (notUpd_isValid) {
          for (int notUpd_index : notUpdate) {
            if (i == notUpd_index) {
              valid = false;
              break;
            }
          }
        }
      }else{
        valid = false;
      }

      if (valid) {
        sql += (!notFirst) ? pre+fields[i] + " = ?, " : pre+fields[i] + " = ? ";
        notFirst = true;
      }
    }
    sql += "WHERE " + field_upd + " = ?";
    return sql;
  }

  public static String sqlCOUNT(String fieldCount, String name, String conds){

    fieldCount = (fieldCount == null) ? "*" : (fieldCount == "") ? "*" : fieldCount;
    String pre = name + "_";
    String tblname = SQL.PRE_TABLE_NAME.toLowerCase() + name;

    String sql = "SELECT COUNT(" + fieldCount + ") FROM " + tblname;
    if (conds != null) {
      if(!conds.equalsIgnoreCase("")){
        sql += " WHERE " + conds;
      }
    }
    return sql;

  }

  public static String sqlCOUNT_BY_CONDS(String fieldCount, String name, String []fields, int []conditions, String []values){
    if(fields==null) return null;
    int len = fields.length;
    String pre = name+"_";
    String conds = "";
    if(conditions!=null && values!=null){
      int conds_len = conditions.length;
      if(conds_len==values.length){
        for(int i = 0; i<conds_len;i++){
            int index = conditions[i];
            conds += (i != 0) ? " AND " : "";
            conds += pre + fields[index] + "=" + values[i] + " ";
        }
      }
    }
    return sqlCOUNT(fieldCount, name, conds);
  }


  public static String sqlCountUp(String name, String []fields, int fieldIndex , int []fields_condition){
     if(fields==null) return null;
     int conditions_len = fields_condition.length;
     if(conditions_len<1) return null;

     String pre = name + "_";
     String tblname = SQL.PRE_TABLE_NAME.toLowerCase()+name;
     String sql = "UPDATE "+tblname+" SET ";
     String field_upd = pre+fields[fieldIndex];
     sql+= field_upd + " = "+ field_upd +" +1  WHERE ";
     for(int i=0; i<conditions_len;i++){
       int index = fields_condition[i];
       String field = pre+fields[index];
       sql+= (i==0) ?  field + " = ? " : " AND " + field + " = ? ";
     }
     return sql;
  }
}
