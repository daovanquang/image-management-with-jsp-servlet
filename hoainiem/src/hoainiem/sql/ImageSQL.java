package hoainiem.sql;

public class ImageSQL {
  public static String NAME = "image";
  public static String []FIELDS = {"id","title","source","created_date","author_name",
      "title_en","tag","tag_en","language","author_permission",
      "notes","article_id","enable","visited","last_modified",
      "group_id","forslide","name","author_image","forimpressive",
      "deleted","restored_date","deleted_date"
  };
}
