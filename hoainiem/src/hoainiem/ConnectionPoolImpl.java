package hoainiem;

import java.sql.*;
import java.util.*;
public class ConnectionPoolImpl implements ConnectionPool {

    //Trinh dieu khien lam viec voi CSDL
    private String driver;
    //Duong dan thuc thi CSDL (mySQL)
    private String url;
    //Tai khoan dang nhap CSDL
    private String username;
    private String userpass;

    //Doi tuong luu tru cac ket noi
    private Stack pool;

    public ConnectionPoolImpl() {
        //Xac dinh trinh dieu khien
        this.driver="com.mysql.jdbc.Driver";

        //Nap trinh dieu khien
        try {
            Class.forName(this.driver).newInstance();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        //Xac dinh duong dan thuc thi
        this.url = "jdbc:mysql://localhost:3306/trikys_db";

        //Xac dinh tai khoan lam viec
        this.username = "trikys_name";
        this.userpass = "trikys_pass";

        //Khoi tao doi tuong luu tru
        this.pool = new Stack();
    }

    public Connection getConnection(String objectName) throws SQLException {

        //Kiem tra doi tuong luu tru ket noi
        if(this.pool.isEmpty()){
            //Khoi tao ket noi moi
            System.out.println(objectName+" have created a new Connection.");
            return DriverManager.getConnection(this.url, this.username, this.userpass);
        }
        else{
            //Lay ket noi da ton tai
            System.out.println(objectName+" have popped the Connection.");
            return (Connection)this.pool.pop();
        }
    }

    public void releaseConnection(Connection con, String objectName) throws SQLException {
        //Yeu cau tra lai ket noi
        System.out.println(objectName +" have pushed the Connection.");
        this.pool.push(con);
    }

    protected void finalize() throws Throwable{
        this.pool.clear();
        this.pool = null;

        super.finalize();

        System.out.println("ConnectionPool is closed.");
    }

}
