
     **RETROGUARD(tm) BYTECODE OBFUSCATOR, OPEN SOURCE RELEASE**

Thank you for downloading the Open Source release of the RetroGuard
bytecode obfuscator. If you have questions about using RetroGuard,
or wish to report a bug to be fixed for the next minor-version 
distribution, please contact info@retrologic.com

Included in this release are the following files and directories:

Readme.txt     : This file.

LGPL.txt       : RetroGuard is free software, distributed under the GNU 
                 Lesser General Public License.

Changes.txt    : Change history of the software.

FAQ.txt        : Answers to a list of common questions about obfuscation 
                 and RetroGuard.

docs.html      : Documentation for RetroGuard in HTML format. 

retroguard.jar : A JAR (Java ARchive) file containing the classes that
                 make up the RetroGuard Bytecode Obfuscator.

src-dist.tar.gz: Source files for RetroGuard.


If you have any questions please contact:

  info@retrologic.com


--29th October 2002--
