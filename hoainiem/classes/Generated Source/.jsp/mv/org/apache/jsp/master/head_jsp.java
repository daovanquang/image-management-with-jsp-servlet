package org.apache.jsp.master;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.home.menu.*;
import hoainiem.library.*;
import hoainiem.values.gui.*;
import hoainiem.lang.gui.*;

public final class head_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "data.jsp", out, true);
      out.write('\r');
      out.write('\n');

String webModule = AllGUI.WebModule;

byte lang_value = Utilities.getLangValue(session);
String title = HomeLang.COMPANY_NAME[lang_value]+" - "+"website lưu trữ ảnh số 0 Việt Nam";

String pos = Utilities.getURI(request,(byte)2);

/* =============== SET TITLE ================= */
HomeMenuObject[] items = HomeMenu.createHomeMenu();

if(!pos.equalsIgnoreCase("")){
  for(int i=1; i<items.length; i++){
    if(items[i].getHomemenu_path().equalsIgnoreCase(pos)){
      if(lang_value==1){
        //English
        title = items[i].getHomemenu_name()+ "&nbsp;&nbsp;|&nbsp;&nbsp;" +title;
      }else{
        title = items[i].getHomemenu_name_en()+ "&nbsp;&nbsp;|&nbsp;&nbsp;" +title;
      }
    }
  }
}

      out.write("\r\n<head>\r\n  <title>");
      out.print(title );
      out.write("</title>\r\n  <meta name=\"viewport\" content=\"width=device-width\"/> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n  <!-- ======================= STYLES & JS =========================== -->\r\n  <link href=\"/");
      out.print(webModule);
      out.write("/css/style.css\" rel=\"stylesheet\" type=\"text/css\">\r\n  <link href=\"/");
      out.print(webModule);
      out.write("/imgs/favicon.png\" rel=\"icon\" type=\"image/x-icon\">\r\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n</head>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
