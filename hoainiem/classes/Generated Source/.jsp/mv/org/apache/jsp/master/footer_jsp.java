package org.apache.jsp.master;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.library.*;
import hoainiem.values.gui.*;
import hoainiem.lang.gui.*;

public final class footer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n");

String webModule = AllGUI.WebModule;
byte lang_value = hoainiem.library.Utilities.getLangValue(session);

      out.write("\r\n\r\n<!--footer-->\r\n<div class=\"footer row\">\r\n  <section class=\"blocks row\">\r\n    <div class=\"container main-1200\">\r\n      <div class=\"about-us-block block\">\r\n        <div class=\"footer__logo\">\r\n          <img class=\"img-fill\" src=\"/");
      out.print(webModule);
      out.write("/imgs/favicon.png\" alt=\"\"/>\r\n        </div>\r\n        <div class=\"mt-10\">\r\n        ");
      out.print(HomeLang.COMPANY_INTRO[lang_value] );
      out.write("\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"information-block block\">\r\n        <h4 class=\"text-uppercase\">\r\n        Site Map\r\n        </h4>\r\n        <ol class=\"list-unstyle\">\r\n          <li>\r\n            <a href=\"#\">");
      out.print(HomeLang.HOME_MENU[lang_value]);
      out.write("</a>\r\n          </li>\r\n          <li>\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/about-us/\">");
      out.print(HomeLang.ABOUTUS_MENU[lang_value]);
      out.write("</a>\r\n          </li>\r\n          <li>\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/services/\">");
      out.print(HomeLang.SERVICES[lang_value]);
      out.write("</a>\r\n          </li>\r\n\r\n          <li>\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/photos/\">");
      out.print(HomeLang.PHOTOS_MENU[lang_value]);
      out.write("</a>\r\n          </li>\r\n\r\n          <li>\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/videos\">");
      out.print(HomeLang.VIDEO[lang_value]);
      out.write("</a>\r\n          </li>\r\n\r\n          <li>\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/community/\">");
      out.print(HomeLang.COMMUNITY_MENU[lang_value]);
      out.write("</a>\r\n          </li>\r\n\r\n          <li>\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/contact-us/\">");
      out.print(HomeLang.CONTACTUS_MENU[lang_value]);
      out.write("</a>\r\n          </li>\r\n        </ol>\r\n      </div>\r\n\r\n      <div class=\"category-block block\">\r\n        <h4 class=\"text-uppercase\">\r\n          ");
      out.print(HomeLang.CATEGORIES[lang_value]);
      out.write("\r\n        </h4>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/photos/control/?tab=0\">\r\n            ");
      out.print(HomeLang.NEWEST_PHOTOS[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/photos/control/?tab=1\">\r\n            ");
      out.print(HomeLang.IMPRESSIVE_PHOTOS[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/photos/control/?tab=0\">\r\n            ");
      out.print(HomeLang.ALL_PHOTO[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <h4 class=\"text-uppercase\">\r\n          ");
      out.print(HomeLang.FOLLOW_US[lang_value]);
      out.write("\r\n        </h4>\r\n        <p>\r\n          <a href=\"");
      out.print(AllGUI.SOCIAL_GOOGLE);
      out.write("\">\r\n          Google+\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"");
      out.print(AllGUI.SOCIAL_FACEBOOK);
      out.write("\">\r\n          Facebook\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"");
      out.print(AllGUI.SOCIAL_INSTAGRAM);
      out.write("\">\r\n            Instagram\r\n          </a>\r\n        </p>\r\n      </div>\r\n\r\n      <div class=\"account-block block\">\r\n        <h4 class=\"text-uppercase\">\r\n          ");
      out.print(HomeLang.ACCOUNT[lang_value]);
      out.write("\r\n        </h4>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/user/signup/\">\r\n            ");
      out.print(HomeLang.SIGNUP[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/user/signin/\">\r\n           ");
      out.print(HomeLang.LOGIN[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/user/forgotpass/\">\r\n            ");
      out.print(HomeLang.FORGOTPASS[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/user/changepass/\">\r\n            ");
      out.print(HomeLang.CHANGEPASS[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <h4 class=\"text-uppercase\">\r\n          ");
      out.print(HomeLang.CONNECT[lang_value]);
      out.write("\r\n        </h4>\r\n        <p>\r\n          <a href=\"#\">\r\n          Google\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"#\">\r\n          Facebook\r\n          </a>\r\n        </p>\r\n      </div>\r\n\r\n      <div class=\"contact-block block\">\r\n        <h4 class=\"text-uppercase\">\r\n           ");
      out.print(HomeLang.HELP_CENTER[lang_value]);
      out.write("\r\n        </h4>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/faqs/\">\r\n          ");
      out.print(HomeLang.FAQS[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/terms-of-service/\">\r\n          ");
      out.print(HomeLang.TERMS_OF_SERVICE[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <p>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/privacy-policy/\">\r\n          ");
      out.print(HomeLang.PRIVACY_POLICY[lang_value]);
      out.write("\r\n          </a>\r\n        </p>\r\n        <h4 class=\"text-uppercase\">\r\n           ");
      out.print(HomeLang.CONTACT_US[lang_value]);
      out.write("\r\n        </h4>\r\n        <p>\r\n          <a href=\"#\">Email</a>\r\n        </p>\r\n        <p>\r\n          <a href=\"#\">Mobiphone</a>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </section>\r\n  <hr>\r\n  <section class=\"copyright text-center row\">\r\n    <div class=\"container main-1200\">\r\n      <p class=\"pull-left\">");
      out.print(HomeLang.COPYRIGHT[lang_value]);
      out.write("</p>\r\n      <p class=\"pull-right\">");
      out.print(HomeLang.AUTHOR_TITLE[lang_value]);
      out.write("<a href=\"#\" class=\"label-bold label-hover\">");
      out.print(HomeLang.AUTHOR_NAME[lang_value]);
      out.write("</a></p>\r\n    </div>\r\n  </section>\r\n</div>\r\n<!--end footer-->\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
