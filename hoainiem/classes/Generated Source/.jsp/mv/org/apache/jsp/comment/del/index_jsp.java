package org.apache.jsp.comment.del;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.comment.*;
import hoainiem.library.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\r');
      out.write('\n');

int comment_id = Utilities.getIntParam(request,"cid");
if(comment_id>0){
  String datetime = UtilitiesCalendar.getCurrentTime("yyyy-MM-dd HH:mm");

  ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
  CommentControl cc = new CommentControl(cp);
  CommentObject delComment = new CommentObject();
  delComment.setComment_id(comment_id);
  delComment.setComment_created_date(datetime);
  boolean result = cc.delComment(delComment);

  if(result) {
    String referer = request.getHeader("Referer");
    response.sendRedirect(referer);
  }
}

      out.write("\r\n\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
