package org.apache.jsp.comment.ae;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONObject.*;
import org.json.*;
import hoainiem.gui.comment.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n");

//Tim bo quan ly ket noi
ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");

BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
String strJson = "";
if(br != null){
    strJson = br.readLine();
}
if(Utilities.checkValidString(strJson)){
  JSONObject json = null;
  try{
    json = new JSONObject(strJson);
  }catch(JSONException ex){
    ex.printStackTrace();
  }
  if(json!=null){
    /*============- Comment Info -============*/
    try{
      int articleId =  json.getInt("article_id");
      String authorName = json.getString("user_fullname");
      String authorImage = json.getString("user_image");
      byte authorPermission = (byte)json.getInt("user_permission");
      String commentContent = json.getString("content");

      String datetime = UtilitiesCalendar.getCurrentTime("yyyy-MM-dd HH:mm");
      boolean commentEnable = true;

      CommentObject nComment = new CommentObject();
      nComment.setComment_article_id(articleId);
      nComment.setComment_author_name(authorName);
      nComment.setComment_author_image(authorImage);
      nComment.setComment_author_permission(authorPermission);
      nComment.setComment_created_date(datetime);
      nComment.setComment_content(commentContent);

      nComment.setComment_enable(commentEnable);
      /*============- Add New Comment -============*/
      CommentControl cc = new CommentControl(cp);

      cc.addComment(nComment);
    }catch(JSONException ex){
      ex.printStackTrace();
    }
  }
}

      out.write("\r\n\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
