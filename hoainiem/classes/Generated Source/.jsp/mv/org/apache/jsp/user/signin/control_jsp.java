package org.apache.jsp.user.signin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.objects.*;
import hoainiem.gui.user.*;
import hoainiem.library.*;
import hoainiem.*;

public final class control_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n");

String referer = request.getHeader("Referer");

String username = Utilities.getValue(request.getParameter("txtUserName"));
String userpass = Utilities.getValue(request.getParameter("txtUserPass"));
userpass = Utilities.md5(userpass);

if(username != null && userpass != null){
  if(!username.equalsIgnoreCase("") && !userpass.equalsIgnoreCase("")){
    ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
    UserControl uc = new UserControl(cp);
    if(cp==null){
      getServletContext().setAttribute("CPool",uc.getCP());
    }
    UserObject user = (UserObject) uc.getUserObject(username , userpass);

    if(user!=null){
      request.getSession(true).setAttribute("userSigned", user );
      response.sendRedirect("../../community/");
    }else{

      response.sendRedirect(referer);
    }
  }else{
    response.sendRedirect(referer);
  }
}else{
  response.sendRedirect(referer);
}

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
