package org.apache.jsp.error;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.values.gui.*;
import hoainiem.lang.gui.*;
import hoainiem.library.Utilities;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n");

String webModule = AllGUI.WebModule;
byte lang_value = Utilities.getLangValue(session);

      out.write("\r\n<html>\r\n<head>\r\n<title>404 Not Found</title>\r\n<link href=\"/");
      out.print(webModule);
      out.write("/imgs/favicon.png\" rel=\"icon\" type=\"image/x-icon\">\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"/");
      out.print(webModule);
      out.write("/css/error.css\">\r\n</head>\r\n<body>\r\n<div class=\"face\">\r\n  <div class=\"band\">\r\n    <div class=\"red\"></div>\r\n    <div class=\"white\"></div>\r\n    <div class=\"blue\"></div>\r\n</div>\r\n<div class=\"eyes\"></div>\r\n<div class=\"dimples\"></div>\r\n<div class=\"mouth\"></div>\r\n</div>\r\n\r\n<h1 class=\"text-center\">");
      out.print(HomeLang.PAGE_NOT_FOUND[lang_value]);
      out.write("</h1>\r\n<a href=\"/");
      out.print(webModule);
      out.write("/\" class=\"text-center\">\r\n  <div class=\"btn\">");
      out.print(HomeLang.RETURN_HOME[lang_value]);
      out.write("</div>\r\n</a>\r\n</body>\r\n</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
