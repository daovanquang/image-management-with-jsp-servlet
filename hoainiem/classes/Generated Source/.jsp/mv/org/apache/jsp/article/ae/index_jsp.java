package org.apache.jsp.article.ae;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.json.JSONObject.*;
import org.json.*;
import java.io.*;
import org.apache.commons.io.*;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import hoainiem.gui.article.*;
import hoainiem.gui.image.*;
import hoainiem.gui.video.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.values.gui.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n");

byte lang_value = Utilities.getLangValue(session);

      out.write("\r\n\r\n@MultipartConfig\r\n");

/*=======- declare for store -=======*/
// location to store file uploaded
String UPLOAD_DIRECTORY = "uploads";
String ARTICLES_DIRECTORY = "articles";
String VIDEOS_DIRECTORY = "videos";
String PHOTOS_DIRECTORY = "photos";
String FILES_DIRECTORY = "files";

// upload settings
int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

/*=======- declare for json -=======*/
JSONObject json = null;
boolean result = false;
String articleImageAttach = "";
String datetime = "";
boolean articleEnable = true;
int articleId = 0;

ArticleObject nArticle = new ArticleObject();
ArrayList<VideoObject> nVideosAttach = new ArrayList<VideoObject>();


boolean isMultipart = ServletFileUpload.isMultipartContent(request);

if(isMultipart) {
  try {

    /*============- CREATE STORE CONFIG UPLOAD -============*/
    DiskFileItemFactory factory = new DiskFileItemFactory();

    factory.setSizeThreshold(MEMORY_THRESHOLD);
    factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

    ServletFileUpload upload = new ServletFileUpload(factory);

    upload.setFileSizeMax(MAX_FILE_SIZE);
    upload.setSizeMax(MAX_REQUEST_SIZE);


    String uploadPath = getServletContext().getRealPath("")+ File.separator + UPLOAD_DIRECTORY+ File.separator+ ARTICLES_DIRECTORY;
    String videosPath = uploadPath+File.separator+VIDEOS_DIRECTORY;
    String photosPath = uploadPath+File.separator+PHOTOS_DIRECTORY;
    String filesPath = uploadPath+File.separator+FILES_DIRECTORY;

    String webmodulePath = "/"+AllGUI.WebModule+"/";
    String realPath = getServletContext().getRealPath("");
    byte pos =  (byte) realPath.lastIndexOf('\\');

    //videos directory
    File uploadVideosDir = UtilitiesMedia.getFile(videosPath);
    //photos directory
    File uploadPhotosDir = UtilitiesMedia.getFile(photosPath);
    //videos directory
    File uploadFilesDir = UtilitiesMedia.getFile(filesPath);

    /*============- END STORE CONFIG UPLOAD -============*/

    //Tim bo quan ly ket noi
    ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");

    try {
      List items = upload.parseRequest(request);

      if(items != null){

        if( items.size()>0 ){
          //Lay files upload
          Iterator iterator = items.iterator();

          while (iterator.hasNext()) {
            FileItem item = (FileItem) iterator.next();

            //Neu la file
            if (!item.isFormField()) {
              //Them tep &  video

              //thong tin file
              String fileName = item.getName();
              String fileType = UtilitiesMedia.classifyFile(fileName);

              if(fileType == "video"){
                //upload videos
                File uploadedFile = new File(videosPath + File.separator + fileName);
                item.write(uploadedFile);

                String webModule = (pos>0)? webmodulePath: "/";
                String filePath = webModule+ UPLOAD_DIRECTORY + "/" + ARTICLES_DIRECTORY + "/" + VIDEOS_DIRECTORY +"/"+ fileName;

                String inputFilePath = uploadPath.replace("\\","//") +"//"+ fileName;
                String outputDirectory = uploadPath.replace("\\","//");
                String thumbnailName = "";
                String thumbnailRelativePath = (thumbnailName!="") ? webmodulePath + UPLOAD_DIRECTORY + "/" + thumbnailName : "";

                VideoObject nVideo = new VideoObject();
                nVideo.setVideo_created_date(datetime);
                nVideo.setVideo_image(thumbnailRelativePath);
                nVideo.setVideo_source(filePath);
                nVideo.setVideo_enable(articleEnable);
                nVideosAttach.add(nVideo);
              }else{
                //upload files
                File uploadedFile = new File(filesPath + File.separator + fileName);
                item.write(uploadedFile);
                String webModule = (pos>0)? webmodulePath: "/";
                String filePath = webModule + UPLOAD_DIRECTORY + "/" + ARTICLES_DIRECTORY + "/" + FILES_DIRECTORY +"/"+ fileName;
              }
            }else{
                articleImageAttach = item.getString();
            }
          }
        }
      }
    }catch(JSONException ex){
      ex.printStackTrace();
    }

    if(Utilities.checkValidString(articleImageAttach)){
      try{
        json = new JSONObject(articleImageAttach);
      }catch(JSONException ex){
        ex.printStackTrace();
      }
    }

    if(json!=null){
      /*============- ARTICLE TEXT INFO -============*/
      String articleAuthorName =  json.getString("author_fullname");
      String articleAuthorImage = json.getString("author_image");
      int articleAuthorGroupId = json.getInt("author_groupid");
      byte articleAuthorPermission = (byte)json.getInt("author_permission");

      String articleTitle = json.getString("article_title");
      String articleTags = json.getString("article_tags");

      datetime = UtilitiesCalendar.getCurrentTime("dd/MM/yyyy HH:mm");
      articleEnable = true;

      nArticle.setArticle_author_name(articleAuthorName);
      nArticle.setArticle_author_image(articleAuthorImage);
      nArticle.setArticle_group_id(articleAuthorGroupId);

      if(lang_value == 1){
        nArticle.setArticle_title_en(articleTitle);
        nArticle.setArticle_tag_en(articleTags);
      }else{
        nArticle.setArticle_title(articleTitle);
        nArticle.setArticle_tag(articleTags);
      }
      nArticle.setArticle_created_date(datetime);
      nArticle.setArticle_enable(articleEnable);
      nArticle.setArticle_author_permission(articleAuthorPermission);
      //Them bai viet moi
      ArticleControl ac = new ArticleControl(cp);

      articleId = ac.addArticle(nArticle);

      /*============- END ARTICLE TEXT INFO -============*/
      if(articleId>0){

        try{
          JSONArray imgs = json.getJSONArray("imgs");

          if(imgs!=null){
            if( imgs.length()>0 ){
              for (byte i = 0; i < imgs.length(); i++) {
                ImageObject nImage = new ImageObject();
                String dataType = imgs.getJSONObject(i).getString("type");
                String dataName = imgs.getJSONObject(i).getString("name");
                String dataValue = imgs.getJSONObject(i).getString("value");
                ImageControl ic = new ImageControl(cp);

                if(dataType.equalsIgnoreCase("img")){
                  String dataPath = photosPath +File.separator+ dataName;
                  UtilitiesMedia.decodeBase64(dataValue ,dataPath);

                  String webModule = (pos>0)? webmodulePath: "/";
                  String dataRelativePath = webmodulePath+ UPLOAD_DIRECTORY + "/" + ARTICLES_DIRECTORY + "/" + PHOTOS_DIRECTORY + "/"+ dataName;

                  nImage= new ImageObject();
                  nImage.setImage_article_id(articleId);
                  nImage.setImage_title(articleTitle);
                  nImage.setImage_author_name(articleAuthorName);
                  nImage.setImage_author_image(articleAuthorImage);
                  nImage.setImage_group_id(articleAuthorGroupId);
                  nImage.setImage_created_date(datetime);
                  nImage.setImage_source(dataRelativePath);
                  nImage.setImage_name(dataName);
                  nImage.setImage_enable(articleEnable);
                  nImage.setImage_author_permission(articleAuthorPermission);

                  ic.addImage(nImage);
                }
              }
            }

            if(nVideosAttach.size() > 0){
              VideoControl vc = new VideoControl(cp);
              for(VideoObject item: nVideosAttach){
                item.setVideo_article_id(articleId);
                item.setVideo_author_name(articleAuthorName);
                item.setVideo_author_image(articleAuthorImage);
                item.setVideo_group_id(articleAuthorGroupId);
                vc.addVideo(item);
              }
            }

          }
        }catch (JSONException e) {
          e.printStackTrace();
        }finally{
        }
      }
      ac.releaseConnection();
    }
  }catch (Exception e) {
    e.printStackTrace();
  }finally{
  }
}


      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
