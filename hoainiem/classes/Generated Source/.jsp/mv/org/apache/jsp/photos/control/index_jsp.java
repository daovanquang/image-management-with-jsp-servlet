package org.apache.jsp.photos.control;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.objects.*;
import hoainiem.gui.image.*;
import hoainiem.library.*;
import hoainiem.*;
import hoainiem.values.gui.*;
import java.util.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n");


String tab_active = (String) request.getParameter("tab");
String page_active = (String) request.getParameter("page");

/*=====- Tabs Handle -======*/
byte tab_index = 1;

if(tab_active!=null){
 tab_index = Byte.valueOf(tab_active);
}

/*=====- Sort Handle -======*/
String []sortObj = UtilitiesExtends.decodeSortValue(request);
String sort_fieldName = "";
boolean sort_type = true;
if(sortObj != null){
  sort_fieldName = PhotosPage.SORT_FIELDS[Byte.parseByte(sortObj[0])];
  sort_type = PhotosPage.SORT_TYPE[Byte.parseByte(sortObj[1])];
}

/*=====- Paging Handle -======*/
byte page_index = 0;

if(page_active!=null){
 page_index = Byte.valueOf(page_active);
}

session.setAttribute("tab_index",tab_index);
session.setAttribute("sort_fieldName",sort_fieldName);
session.setAttribute("sort_type",sort_type);
session.setAttribute("page_index",page_index);

/*=====- return PhotosPage -=====*/
response.sendRedirect("../");

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
