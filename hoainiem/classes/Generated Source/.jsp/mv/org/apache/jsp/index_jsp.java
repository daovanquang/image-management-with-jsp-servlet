package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n<html>\r\n  ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/master/head.jsp", out, true);
      out.write("\r\n  <body>\r\n    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/master/header.jsp", out, true);
      out.write("\r\n    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/main/slider.jsp", out, true);
      out.write("\r\n    <!--======================= CONTENT ===========================-->\r\n    <div class=\"main-1200 mb-20\">\r\n      <!--newest-photo-shortcuts-->\r\n      ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/main/newestphotos.jsp", out, true);
      out.write("\r\n      <!--end newest-photo-shortcuts-->\r\n\r\n      <!--impressive-photos-->\r\n      ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/main/impressivephotos.jsp", out, true);
      out.write("\r\n      <!--end impressive-photos-->\r\n\r\n      <!--all-photo-->\r\n      ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/main/allphoto.jsp", out, true);
      out.write("\r\n      <!--end all photo-->\r\n\r\n      <!--services & video-->\r\n      <div class=\"multi-purpose\">\r\n        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/main/services.jsp", out, true);
      out.write("\r\n        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/main/newestvideos.jsp", out, true);
      out.write("\r\n      </div>\r\n      <!--end services & video-->\r\n     </div>\r\n     <!--======================= END CONTENT ===========================-->\r\n  ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/master/footer.jsp", out, true);
      out.write("\r\n  ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/master/scripts.jsp", out, true);
      out.write("\r\n  <script src=\"js/homepage.js\" language=\"javascript\"></script>\r\n</body>\r\n</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
