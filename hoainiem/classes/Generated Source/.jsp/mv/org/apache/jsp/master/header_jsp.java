package org.apache.jsp.master;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.home.topbar.*;
import hoainiem.home.menu.*;
import hoainiem.library.*;
import hoainiem.lang.gui.*;
import hoainiem.values.gui.*;

public final class header_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n");

String keyword = Utilities.getValue((String)session.getAttribute("keyword"));
session.removeAttribute("keyword");

      out.write("\r\n\r\n");

String webModule = AllGUI.WebModule;

byte lang_value = Utilities.getLangValue(session);
Integer totalphoto = Utilities.getIntValue((Integer)session.getAttribute("totalphoto"));

HomeTopbarObject[] items = HomeTopbar.createTopbar();
String path = "";
if(lang_value == 0){
  path = items[5].getTopbar_path();
}else{
  path = items[4].getTopbar_path();
}


      out.write("\r\n<div class=\"header\">\r\n  <!-- ============ TOPBAR ============ -->\r\n  <nav class=\"topbar row relative\">\r\n    <div class=\"main-1200\">\r\n      <div class=\"topbar--left pull-left\">\r\n        <ul class=\"horizontal-nav navbar__link list-unstyle\">\r\n          <li class=\"navbar__link-text\">\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/faqs/\">\r\n              ");
      out.print(HomeLang.FAQS[lang_value]);
      out.write("\r\n            </a>\r\n          </li>\r\n          <li class=\"navbar__link--separator\">&nbsp;</li>\r\n          <li class=\"navbar__link-text\">\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/contact-us/\">\r\n              ");
      out.print(HomeLang.CONTACT_US[lang_value]);
      out.write("\r\n            </a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"topbar--right pull-right\">\r\n        <ul class=\"horizontal-nav list-unstyle pull-right\">\r\n          <li class=\"navbar__link-text\">\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/user/signup/\">\r\n              <i class=\"ic ic-sm ic_utl-sm ic_user-sm pull-left mr-5\"></i>\r\n              <span>");
      out.print(HomeLang.SIGNUP[lang_value]);
      out.write("</span>\r\n            </a>\r\n          </li>\r\n          <li class=\"navbar__link--separator\">&nbsp;</li>\r\n          <li class=\"navbar__link-text\">\r\n            <a href=\"/");
      out.print(webModule);
      out.write("/user/signin/\">\r\n              <i class=\"ic ic-sm ic_utl-sm ic_lock-sm pull-left mr-5\"></i>\r\n              <span>");
      out.print(HomeLang.LOGIN[lang_value]);
      out.write("</span>\r\n            </a>\r\n          </li>\r\n          <li class=\"navbar__link--separator\">&nbsp;</li>\r\n          <li class=\"topbar__link-text\">\r\n            <a href=\"/");
      out.print(webModule);
      out.write('/');
      out.print(path);
      out.write("/\">\r\n              <i class=\"ic ic_utl-sm ic-sm ic_");
      out.print(HomeLang.FLAG[lang_value]);
      out.write("flag-sm\"></i>\r\n            </a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n\r\n      <div class=\"topbar--center absolute\">\r\n        <p class=\"text-center topbar__img-saved\">\r\n          ");
      out.print(HomeLang.MORE_THAN[lang_value] );
      out.write("&nbsp;<strong id=\"img-saved\">");
      out.print(totalphoto);
      out.write("</strong>&nbsp;");
      out.print(HomeLang.PHOTOS[lang_value] );
      out.write("\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </nav>\r\n  <!-- ============ HEAD ============ -->\r\n  <section class=\"head row\">\r\n    <div class=\"main-1200\">\r\n      <div class=\"logo col-2\">\r\n        <a href=\"\">\r\n          <div class=\"logo__image pull-left\">\r\n            <img src=\"/");
      out.print(webModule);
      out.write("/imgs/favicon.png\" alt=\"logo\">\r\n          </div>\r\n          <h1 hidden=\"true\">");
      out.print(HomeLang.COMPANY_NAME[lang_value] );
      out.write("</h1>\r\n        </a>\r\n      </div>\r\n      <div class=\"search-box col-6\">\r\n        <form action=\"\" method=\"\">\r\n        <div class=\"search-bar\">\r\n          <input type=\"text\" class=\"search-bar__text pull-left\" name=\"txtKeywords\" value=\"");
      out.print(keyword);
      out.write("\" autocomplete=\"off\" placeholder=\"");
      out.print(HomeLang.SEARCH_INPUT[lang_value]);
      out.write("\" onkeypress=\"searchPressEnter(event,this.form);\">\r\n          <button type=\"button\" class=\"search-bar__btn pull-right\" onclick=\"search(this.form);\">\r\n            <i class=\"ic ic-sm ic_utl-sm ic_search-sm\"></i>\r\n          </button>\r\n        </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"toggle pull-right\">\r\n        <i class=\"ic ic_menu-md ic_utl-md ic-md\" onclick=\"swapMenu('MainMenu','menu--hide',this);\"></i>\r\n      </div>\r\n    </div>\r\n  </section>\r\n  ");

  lang_value = hoainiem.library.Utilities.getLangValue(session);
  String pos = Utilities.getURI(request,(byte)2);

  HomeMenuObject[] menu = HomeMenu.createHomeMenu();
  path = "";

  for(int i=0; i<items.length; i++){
    if(menu[i].getHomemenu_path().equalsIgnoreCase(pos)){
      menu[i].setHomemenu_active("class=\"active\"");
    }
  }

  String home_active = "class=\"active\"";
  String about_us_active = menu[1].getHomemenu_active();
  String services_active = menu[2].getHomemenu_active();
  String photos_active = menu[3].getHomemenu_active();
  String community_active = menu[4].getHomemenu_active();
  String contact_us_active = menu[5].getHomemenu_active();
  if(!about_us_active.equalsIgnoreCase("") || !services_active.equalsIgnoreCase("") ||
  !services_active.equalsIgnoreCase("") || !photos_active.equalsIgnoreCase("") ||
  !community_active.equalsIgnoreCase("") || !contact_us_active.equalsIgnoreCase("")){
    home_active = "";
  }
  
      out.write("\r\n  <!-- ============ MAINMENU ============ -->\r\n  <section id=\"MainMenu\" class=\"menu menu--hide\">\r\n    <div  class=\"main-1200\">\r\n      <ul class=\"horizontal-nav list-unstyle pull-left\">\r\n        <li>\r\n          <a href=\"/");
      out.print(webModule);
      out.write("/\">\r\n           <i class=\"ic ic-sm ic_utl-sm ic_home-sm pull-left mr-5\"></i>\r\n           <span>");
      out.print(HomeLang.HOME_MENU[lang_value]);
      out.write("</span>\r\n          </a>\r\n        </li>\r\n        <li ");
      out.print(about_us_active);
      out.write(" >\r\n          <a class=\"\" href=\"/");
      out.print(webModule);
      out.write("/about-us/\">\r\n           ");
      out.print(HomeLang.ABOUTUS_MENU[lang_value]);
      out.write("\r\n          </a>\r\n        </li>\r\n        <li ");
      out.print(services_active);
      out.write(" >\r\n          <a class=\"\" href=\"/");
      out.print(webModule);
      out.write("/services/\">\r\n            ");
      out.print(HomeLang.SERVICES_MENU[lang_value]);
      out.write("\r\n          </a>\r\n        </li>\r\n        <li ");
      out.print(photos_active);
      out.write(" >\r\n          <a class=\"\" href=\"/");
      out.print(webModule);
      out.write("/photos/control/\">\r\n            ");
      out.print(HomeLang.PHOTOS_MENU[lang_value]);
      out.write("\r\n          </a>\r\n        </li>\r\n        <li ");
      out.print(community_active);
      out.write(" >\r\n          <a class=\"\" href=\"/");
      out.print(webModule);
      out.write("/community/control/\">\r\n            ");
      out.print(HomeLang.COMMUNITY_MENU[lang_value]);
      out.write("\r\n          </a>\r\n        </li>\r\n        <li ");
      out.print(contact_us_active);
      out.write(" >\r\n          <a class=\"\" href=\"/");
      out.print(webModule);
      out.write("/contact-us/\">\r\n            ");
      out.print(HomeLang.CONTACTUS_MENU[lang_value]);
      out.write("\r\n          </a>\r\n        </li>\r\n        <li>\r\n          <a class=\"\" href=\"#\">\r\n             ");
      out.print(HomeLang.DOTS[lang_value]);
      out.write("\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n</section>\r\n</div>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
