package org.apache.jsp.community;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.lang.gui.*;
import hoainiem.library.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.gui.user.*;

public final class navleft_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

//get User Signed
UserObject user = (UserObject) session.getAttribute("userSigned");

byte lang_value = Utilities.getLangValue(session);
//Tim bo quan ly ket noi
ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
String title = HomeLang.LOGIN[lang_value];
String path = "../user/signin";
String icon = "black-lock";

if(user!=null){
   title = HomeLang.LOGOUT[lang_value];
   path = "../user/signout";
   icon = "logout";
}

      out.write("\r\n");
      out.write("\r\n");
      out.write("<section class=\"section-navleft\">\r\n");
      out.write("<ul class=\"section-navleft__info list-unstyle\">\r\n");
      out.write("  <li class=\"sub-account-icon\">\r\n");
      out.write("  <a href=\"");
      out.print(path);
      out.write("\">\r\n");
      out.write("    <i class=\"ic ic-md ic_utl-md ic_");
      out.print(icon);
      out.write("-md pull-left mr-5\"></i>\r\n");
      out.write("    <span>");
      out.print(title);
      out.write("</span>\r\n");
      out.write("  </a>\r\n");
      out.write("  </li>\r\n");
      out.write("  <li class=\"active\">\r\n");
      out.write("    <a href=\"\">\r\n");
      out.write("      <i class=\"ic ic-md ic_gui-md ic_news-md pull-left mr-5\"></i>\r\n");
      out.write("      <span>");
      out.print(HomeLang.NEWS[lang_value] );
      out.write("</span>\r\n");
      out.write("    </a>\r\n");
      out.write("  </li>\r\n");
      out.write("</ul>\r\n");
 if(user!=null){
      out.write("\r\n");
      out.write("<div  class=\"toggle btn btn-default btn-sm\" onclick=\"swapHide('section-navleft__community','section-navleft__management--hide')\">\r\n");
      out.write("  <span class=\"text-uppercase\">");
      out.print(HomeLang.MANAGEMENT[lang_value]);
      out.write("</span>\r\n");
      out.write("  <i class=\"pull-right ic ic-sm ic_utl-sm ic_arwdown-sm\"></i>\r\n");
      out.write("</div>\r\n");
      out.write("<ul id=\"section-navleft__community\" class=\"section-navleft__management section-navleft__management--hide list-unstyle\">\r\n");
      out.write("  <li  class=\"heading\">\r\n");
      out.write("    ");
      out.print(HomeLang.GROUP[lang_value]);
      out.write("\r\n");
      out.write("  </li>\r\n");
      out.write("\r\n");
      out.write("  <li class=\"\">\r\n");
      out.write("    <a href=\"\">\r\n");
      out.write("      <i class=\"ic ic-md ic_gui-md ic_members-md pull-left mr-5\"></i>\r\n");
      out.write("      <span>");
      out.print(HomeLang.MEMBERS[lang_value] );
      out.write("</span>\r\n");
      out.write("    </a>\r\n");
      out.write("  </li>\r\n");
      out.write("  <li class=\"\">\r\n");
      out.write("    <a href=\"\">\r\n");
      out.write("      <i class=\"ic ic-md ic_gui-md ic_photos-md pull-left mr-5\"></i>\r\n");
      out.write("      <span>");
      out.print(HomeLang.PHOTOS[lang_value] );
      out.write("</span>\r\n");
      out.write("    </a>\r\n");
      out.write("  </li>\r\n");
      out.write("  <li>\r\n");
      out.write("    <a href=\"\">\r\n");
      out.write("      <i class=\"ic ic-md ic_gui-md ic_comments-md pull-left mr-5\"></i>\r\n");
      out.write("      <span>");
      out.print(HomeLang.COMMENTS[lang_value] );
      out.write("</span>\r\n");
      out.write("    </a>\r\n");
      out.write("  </li>\r\n");
      out.write("</ul>\r\n");
}
      out.write("\r\n");
      out.write("<ul class=\"section-navleft__support\">\r\n");
      out.write("  <li  class=\"heading\">\r\n");
      out.write("  <span>");
      out.print(HomeLang.HELP_CENTER[lang_value] );
      out.write("</span>\r\n");
      out.write("  </li>\r\n");
      out.write("  <li class=\"\">\r\n");
      out.write("    <a href=\"../faqs/\">\r\n");
      out.write("      <i class=\"ic ic-md ic_gui-md ic_faqs-md pull-left mr-5\"></i>\r\n");
      out.write("      <span>");
      out.print(HomeLang.FAQS[lang_value] );
      out.write("</span>\r\n");
      out.write("    </a>\r\n");
      out.write("  </li>\r\n");
      out.write("  <li class=\"\">\r\n");
      out.write("    <a href=\"../contact-us/\">\r\n");
      out.write("      <i class=\"ic ic-md ic_gui-md ic_contactus-md pull-left mr-5\"></i>\r\n");
      out.write("      <span>");
      out.print(HomeLang.CONTACT_US[lang_value] );
      out.write("</span>\r\n");
      out.write("    </a>\r\n");
      out.write("  </li>\r\n");
      out.write("</ul>\r\n");
      out.write("</section>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
