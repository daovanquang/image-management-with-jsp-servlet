package org.apache.jsp.article.del;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.gui.article.*;
import hoainiem.gui.image.*;
import hoainiem.gui.comment.*;
import hoainiem.gui.video.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");

int aid = Utilities.getIntParam(request,"aid");
String date = UtilitiesCalendar.getCurrentTime("yyyy-MM-dd HH:mm");
if(aid>0){
  ArticleObject article = new ArticleObject();
  article.setArticle_deleted_date(date);
  article.setArticle_id(aid);
  ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CPool");
  ArticleControl ac = new ArticleControl(cp);
  ac.delArticle(article);

  ImageObject image = new ImageObject();
  image.setImage_deleted_date(date);
  image.setImage_article_id(aid);
  ImageControl ic = new ImageControl(cp);
  //del in dir

  //del in db
  ic.delImage(image);

  CommentObject comment = new CommentObject();
  comment.setComment_deleted_date(date);
  comment.setComment_article_id(aid);
  CommentControl cc = new CommentControl(cp);
  cc.delComment(comment);

  VideoObject video = new VideoObject();
  video.setVideo_article_id(aid);
  video.setVideo_deleted_date(date);
  VideoControl vc = new VideoControl(cp);
  vc.delVideo(video);

  vc.releaseConnection();

  String referer = request.getHeader("Referer");
  response.sendRedirect(referer);
}

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
