package org.apache.jsp.user.verify;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.util.*;
import java.text.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.*;
import hoainiem.gui.user.*;
import hoainiem.values.gui.*;
import org.apache.commons.io.*;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;

public final class control_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");


String webModule = AllGUI.WebModule;

UserObject userCreatedPass = (UserObject) session.getAttribute("userCreatedPass");
session.removeAttribute("userConfirmed");

if (userCreatedPass!=null) {
    session.removeAttribute("userCreatedPass");

    String name = (String) userCreatedPass.getUser_name();
    String pass = (String) userCreatedPass.getUser_pass();

    if(name!=null&&pass!=null){
      //get image
      /*=======- declare for store -=======*/
      // location to store file uploaded
      String UPLOAD_DIRECTORY = "uploads";
      String USERS_DIRECTORY = "users";
      String AVATAR_DIRECTORY = "avatar";


      // upload settings
      int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
      int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
      int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
      try {

        /*============- CREATE STORE CONFIG UPLOAD -============*/
        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setSizeThreshold(MEMORY_THRESHOLD);
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);

        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);


        String uploadPath = getServletContext().getRealPath("")+ File.separator + UPLOAD_DIRECTORY;
        String usersPath = uploadPath + File.separator + USERS_DIRECTORY;
        String avatarPath = usersPath + File.separator +(String) AVATAR_DIRECTORY;

        String webmodulePath = "/"+AllGUI.WebModule+"/";
        String realPath = getServletContext().getRealPath("");
        byte pos =  (byte) realPath.lastIndexOf('\\');

        //uploads directory
        File uploadDir = UtilitiesMedia.getFile(uploadPath);

        //users directory
        File uploadUsersDir = UtilitiesMedia.getFile(usersPath);
        //avatar directory
        File uploadAvatarDir = UtilitiesMedia.getFile(avatarPath);

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if(isMultipart) {
          List items = upload.parseRequest(request);

          if(items != null){

            if( items.size()>0 ){
              //Lay files upload
              Iterator iterator = items.iterator();

              while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();

                //Neu la file
                if (!item.isFormField()) {
                  //Them tep &  video

                  //thong tin file
                  String fileName = item.getName();
                  String fileType = UtilitiesMedia.classifyFile(fileName);

                  if(fileType == "img"){
                    //upload videos
                    File uploadedFile = new File(avatarPath + File.separator + fileName);
                    item.write(uploadedFile);

                    webModule = (pos>0)? webmodulePath: "/";
                    String filePath = webModule+ UPLOAD_DIRECTORY + "/" + AVATAR_DIRECTORY +"/"+ fileName;

                    userCreatedPass.setUser_image(filePath);
                  }
                }
              }
            }
          }
        }

      }catch(Exception ex){
        ex.printStackTrace();
      }

      //get text
      String fullname = request.getParameter("txtUserFullName");
      String phone = request.getParameter("txtUserPhone");
      String birthday = request.getParameter("txtUserBirthDay");
      String address = request.getParameter("txtUserAddress");
      byte gender = Utilities.getByteParam(request, "slcUserGender");

      //ma hoa
      fullname = UtilitiesHelper.toENTITIES(fullname);
      address = UtilitiesHelper.toENTITIES(address);

      userCreatedPass.setUser_fullname(fullname);
      userCreatedPass.setUser_phone(phone);
      userCreatedPass.setUser_birthday(birthday);
      userCreatedPass.setUser_address(address);
      userCreatedPass.setUser_gender(gender);

      //Tim bo quan ly ket noi
      ConnectionPool cp = (ConnectionPool) request.getSession().getAttribute("CPool"); ;

      //Tao doi tuong thuc thi chuc nang
      UserControl uc = new UserControl(cp);
      //set ConnectionPool if it is null
      if (cp == null) {
        getServletContext().setAttribute("CPool", uc.getCP());
      }

      //set date created
      Date date = new Date();
      String createDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
      userCreatedPass.setUser_created_date(createDate);

      boolean result = uc.addUser(userCreatedPass);
      uc.releaseConnection();

      if (result) {
        response.sendRedirect("/"+webModule+"/user/signin/");
      }else {
        response.sendRedirect("/"+webModule+"/user/verify/?err=notok");
      }
  }else{
    response.sendRedirect("/"+webModule+"/user/verify/?err=value");
  }
}else {
  response.sendRedirect("/"+webModule+"/user/verfiy/?err=param");
}

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
