package org.apache.jsp.user.signup;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.gui.user.*;
import hoainiem.values.gui.*;

public final class control_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

String webModule = AllGUI.WebModule;

//check user signined will return
UserObject user = (UserObject) session.getAttribute("userSigned");
if(user==null){
  String username = request.getParameter("txtUserName");
  String usermail = request.getParameter("txtUserMail");

  if (username != null && usermail != null) {

    username = username.trim();
    usermail = usermail.trim();
    if (!username.equalsIgnoreCase("") && !usermail.equalsIgnoreCase("")) {
      //Tham chieu ngu canh ung dung
      ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
      //Tao doi tuong thuc thi chuc nang
      UserControl uc = new UserControl(cp);

      //set ConnectionPool if it is null
      if (cp == null) {
        getServletContext().setAttribute("CPool", uc.getCP());
      }

      //kiem tra da ton tai user muon them da ton tai chua
      UserObject validUser = uc.getUserObject(username);

      if(validUser==null){
        UserObject nUser = new UserObject();
        nUser.setUser_name(username);
        nUser.setUser_mail(usermail);
        request.getSession(true).setAttribute("userSignup", nUser);
        response.sendRedirect("/"+webModule+"/user/confirm/activecode.jsp");
      }else {
        response.sendRedirect("/"+webModule+"/user/signup/?err=notok");
      }
    }
    else {
      response.sendRedirect("/"+webModule+"/user/signup/?err=value");
    }

  }
  else {
    response.sendRedirect("/"+webModule+"/user/signup/?err=param");
  }
}else {
  response.sendRedirect("/"+webModule+"/user/signin/?err=haveanaccount");
}

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
