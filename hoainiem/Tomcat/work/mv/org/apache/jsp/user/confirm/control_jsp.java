package org.apache.jsp.user.confirm;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.gui.user.*;
import hoainiem.values.gui.*;

public final class control_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

  String webModule = AllGUI.WebModule;

  String confirmCode = request.getParameter("txtConfirmCode");
  String sendCode = (String) session.getAttribute("sendCode");
  UserObject userSignup = (UserObject) session.getAttribute("userSignup");

  if(userSignup!=null && sendCode!=null  && confirmCode!=null){
    if(!confirmCode.equalsIgnoreCase("") && !sendCode.equalsIgnoreCase("")){
      confirmCode = Utilities.md5(confirmCode);
      boolean result = confirmCode.equalsIgnoreCase(sendCode);
      if(result){
        session.removeAttribute("userSignup");
        request.getSession().setAttribute("userConfirmed",userSignup);
        response.sendRedirect("/"+webModule+"/user/createpass/");
      }else{
        response.sendRedirect("/"+webModule+"/user/confirm/?err=notok");
      }
    }else{
      response.sendRedirect("/"+webModule+"/user/confirm/?err=value");
    }
  }else{
      response.sendRedirect("/"+webModule+"/user/confirm/?err=param");
  }

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
