package org.apache.jsp.user.confirm;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.gui.user.*;
import hoainiem.mail.*;
import hoainiem.mail.*;
import hoainiem.values.gui.*;

public final class activecode_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

  String webModule = AllGUI.WebModule;
  session.removeAttribute("sendCode");
  byte lang_value = Utilities.getLangValue(session);

  UserObject userSignup = (UserObject) session.getAttribute("userSignup");
  if(userSignup!=null){
    String to = userSignup.getUser_mail();
    if(to!=null && !to.equalsIgnoreCase("")){
      //L\u1EA5y th\uFFFDng tin \u0111\u1ED1i t\u01B0\u1EE3ng ch\u1EE9c n\u0103ng g\u1EEDi tin m\u1EB7c \u0111\u1ECBnh l\uFFFD index 0
      ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
      UserControl uc = new UserControl(cp);

      String sendCode  = EmailSender.randomCode((byte)4);

      String subject = EmailStruct.create_Mail_Confirm_Code_Title(lang_value);
      String message = EmailStruct.create_Mail_Confirm_Code_Content(sendCode, lang_value);
      boolean result = EmailSender.sendMail(to, subject, message);
      if(result) {
        sendCode = Utilities.md5(sendCode);
        request.getSession().setAttribute("sendCode",sendCode);
        response.sendRedirect("/"+webModule+"/user/confirm/");
      }else{
        response.sendRedirect("/"+webModule+"/user/signup/?err=notok");
      }
    }else{
      response.sendRedirect("/"+webModule+"/user/signup/?err=value");
    }
  }else{
      response.sendRedirect("/"+webModule+"/user/signup/?err=param");
  }

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
