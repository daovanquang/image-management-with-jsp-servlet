package org.apache.jsp.master;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.lang.gui.*;
import hoainiem.library.*;

public final class messagebox_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");

byte lang_value = Utilities.getLangValue(session);

      out.write("\r\n");
      out.write("\r\n");
      out.write("<div id = \"messagebox\" class=\"screen-fade absolute absolute-top hide\">\r\n");
      out.write("<div class=\"message-box main-250 mt-30\">\r\n");
      out.write("<div class=\"message-box__header\">\r\n");
      out.write("<h3 class=\"title\">");
      out.print(HomeLang.MESSAGE[lang_value]);
      out.write("</h3>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"message-box--bg-sky row\">\r\n");
      out.write("<div class=\"message-box__body mt-10\">\r\n");
      out.write("<h4 class=\"content text-center\"></h4>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"message-box__footer row mt-15\">\r\n");
      out.write("<button class=\"yes-btn btn btn-default\" onclick=\"swapHide('messagebox','hide');\">");
      out.print(HomeLang.MESSAGE_YES[lang_value]);
      out.write("</button>\r\n");
      out.write("<button class=\"cancel-btn btn btn-default\" onclick=\"swapHide('messagebox','hide');\">");
      out.print(HomeLang.MESSAGE_CANCEL[lang_value]);
      out.write("</button>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
