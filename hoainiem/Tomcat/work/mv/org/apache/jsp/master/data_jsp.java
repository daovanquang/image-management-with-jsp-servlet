package org.apache.jsp.master;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.gui.info.*;
import hoainiem.gui.video.*;
import hoainiem.gui.article.*;
import hoainiem.gui.image.*;
import hoainiem.gui.comment.*;
import hoainiem.gui.user.*;
import hoainiem.gui.group.*;
import hoainiem.values.gui.*;

public final class data_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

  byte lang_value = Utilities.getLangValue(session);

  String pos = Utilities.getURI(request,(byte)2);

  /*=====-Tim bo quan ly ket noi -======*/
  ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");

  /*=====- User Signed  -======*/
  UserObject user = (UserObject) session.getAttribute("userSigned");

  /*=======================- FOR ALL  -=======================*/
  ImageControl ic = new ImageControl(cp);
  int totalphoto = ic.getTotal(null);
  session.setAttribute("totalphoto",totalphoto);

  /*===================- COMMUNITY PAGE -=====================*/
  if (pos.equalsIgnoreCase("community")) {

    ArticleControl ac = new ArticleControl(cp);
    ArticleObject similar = new ArticleObject();

    /*=====- post -======*/
    String post = ac.viewPost(user,lang_value);
    session.setAttribute("post", post);

    /*=====- article -======*/
    String strArticles = "";
    ArrayList<ArticleObject> articles = new ArrayList<ArticleObject>();
    if (user != null) {
      similar.setArticle_group_id(user.getUser_group_id());
    }
    else {
      similar.setArticle_group_id(0);
    }

    articles = ac.getArticleObjects(similar, 0, (byte) 15);

    /*=====- article comment & image-video-file attach -======*/
    if(articles.size()>0){
      ic = new ImageControl(cp);
      CommentControl cc = new CommentControl(cp);
      for (ArticleObject article : articles) {
        /*=====- image attach -======*/
        ImageObject imgSimilar = new ImageObject();
        imgSimilar.setImage_article_id(article.getArticle_id());
        int groupid = (user!=null) ? user.getUser_group_id() : 0;
        imgSimilar.setImage_group_id(groupid);
        ArrayList<ImageObject> imgs = ic.getImageOjects(imgSimilar, 0, (byte) 15,null,true);

        /*=====- comment -======*/
        CommentObject cmtSimilar = new CommentObject();
        cmtSimilar.setComment_article_id(article.getArticle_id());
        ArrayList<CommentObject> cmts = cc.getComments(cmtSimilar, 0, (byte) 15);
        strArticles += ac.viewArticle(article, imgs, cmts, user, lang_value);
      }
      session.setAttribute("article", strArticles);
    }

    /*=====- recent photos -======*/
    ImageObject imgSimilar = new ImageObject();
    int groupid = (user!=null) ? user.getUser_group_id() : 0;
    imgSimilar.setImage_group_id(groupid);
    ic = new ImageControl(cp);
    String strRecentphotos = ic.viewRecentPhotos(imgSimilar, lang_value);
    session.setAttribute("recentphotos", strRecentphotos);

    GroupControl gc = new GroupControl(cp);
    String strProposedgroups = gc.viewProposedGroups(lang_value);
    session.setAttribute("proposedgroups", strProposedgroups);

  } else if(pos.equalsIgnoreCase("photos")){

    /*========================- PHOTO PAGE -=======================*/
    int at = 0;
    byte total = 14;
    ImageObject similar = null;


    String sort_fieldName = (String)session.getAttribute("sort_fieldName");
    boolean sort_type = Utilities.getBoolSS(session, "sort_type");
    byte tab_index = Utilities.getByteSS(session, "tab_index");
    if(tab_index==1){
      similar = new ImageObject();
      similar.setImage_forimpressive(true);
    }
    byte page_index = Utilities.getByteSS(session, "page_index");

    String strAlbum = ic.viewAlbum(similar, at, total, sort_fieldName, sort_type, tab_index, page_index, lang_value);
    session.setAttribute("photos", strAlbum);
  } else{

    /*====================- HOMEPAGE -=======================*/
    ImageObject imgSimilar = new ImageObject();

    /*=====- infos -======*/
    String strServices = "";
    InfoControl infoControl = new InfoControl(cp);
    strServices = infoControl.viewServices(lang_value);
    session.setAttribute("services", strServices);

    /*=====- slider -======*/
    ic = new ImageControl(cp);
    String strSlider = "";
    strSlider = ic.viewSlider(0, (byte) 5);
    session.setAttribute("slider", strSlider);

    /*=====- newestvideos -======*/
    String newestvideos = "";
    VideoControl vc = new VideoControl(cp);
    newestvideos = vc.viewNewestVideos(null, 0, (byte) 6);
    session.setAttribute("newestvideos", newestvideos);

    /*=====- newsest photo -======*/
    String strNewestPhotos = "";
    ic = new ImageControl(cp);
    strNewestPhotos = ic.viewNewestPhotos(lang_value);
    session.setAttribute("newestphotos", strNewestPhotos);
    /*=====- impressive photos -======*/
    String strImpressivePhotos = "";
    strImpressivePhotos = ic.viewImpressivePhotos(0, (byte) 10,lang_value);
    session.setAttribute("impressivephotos", strImpressivePhotos);

    /*=====- all photo -======*/

    /*===-paging -===*/
    int hp_at  = 0;
    byte hp_total =  21;
    /*===- end paging -===*/

    String strAllPhoto = "";
    ic = new ImageControl(cp);
    strAllPhoto = ic.viewAllPhoto(null, hp_at, hp_total, lang_value);
    session.setAttribute("allphoto", strAllPhoto);
  }


  /*======================- USER ACTION -======================*/
  UserControl uc = new UserControl(cp);
  String sectionSignin = uc.viewSectionSignin(lang_value);
  session.setAttribute("sectionSignin",sectionSignin);

  String sectionSignup = uc.viewSectionSignup(lang_value);
  session.setAttribute("sectionSignup",sectionSignup);

  UserObject userSignup = (UserObject) session.getAttribute("userSignup");
  String sectionConfirm = uc.viewSectionConfirm(userSignup, lang_value);
  session.setAttribute("sectionConfirm",sectionConfirm);

  String sectionCreatePass = uc.viewSectionCreatePass(lang_value);
  session.setAttribute("sectionCreatePass",sectionCreatePass);

  UserObject userCreatedPass = (UserObject) session.getAttribute("userCreatedPass");
  String sectionVerify = uc.viewSectionVerify(userCreatedPass, lang_value);
  session.setAttribute("sectionVerify",sectionVerify);

  String sectionChangePass = uc.viewSectionChangePass(user, lang_value);
  session.setAttribute("sectionChangePass",sectionChangePass);

  uc.releaseConnection();

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
