package org.apache.jsp.search;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import hoainiem.*;
import hoainiem.objects.*;
import hoainiem.library.*;
import hoainiem.gui.keyword.*;
import hoainiem.sql.*;
import hoainiem.machinelearning.objects.*;
import hoainiem.machinelearning.decisiontree.*;
import hoainiem.machinelearning.trainingdata.*;
import hoainiem.lang.gui.*;
import java.util.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

String keyword = request.getParameter("q");
session.setAttribute("keyword",keyword);

if(keyword!=null){
  keyword = Utilities.getValue(keyword).trim();

  byte lang_value = Utilities.getLangValue(session);

  /*====- Find the connection manager -===*/
  ConnectionPool cp = (ConnectionPool) application.getAttribute("CPool");
  KeywordControl kc = new KeywordControl(cp);

  KeywordObject similar = new KeywordObject();
  similar.setKeyword_value(keyword);
  String result = kc.getKeywords(similar, 0, (byte)10, false, lang_value);

  boolean isExistsKey = Utilities.containsString(result, HomeLang.NOT_FOUND_RESULTS[lang_value]);
  if(!isExistsKey){
    /*====- update count -====*/
    kc.editKeyword(similar,KeywordSQL.COMMAND_UPDATE_SEARCH_COUNT);
  } else {
    /*====- Analysis of Data -====*/
    TrainingDataControl tdc = new TrainingDataControl(cp);
    ArrayList<TrainingDataObject> items = tdc.getTrainingDataObjects(null,0,(byte)10);

    if(items!=null){

      int rows = items.size();

      if(rows>0){
        /*declare input data*/
        ArrayList<String> dependent_titles = new ArrayList<String>();
        dependent_titles.add("avg_search_month");
        dependent_titles.add("word_num");
        dependent_titles.add("compete_level");

        /*3 is col number of data to training*/
        String [][]dependent_values = new  String[rows][3];

        String independent_title = "INSERT";
        ArrayList<Boolean>  independent_values = new ArrayList<Boolean>();
        int i = 0;
        for(TrainingDataObject item:items){
          dependent_values[i][0] = item.getTrainingdata_avg_search_month();
          dependent_values[i][1] = item.getTrainingdata_word_num();
          dependent_values[i][2] = item.getTrainingdata_compete_level();

          independent_values.add(item.isTrainingdata_insert());

          i++;
        }

        DecisionTreeObject nDecisionTreeObject = new DecisionTreeObject(dependent_titles, dependent_values, independent_title, independent_values);
        DecisionTreeControl dtc = new DecisionTreeControl();

        /*over bround*/
        Tree tree = null;
        if(nDecisionTreeObject!=null){
          tree = dtc.generateTree(nDecisionTreeObject);
        }

        if(tree!=null){
          /*decision insert key*/

          /*Test case 1*/
          ArrayList<String> names = new ArrayList<String>();
          names.add("avg");
          names.add("word_num");
          names.add("compete_level");

          ArrayList<String> values = new ArrayList<String>();
          values.add("big");
          values.add("extra-large");
          values.add("hight");

          String isInsert = tree.getPropertyByValues(names, values, null);
          /*End test case 1*/

          boolean valid = isInsert.equalsIgnoreCase("true") ? true : false;

          if(valid){
            KeywordObject nKeyword = new KeywordObject();
            nKeyword.setKeyword_title("");
            nKeyword.setKeyword_summary("");
            nKeyword.setKeyword_created_date(UtilitiesCalendar.getCurrentTime("yyyy/MM/dd HH:mm"));
            nKeyword.setKeyword_value(keyword);

            kc.addKeyword(nKeyword);
          }
        }
      }
    }
  }
  session.setAttribute("result", result);
  response.sendRedirect("../result/");
}

      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
